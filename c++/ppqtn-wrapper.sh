#!/usr/bin/env bash

if [ $# -lt 3 ]; then
	echo "Usage: $0 <min num vertices> <max num vertices> <num vertices step> [<bridge length>] [<random seed>]"
	exit 1
fi

min_nv=$1
max_nv=$2
step=$3

brlen=4
min_nreps=3
out_path=./out
seed=5489
use_process_watcher=0

if [ $# -gt 3 ]; then
	brlen=$4
fi
if [ $# -gt 4 ]; then
	seed=$5
fi

mkdir -p $out_path

for (( nv = $min_nv; nv <= $max_nv;  )); do  
	scenario=exp-nv_$nv-brlen_$brlen-seed_$seed
	outfile_prefix=$out_path/$scenario

	echo "SCENARIO: $scenario"

	./ppqtn.sh	--rand \
				--rand-linear-nverts $nv \
				--rand-linear-brlen $brlen \
				--rand-linear-limit-conslen \
				--rand-seed $seed \
				--bench-min-nreps $min_nreps \
				--outfile "${outfile_prefix}-graph.dot" \
				--verbose \
				> "${outfile_prefix}.log" 2>&1

#	if [ $use_process_watcher -eq 1 ]; then
#		pid=$!
#		host=$(hostname)
#		python3 ./process_watcher.py --quiet --pid ${pid} --tag "scenario: ${scenario}" --tag "host: ${host}" --to "marco.guazzone@uniupo.it"
#	else
#		fg
#	fi

	(( nv += step ))
done

# To use this script on a remote machine and get notified when its execution is
## done, you can use the following commands:
## Alternative #1
##  dtstart=$(date +"%F %T %z")
##  hname=$(hostname)
##  ./ppqtn.sh ... &
##  pid=$!
##  cmdline=$(cat /proc/${pid}/cmdline)
##  fg
##  ret=$?
##  dtend=$(date +"%F %T %z")
#
##  body=<<EOT
##  PID $pid: $cmdline
##   * Started: $dtstart
##   * Ended: $dtend
##   * Status: $ret
##   * Host: $hname
#
##  (email automatically sent)
##  EOT
##  echo "$body" | mail -s "Experiment ${scenario}' ended" "user@email.com"
## Alternative #2 (requires https://github.com/arlowhite/process-watcher):
##  ./ppqtn.sh ... &
##  pid=$!
##  host=$(hostname)
##  python3 $HOME/process-watcher/process_watcher.py --quiet --pid ${pid} --tag "scenario: ${scenario}" --tag "host: ${host}" --to "user@email.com"
