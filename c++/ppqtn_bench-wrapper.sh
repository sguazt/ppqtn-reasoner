#!/usr/bin/env bash

if [ $# -lt 3 ]; then
	echo "Usage: $0 <min num vertices> <max num vertices> <num vertices step> [<random seed>]"
	exit 1
fi

min_nv=$1
max_nv=$2
step=$3

min_nreps=3
out_path=./out
seed=5489

if [ $# -gt 3 ]; then
	seed=$4
fi

mkdir -p $out_path

for (( nv = $min_nv; nv <= $max_nv;  )); do  
	scenario=exp-nv_$nv-seed_$seed
	outfile_prefix=$out_path/$scenario

	echo "SCENARIO: $scenario"

	./ppqtn_bench.sh	--nverts $nv \
						--rand-seed $seed \
						--bench-min-nreps $min_nreps \
						--verbose \
						> "${outfile_prefix}.log" 2>&1

	(( nv += step ))
done
