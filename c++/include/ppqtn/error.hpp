/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file error.hpp
 *
 * \brief Functions related to error handling.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_ERROR_HPP
#define PPQTN_ERROR_HPP

#include <string>


/// Terminate the program with a fatal error
void fatal_error(const std::string& msg);

/// Terminate the program with a fatal error
void fatal_syserror(const std::string& msg);

/// Throw a `std::system_error` exception whose explanatory string is built as follow:
///   \a msg + ": " + <error message according to the current value of `errno`>
void throw_syserror(const std::string& msg);


#endif // PPQTN_ERROR_HPP
