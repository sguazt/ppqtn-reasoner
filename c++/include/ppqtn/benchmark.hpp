/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file benchmark.hpp
 *
 * \brief Benchmark type.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_BENCHMARK_HPP
#define PPQTN_BENCHMARK_HPP

#include <cstddef>
#include <limits>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_generator.hpp>
#include <string>
#include <utility>


class benchmark_t;
class benchmark_result_t;
class statistics_t;
class ci_relative_precision_num_runs_estimator_t;


/// Runs a benchmark and collects statistics.
class benchmark_t
{
public:
    static constexpr double default_ci_level = 0.95;
    static constexpr double default_ci_target_relative_precision = 0.04;
    static const std::size_t default_max_num_repetitions = std::numeric_limits<std::size_t>::max();
    static const std::size_t default_min_num_repetitions = 3;
    static const bool default_verbose = true;


public:
    /// Default constructor
    benchmark_t() = default;

    /// Destructor
    virtual ~benchmark_t() = default;

    /// Copy constructor (disallowed)
    benchmark_t(const benchmark_t& g) = delete;

    /// Copy assignment (disallowed)
    benchmark_t& operator=(const benchmark_t& g) = delete;

    /// Sets the level of the confidence intervals.
    void ci_level(double value);

    /// Gets the level of the confidence intervals.
    double ci_level() const;

    /// Sets the maximum number of repetitions.
    void max_num_repetitions(std::size_t value);

    /// Gets the maximum number of repetitions.
    std::size_t max_num_repetitions() const;

    /// Sets the minimum number of repetitions.
    void min_num_repetitions(std::size_t value);

    /// Gets the minimum number of repetitions.
    std::size_t min_num_repetitions() const;

    /// Runs a benchmark for the given graph \a graph.
    benchmark_result_t run(graph_generator_t& graph_gen);

    /// Enables (`true`) or disable (`false`) verbosity messages. 
    void verbose(bool value);

    /// Tells whether verbosity messages are enabled or not.
    bool verbose() const;

private:
    double ci_level_ = default_ci_level; ///< Level of confidence intervals
    double ci_target_rel_prec_ = default_ci_target_relative_precision; ///< Target relative precision of confidence intervals
    std::size_t max_nreps_; ///< Maximum number of repetitions
    std::size_t min_nreps_; ///< Minimum number of repetitions
    bool verbose_ = default_verbose; ///< Enable verbose messages
}; // benchmark_t


/// Runs a benchmark and collects statistics.
class fixed_graph_benchmark_t
{
public:
    static constexpr double default_ci_level = 0.95;
    static constexpr double default_ci_target_relative_precision = 0.04;
    static const std::size_t default_max_num_repetitions = std::numeric_limits<std::size_t>::max();
    static const std::size_t default_min_num_repetitions = 3;
    static const bool default_verbose = true;


public:
    /// Default constructor
    fixed_graph_benchmark_t() = default;

    /// Destructor
    virtual ~fixed_graph_benchmark_t() = default;

    /// Copy constructor (disallowed)
    fixed_graph_benchmark_t(const fixed_graph_benchmark_t& g) = delete;

    /// Copy assignment (disallowed)
    fixed_graph_benchmark_t& operator=(const fixed_graph_benchmark_t& g) = delete;

    /// Sets the level of the confidence intervals.
    void ci_level(double value);

    /// Gets the level of the confidence intervals.
    double ci_level() const;

    /// Sets the maximum number of repetitions.
    void max_num_repetitions(std::size_t value);

    /// Gets the maximum number of repetitions.
    std::size_t max_num_repetitions() const;

    /// Sets the minimum number of repetitions.
    void min_num_repetitions(std::size_t value);

    /// Gets the minimum number of repetitions.
    std::size_t min_num_repetitions() const;

    /// Runs a benchmark for the given graph \a graph.
    benchmark_result_t run(const graph_t& graph);

    /// Enables (`true`) or disable (`false`) verbosity messages. 
    void verbose(bool value);

    /// Tells whether verbosity messages are enabled or not.
    bool verbose() const;

private:
    double ci_level_ = default_ci_level; ///< Level of confidence intervals
    double ci_target_rel_prec_ = default_ci_target_relative_precision; ///< Target relative precision of confidence intervals
    std::size_t max_nreps_; ///< Maximum number of repetitions
    std::size_t min_nreps_; ///< Minimum number of repetitions
    bool verbose_ = default_verbose; ///< Enable verbose messages
}; // fixed_graph_benchmark_t


/// Estimators of the number of runs of a benchmark based on the
/// relative precision of confidence intervals.
class ci_relative_precision_num_runs_estimator_t
{
private:
    friend benchmark_t;
    friend fixed_graph_benchmark_t;


public:
    static constexpr double default_ci_level = 0.95;
    static constexpr double default_ci_target_relative_precision = 0.04;
    static const std::size_t default_max_num_runs = std::numeric_limits<std::size_t>::max();
    static const std::size_t default_min_num_runs = 3;


public:
    /// Estimates the new number of runs requires to achieve the desired
    /// relative precision of the confidence interval associated with the given
    /// statistics \a stats.
    std::size_t estimate(const statistics_t& stats);


private:
    double ci_level_ = default_ci_level;
    double target_rel_prec_ = default_ci_target_relative_precision;
    std::size_t n_max_ = default_max_num_runs;
    std::size_t n_min_ = default_min_num_runs;
    std::size_t n_target_ = std::numeric_limits< std::size_t >::max();
    bool aborted_ = false;
    bool detected_ = false;
    bool done_ = false;
    bool initial_check_ = true;
    bool unstable_ = false;
}; // ci_relative_precision_num_runs_estimator_t


/// Computes common statistics.
class statistics_t
{
public:
    //statistics_t();

    /// Resets the statistics.
    void clear();

    /// Collects a new value.
    void collect(double val);

    /// Returns the confidence interval of the mean.
    std::tuple<double,double,double,double> ci_mean(double level) const;

    /// Returns the number of collected values.
    std::size_t count() const;

    /// Returns the maximum collected value.
    double max() const;

    /// Returns the mean of collected values.
    double mean() const;

    /// Returns the minimum collected value.
    double min() const;

    /// Sets the mnemonic name to \a s.
    void name(const std::string& s);

    /// Gets the mnemonic name.
    const std::string& name() const;

    /// Returns the sum of collected values.
    double sum() const;

    /// Returns the standard deviation of collected values.
    double standard_deviation() const;

    /// Returns the variance of collected values.
    double variance() const;


private:
    std::string name_ = "Unnamed statistics";
    std::size_t count_ = 0;
    double min_ = std::numeric_limits<double>::infinity();
    double max_ = - std::numeric_limits<double>::infinity();
    double m1_ = 0;
    double m2_ = 0;
}; // statistics_t


/// Stores the results of a benchmark.
class benchmark_result_t
{
    friend benchmark_t;
    friend fixed_graph_benchmark_t;

public:
    /// Returns CPU time statistics.
    const statistics_t& cpu_time_stats() const;

    /// Returns real time statistics.
    const statistics_t& real_time_stats() const;

    /// Returns graph constraint length statistics.
    const statistics_t& graph_constraint_length_stats() const;

    /// Returns graph constraint distance statistics.
    const statistics_t& graph_constraint_distance_stats() const;

    /// Returns the output graph.
    const graph_t& out_graph() const;


private:
    statistics_t cpu_time_stats_; ///< CPU time statistics.
    statistics_t real_time_stats_; ///< Real time statistics.
    statistics_t graph_constraint_length_stats_; ///< Graph constraint length statistics.
    statistics_t graph_constraint_distance_stats_; ///< Graph constraint distance statistics.
    graph_t out_graph_; /// The output graph.
}; // benchmark_result_t


#endif // PPQTN_BENCHMARK_HPP
