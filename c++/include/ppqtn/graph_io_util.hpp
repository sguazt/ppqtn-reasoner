/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph_io_util.hpp
 *
 * \brief Utility functions for reading/writing graphs.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_IO_UTIL_HPP
#define PPQTN_GRAPH_IO_UTIL_HPP

#include <ppqtn/graph.hpp>
#include <string>
#include <vector>


struct parsed_constraint_t
{
    std::string left_time_point;
    std::vector<terne> triples;
    std::string right_time_point;
};

/// Parses a single constraint (i.e., a collection of triples) from the given string \a text.
parsed_constraint_t parse_constraint(const std::string& text);

/// Parses a collection of triples (part of a constraint) from the given string \a text.
std::vector<terne> parse_constraint_triples(const std::string& text);


#endif // PPQTN_GRAPH_IO_UTIL_HPP
