/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph_ops.hpp
 *
 * \brief Graph operatiosn.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_OPS_HPP
#define PPQTN_GRAPH_OPS_HPP

#include <cstddef>
#include <ppqtn/graph.hpp>
#include <tuple>


/**
 * \brief Performs the qualitative point-point query "t1 = t2 ?"
 *  where t1 and t2 are time points.
 *
 * \param graph The input graph.
 * \param left_vertex The first (leftmost) operand.
 * \param right_vertex The second (rightmost) operand.
 * \return A pair where the first element is the probability and the
 *  second one is the preference of the "=" relationship between
 *  the given time points.
 *
 * Given two time points t_1 and t_2, and given the temporal constraint
 * t1 <(d1,p1,P1),...,(dk,pk,Pk)> t2, the result of the "t1 = t2" query is
 * computed as follows:
 * - Prob(t2 = t1) = phi(0), if 0 in {d1,...,dk} and phi(0) != %,
 *                   1/k,    if 0 in {d1,...,dk} and phi(0) = %,
 *                   0,      otherwise.
 * - Pref(t2 = t1) = sigma(0), if 0 in {d1,...,dk} and sigma(0) != #,
 *                   #,        if 0 in {d1,...,dk} and sigma(0) = #,
 *                   0,        otherwise.
 *  where Prob() denotes the probability, Pref() denotes the preference, phi(d),
 *  '%' denotes undefined probability, '#' denotes undefined preference, and
 *  sigma(d) denote the probability and the preference of distance d (i.e.,
 *  phi(d)=pi and sigma(d)=Pi, respectively).
 */
std::tuple<double,double> query_eq(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex);

/**
 * \brief Performs the qualitative point-point query "t1 >= t2 ?"
 *  where t1 and t2 are time points.
 *
 * \param graph The input graph.
 * \param left_vertex The first (leftmost) operand.
 * \param right_vertex The second (rightmost) operand.
 * \return A pair where the first element is the probability and the
 *  second one is the preference of the ">=" relationship between
 *  the given time points.
 *
 * Given two time points t_1 and t_2, and given the temporal constraint
 * t1 <(d1,p1,P1),...,(dk,pk,Pk)> t2, the result of the "t1 >= t2" query is
 * computed as follows:
 * - Prob(t2 >= t1) = Prob(t2 > t1) + Prob(t2 = t1)
 * - Pref(t2 >= t1) = MAX(Pref(t2 > t1), Pref(t2 = t1)), if Pref(t2 > t1) != # and Pref(t2 = t1) != #,
 *                    #,                                 otherwise.
 *  where Prob() denotes the probability, Pref() denotes the preference,
 *  '%' denotes undefined probability, and '#' denotes undefined preference.
 */
std::tuple<double,double> query_ge(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex);

/**
 * \brief Performs the qualitative point-point query "t1 > t2 ?"
 *  where t1 and t2 are time points.
 *
 * \param graph The input graph.
 * \param left_vertex The first (leftmost) operand.
 * \param right_vertex The second (rightmost) operand.
 * \return A pair where the first element is the probability and the
 *  second one is the preference of the ">" relationship between
 *  the given time points.
 *
 * Given two time points t_1 and t_2, and given the temporal constraint
 * t1 <(d1,p1,P1),...,(dk,pk,Pk)> t2, the result of the "t1 > t2" query is
 * computed as follows:
 * - Prob(t2 > t1) = sum_{di > 0} phi(di), if there exists di in {d1,...,dk} such that di > 0 and phi(di) != %,
 *                   %,                    if there exists di in {d1,...,dk} such that di > 0 and phi(di) = %,
 *                   0,                    otherwise.
 * - Pref(t2 > t1) = MAX_{d in D}(sigma(d)), if D={d | d in {d1,...,dk} and d > 0} != empty and sigma(d) != #,
 *                   #,                      if D={d | d in {d1,...,dk} and d > 0} != empty and sigma(d) = #,
 *                   0,                      otherwise.
 *  where Prob() denotes the probability, Pref() denotes the preference,
 *  '%' denotes undefined probability, and '#' denotes undefined preference,
 *  phi(d) and sigma(d) denote the probability and the preference of distance d
 *  (i.e., phi(d)=pi and sigma(d)=Pi, respectively).
 */
std::tuple<double,double> query_gt(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex);

/**
 * \brief Performs the qualitative point-point query "t1 <= t2 ?"
 *  where t1 and t2 are time points.
 *
 * \param graph The input graph.
 * \param left_vertex The first (leftmost) operand.
 * \param right_vertex The second (rightmost) operand.
 * \return A pair where the first element is the probability and the
 *  second one is the preference of the "<=" relationship between
 *  the given time points.
 *
 * Given two time points t_1 and t_2, and given the temporal constraint
 * t1 <(d1,p1,P1),...,(dk,pk,Pk)> t2, the result of the "t1 <= t2" query is
 * computed as follows:
 * - Prob(t2 <= t1) = Prob(t2 > t1) + Prob(t2 = t1)
 * - Pref(t2 <= t1) = MAX(Pref(t2 > t1), Pref(t2 = t1))
 *  where Prob() denotes the probability, and Pref() denotes the preference.
 */
std::tuple<double,double> query_le(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex);

/**
 * \brief Performs the qualitative point-point query "t1 < t2 ?"
 *  where t1 and t2 are time points.
 *
 * \param graph The input graph.
 * \param left_vertex The first (leftmost) operand.
 * \param right_vertex The second (rightmost) operand.
 * \return A pair (p,P) where the first element p is the probability and the
 *  second one P is the preference of the "<" relationship between
 *  the given time points.
 *
 * Given two time points t_1 and t_2, and given the temporal constraint
 * t1 <(d1,p1,P1),...,(dk,pk,Pk)> t2, the result of the "t1 < t2" query is
 * computed as follows:
 * - Prob(t2 < t1) = sum_{di > 0} phi(di) if exists di in {d1,...,dk} such that di < 0, 0 otherwise.
 * - Pref(t2 < t1) = MAX_{d in D}(sigma(d)) if D={d | d in {d1,...,dk} and d < 0} != empty, 0 otherwise.
 *  where Prob() denotes the probability, Pref() denotes the preference, phi(d)
 *  and sigma(d) denote the probability and the preference of distance d (i.e.,
 *  phi(d)=pi and sigma(d)=Pi, respectively).
 */
std::tuple<double,double> query_lt(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex);

/**
 * \brief Performs the qualitative point-point query "t1 != t2 ?"
 *  where t1 and t2 are time points.
 *
 * \param graph The input graph.
 * \param left_vertex The first (leftmost) operand.
 * \param right_vertex The second (rightmost) operand.
 * \return A pair where the first element is the probability and the
 *  second one is the preference of the "=" relationship between
 *  the given time points.
 *
 * Given two time points t_1 and t_2, and given the temporal constraint
 * t1 <(d1,p1,P1),...,(dk,pk,Pk)> t2, the result of the "t1 != t2" query is
 * computed as follows:
 * - Prob(t1 != t2) = Prob(t2 > t1) + Prob(t2 < t1)
 * - Pref(t1 != t2) = MAX(Pref(t2 > t1), Pref(t2 < t1))
 *  where Prob() denotes the probability, and Pref() denotes the preference.
 */
std::tuple<double,double> query_ne(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "AFTER(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "AFTER" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "AFTER(e1,e2)" query is computed as follows:
 * - Prob(AFTER(e1,e2)) = Prob((end(e2)+1) < start(e1))
 * - Pref(AFTER(e1,e2)) = Pref((end(e2)+1) < start(e1))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_after(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "BEFORE(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "BEFORE" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "BEFORE(e1,e2)" query is computed as follows:
 * - Prob(BEFORE(e1,e2)) = Prob((end(e1)+1) < start(e2))
 * - Pref(BEFORE(e1,e2)) = Pref((end(e1)+1) < start(e2))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_before(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "CONTAINS(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "CONTAINS" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "CONTAINS(e1,e2)" query is computed as follows:
 * - Prob(CONTAINS(e1,e2)) = Prob(start(e1) < start(e2)) * Prob(end(e1) > end(e2))
 * - Pref(CONTAINS(e1,e2)) = MIN(Pref(start(e1) < start(e2)), Pref(end(e1) > end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_contains(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "DISJOINT(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "DISJOINT" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "DISJOINT(e1,e2)" query is computed as follows:
 * - Prob(DISJOINT(e1,e2)) = Prob(BEFORE(e1,e2) + Prob(MEETS(e1,e2)) + Prob(MET-BY(e1,e2)) + Prob(AFTER(e1,e2))
 * - Pref(DISJOINT(e1,e2)) = Pref(BEFORE(e1,e2) + Pref(MEETS(e1,e2)) + Pref(MET-BY(e1,e2)) + Pref(AFTER(e1,e2))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_disjoint(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "DURING(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "DURING" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "DURING(e1,e2)" query is computed as follows:
 * - Prob(DURING(e1,e2)) = Prob(start(e1) > start(e2)) * Prob(end(e1) < end(e2))
 * - Pref(DURING(e1,e2)) = MIN(Pref(start(e1) > start(e2)), Pref(end(e1) < end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_during(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "ENDED-BY(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "ENDED-BY" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "ENDED-BY(e1,e2)" query is computed as follows:
 * - Prob(ENDED-BY(e1,e2)) = Prob(start(e1) < start(e2)) * Prob(end(e1) = end(e2))
 * - Pref(ENDED-BY(e1,e2)) = MIN(Pref(start(e1) < start(e2)), Pref(end(e1) = end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_ended_by(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "ENDS(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "ENDS" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "ENDS(e1,e2)" query is computed as follows:
 * - Prob(ENDS(e1,e2)) = Prob(start(e1) < start(e2)) * Prob(end(e1) = end(e2))
 * - Pref(ENDS(e1,e2)) = MIN(Pref(start(e1) < start(e2)), Pref(end(e1) = end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_ends(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "EQUAL(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "EQUAL" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "EQUAL(e1,e2)" query is computed as follows:
 * - Prob(EQUAL(e1,e2)) = Prob(start(e1) = start(e2)) * Prob(end(e1) = end(e2))
 * - Pref(EQUAL(e1,e2)) = MIN(Pref(start(e1) = start(e2)), Pref(end(e1) = end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_equal(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "INTERSECT(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "INTERSECT" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "INTERSECT(e1,e2)" query is computed as follows:
 * - Prob(INTERSECT(e1,e2)) = 1 - Prob(DISJOINT(e1,e2))
 * - Pref(INTERSECT(e1,e2)) = MAX(Pref(OVERLAPS(e1,e2)), Pref(ENDED_BY(e1,e2)), Pref(CONTAINS(e1,e2)),
 *                                Pref(STARTS(e1,e2)), Pref(EQUAL(e1,e2)), Pref(STARTED_BY(e1,e2)),
 *                                Pref(DURING(e1,e2)), Pref(ENDS(e1,e2)), Pref(OVERLAPPED_BY(e1,e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_intersect(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "MEETS(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "MEETS" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "MEETS(e1,e2)" query is computed as follows:
 * - Prob(MEETS(e1,e2)) = Prob((end(e1)+1) = start(e2))
 * - Pref(MEETS(e1,e2)) = Pref((end(e1)+1) = start(e2))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_meets(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "MET-BY(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "MET-BY" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "MET-BY(e1,e2)" query is computed as follows:
 * - Prob(MET-BY(e1,e2)) = Prob((end(e2)+1) = start(e1))
 * - Pref(MET-BY(e1,e2)) = Pref((end(e2)+1) = start(e1))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_met_by(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "OVERLAPPED-BY(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "OVERLAPPED-BY" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "OVERLAPPED-BY(e1,e2)" query is computed as follows:
 * - Prob(OVERLAPPED-BY(e1,e2)) = Prob(start(e1) > start(e2)) * Prob(start(e1) <= end(e2)) * Prob(end(e1) > end(e2))
 * - Pref(OVERLAPPED-BY(e1,e2)) = MIN(Pref(start(e1) > start(e2)), Pref(start(e1) <= end(e2)), Pref(end(e1) > end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_overlapped_by(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "OVERLAPS(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "OVERLAPS" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "OVERLAPS(e1,e2)" query is computed as follows:
 * - Prob(OVERLAPS(e1,e2)) = Prob(start(e1) < start(e2)) * Prob(end(e1) >= start(e2)) * Prob(end(e1) < end(e2))
 * - Pref(OVERLAPS(e1,e2)) = MIN(Pref(start(e1) < start(e2)), Pref(end(e1) >= start(e2)), Pref(end(e1) < end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_overlaps(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "STARTED-BY(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "STARTED-BY" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "STARTED-BY(e1,e2)" query is computed as follows:
 * - Prob(STARTED-BY(e1,e2)) = Prob(start(e1) = start(e2)) * Prob(end(e1) > end(e2))
 * - Pref(STARTED-BY(e1,e2)) = MIN(Pref(start(e1) = start(e2)), Pref(end(e1) > end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_started_by(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);

/**
 * \brief Performs the qualitative interval-interval query "STARTS(e1,e2)",
 *  where e1 and e2 are durative events.
 *
 * \param event1_left_vertex The vertex associated to the starting time point of
 *  the first (leftmost) event.
 * \param event1_right_vertex The vertex associated to the ending time point of
 *  the first (leftmost) event.
 * \param event2_left_vertex The vertex associated to the starting time point of
 *  the second (rightmost) event.
 * \param event2_right_vertex The vertex associated to the ending time point of
 *  the second (rightmost) event.
 *  \return A pair (p,P) where the first element p is the probability and the
 *   second one P is the preference of the "STARTS" relationship between
 *   the given durative events.
 *
 * A durative event is represented by its starting and ending time points,
 * which in turn are represented as vertex in the graph.
 *
 * The result of the "STARTS(e1,e2)" query is computed as follows:
 * - Prob(STARTS(e1,e2)) = Prob(start(e1) = start(e2)) * Prob(end(e1) < end(e2))
 * - Pref(STARTS(e1,e2)) = MIN(Pref(start(e1) = start(e2)), Pref(end(e1) < end(e2)))
 *  where Prob() denotes the probability, Pref() denotes the preference, end(e)
 *  and start(e) denote the endpoints of durative event e.
 */
std::tuple<double,double> query_starts(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex);


#endif // PPQTN_GRAPH_ALGO_HPP
