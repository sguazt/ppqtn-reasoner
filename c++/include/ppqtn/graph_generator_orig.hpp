/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph_generator.hpp
 *
 * \brief Graph generators.
 *
 * \author Antonella Andolina
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_GENERATOR_ORIG_HPP
#define PPQTN_GRAPH_GENERATOR_ORIG_HPP

#include <ppqtn/graph.hpp>


void random_graph_orig(unsigned long seed, std::size_t nv, graph_t& graph);


#endif // PPQTN_GRAPH_GENERATOR_ORIG_HPP
