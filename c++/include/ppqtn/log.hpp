/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file log.hpp
 *
 * \brief Functions related to logging.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_LOG_HPP
#define PPQTN_LOG_HPP

#include <iosfwd>
#include <string>

#define PPQTN_LOG_STRINGIFY_(x) #x
#define PPQTN_LOG_TOSTRING_(x) PPQTN_LOG_STRINGIFY_(x)
#define PPQTN_LOG_AT0_ __FILE__ ":" PPQTN_LOG_TOSTRING_(__LINE__)
#if __STDC_VERSION__ >= 199901L || defined(__GNUC__)
// C99 or more recent versions of GCC have __func__ macro
# define PPQTN_LOG_AT1_ (std::string(":(") + std::string(__func__) + std::string(")"))
#elif defined(__GNUC__) || defined(__MSC_VER__)
// GCC and MS Visual Studio have __FUNCTION__ macro
# define PPQTN_LOG_AT1_ (std::string(":(") + std::string(__FUNCTION__) + std::string(")"))
#else
// Can't provide function name info
# define PPQTN_LOG_AT1_ /*empty*/
#endif // __STDC_VERSION__
#define PPQTN_LOG_AT (std::string(PPQTN_LOG_AT0_) + std::string(PPQTN_LOG_AT1_))

#define PPQTN_LOG_DEBUG_AT(msg) log_debug(std::string("[") + PPQTN_LOG_AT + std::string("] ") + msg)

#define PPQTN_LOG_ERROR_AT(msg) log_error(std::string("[") + PPQTN_LOG_AT + std::string("] ") + msg)

#define PPQTN_LOG_INFO_AT(msg) log_info(std::string("[") + PPQTN_LOG_AT + std::string("] ") + msg)

#define PPQTN_LOG_WARN_AT(msg) log_warn(std::string("[") + PPQTN_LOG_AT + std::string("] ") + msg)


/// Logs the given debugging message \a msg.
void log_debug(const std::string& msg);

/// Logs the given debugging message \a msg.
void log_debug(const std::string& msg);

/// Logs the given error message \a msg.
void log_error(const std::string& msg);

/// Logs the given information message \a msg.
void log_info(const std::string& msg);

/// Returns the underlying stream used for logging.
std::ostream& log_stream();

/// Logs the given error message \a msg as follows:
///  msg + ": " + <error message according to the current value of `errno`>
void log_syserror(const std::string& msg);

/// Logs the given warning message \a msg.
void log_warn(const std::string& msg);


#endif // PPQTN_LOG_HPP
