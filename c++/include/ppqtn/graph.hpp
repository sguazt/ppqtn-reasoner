/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph.hpp
 *
 * \brief Graph type.
 *
 * \author Antonella Andolina
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_HPP
#define PPQTN_GRAPH_HPP

#include <cstddef>
#include <ppqtn/error.hpp>
#include <string>
#include <vector>


// -----------------------------------------------------------------------------
// CONSTANTS

namespace constants {

const double def_pref = 1; ///< Indicates that triples in a temporal constraint contains preferences
const double def_prob = 1; ///< Indicates that triples in a temporal constraint contains probabilities
const double undef_pref = -1; ///< Indicates that triples in a temporal constraint does not contain probabilities
const double undef_prob = -1; ///< Indicates that triples in a temporal constraint does not contain probabilities
const char* const undef_prob_symbol = "%"; ///< Text symbol denoting undefined probabilities
const char* const undef_pref_symbol = "#"; ///< Text symbol denoting undefined preferences

} // Namespace constants


// -----------------------------------------------------------------------------
// TERNE

struct terne     //struttura delle terne distanza/probabilità/preferenze.
{
    /// Default constructor
    terne();

    /// Copy constructor
    terne(const terne& other);

    /// Copy assignment
    terne& operator=(const terne& other);

    /// Tells whether probability is undefined or not
    bool is_prob_undef() const;

    /// Tells whether preference is undefined or not
    bool is_pref_undef() const;

    /// Tells whether this triple is valid or not
    bool is_valid() const;


    int dist;
    double prob;    // se il vincolo non ha delle probabilità il valore è NULL 
    double pref;    // se il vincolo non ha delle preferenze il valore è NULL
}; // terne


// -----------------------------------------------------------------------------
// PUNT

typedef terne *punt; //tipo puntatore alle terne


// -----------------------------------------------------------------------------
// GRAPH_T

/**
 * \brief Adjacency-based PPQTN graph representation
 *
 * Each element `e` of the adjacency matrix (an edge) is a sequence of triples,
 * each of type `terne`, denoting the temporal+probability+preference
 * information attached to that edge.
 * That sequence has the following structure:
 * - The first element `e[0]` is the descriptor of the sequence and provides the
 *   following information:
 *   1. `e[0].dist` is the number of the remaning triples in the temporal
 *      constraint; hence, if `e[0].dist == 1`, the whole sequence `e` has two
 *      triples, where `e[0]` is the sequence descriptor and `e[1]` is the
 *      temporal constraint;
 *   2. `e[0].prob` tells whether the subsequent triples contains a valid
 *      probability value (`e[0].prob` is set to a value different from
 *      `constants::undef_prob`) or not (`e[0].prob` is set to
 *      `constants::undef_prob`);
 *   3. `e[0].pref` tells whether the subsequent triples contains a valid
 *      preference value (`e[0].pref` is set to a value different from
 *      `constants::undef_pref`) or not (`e[0].prob` is set to
 *      `constants::undef_prob`).
 *   .
 * - The remaining elements `e[1],...,e[n]` represents elements of a temporal
 *   constraint, where `e[i].dist` is the distance, `e[i].prob` is the
 *   probability, and `e[i].pref` is the preference.
 *
 * TODO: to improve performance, use `point*` instead of `point**` to store the
 *       adjacency matrix. The only drawback is that you cannot use the double
 *       bracket notation mat_[r][c] to access row r and column c; to access the
 *       same location you need to write mat_[r * d_ + c]; this, can be easily
 *       masked by overloading `operator()` as follows:
 *         `point& operator()(size_t r, size_tc) { return mat_[r*d_+c]; }`
 */
class graph_t
{
//private:
    //using container = punt**;
    //using value_type = punt;
    //using reference = punt&;
    //using size_type = std::size_t;
    ////using container = std::vector<std::vector<punt>>;
    ////using value_type = typename container::value_type;
    ////using reference = typename container::reference;
    ////using size_type = typename container::size_type;


public:
    /// Creates an empty graph
    graph_t();

    /// Creates a disconnected graph with \a n vertices.
    explicit graph_t(std::size_t n);

    /// Destructor
    virtual ~graph_t();

    /// Copy constructor
    graph_t(const graph_t& g);

    /**
     * \brief Creates a graph from the an adjacency matrix.
     *
     * \param adj_mat The adjacency matrix.
     * \param n The dimension of the adjacency matrix.
     * \param copy Tells whether this graph should use a copy of the given
     *             adjacency matrix or not.
     *
     * This is a convenient constructor useful to easily mix user code that uses
     * this graph representation with the one that instead works directly on the
     * adjacency matrix.
     *
     * The \a copy parameter determines the ownership of the adjacency matrix
     * (i.e., who is responsible to allocate/deallocate memory for it) as
     * follows:
     * - if \a copy is `true`, this graph will not own \a adj_mat; instead, it
     *   will create of an internal copy of \a adj_mat whose memory is managed
     *   by this class; hence, any change performed to this internal copy will
     *   not affect \a adj_mat.
     * - if \a copy is `false`, this graph will own \a adj_mat and it will work
     *   directly on it; hence, any change performed to \a adj_mat done outside
     *   this class will affect the state of this graph and any change to
     *   \a adj_mat done inside this class could potentially affect the user
     *   code that uses the matrix; furthermore, on destruction the memory
     *   allocated for the adjacency matrix will not be deallocated.
     * Note, if this graph will be copied or assigned to another graph, the
     * ownership of the adjacency matrix will affect also the target graph.
     */
    graph_t(punt** adj_mat, std::size_t n, bool copy = false);

    /// Copy assignment
    graph_t& operator=(const graph_t& g);

    /**
     * \brief Directly accesses to the cell of the adjacency matrix located at
     *        row \a row and column \a col.
     *
     * \param row The row index.
     * \param col The column index.
     * \return A reference to the list of triples associated to the cell
     *         `(row,col)`.
     *
     * \note It is responsibility of the caller to keep the graph structure
     *       consistent; for instance, every time the cell `(row,col)` is
     *       updated, the caller should update the cell `(col,row)`,
     *       accordingly.
     *       It is strongly suggested to use the `connect()` method to keep the
     *       graph structure consistent.
     */
    punt& operator()(std::size_t row, std::size_t col);
 
    /**
     * \brief Returns the content of the cell of the adjacency matrix located
     *        at row \a row and column \a col.
     */
    const punt& operator()(std::size_t row, std::size_t col) const;

    /// Directly accesses to the underlying adjacency matrix.
    punt** adjacency_matrix();

    /// Directly accesses to the underlying adjacency matrix.
    punt* const* adjacency_matrix() const;

    /**
     * \brief Adds an edge between vertices \a v1 and \a v2.
     *
     * \param v1 The first vertex.
     * \param v2 The second vertex.
     *
     * Side-effects:
     * - This function also connects \a v2 to \a v1, by associating the same
     *   temporal constraint associated with the edge from \a v1 to \a v2 except
     *   for time distances, which are set to the opposite value.
     * - If \a v1 and \a v2 are already connected, their temporal constraint
     *   will be overwritten.
     * .
     */
    void add_edge(std::size_t v1, std::size_t v2);
    void connect(std::size_t v1, std::size_t v2) { add_edge(v1, v2); }//XXX: deprecated

    /**
     * \brief Adds an edge between vertices \a v1 and \a v2, and associates the
     *  given sequence of triples as constraint.
     *
     * \param v1 The first vertex.
     * \param v2 The second vertex.
     *
     * Side-effects:
     * - This function also connects \a v2 to \a v1, by associating the same
     *   temporal constraint associated with the edge from \a v1 to \a v2 except
     *   for time distances, which are set to the opposite value.
     * - If \a v1 and \a v2 are already connected, their temporal constraint
     *   will be overwritten.
     * - If \a v1 and \a v2 are the same, the arguments \a first_triple and
     *   \a last_triple will be ignored.
     */
    template <typename IterT>
    void add_edge(std::size_t v1, std::size_t v2, IterT first_triple, IterT last_triple);
    template <typename IterT>
    void connect(std::size_t v1, std::size_t v2, IterT first_triple, IterT last_triple) { add_edge(v1, v2, first_triple, last_triple); } //XXX: deprecated

    /// Tells whehter this graph is empty (i.e., without vertices) or not.
    bool empty() const;

    /// Tells whether vertices \a v1 and \a v2 are adjacent.
    bool has_edge(std::size_t v1, std::size_t v2) const;
    bool connected(std::size_t v1, std::size_t v2) const { return has_edge(v1, v2); } //XXX: deprecated

    /// Removes the edge between \a v1 and \a v2.
    std::vector<terne> remove_edge(std::size_t v1, std::size_t v2);
    std::vector<terne> disconnect(std::size_t v1, std::size_t v2) { return remove_edge(v1, v2); } //XXX: deprecated

    /// Sets or change the number of vertices to \a n.
    void num_vertices(std::size_t n);

    /// Returns the number of vertices.
    std::size_t num_vertices() const;

    /// Returns the degree of the given vertex \a v, that is the number of edges
    /// that are incident to that vertex.
    std::size_t vertex_degree(std::size_t v) const;


protected:
    static void add_edge(punt** mat, std::size_t d, std::size_t r, std::size_t c, bool both_dir);

    template <typename IterT>
    static void add_edge(punt** mat, std::size_t d, std::size_t r, std::size_t c, bool both_dir, IterT first_triple, IterT last_triple);

    static void destroy_matrix(punt** mat, std::size_t d);

    static bool has_edge(punt** mat, std::size_t d, std::size_t r, std::size_t c);

    static punt remove_edge(punt** mat, std::size_t d, std::size_t r, std::size_t c, bool both_dir);


private:
    std::size_t d_; ///< Number of vertices
    punt** mat_; ///< Adjacency matrix
    //std::vector<std::vector<punt>> mat_; ///< Adjacency matrix - TODO: use this to simplify mem management
    bool own_mat_; ///< Tells whether this class must care of memory (de)allocation of the adjacency matrix or not.
}; // graph_t


/// Store some properties associated with a graph
struct graph_properties_t
{
    std::string graph_name; //< The name of the root graph
    std::vector<std::string> node_names; //< Map a node `x` to the associated name `node_names[x]`
}; // graph_properties_t


// Include template definitions
#include "graph.ipp"


#endif // PPQTN_GRAPH_HPP
