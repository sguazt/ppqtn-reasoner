/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file util.hpp
 *
 * \brief Utility functions.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_UTIL_HPP
#define PPQTN_UTIL_HPP

#include <string>


/// Reads the whole file \a filename into a string and returns the result.
std::string read_file(const char* filename);

/// Reads the whole file \a filename into a string and returns the result.
std::string read_file(const std::string& filename);

/// Strips heading spaces from the given string \a s.
void ltrim(std::string& s);

/// Strips trailing spaces from the given string \a s.
void rtrim(std::string& s);

/// Strips heading and trailing spaces from the given string \a s.
void trim(std::string& s);

/// Strips heading spaces from a copy of the given string \a s.
std::string ltrim_copy(std::string s);

/// Strips trailing spaces from a copy of the given string \a s.
std::string rtrim_copy(std::string s);

/// Strips heading and trailing spaces from a copy of the given string \a s.
std::string trim_copy(std::string s);

template <typename IterT>
std::string join(IterT first, IterT last, const std::string& sep);

void to_lower(std::string& s);

std::string to_lower_copy(std::string s);

void to_upper(std::string& s);

std::string to_upper_copy(std::string s);


// Include template definitions
#include "util.ipp"


#endif // PPQTN_UTIL_HPP
