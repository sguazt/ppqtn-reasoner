/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph_io.hpp
 *
 * \brief Functions for reading/writing graphs.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_IO_HPP
#define PPQTN_GRAPH_IO_HPP

#include <iostream>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_io_dot.hpp>
#include <ppqtn/graph_io_util.hpp>


enum graph_file_format_t
{
    dot_graph_file_format
}; // graph_file_format_t


/// Reads a graph from the given string \a txt_graph, assuming a default graph
/// format and returns the read graph as value.
std::unique_ptr<graph_t> read_graph(const std::string& txt_graph, graph_file_format_t graph_format = dot_graph_file_format);

/// Reads a graph from the given string \a txt_graph, assuming a default graph
/// format and returns the read graph as value; in addition to returning the
/// read graph, it also stores in the parameter \a graph_propos various graph properties.
std::unique_ptr<graph_t> read_graph(const std::string& txt_graph, graph_properties_t& graph_props, graph_file_format_t graph_format = dot_graph_file_format);

/// Writes the given graph \a graph to the given stream \a os.
void write_graph(const graph_t& g, std::ostream& os, bool both_dir, graph_file_format_t graph_format = dot_graph_file_format);

/// Writes the given graph \a graph to the given stream \a os.
void write_graph(const graph_t& g, std::ostream& os, bool both_dir, const graph_properties_t& graph_props, graph_file_format_t graph_format = dot_graph_file_format);

//[FIXME]: first implement read_graph(std::istream)
///// Reads a graph from the given stream \a is and saves the result in \a graph.
//template <typename CharT, typename CharTraitsT>
//std::basic_istream<CharT,CharTraitsT>& operator>>(std::basic_istream<CharT,CharTraitsT>& is, graph_t& graph);
//[/FIXME]: first implement read_graph(std::istream)

/// Writes the given graph \a graph to the given stream \a os.
template <typename CharT, typename CharTraitsT>
std::basic_ostream<CharT,CharTraitsT>& operator<<(std::basic_ostream<CharT,CharTraitsT>& os, const graph_t& graph);

//#ifdef PPQTN_DEBUG

template <typename CharT, typename CharTraitsT>
std::basic_ostream<CharT,CharTraitsT>& operator<<(std::basic_ostream<CharT,CharTraitsT>& os, const punt& p);

//#endif // PPQTN_DEBUG


template <typename CharT, typename CharTraitsT>
inline
std::basic_ostream<CharT,CharTraitsT>& operator<<(std::basic_ostream<CharT,CharTraitsT>& os, const graph_t& graph)
{
    write_graph(graph, os, false);

    return os;
}

//[FIXME]: first implement read_graph(std::istream)
/*
template <typename CharT, typename CharTraitsT>
inline
std::basic_istream<CharT,CharTraitsT>& operator>>(std::basic_istream<CharT,CharTraitsT>& is, graph_t& graph)
{
    try
    {
        auto p_graph = read_graph(is);
        graph = *p_graph;
    }
    catch (...)
    {
        is.setstate(std::ios::failbit);
        throw;
    }

    return is;
}
*/
//[/FIXME]: first implement read_graph(std::istream)

//#ifdef PPQTN_DEBUG

template <typename CharT, typename CharTraitsT>
std::basic_ostream<CharT,CharTraitsT>& operator<<(std::basic_ostream<CharT,CharTraitsT>& os, const punt& p)
{
    os << "<";
    if (p != nullptr)
    {
        os << "[" << p[0].dist << ", " << p[0].prob << ", " << p[0].pref << "]";
        for (int i = 1; i <= p[0].dist; ++i)
        {
            if (i > 1)
            {
                os << ", ";
            }
            os << "(";
            os << p[i].dist;
            if (p[i].is_prob_undef())
            {
                os << ", %";
            }
            else
            {
                os << ", " << p[i].prob;
            }
            if (p[i].is_pref_undef())
            {
                os << ", #";
            }
            else
            {
                os << ", " << p[i].pref;
            }
            os << ")";
        }
    }
    os << ">";

    return os;
}

//#endif // PPQTN_DEBUG


#endif // PPQTN_GRAPH_IO_HPP
