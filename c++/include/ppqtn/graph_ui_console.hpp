/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph_ui_console.hpp
 *
 * \brief Functions for console-based user interfaces.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_UI_CONSOLE_HPP
#define PPQTN_GRAPH_UI_CONSOLE_HPP

#include <cstddef>
#include <string>
#include <utility>


struct ui_params_t
{
    ui_params_t()
    : rand_constraint_length_bounds(0,0),
      rand_distance_bounds(0,0)
    {
    }


    std::string graph_input_file;
    std::string graph_input_file_format;
    bool graph_output_both_dir = false;
    std::string graph_output_file;
    std::string graph_output_file_format;
    std::size_t rand_bridge_length = 0;
    std::pair<std::size_t,std::size_t> rand_constraint_length_bounds;
    std::pair<std::size_t,std::size_t> rand_distance_bounds;
    bool rand_limit_constraint_length = false;
    std::size_t rand_num_vertices = 0;
    unsigned long rand_seed = 0;
}; // ui_params_t


void show_console_ui(const ui_params_t& default_setings);


#endif // PPQTN_GRAPH_UI_CONSOLE_HPP
