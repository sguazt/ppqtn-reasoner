/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph_generator.hpp
 *
 * \brief Graph generators.
 *
 * \author Antonella Andolina
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_GENERATOR_HPP
#define PPQTN_GRAPH_GENERATOR_HPP

#include <cstddef>
#include <memory>
#include <ppqtn/graph.hpp>


//TODO
///**
// * \brief Generates a random graph with \a nv vertices and \a ne edges.
// * 
// * \param nv Number of vertices.
// * \param ne Number of edges.
// * \return The randomly generated graph.
// */
//std::unique_ptr<graph_t> random_graph(unsigned long seed, std::size_t nv, std::size_t ne);

///XXX: deprecated
/**
 * \brief Generates a random linear graph with \a nv vertices.
 * 
 * \param nv Number of vertices.
 * \return The randomly generated graph.
 */
//std::unique_ptr<graph_t> random_linear_graph(unsigned long seed, std::size_t nv);

/**
 * \brief Generates a random linear graph with \a nv vertices.
 * 
 * \param rng The random number generator.
 * \param num_vertices Number of vertices.
 * \param bridge_len Length of a bridge
 * \param max_num_triples Maximum number of triples in a constraint.
 * \param min_dist Minimum temporal distance.
 * \param max_dist Maximum temporal distance.
 * \param limit_composed_constraint_size Tells whether the number of triples in a constraint
 *  obtained by composition should be reduced to a randomly generated number between
 *  \a min_num_triples and \a max_num_triples, or not.
 * \return The randomly generated graph.
 */
template <typename GeneratorT>
std::unique_ptr<graph_t> generate_random_linear_graph(GeneratorT& rng, std::size_t num_vertices, std::size_t bridge_len, std::size_t min_num_triples, std::size_t max_num_triples, std::size_t min_dist, std::size_t max_dist, bool limit_composed_constraint_size);


struct graph_generator_t
{
    virtual std::unique_ptr<graph_t> operator()() = 0;
}; // graph_generator_t


class identity_graph_generator_t: public graph_generator_t
{
public:
    identity_graph_generator_t(const graph_t& graph);

    std::unique_ptr<graph_t> operator()();


private:
    graph_t graph_;
}; // identity_graph_generator_t



template <typename GeneratorT>
class random_linear_graph_generator_t: public graph_generator_t
{
public:
    random_linear_graph_generator_t(std::shared_ptr<GeneratorT> p_rng, std::size_t num_vertices, std::size_t bridge_len, std::size_t min_num_triples, std::size_t max_num_triples, std::size_t min_dist, std::size_t max_dist, bool limit_composed_constraint_size);

    ~random_linear_graph_generator_t() = default;

    std::unique_ptr<graph_t> operator()();


private:
    std::shared_ptr<GeneratorT> p_rng_;
    std::size_t num_vertices_;
    std::size_t bridge_len_;
    std::size_t min_num_triples_;
    std::size_t max_num_triples_;
    std::size_t min_dist_;
    std::size_t max_dist_;
    bool limit_composed_constraint_size_;
}; // random_linear_graph_generator_t


// Include template definitions
#include "graph_generator.ipp"

#if 0
template <typename GeneratorT>
std::unique_ptr<graph_t> generate_random_linear_graph(GeneratorT& rng, std::size_t nv, std::size_t max_nt, std::size_t min_dist, std::size_t max_dist)
{
    std::unique_ptr<graph_t> p_graph(new graph_t(nv));

    std::uniform_int_distribution<std::size_t> nt_distr(1, max_nt); // Probability distribution for generating the number of triples in a constraint
    std::uniform_int_distribution<int> dist_distr(min_dist, max_dist); // Probability distribution for generating temporal distances
    std::bernoulli_distribution have_prob_distr; // Probability distribution for deciding if a constraint has probabilities or not
    std::bernoulli_distribution have_pref_distr; // Probability distribution for deciding if a constraint has preferences or not
    std::uniform_real_distribution<double> prob_distr; // Probability distribution for generating probabilities
    std::uniform_real_distribution<double> pref_distr; // Probability distribution for generating preferences


    // Connects each vertex with its successor to generate a linear graph
    for (std::size_t r = 0; r < (nv-1); ++r)
    {
        // Generate a costraints with nt triples with consecutive distances,
        // that is:
        //   <d_1,p_1,q_1>, <d_2,p_2,q_2>, ..., <d_nt, p_nt, q_nt>
        // where d_k, p_k, q_k denote the distance, the probability and the
        // preference associated to the i-th triple of the constraints.
        // Specifically, d_k = b + k, where b is randomly generated as
        // b=Unif(mindist,maxdist)-nt/2 so that generated distance will be
        // centered in the generated value Unif(mindist,maxdist).

        auto const nt = nt_distr(rng); // Constraint size
        auto const have_prob = have_prob_distr(rng); // Have probabilities?
        auto const have_pref = have_pref_distr(rng); // Have preferences?
        auto const dist_max = dist_distr(rng); // Max distance in this constraint
        auto const dist_base = dist_max - nt/2; // Base value for distances

        // Creates the constraint
        std::vector<terne> triples(nt);
        double prob_sum = 0; // Normalization constant for probabilities
        double pref_sum = 0; // Normalization constant for preferences
        //double pref_sum = 0; // Normalization constant for preferences
        for (std::size_t k = 0; k < nt; ++k)
        {
            triples[k].dist = dist_base + k;
            triples[k].prob = have_prob ? prob_distr(rng) : constants::undef_prob;
            triples[k].pref = have_pref ? pref_distr(rng) : constants::undef_pref;
            if (have_prob)
            {
                prob_sum += triples[k].prob;
            }
            if (have_pref)
            {
                pref_sum += triples[k].pref;
            }
        }
        // Normalizes probabilities and preferences (if needed)
        if (have_prob || have_pref)
        {
            for (std::size_t k = 0; k < nt; ++k)
            {
                if (have_prob)
                {
                    triples[k].prob /= prob_sum;
                }
                if (have_pref)
                {
                    triples[k].pref /= pref_sum;
                }
            }
        }
        // Creates a new edge and attaches the above constraint
        p_graph->connect(r, r+1, triples.begin(), triples.end());
    }

    return p_graph;
}
#endif


#endif // PPQTN_GRAPH_GENERATOR_HPP
