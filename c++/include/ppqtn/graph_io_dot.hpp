/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph_io_dot.hpp
 *
 * \brief Functions for reading/writing graphs in GraphViz DOT format.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_IO_DOT_HPP
#define PPQTN_GRAPH_IO_DOT_HPP

#include <iosfwd>
#include <memory>
#include <ppqtn/graph.hpp>
#include <string>


///// Store some properties associated with a DOT graph
//struct dot_graph_properties_t
//{
//    std::string graph_name; //< The name of the root graph
//    std::vector<std::string> node_names; //< Map a node `x` to the associated name `node_names[x]`
//}; // dot_graph_properties_t


/// Parses a DOT graph from the given string \a dot_graph and stores the resulting graph in \a graph.
//void read_dot(const std::string& dot_graph, graph_t& graph);
std::unique_ptr<graph_t> read_dot_graph(const std::string& dot_graph);

/// Parses a DOT graph from the given string \a dot_graph and stores the resulting graph in \a graph and properties in \a graph_props.
//void read_dot(const std::string& dot_graph, graph_t& graph, graph_properties_t& graph_props);
std::unique_ptr<graph_t> read_dot_graph(const std::string& dot_graph, graph_properties_t& graph_props);

/// Export the given graph \a graph to the given stream \a os as a DOT graph.
/// The \a both_dir parameter tells whether both (u,v) and (v,u) edges must be
/// exported, for each connected pair of vertices u and v.
void write_dot_graph(const graph_t& graph, std::ostream& os, bool both_dir);

/// Export the given graph \a graph to the given stream \a os as a DOT graph, by using the given properties \a graph_props.
/// The \a both_dir parameter tells whether both (u,v) and (v,u) edges must be
/// exported, for each connected pair of vertices u and v.
void write_dot_graph(const graph_t& graph, std::ostream& os, bool both_dir, const graph_properties_t& graph_props);


#endif // PPQTN_GRAPH_IO_HPP
