/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file stopwatch.hpp
 *
 * \brief Timer type.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_STOPWATCH_HPP
#define PPQTN_STOPWATCH_HPP

#include <chrono>
#include <cstddef>
#include <type_traits>
#include <utility>


class stopwatch_t
{
public:
    /**
     * \brief Create a new stopwatch instance.
     *
     * \param use_process_cpu_time Tells whether to measure the per-process CPU
     *  time (`true` value), i.e., the CPU time consumed by all threads in the
     *  process, or the per-thread CPU time (`false` value), i.e., the CPU time
     *  of current thread.
     */
    explicit stopwatch_t(bool use_process_cpu_time = true);

    stopwatch_t(const stopwatch_t&) = default;

    //stopwatch_t& operator=(const stopwatch_t&);

    /// Starts (or restarts) the timer.
    void start();

    /// Stops the timer.
    void stop();

    /// Resets the timer.
    void reset();

    /// Tells whether this timer has been started or not.
    bool started() const;

    /// Returns the elapsed process or thread CPU time (user+system) since started.
    double elapsed_cpu_time() const;

    /// Returns the elapsed wallclock time since started.
    double elapsed_real_time() const;


private:
    /// Retrieves either the process or thread CPU time
    double get_cpu_time() const;


private:
    /**
     * \brief Clock type.
     *
     * NOTE: prefer a steady clock to a non-steady one because the former is a
     *       monotonic clock, that is a clock generating time points that
     *       cannot decrease as physical time moves forward and whereby the
     *       time between ticks is constant.
     */
    using clock_type = typename std::conditional<std::chrono::high_resolution_clock::is_steady,
                                                 std::chrono::high_resolution_clock,
                                                 std::chrono::steady_clock>::type;


private:
    bool use_proc_cpu_time_;
    bool started_;
    double cpu_time_start_;
    double cpu_time_stop_;
    clock_type::time_point real_time_start_;
    clock_type::time_point real_time_stop_;
}; // stopwatch_t


template<typename FuncT, typename... ArgsT>
stopwatch_t time_it(bool use_process_cpu_time, FuncT func, ArgsT&&... args)
{
    stopwatch_t timer(use_process_cpu_time);

    timer.start();
    //std::invoke(std::forward<FuncT>(func), std::forward<ArgsT>(args)...); // requires C++17
    func(std::forward<ArgsT>(args)...);
    timer.stop();

    return timer;
}

#endif // PPQTN_STOPWATCH_HPP
