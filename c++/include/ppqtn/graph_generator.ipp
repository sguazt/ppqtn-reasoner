/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph_generator.ipp
 *
 * \brief Template definitions.
 *
 * \author Antonella Andolina
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_GENERATOR_IPP
#define PPQTN_GRAPH_GENERATOR_IPP


#include <algorithm>
#include <cassert>
#include <ppqtn/error.hpp>
#include <ppqtn/graph_algo.hpp>
#include <ppqtn/log.hpp>
#include <random>

#ifdef PPQTN_DEBUG
# include <iostream>
# include <ppqtn/graph_io.hpp>
#endif //PPQTN_DEBUG


template <typename GeneratorT>
std::unique_ptr<graph_t> generate_random_linear_graph(GeneratorT& rng, std::size_t num_vertices, std::size_t bridge_len, std::size_t min_num_triples, std::size_t max_num_triples, std::size_t min_dist, std::size_t max_dist, bool limit_composed_constraint_size)
{
    // Check arguments
    if (min_dist == 0)
    {
        throw std::invalid_argument("Distance cannot be zero");
    }
    if (min_dist > max_dist)
    {
        throw std::invalid_argument("Mininum distance cannot be greater than maximum distance");
    }
    if (min_num_triples > max_num_triples)
    {
        throw std::invalid_argument("Mininum number of triples cannot be greater than maximum number of triples");
    }

    std::unique_ptr<graph_t> p_graph(new graph_t(num_vertices));

    if (num_vertices > 0)
    {
        std::uniform_int_distribution<std::size_t> nt_distr(min_num_triples, max_num_triples); // Probability distribution for generating the number of triples in a constraint
        std::uniform_int_distribution<int> dist_distr(min_dist, max_dist); // Probability distribution for generating temporal distances
        std::bernoulli_distribution have_prob_distr; // Probability distribution for deciding if a constraint has probabilities or not
        std::bernoulli_distribution have_pref_distr; // Probability distribution for deciding if a constraint has preferences or not
        std::uniform_real_distribution<double> prob_distr; // Probability distribution for generating probabilities
        std::uniform_real_distribution<double> pref_distr; // Probability distribution for generating preferences

        for (std::size_t u = 0; u < (num_vertices-1); u += bridge_len)
        {
            // Iterates over each group of vertices [u, u+bridge_len]

            auto v = std::min(u+bridge_len, num_vertices-1);

            // First, creates paths of length 1
            for (std::size_t x = u; x < v; ++x)
            {
                // Generates a costraints made of a random number nt of
                // triples with consecutive distances, that is:
                //   <d_1,p_1,q_1>, <d_2,p_2,q_2>, ..., <d_nt, p_nt, q_nt>
                // where d_k, p_k, q_k denote the distance, the probability and the
                // preference associated to the i-th triple of the constraints.
                // Specifically, d_k = b + k, where b is randomly generated as
                // b=Unif(mindist,maxdist)-nt/2 so that generated distance will be
                // centered in the generated value Unif(mindist,maxdist).
                // Note, if nt is 1, the resulting triple will always have
                // no probabilities and no preferences.

                auto const nt = nt_distr(rng); // Constraint size
                auto const have_prob = (nt > 1) ? have_prob_distr(rng) : false; // Have probabilities?
                auto const have_pref = (nt > 1) ? have_pref_distr(rng) : false; // Have preferences?
                auto const dist_base = dist_distr(rng); // Base value for distances

                // Creates the constraint

                punt triples = new terne[nt+1];
                if (triples == nullptr)
                {
                    fatal_syserror("Unable to allocate memory for constraint");
                }
                // Fills the descriptor
                triples[0].dist = nt;
                triples[0].prob = have_prob ? constants::def_prob : constants::undef_prob;
                triples[0].pref = have_pref ? constants::def_pref : constants::undef_pref;
                // Fill the triples
                double prob_sum = 0; // Normalization constant for probabilities
                double pref_sum = 0; // Normalization constant for preferences
                for (std::size_t k = 1; k <= nt; ++k)
                {
                    triples[k].dist = dist_base + (k-1);
                    triples[k].prob = have_prob ? prob_distr(rng) : constants::undef_prob;
                    triples[k].pref = have_pref ? pref_distr(rng) : constants::undef_pref;
                    if (have_prob)
                    {
                        prob_sum += triples[k].prob;
                    }
                    if (have_pref)
                    {
                        pref_sum += triples[k].pref;
                    }
                }
                // Normalizes probabilities and preferences (if needed)
                if (have_prob || have_pref)
                {
                    for (std::size_t k = 1; k <= nt; ++k)
                    {
                        if (have_prob)
                        {
                            triples[k].prob /= prob_sum;
                        }
                        if (have_pref)
                        {
                            triples[k].pref /= pref_sum;
                        }
                    }
                }

                // Creates a new edge and attaches the constraint
#ifdef PPQTN_DEBUG
log_stream() << "ADD EDGE(" << x << ", " << (x+1) << "): " << triples << std::endl;
#endif // PPQTN_DEBUG
                if (triples != nullptr)
                {
                    auto const nt = triples[0].dist;
                    p_graph->add_edge(x, x+1, triples+1, triples+nt+1);
                    delete[] triples;
                }
                else
                {
                    p_graph->add_edge(x, x+1);
                }
            }
 
            // Then, creates paths of length 2, 3, ..., bridge_len if possible

            if ((u + bridge_len) < num_vertices)
            {
                for (std::size_t step = 2; step <= bridge_len && (u+step) <= v; ++step)
                {

                    // Creates edge (u,u+step) by composing edge (u, u+step-1)
                    // with edge (u+step-1, u+step)

                    auto const x = u + step - 1;
                    auto const y = u + step;
#ifdef PPQTN_DEBUG
log_stream() << "======> U: " << u << ", V: " << v << ", X: " << x << ", Y: " << y << ", STEP: " << step << std::endl;
#endif // PPQTN_DEBUG

#ifdef PPQTN_DEBUG
log_stream() << "COMPOSING: (" << u << ", " << x << ") @ (" << x << ", " << y << ")" << std::endl;
#endif // PPQTN_DEBUG
                    punt triples = compose((*p_graph)(u, x), (*p_graph)(x, y));

                    if (limit_composed_constraint_size
                        && triples != nullptr
                        && triples[0].dist >= 0
                        && static_cast<std::size_t>(triples[0].dist) > max_num_triples)
                    {
                        auto const nt = nt_distr(rng); // Constraint size

                        punt new_triples = new terne[nt+1];
                        if (new_triples == nullptr)
                        {
                            fatal_syserror("Unable to allocate memory for constraint");
                        }

                        // Copies the nt middle triples from `triples` into `new_triples`

                        auto const nt_cut = triples[0].dist-nt;
                        auto const start_idx = 1 + nt_cut/2;
                        auto const stop_idx = start_idx + nt;
                        double prob_sum = 0;
                        double pref_sum = 0;
                        bool have_prob = triples[0].is_prob_undef() ? false : true;
                        bool have_pref = triples[0].is_pref_undef() ? false : true;
                        for (std::size_t i = start_idx; i < stop_idx; ++i)
                        {
                            new_triples[i-start_idx+1] = triples[i];
                            if (have_prob)
                            {
                                prob_sum += triples[i].prob;
                            }
                            if (have_pref)
                            {
                                pref_sum += triples[i].pref;
                            }
                        }
                        if (have_prob || have_pref)
                        {
                            for (std::size_t i = 1; i <= nt; ++i)
                            {
                                if (have_prob)
                                {
                                    new_triples[i].prob /= prob_sum;
                                }
                                if (have_pref)
                                {
                                    new_triples[i].pref /= pref_sum;
                                }
                            }
                        }

                        new_triples[0].dist = nt;
                        new_triples[0].prob = triples[0].prob;
                        new_triples[0].pref = triples[0].pref;

                        delete[] triples;

                        triples = new_triples;
                    }

                    // Creates a new edge and attaches the constraint
#ifdef PPQTN_DEBUG
log_stream() << "ADD EDGE(" << u << ", " << y << "): " << triples << std::endl;
#endif // PPQTN_DEBUG
                    //delete[] (*p_graph)(x, w);
                    //(*p_graph)(x, w) = triples;
                    if (triples != nullptr)
                    {
                        auto const nt = triples[0].dist;
                        p_graph->add_edge(u, y, triples+1, triples+nt+1);
                        delete[] triples;
                    }
                    else
                    {
                        p_graph->add_edge(u, y);
                    }

                    // Removes intermediate edges
                    if (step > 2)
                    {
#ifdef PPQTN_DEBUG
log_stream() << "REMOVE EDGE(" << u << ", " << x << "): " << (*p_graph)(u,x) << std::endl;
#endif // PPQTN_DEBUG
                        p_graph->remove_edge(u, x);
                    }
                }
            }
        }
    }

    return p_graph;
}

//[FIXME]: still some problems
#if 0
template <typename GeneratorT>
std::unique_ptr<graph_t> generate_random_linear_graph(GeneratorT& rng, std::size_t num_vertices, std::size_t bridge_len, std::size_t min_num_triples, std::size_t max_num_triples, std::size_t min_dist, std::size_t max_dist, bool limit_composed_constraint_size)
{
    // Check arguments
    if (min_dist == 0)
    {
        throw std::invalid_argument("Distance cannot be zero");
    }
    if (min_dist > max_dist)
    {
        throw std::invalid_argument("Mininum distance cannot be greater than maximum distance");
    }
    if (min_num_triples > max_num_triples)
    {
        throw std::invalid_argument("Mininum number of triples cannot be greater than maximum number of triples");
    }

    std::unique_ptr<graph_t> p_graph(new graph_t(num_vertices));

    if (num_vertices > 0)
    {
        std::uniform_int_distribution<std::size_t> nt_distr(min_num_triples, max_num_triples); // Probability distribution for generating the number of triples in a constraint
        std::uniform_int_distribution<int> dist_distr(min_dist, max_dist); // Probability distribution for generating temporal distances
        std::bernoulli_distribution have_prob_distr; // Probability distribution for deciding if a constraint has probabilities or not
        std::bernoulli_distribution have_pref_distr; // Probability distribution for deciding if a constraint has preferences or not
        std::uniform_real_distribution<double> prob_distr; // Probability distribution for generating probabilities
        std::uniform_real_distribution<double> pref_distr; // Probability distribution for generating preferences

        for (std::size_t u = 0; u < (num_vertices-1); u += bridge_len)
        {
            // Iterates over each group of vertices [u, u+bridge_len]

            auto v = std::min(u+bridge_len, num_vertices-1);

            // For each group of vertices, considers as many paths of power-of-2
            // length as possible and for each of them creates a new edge
            // between its source and target vertices.
            // For instance, consider bridge_len = 4 and num_vertices = 9, and assume u = 0 (so you are in the first group of vertices [0, 4]):
            // - Creates edges of "length" 1: (0,1), (1,2), (2,3), (3,4)
            // - Creates edges of "length" 2: (0,2), (2,4); the triples of edge (0,2) are obtained by composing triples of edge (0,1) with those of edge (1,2); similar operation are performed for obtaining triples of edge (2,4).
            // - Creates edges of "length" 4: (0,4); the triples of edge (0,4) are obtained by composing triples of edge (0,2) with those of edge (2,4).
            // Note, path of length 1 are always created (provided the graph has at
            // least 2 vertices)

            for (std::size_t step = 1;
                 step <= bridge_len && (u+step) <= v && (step == 1 || (u+bridge_len) < num_vertices);
                 ++step)
            {
                // Creates edges (u,u+1) and (u,u+bridge_len)

                for (auto x = u; (x+step) <= v; x += step)
                {
                    // Creates edge (x,w), where w=x+step

                    auto const w = x + step;

#ifdef PPQTN_DEBUG
log_stream() << "======> U: " << u << ", V: " << v << ", X: " << x << ", W: " << w << ", STEP: " << step << std::endl;
#endif // PPQTN_DEBUG
                    punt triples = nullptr;
                    if (step > 1)
                    {
                        // Compose edge (x, x+step/2) with edge (x+step/2, x+step)
                        auto const y = x + (step - 1);
#ifdef PPQTN_DEBUG
log_stream() << "COMPOSING: (" << x << ", " << y << ") @ (" << y << ", " << w << ")" << std::endl;
#endif // PPQTN_DEBUG
                        triples = compose((*p_graph)(x, y), (*p_graph)(y, w));

                        if (limit_composed_constraint_size
                            && triples != nullptr
                            && triples[0].dist >= 0
                            && static_cast<std::size_t>(triples[0].dist) > max_num_triples)
                        {
                            auto const nt = nt_distr(rng); // Constraint size

                            punt new_triples = new terne[nt+1];
                            if (new_triples == nullptr)
                            {
                                fatal_syserror("Unable to allocate memory for constraint");
                            }

                            // Copies the nt middle triples from `triples` into `new_triples`

                            auto const nt_cut = triples[0].dist-nt;
                            auto const start_idx = 1 + nt_cut/2;
                            auto const stop_idx = start_idx + nt;
                            double prob_sum = 0;
                            double pref_sum = 0;
                            bool have_prob = triples[0].is_prob_undef() ? false : true;
                            bool have_pref = triples[0].is_pref_undef() ? false : true;
                            for (std::size_t i = start_idx; i < stop_idx; ++i)
                            {
                                new_triples[i-start_idx+1] = triples[i];
                                if (have_prob)
                                {
                                    prob_sum += triples[i].prob;
                                }
                                if (have_pref)
                                {
                                    pref_sum += triples[i].pref;
                                }
                            }
                            if (have_prob || have_pref)
                            {
                                for (std::size_t i = 1; i <= nt; ++i)
                                {
                                    if (have_prob)
                                    {
                                        new_triples[i].prob /= prob_sum;
                                    }
                                    if (have_pref)
                                    {
                                        new_triples[i].pref /= pref_sum;
                                    }
                                }
                            }

                            new_triples[0].dist = nt;
                            new_triples[0].prob = triples[0].prob;
                            new_triples[0].pref = triples[0].pref;

                            delete[] triples;

                            triples = new_triples;
                        }
                    }
                    else
                    {
                        // Generates a costraints made of a random number nt of
                        // triples with consecutive distances, that is:
                        //   <d_1,p_1,q_1>, <d_2,p_2,q_2>, ..., <d_nt, p_nt, q_nt>
                        // where d_k, p_k, q_k denote the distance, the probability and the
                        // preference associated to the i-th triple of the constraints.
                        // Specifically, d_k = b + k, where b is randomly generated as
                        // b=Unif(mindist,maxdist)-nt/2 so that generated distance will be
                        // centered in the generated value Unif(mindist,maxdist).
                        // Note, if nt is 1, the resulting triple will always have
                        // no probabilities and no preferences.

                        auto const nt = nt_distr(rng); // Constraint size
                        auto const have_prob = (nt > 1) ? have_prob_distr(rng) : false; // Have probabilities?
                        auto const have_pref = (nt > 1) ? have_pref_distr(rng) : false; // Have preferences?
                        auto const dist_base = dist_distr(rng); // Base value for distances

                        // Creates the constraint

                        triples = new terne[nt+1];
                        if (triples == nullptr)
                        {
                            fatal_syserror("Unable to allocate memory for constraint");
                        }
                        // Fills the descriptor
                        triples[0].dist = nt;
                        triples[0].prob = have_prob ? constants::def_prob : constants::undef_prob;
                        triples[0].pref = have_pref ? constants::def_pref : constants::undef_pref;
                        // Fill the triples
                        double prob_sum = 0; // Normalization constant for probabilities
                        double pref_sum = 0; // Normalization constant for preferences
                        for (std::size_t k = 1; k <= nt; ++k)
                        {
                            triples[k].dist = dist_base + (k-1);
                            triples[k].prob = have_prob ? prob_distr(rng) : constants::undef_prob;
                            triples[k].pref = have_pref ? pref_distr(rng) : constants::undef_pref;
                            if (have_prob)
                            {
                                prob_sum += triples[k].prob;
                            }
                            if (have_pref)
                            {
                                pref_sum += triples[k].pref;
                            }
                        }
                        // Normalizes probabilities and preferences (if needed)
                        if (have_prob || have_pref)
                        {
                            for (std::size_t k = 1; k <= nt; ++k)
                            {
                                if (have_prob)
                                {
                                    triples[k].prob /= prob_sum;
                                }
                                if (have_pref)
                                {
                                    triples[k].pref /= pref_sum;
                                }
                            }
                        }
                    }

                    // Creates a new edge and attaches the constraint
#ifdef PPQTN_DEBUG
log_stream() << "ADD EDGE(" << x << ", " << w << "): " << triples << std::endl;
#endif // PPQTN_DEBUG
                    //delete[] (*p_graph)(x, w);
                    //(*p_graph)(x, w) = triples;
                    if (triples != nullptr)
                    {
                        auto const nt = triples[0].dist;
                        p_graph->add_edge(x, w, triples+1, triples+nt+1);
                        delete[] triples;
                    }
                    else
                    {
                        p_graph->add_edge(x, w);
                    }

                    // Removes intermediate edges
                    if (step > 2)
                    {
                        //auto y = x + (step >> 1);
                        auto y = x + (step - 1);
#ifdef PPQTN_DEBUG
log_stream() << "REMOVE EDGE(" << x << ", " << y << "): " << (*p_graph)(x,y) << std::endl;
//log_stream() << "REMOVE EDGE(" << y << ", " << w << "): " << p_graph(y,w) << std::endl;
#endif // PPQTN_DEBUG
                        p_graph->remove_edge(x, y);
                        //p_graph->remove_edge(y, w);
                    }
                }
            }
        }
    }

    return p_graph;
}
#endif //[/FIXME]: still some problems

//[FIXME]: need to fix cases when bridge len is not a power of two.
#if 0
template <typename GeneratorT>
std::unique_ptr<graph_t> generate_random_linear_graph(GeneratorT& rng, std::size_t num_vertices, std::size_t bridge_len, std::size_t min_num_triples, std::size_t max_num_triples, std::size_t min_dist, std::size_t max_dist, bool limit_composed_constraint_size)
{
    // Check arguments
    if (min_dist == 0)
    {
        throw std::invalid_argument("Distance cannot be zero");
    }
    if (min_dist > max_dist)
    {
        throw std::invalid_argument("Mininum distance cannot be greater than maximum distance");
    }
    if (min_num_triples > max_num_triples)
    {
        throw std::invalid_argument("Mininum number of triples cannot be greater than maximum number of triples");
    }

    std::unique_ptr<graph_t> p_graph(new graph_t(num_vertices));

    if (num_vertices > 0)
    {
        std::uniform_int_distribution<std::size_t> nt_distr(min_num_triples, max_num_triples); // Probability distribution for generating the number of triples in a constraint
        std::uniform_int_distribution<int> dist_distr(min_dist, max_dist); // Probability distribution for generating temporal distances
        std::bernoulli_distribution have_prob_distr; // Probability distribution for deciding if a constraint has probabilities or not
        std::bernoulli_distribution have_pref_distr; // Probability distribution for deciding if a constraint has preferences or not
        std::uniform_real_distribution<double> prob_distr; // Probability distribution for generating probabilities
        std::uniform_real_distribution<double> pref_distr; // Probability distribution for generating preferences

        for (std::size_t u = 0; u < (num_vertices-1); u += bridge_len)
        {
            // Iterates over each group of vertices [u, u+bridge_len]

            auto v = std::min(u+bridge_len, num_vertices-1);

            // For each group of vertices, considers as many paths of power-of-2
            // length as possible and for each of them creates a new edge
            // between its source and target vertices.
            // For instance, consider bridge_len = 4 and num_vertices = 9, and assume u = 0 (so you are in the first group of vertices [0, 4]):
            // - Creates edges of "length" 1: (0,1), (1,2), (2,3), (3,4)
            // - Creates edges of "length" 2: (0,2), (2,4); the triples of edge (0,2) are obtained by composing triples of edge (0,1) with those of edge (1,2); similar operation are performed for obtaining triples of edge (2,4).
            // - Creates edges of "length" 4: (0,4); the triples of edge (0,4) are obtained by composing triples of edge (0,2) with those of edge (2,4).
            // Note, path of length 1 are always created (provided the graph has at
            // least 2 vertices)

            for (std::size_t step = 1;
                 step <= bridge_len && (u+step) <= v && (step == 1 || (u+bridge_len) < num_vertices);
                 step *= 2)
            {
                // Creates edges (u,u+1) and (u,u+bridge_len)

                for (auto x = u; (x+step) <= v; x += step)
                {
                    // Creates edge (x,w), where w=x+step

                    auto const w = x + step;

#ifdef PPQTN_DEBUG
log_stream() << "======> U: " << u << ", V: " << v << ", X: " << x << ", W: " << w << std::endl;
#endif // PPQTN_DEBUG
                    punt triples = nullptr;
                    if (step > 1)
                    {
                        // Compose edge (x, x+step/2) with edge (x+step/2, x+step)
                        auto const y = x + (step >> 1);
                        triples = compose((*p_graph)(x, y), (*p_graph)(y, w));

                        if (limit_composed_constraint_size
                            && triples != nullptr
                            && triples[0].dist >= 0
                            && static_cast<std::size_t>(triples[0].dist) > max_num_triples)
                        {
                            auto const nt = nt_distr(rng); // Constraint size

                            punt new_triples = new terne[nt+1];
                            if (new_triples == nullptr)
                            {
                                fatal_syserror("Unable to allocate memory for constraint");
                            }

                            // Copies the nt middle triples from `triples` into `new_triples`

                            auto const nt_cut = triples[0].dist-nt;
                            auto const start_idx = 1 + nt_cut/2;
                            auto const stop_idx = start_idx + nt;
                            double prob_sum = 0;
                            double pref_sum = 0;
                            bool have_prob = triples[0].is_prob_undef() ? false : true;
                            bool have_pref = triples[0].is_pref_undef() ? false : true;
                            for (std::size_t i = start_idx; i < stop_idx; ++i)
                            {
                                new_triples[i-start_idx+1] = triples[i];
                                if (have_prob)
                                {
                                    prob_sum += triples[i].prob;
                                }
                                if (have_pref)
                                {
                                    pref_sum += triples[i].pref;
                                }
                            }
                            if (have_prob || have_pref)
                            {
                                for (std::size_t i = 1; i <= nt; ++i)
                                {
                                    if (have_prob)
                                    {
                                        new_triples[i].prob /= prob_sum;
                                    }
                                    if (have_pref)
                                    {
                                        new_triples[i].pref /= pref_sum;
                                    }
                                }
                            }

                            new_triples[0].dist = nt;
                            new_triples[0].prob = triples[0].prob;
                            new_triples[0].pref = triples[0].pref;

                            delete[] triples;

                            triples = new_triples;
                        }
                    }
                    else
                    {
                        // Generates a costraints made of a random number nt of
                        // triples with consecutive distances, that is:
                        //   <d_1,p_1,q_1>, <d_2,p_2,q_2>, ..., <d_nt, p_nt, q_nt>
                        // where d_k, p_k, q_k denote the distance, the probability and the
                        // preference associated to the i-th triple of the constraints.
                        // Specifically, d_k = b + k, where b is randomly generated as
                        // b=Unif(mindist,maxdist)-nt/2 so that generated distance will be
                        // centered in the generated value Unif(mindist,maxdist).
                        // Note, if nt is 1, the resulting triple will always have
                        // no probabilities and no preferences.

                        auto const nt = nt_distr(rng); // Constraint size
                        auto const have_prob = (nt > 1) ? have_prob_distr(rng) : false; // Have probabilities?
                        auto const have_pref = (nt > 1) ? have_pref_distr(rng) : false; // Have preferences?
                        auto const dist_base = dist_distr(rng); // Base value for distances

                        // Creates the constraint

                        triples = new terne[nt+1];
                        if (triples == nullptr)
                        {
                            fatal_syserror("Unable to allocate memory for constraint");
                        }
                        // Fills the descriptor
                        triples[0].dist = nt;
                        triples[0].prob = have_prob ? constants::def_prob : constants::undef_prob;
                        triples[0].pref = have_pref ? constants::def_pref : constants::undef_pref;
                        // Fill the triples
                        double prob_sum = 0; // Normalization constant for probabilities
                        double pref_sum = 0; // Normalization constant for preferences
                        for (std::size_t k = 1; k <= nt; ++k)
                        {
                            triples[k].dist = dist_base + (k-1);
                            triples[k].prob = have_prob ? prob_distr(rng) : constants::undef_prob;
                            triples[k].pref = have_pref ? pref_distr(rng) : constants::undef_pref;
                            if (have_prob)
                            {
                                prob_sum += triples[k].prob;
                            }
                            if (have_pref)
                            {
                                pref_sum += triples[k].pref;
                            }
                        }
                        // Normalizes probabilities and preferences (if needed)
                        if (have_prob || have_pref)
                        {
                            for (std::size_t k = 1; k <= nt; ++k)
                            {
                                if (have_prob)
                                {
                                    triples[k].prob /= prob_sum;
                                }
                                if (have_pref)
                                {
                                    triples[k].pref /= pref_sum;
                                }
                            }
                        }
                    }

                    // Creates a new edge and attaches the constraint
#ifdef PPQTN_DEBUG
log_stream() << "EDGE(" << x << ", " << w << "): " << triples << std::endl;
#endif // PPQTN_DEBUG
                    //delete[] (*p_graph)(x, w);
                    //(*p_graph)(x, w) = triples;
                    if (triples != nullptr)
                    {
                        auto const nt = triples[0].dist;
                        p_graph->add_edge(x, w, triples+1, triples+nt+1);
                        delete[] triples;
                    }
                    else
                    {
                        p_graph->add_edge(x, w);
                    }

                    // Removes intermediate edges
                    if (step > 2)
                    {
                        auto y = x + (step >> 1);
                        p_graph->remove_edge(x, y);
                        p_graph->remove_edge(y, w);
                    }
                }
            }
        }
    }

    return p_graph;
}
#endif //[/FIXME]: need to fix cases when bridge len is not a power of two.

#if 0
template <typename GeneratorT>
std::unique_ptr<graph_t> generate_random_linear_graph(GeneratorT& rng, std::size_t num_vertices, std::size_t bridge_len, std::size_t max_num_triples, std::size_t min_dist, std::size_t max_dist)
{
    std::unique_ptr<graph_t> p_graph(new graph_t(num_vertices));

    std::uniform_int_distribution<std::size_t> nt_distr(1, max_num_triples); // Probability distribution for generating the number of triples in a constraint
    std::uniform_int_distribution<int> dist_distr(min_dist, max_dist); // Probability distribution for generating temporal distances
    std::bernoulli_distribution have_prob_distr; // Probability distribution for deciding if a constraint has probabilities or not
    std::bernoulli_distribution have_pref_distr; // Probability distribution for deciding if a constraint has preferences or not
    std::uniform_real_distribution<double> prob_distr; // Probability distribution for generating probabilities
    std::uniform_real_distribution<double> pref_distr; // Probability distribution for generating preferences


    // Connects each vertex v with its successor (v+1) to generate a linear graph
    for (std::size_t v = 0; v < (num_vertices-1); ++v)
    {
        // Generate a costraints with nt triples with consecutive distances,
        // that is:
        //   <d_1,p_1,q_1>, <d_2,p_2,q_2>, ..., <d_nt, p_nt, q_nt>
        // where d_k, p_k, q_k denote the distance, the probability and the
        // preference associated to the i-th triple of the constraints.
        // Specifically, d_k = b + k, where b is randomly generated as
        // b=Unif(mindist,maxdist)-nt/2 so that generated distance will be
        // centered in the generated value Unif(mindist,maxdist).

        auto const nt = nt_distr(rng); // Constraint size
        auto const have_prob = have_prob_distr(rng); // Have probabilities?
        auto const have_pref = have_pref_distr(rng); // Have preferences?
        auto const dist_max = dist_distr(rng); // Max distance in this constraint
        auto const dist_base = dist_max - nt/2; // Base value for distances

        // Creates the constraint
        std::vector<terne> triples(nt);
        double prob_sum = 0; // Normalization constant for probabilities
        double pref_sum = 0; // Normalization constant for preferences
        //double pref_sum = 0; // Normalization constant for preferences
        for (std::size_t k = 0; k < nt; ++k)
        {
            triples[k].dist = dist_base + k;
            triples[k].prob = have_prob ? prob_distr(rng) : constants::undef_prob;
            triples[k].pref = have_pref ? pref_distr(rng) : constants::undef_pref;
            if (have_prob)
            {
                prob_sum += triples[k].prob;
            }
            if (have_pref)
            {
                pref_sum += triples[k].pref;
            }
        }
        // Normalizes probabilities and preferences (if needed)
        if (have_prob || have_pref)
        {
            for (std::size_t k = 0; k < nt; ++k)
            {
                if (have_prob)
                {
                    triples[k].prob /= prob_sum;
                }
                if (have_pref)
                {
                    triples[k].pref /= pref_sum;
                }
            }
        }
        // Creates a new edge and attaches the above constraint
        p_graph->connect(v, v+1, triples.begin(), triples.end());
    }

    // Generates bridges (i.e., edges between non-adjacent vertices)
    // To assure consistency, bridge constraints are obtained by composition
    for (std::size_t v = 0; (v+bridge_len) < num_vertices; ++v)
    {
        // Creates the following linkgs:
        // - (v,v+2), (v+1,v+3), ...
        // - (v,v+3), (v+1,v+4), ...
        // ...
        // - (v,
    }

    return p_graph;
}
#endif


//////////////////////////////////
// RANDOM_LINEAR_GRAPH_GENERATOR_T
//////////////////////////////////


template <typename GeneratorT>
random_linear_graph_generator_t<GeneratorT>::random_linear_graph_generator_t(std::shared_ptr<GeneratorT> p_rng, std::size_t num_vertices, std::size_t bridge_len, std::size_t min_num_triples, std::size_t max_num_triples, std::size_t min_dist, std::size_t max_dist, bool limit_composed_constraint_size)
: p_rng_(p_rng),
  num_vertices_(num_vertices),
  bridge_len_(bridge_len),
  min_num_triples_(min_num_triples),
  max_num_triples_(max_num_triples),
  min_dist_(min_dist),
  max_dist_(max_dist),
  limit_composed_constraint_size_(limit_composed_constraint_size)
{
    assert( p_rng_ );
}

template <typename GeneratorT>
std::unique_ptr<graph_t> random_linear_graph_generator_t<GeneratorT>::operator()()
{
    return generate_random_linear_graph(*p_rng_,
                                        num_vertices_,
                                        bridge_len_,
                                        min_num_triples_,
                                        max_num_triples_,
                                        min_dist_,
                                        max_dist_,
                                        limit_composed_constraint_size_);
}


#endif // GRAPH_GENERATOR_IPP
