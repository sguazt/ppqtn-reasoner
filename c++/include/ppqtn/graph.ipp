/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file graph.ipp
 *
 * \brief Template definitions.
 *
 * \author Antonella Andolina
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PPQTN_GRAPH_IPP
#define PPQTN_GRAPH_IPP

#include <cassert>
#include <cstddef>
#include <iterator>
#include <stdexcept>

#if PPQTN_DEBUG
#include <iostream>
#include <ppqtn/graph_io.hpp>
#endif // PPQTN_DEBUG


template <typename IterT>
void graph_t::add_edge(std::size_t v1, std::size_t v2, IterT first_triple, IterT last_triple)
{
    if (v1 >= d_ || v2 >= d_)
    {
        throw std::invalid_argument("Invalid vertex/vertices");
    }

    add_edge(mat_, d_, v1, v2, true, first_triple, last_triple);
}

template <typename IterT>
void graph_t::add_edge(punt** mat, std::size_t d, std::size_t r, std::size_t c, bool both_dir, IterT first_triple, IterT last_triple)
{
    assert( mat );
    assert( r < d );
    assert( c < d );

    // Remove old contents (if any)
    if (mat[r][c] != nullptr)
    {
        delete[] mat[r][c];
    }
    if (both_dir && mat[c][r] != nullptr)
    {
        delete[] mat[c][r];
    }

    // Add new contents

    auto nt = std::distance(first_triple, last_triple);
    if (nt < 0)
    {
        throw std::logic_error("Bad temporal constraint to associate to an edge");
    }
    else if (nt == 0 && r == c)
    {
        // Diagonal elements have always a temporal constraint with a single triple
        nt = 1;
    }

    // Allocate memory
    mat[r][c] = new terne[nt+1];
    if (mat[r][c] == nullptr)
    {
        if (r == c)
        {
            throw_syserror("Unable to allocate memory for a diagonal element of the adjacency matrix");
        }
        else
        {
            throw_syserror("Unable to allocate memory for a non-diagonal element of the adjacency matrix");
        }
    }
    if (both_dir && r != c)
    {
        mat[c][r] = new terne[nt+1];
        if (mat[c][r] == nullptr)
        {
            throw_syserror("Unable to allocate memory for a non-diagonal element of the adjacency matrix");
        }
    }

    if (r != c)
    {
        // Initialize the descriptor
        mat[r][c][0].dist = nt;
        mat[r][c][0].prob = constants::undef_prob;
        mat[r][c][0].pref = constants::undef_pref;
        if (both_dir)
        {
            mat[c][r][0].dist = nt;
            mat[c][r][0].prob = constants::undef_prob;
            mat[c][r][0].pref = constants::undef_pref;
        }

        // Fill the temporal constraint
        bool first_check = true;
        bool have_probs = false;
        bool have_prefs = false;
        std::size_t i = 1;
        while (first_triple != last_triple)
        {
            assert( i <= static_cast<std::size_t>(nt) );

            // Check the current triple for validity and consistency wrt already stored triples
            if (!first_triple->is_valid())
            {
                throw std::logic_error("Creating an edge with an invalid triple");
            }
            if (first_check)
            {
                first_check = false;
                have_probs = !first_triple->is_prob_undef();
                have_prefs = !first_triple->is_pref_undef();

                mat[r][c][0].prob = have_probs ? constants::def_prob : constants::undef_prob;
                mat[r][c][0].pref = have_prefs ? constants::def_pref : constants::undef_pref;
                if (both_dir)
                {
                    mat[c][r][0].prob = mat[r][c][0].prob;
                    mat[c][r][0].pref = mat[r][c][0].pref;
                }
            }
            else
            {
                if ((first_triple->is_prob_undef() && have_probs)
                    || (!first_triple->is_prob_undef() && !have_probs))
                {
                    throw std::logic_error("Cannot mix triples with probabilities with triples without probabilities");
                }
                if ((first_triple->is_pref_undef() && have_prefs)
                    || (!first_triple->is_pref_undef() && !have_prefs))
                {
                    throw std::logic_error("Cannot mix triples with preferences with triples without preferences");
                }
            }

            mat[r][c][i] = *first_triple;
            if (both_dir)
            {
                mat[c][r][i].dist = - mat[r][c][i].dist;
                mat[c][r][i].prob = mat[r][c][i].prob;
                mat[c][r][i].pref = mat[r][c][i].pref;
            }
            ++i;
            ++first_triple;
        }
    }
    else
    {
        // Fill the descriptor
        mat[r][r][0].dist = 1;
        mat[r][r][0].prob = 1;
        mat[r][r][0].pref = 1;

        // Fill the temporal constraint
        mat[r][r][1].dist = 0;
        mat[r][r][1].prob = 1;
        mat[r][r][1].pref = 1;
    }
}


#endif // PPQTN_GRAPH_IPP
