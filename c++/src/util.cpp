/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm> 
#include <cctype>
#include <cerrno>
#include <fstream>
#include <iostream>
//#include <locale>
#include <ppqtn/util.hpp>
#include <stdexcept>
#include <string>


std::string read_file(const char* filename)
{
    std::ifstream ifs(filename, std::ios::in | std::ios::binary);
    if (ifs)
    {
        std::string contents;
        ifs.seekg(0, std::ios::end);
        contents.resize(ifs.tellg());
        ifs.seekg(0, std::ios::beg);
        ifs.read(&contents[0], contents.size());
        ifs.close();

        return(contents);
    }
    throw std::system_error(errno, std::generic_category());
}

std::string read_file(const std::string& filename)
{
    return read_file(filename.c_str());
}

// trim from start (in place)
void ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
void rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
void trim(std::string& s)
{
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
std::string ltrim_copy(std::string s)
{
    ltrim(s);
    return s;
}

// trim from end (copying)
std::string rtrim_copy(std::string s)
{
    rtrim(s);
    return s;
}

// trim from both ends (copying)
std::string trim_copy(std::string s)
{
    trim(s);
    return s;
}

void to_lower(std::string& s)
{
    //NOTE: the behavior of std::tolower is undefined if the argument's value is
    //      neither representable as unsigned char nor equal to EOF.
    //      To use these functions safely with plain chars (or signed chars),
    //      the argument should first be converted to unsigned char.
    //      See: https://en.cppreference.com/w/cpp/string/byte/tolower
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c){ return std::tolower(c); });
}

std::string to_lower_copy(std::string s)
{
    to_lower(s);
    return s;
}

void to_upper(std::string& s)
{
    //NOTE: the behavior of std::toupper is undefined if the argument's value is
    //      neither representable as unsigned char nor equal to EOF.
    //      To use these functions safely with plain chars (or signed chars),
    //      the argument should first be converted to unsigned char.
    //      See: https://en.cppreference.com/w/cpp/string/byte/toupper
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c){ return std::toupper(c); });
}

std::string to_upper_copy(std::string s)
{
    to_upper(s);
    return s;
}
