/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#include <memory>
//#include <ppqtn/disjoint_set.hpp>
//#include <ppqtn/graph.hpp>
#include <ppqtn/graph_generator.hpp>
//#include <random>
//#include <vector>


/*TODO
std::unique_ptr<graph_t> generate_random_graph(unsigned long seed, std::size_t nv, std::size_t ne)
{
    const std::size_t max_ne = nv*(nv-1)/2;

    std::unique_ptr<graph_t> p_graph(new graph_t(nv));

    std::default_random_engine rng(seed); //FIXME: pass RNG as parameter

    if (ne >= max_ne)
    {
        // Generates a complete graph
        for (std::size_t u = 0; u < nv; ++u)
        {
            for (std::size_t v = 0; v < nv; ++v)
            {
                if (u != v)
                {
                    p_graph->connect(u, v);
                }
            }
        }
    }
    else
    {
        // Generate a sparse graph
        std::uniform_int_distribution<std::size_t> vertex_distr(0, nv-1); // Probability distribution for generating `nt`s
        disjoint_set_t reachability_set(nv);

        std::size_t edge_count = 0;
        while (edge_count < ne)
        {
            auto u = vertex_distr(rng);
            auto v = vertex_distr(rng);
            if (u != v && !p_graph->connected(u, v))
            {
                std::vector<terne> triples;
                if (reachability_set.connected(u, v))
                {
                    // Generates constraints by composition of constraints on the
                    // path u -> ... -> v
                    compose(
                }
                else
                {
                    // Generate a free constraint
                }
                p_graph->connect(u, v);
                ++edge_count;
                reachability_set.union_set(u, v);
            }
        }
    }

    return p_graph;
}
*/

//XXX: deprecated
#if 0
std::unique_ptr<graph_t> random_linear_graph(unsigned long seed, std::size_t nv)
{
    const std::size_t max_nt = 20; // Max number of triples in a constraint
    const int min_dist = 0;
    const int max_dist = 100;

    std::unique_ptr<graph_t> p_graph(new graph_t(nv));

    std::default_random_engine rng(seed); //FIXME: pass RNG as parameter
    std::uniform_int_distribution<std::size_t> nt_distr(1, max_nt); // Probability distribution for generating `nt`s
    std::uniform_int_distribution<int> dist_distr(min_dist, max_dist); // Probability distribution for generating distances
    std::uniform_real_distribution<double> have_prob_distr; // Probability distribution for deciding if a constraint has probabilities or not
    std::uniform_real_distribution<double> have_pref_distr; // Probability distribution for deciding if a constraint has preferences or not
    std::uniform_real_distribution<double> prob_distr; // Probability distribution for generating probabilities
    std::uniform_real_distribution<double> pref_distr; // Probability distribution for generating preferences

    // Connects each vertex with its successor to generate a linear graph
    for (std::size_t r = 0; r < (nv-1); ++r)
    {
        // Generate a costraints with nt triples with consecutive distances,
        // that is:
        //   <d_1,p_1,q_1>, <d_2,p_2,q_2>, ..., <d_nt, p_nt, q_nt>
        // where d_k, p_k, q_k denote the distance, the probability and the
        // preference associated to the i-th triple of the constraints.
        // Specifically, d_k = b + k, where b is randomly generated as
        // b=Unif(mindist,maxdist)-nt/2 so that generated distance will be
        // centered in the generated value Unif(mindist,maxdist).

        auto const nt = nt_distr(rng); // Constraint size
        auto const have_prob = have_prob_distr(rng) <= 0.5 ? true : false; // Have probabilities?
        auto const have_pref = have_pref_distr(rng) <= 0.5 ? true : false; // Have preferences?
        auto const dist_max = dist_distr(rng); // Max distance in this constraint
        auto const dist_base = dist_max - nt/2; // Base value for distances

        // Creates the constraint
        std::vector<terne> triples(nt);
        double prob_sum = 0; // Normalization constant for probabilities
        double pref_sum = 0; // Normalization constant for preferences
        //double pref_sum = 0; // Normalization constant for preferences
        for (std::size_t k = 0; k < nt; ++k)
        {
            triples[k].dist = dist_base + k;
            triples[k].prob = have_prob ? prob_distr(rng) : constants::undef_prob;
            triples[k].pref = have_pref ? pref_distr(rng) : constants::undef_pref;
            if (have_prob)
            {
                prob_sum += triples[k].prob;
            }
            if (have_pref)
            {
                pref_sum += triples[k].pref;
            }
        }
        // Normalizes probabilities and preferences (if needed)
        if (have_prob || have_pref)
        {
            for (std::size_t k = 0; k < nt; ++k)
            {
                if (have_prob)
                {
                    triples[k].prob /= prob_sum;
                }
                if (have_pref)
                {
                    triples[k].pref /= pref_sum;
                }
            }
        }
        // Creates a new edge and attaches the above constraint
        p_graph->connect(r, r+1, triples.begin(), triples.end());
    }

    return p_graph;
}
#endif


/////////////////////////////
// IDENTITY_GRAPH_GENERATOR_T
/////////////////////////////

identity_graph_generator_t::identity_graph_generator_t(const graph_t& graph)
: graph_(graph)
{
    // Empty
}

std::unique_ptr<graph_t> identity_graph_generator_t::operator()()
{
    return std::unique_ptr<graph_t>(new graph_t(graph_));
}
