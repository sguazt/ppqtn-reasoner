/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <iterator>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_io_util.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/util.hpp>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#ifdef PPQTN_DEBUG
# include <iostream>
# include <sstream>
#endif // PPQTN_DEBUG


parsed_constraint_t parse_constraint(const std::string& text)
{
    // Expected format:
    //   X <(i,r|%,r|#), ..., (i,r|%,r|#)> Y
    // where:
    // - X is the left time point
    // - (i,r|%,r|#) is a triple where 'i' is an integer number, 'r' is a real number, '%' denotes an undefined probability, and '#' denotes an undefined preference
    // - Y is the left time point

    //FIXME: be more specific for characters allowed in a time point identifier

    std::regex re(R"(\s*([^\s<]+)\s*<([^>]*)>\s*((?:[^\s]|$)+))");

    auto cons_begin = std::sregex_iterator(text.begin(), text.end(), re);
    auto cons_end = std::sregex_iterator();

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "Found " << std::distance(cons_begin, cons_end) << " constraints\n";
    log_debug(oss.str());
}
#endif // PPQTN_DEBUG

    parsed_constraint_t parsed_constraint;

    //// Reserves a slot for triples descriptor
    //triples.resize(1);

    // Fills triples
    //bool have_prob = true;
    //bool have_pref = true;
    for (std::sregex_iterator it = cons_begin; it != cons_end; ++it)
    {
        std::smatch match = *it;

        // We expect 4 matches:
        // - the first one is the whole matched string,
        // - the second one is the left time point,
        // - the third one is the list of triples
        // - the fourth one is the right time point.
        if (match.size() == 4)
        {
            std::istringstream iss;

            iss.str(match[1]);
            iss >> parsed_constraint.left_time_point;
            trim(parsed_constraint.left_time_point);

            parsed_constraint.triples = parse_constraint_triples(match[2]);

            iss.clear();
            iss.str(match[3]);
            iss >> parsed_constraint.right_time_point;
            trim(parsed_constraint.right_time_point);

#ifdef PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "Matched Constraint -> left time point: " << match[1] << ", triples: " << match[2] << ", right time point: " << match[3];
            log_debug(oss.str());

            oss.clear();
            oss.str("");
            oss << "Built Constraint -> left time point: " << parsed_constraint.left_time_point << ", triples (" << parsed_constraint.triples.size() << "): <";
            for (std::size_t i = 0; i < parsed_constraint.triples.size(); ++i)
            {
                oss << ", (" << parsed_constraint.triples[i].dist << ", " << parsed_constraint.triples[i].prob << ", " << parsed_constraint.triples[i].pref << ")";
            }
            oss << ">, right time point: " << parsed_constraint.right_time_point;
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG
        }
        else
        {
            std::ostringstream oss;
            oss << "Unexpected constraint format in '" << match.str() << "' (match size: " << match.size() << "): skip";
            log_warn(oss.str());
        }
    }

    //return std::make_tuple(left_time_point, parsed_triples, right_time_point);
    return parsed_constraint;
}

std::vector<terne> parse_constraint_triples(const std::string& text)
{
    std::vector<terne> parsed_triples;

    // Expected format:
    //   (i,r|%,r|#), ..., (i,r|%,r|#),
    // where: 'i' is an integer number, 'r' is a real number, '%' denotes an undefined probability, and '#' denotes an undefined preference

    std::regex re(R"(\(\s*([+-]?\d+)\s*,\s*((?:[+-]?(?:\.\d+)|(?:\d+(?:\.\d*)?))|%)\s*,\s*((?:[+-]?(?:\.\d+)|(?:\d+(?:\.\d*)?))|#)\s*\)\s*,?)");

    auto triples_begin = std::sregex_iterator(text.begin(), text.end(), re);
    auto triples_end = std::sregex_iterator();

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "Found " << std::distance(triples_begin, triples_end) << " triples\n";
    log_debug(oss.str());
}
#endif // PPQTN_DEBUG

    //// Reserves a slot for triples descriptor
    //triples.resize(1);

    // Fills triples
    //bool have_prob = true;
    //bool have_pref = true;
    for (std::sregex_iterator it = triples_begin; it != triples_end; ++it)
    {
        std::smatch match = *it;

        // We expect 4 matches:
        // - the first one is the whole matched string,
        // - the second one is the first grouping (i.e., the time distance),
        // - the third one is the second grouping (i.e., the probability),
        // - the fourth one is the third grouping (i.e., the preference).
        if (match.size() == 4)
        {
            terne t;

            std::istringstream iss;

            iss.str(match[1]);
            iss >> t.dist;

            if (match[2] == constants::undef_prob_symbol)
            {
                t.prob = constants::undef_prob;
                //have_prob = false;
            }
            else
            {
                iss.clear();
                iss.str(match[2]);
                iss >> t.prob;
                if (t.prob < 0)
                {
                    t.prob = constants::undef_prob;
                    //have_prob = false;
                }
            }

            if (match[3] == constants::undef_pref_symbol)
            {
                t.pref = constants::undef_pref;
                //have_pref = false;
            }
            else
            {
                iss.clear();
                iss.str(match[3]);
                iss >> t.pref;
                if (t.pref < 0)
                {
                    t.pref = constants::undef_pref;
                    //have_pref = false;
                }
            }
#ifdef PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "Matched Triple -> distance: " << match[1] << ", prob: " << match[2] << ", pref: " << match[3];
            log_debug(oss.str());

            oss.clear();
            oss.str("");
            oss << "Built Triple -> distance: " << t.dist << ", prob: " << t.prob << ", pref: " << t.pref;
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            parsed_triples.push_back(t);
        }
        else
        {
            std::ostringstream oss;
            oss << "Unexpected triple format in '" << match.str() << "': skip";
            log_warn(oss.str());
        }
    }

    return parsed_triples;
}
