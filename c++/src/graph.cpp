/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <ppqtn/error.hpp>
#include <ppqtn/graph.hpp>
#include <stdexcept>


// TERNE -----------------------------------------------------------------------


terne::terne()
: dist(0),
  prob(constants::undef_prob),
  pref(constants::undef_pref)
{
}

terne::terne(const terne& t)
: dist(t.dist),
  prob(t.prob),
  pref(t.pref)
{
}

terne& terne::operator=(const terne& other)
{
    if (this != &other)
    {
        dist = other.dist;
        prob = other.prob;
        pref = other.pref;
    }
    return *this;
}

bool terne::is_prob_undef() const
{
    // Don't use equality because prob is a real number
    return prob <= constants::undef_prob;
}

bool terne::is_pref_undef() const
{
    // Don't use equality because pref is a real number
    return pref <= constants::undef_pref;
}

bool terne::is_valid() const
{
    // NOTE: no check on the time distance since any integer value
    //       (e.g., a negative value) is a valid value
    return  (is_prob_undef() || (prob >= 0 && prob <= 1))
            && (is_pref_undef() || (pref >= 0 && pref <= 1));
}


// GRAPH -----------------------------------------------------------------------


graph_t::graph_t()
: d_(0),
  mat_(nullptr),
  own_mat_(true)
{
}

graph_t::graph_t(std::size_t n)
: d_(n),
  mat_(nullptr),
  own_mat_(true)
{
    mat_ = new punt*[d_];
    if (mat_ == nullptr)
    {
        throw_syserror("Unable to allocate memory for the adjacency matrix");
    }
    for (std::size_t r = 0; r < d_; ++r)
    {
        mat_[r] = new punt[d_];
        if (mat_[r] == nullptr)
        {
            throw_syserror("Unable to allocate memory for a row of the adjacency matrix");
        }
        std::fill(mat_[r], mat_[r]+d_, nullptr);
    }
    for (std::size_t r = 0; r < d_; ++r)
    {
        for (std::size_t c = r; c < d_; ++c)
        {
            add_edge(mat_, d_, r, c, true);
        }
    }
}

graph_t::graph_t(const graph_t& g)
: d_(g.d_),
  mat_(nullptr),
  own_mat_(g.own_mat_)
{
    if (own_mat_)
    {
        // Creates the rows
        mat_ = new punt*[d_];
        if (mat_ == nullptr)
        {
            throw_syserror("Unable to allocate memory for the adjacency matrix");
        }
        // Creates the columns and fill
        for (std::size_t r = 0; r < d_; ++r)
        {
            mat_[r] = new punt[d_];
            if (mat_[r] == nullptr)
            {
                throw_syserror("Unable to allocate memory for a row of the adjacency matrix");
            }
            std::fill(mat_[r], mat_[r]+d_, nullptr);
        }
        for (std::size_t r = 0; r < d_; ++r)
        {
            for (std::size_t c = r; c < d_; ++c)
            {
                if (g.mat_[r][c] != nullptr && g.mat_[r][c][0].dist > 0)
                {
                    auto nt = g.mat_[r][c][0].dist;
                    add_edge(mat_, d_, r, c, true, g.mat_[r][c]+1, g.mat_[r][c]+nt+1);
                }
                else
                {
                    // Creates an edge with an empty constraint
                    add_edge(mat_, d_, r, c, true);
                }
            }
        }
    }
    else
    {
        mat_ = g.mat_;
    }
}

graph_t::graph_t(punt** adj_mat, std::size_t n, bool copy)
: d_(n),
  mat_(nullptr),
  own_mat_(copy)
{
    if (own_mat_)
    {
        mat_ = new punt*[d_];
        if (mat_ == nullptr)
        {
            throw_syserror("Unable to allocate memory for the adjacency matrix");
        }
        for (std::size_t r = 0; r < d_; ++r)
        {
            mat_[r] = new punt[d_];
            if (mat_[r] == nullptr)
            {
                throw_syserror("Unable to allocate memory for a row of the adjacency matrix");
            }
            std::fill(mat_[r], mat_[r]+d_, nullptr);
        }
        for (std::size_t r = 0; r < d_; ++r)
        {
            for (std::size_t c = r; c < d_; ++c)
            {
                if (adj_mat[r][c] != nullptr && adj_mat[r][c][0].dist > 0)
                {
                    auto nt = adj_mat[r][c][0].dist;
                    add_edge(mat_, d_, r, c, true, adj_mat[r][c]+1, adj_mat[r][c]+nt+1);
                }
                else
                {
                    add_edge(mat_, d_, r, c, true);
                }
            }
        }
    }
    else
    {
        mat_ = adj_mat;
    }
}

graph_t::~graph_t()
{
    if (own_mat_)
    {
        destroy_matrix(mat_, d_);
    }
}

graph_t& graph_t::operator=(const graph_t& other)
{
    if (&other != this)
    {
        // Deallocate memory for the old adjacency matrix (if needed)
        if (own_mat_)
        {
            destroy_matrix(mat_, d_);
            mat_ = nullptr;
            d_ = 0;
        }

        // Copy the new adjacency matrix
        d_ = other.d_;
        own_mat_ = other.own_mat_;
        if (other.own_mat_)
        {
            if (other.mat_ != nullptr)
            {
                mat_ = new punt*[d_];
                if (mat_ == nullptr)
                {
                    throw_syserror("Unable to allocate memory for the adjacency matrix");
                }
                for (std::size_t r = 0; r < d_; ++r)
                {
                    mat_[r] = new punt[d_];
                    if (mat_[r] == nullptr)
                    {
                        throw_syserror("Unable to allocate memory for a row of the adjacency matrix");
                    }
                    std::fill(mat_[r], mat_[r]+d_, nullptr);
                }
                for (std::size_t r = 0; r < d_; ++r)
                {
                    for (std::size_t c = r; c < d_; ++c)
                    {
                        if (other.mat_[r][c])
                        {
                            auto nt = other.mat_[r][c][0].dist;
                            add_edge(mat_, d_, r, c, true, other.mat_[r][c]+1, other.mat_[r][c]+nt+1);
                        }
                        else
                        {
                            mat_[r][c] = mat_[c][r] = nullptr;
                        }
                    }
                }
            }
        }
        else
        {
            mat_ = other.mat_;
        }
    }

    return *this;
}

punt& graph_t::operator()(std::size_t row, std::size_t col)
{
    if (row >= d_)
    {
        throw std::invalid_argument("Row index is out-of-bound");
    }
    if (col >= d_)
    {
        throw std::invalid_argument("Column index is out-of-bound");
    }

    return mat_[row][col];
}

const punt& graph_t::operator()(std::size_t row, std::size_t col) const
{
    if (row >= d_)
    {
        throw std::invalid_argument("Row index is out-of-bound");
    }
    if (col >= d_)
    {
        throw std::invalid_argument("Column index is out-of-bound");
    }

    return mat_[row][col];
}

void graph_t::add_edge(std::size_t v1, std::size_t v2)
{
    if (v1 >= d_ || v2 >= d_)
    {
        throw std::invalid_argument("Invalid vertex/vertices");
    }

    add_edge(mat_, d_, v1, v2, true);
}

void graph_t::add_edge(punt** mat, std::size_t d, std::size_t r, std::size_t c, bool both_dir)
{
    const terne* dummy = nullptr;

    add_edge(mat, d, r, c, both_dir, dummy, dummy);
}

punt** graph_t::adjacency_matrix()
{
    return mat_;
}

punt* const* graph_t::adjacency_matrix() const
{
    return mat_;
}

void graph_t::destroy_matrix(punt** mat, std::size_t d)
{
    if (mat != nullptr && d > 0)
    {
        for (std::size_t r = 0; r < d; ++r)
        {
            for (std::size_t c = 0; c < d; ++c)
            {
                if (mat[r][c] != nullptr)
                {
                    delete[] mat[r][c];
                }
            }
            delete[] mat[r];
        }
        delete[] mat;
    }
}

bool graph_t::empty() const
{
    return d_ == 0;
}

bool graph_t::has_edge(std::size_t v1, std::size_t v2) const
{
    if (v1 >= d_ || v2 >= d_)
    {
        throw std::invalid_argument("Invalid vertex/vertices)");
    }

    return has_edge(mat_, d_, v1, v2);
}

bool graph_t::has_edge(punt** mat, std::size_t d, std::size_t r, std::size_t c)
{
    assert( mat );
    assert( r < d );
    assert( c < d );

    // Two vertices are connected if both their corresponding cell of the
    // adjacent matrix is not null and does not contain an empty constraint
    //FIXME: what about self-loop (i.e., r == c)?
    return mat[r][c] != nullptr && mat[r][c][0].dist != 0;
}

void graph_t::num_vertices(std::size_t n)
{
    if (n != d_)
    {
        // Reallocate the adjacency matrix, set default values and do memory clean-up

        // Since in C++ there is no memory allocation operator equivalent to C's
        // realloc, we need to create a fresh new memory buffer where to store the resized matrix
        punt** new_mat = new punt*[n];
        if (new_mat == nullptr)
        {
            throw_syserror("Unable to allocate memory for the adjacency matrix");
        }
        for (std::size_t r = 0; r < n; ++r)
        {
            new_mat[r] = new punt[n];
            if (new_mat[r] == nullptr)
            {
                throw_syserror("Unable to allocate memory for a row of the adjacency matrix");
            }
            std::fill(new_mat[r], new_mat[r]+n, nullptr);
        }
        for (std::size_t r = 0; r < n; ++r)
        {
            for (std::size_t c = r; c < n; ++c)
            {
                if (c < d_)
                {
                    // Copy contents for cells must be preserved (i.e., mat_[r][c], for r,c < min(d_, n))
                    // NOTE: instead of calling create_edge() we reuse already
                    //       allocated memory by copying the pointer to triples
                    //       stored in mat_[r][c] into new_mat[r][c]
                    new_mat[r][c] = mat_[r][c];
                    new_mat[c][r] = mat_[c][r];
                }
                else
                {
                    // Create default contents for new cells (i.e.,  mat_[r][c], for r,c >= min(d_, n))
                    add_edge(new_mat, n, r, c, true);
                }
            }
        }

        if (mat_ != nullptr)
        {
            // Clean-up memory allocated for cells that must not be preserved (i.e., mat_[r][c], for r,c >= n)
            if (d_ > n)
            {
                for (std::size_t r = n; r < d_; ++r)
                {
                    for (std::size_t c = n; c < d_; ++c)
                    {
                        if (mat_[r][c])
                        {
                            delete[] mat_[r][c];
                        }
                    }
                    delete[] mat_[r];
                }
            }
            // Clean-up memory allocated for the old adjacency matrix.
            // NOTE: cell contents must not be deleted because it has been
            //       copied into the new adjacency matrix
            for (std::size_t r = 0; r < std::min(d_, n); ++r)
            {
                delete[] mat_[r];
            }
            delete[] mat_;
        }

        // Set the adjacency matrix to point to the just created memory buffer
        mat_ = new_mat;
        d_ = n;
    }
}

std::size_t graph_t::num_vertices() const
{
    return d_;
}

std::vector<terne> graph_t::remove_edge(std::size_t v1, std::size_t v2)
{
    std::vector<terne> ret;

    punt p_triples = remove_edge(mat_, d_, v1, v2, true);
    if (p_triples != nullptr)
    {
        auto nt = p_triples[0].dist;
        ret.assign(p_triples+1, p_triples+nt+1);
        delete[] p_triples;
    }

    return ret;
}

punt graph_t::remove_edge(punt** mat, std::size_t d, std::size_t r, std::size_t c, bool both_dir)
{
    assert( mat );
    assert( r < d );
    assert( c < d );

    punt ret = nullptr;

    if (r != c && has_edge(mat, d, r, c))
    {
        ret = mat[r][c];

        mat[r][c] = new terne[1];
        if (mat[r][c] == nullptr)
        {
            throw_syserror("Unable to allocate memory for an element of the adjacency matrix");
        }
        mat[r][c][0].dist = 0;
        mat[r][c][0].prob = constants::undef_prob;
        mat[r][c][0].prob = constants::undef_prob;

        if (both_dir)
        {
            delete[] mat[c][r];

            mat[c][r] = new terne[1];
            if (mat[c][r] == nullptr)
            {
                throw_syserror("Unable to allocate memory for an element of the adjacency matrix");
            }
            mat[c][r][0].dist = 0;
            mat[c][r][0].prob = constants::undef_prob;
            mat[c][r][0].prob = constants::undef_prob;
        }
    }

    return ret;
}

std::size_t graph_t::vertex_degree(std::size_t v) const
{
    if (v >= d_)
    {
        throw std::invalid_argument("Invalid vertex)");
    }

    std::size_t count = 0;

    for (std::size_t c = 0; c < d_; ++c)
    {
        if (v != c && has_edge(v, c))
        {
            ++count;
        }
    }

    return count;
}
