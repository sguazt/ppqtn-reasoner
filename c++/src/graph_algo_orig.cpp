/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <iostream>
#include <ppqtn/graph.hpp>


namespace {

bool	Floyd(punt** mat, int r_max/*, int c_max*/);									//algoritmo di Foyd Warshall
double	CalcolaCompoProb(punt p, punt q,  int i, int j);				//calcola la probabilità di un elemento nella composizione
double	CalcolaCompoPref(punt p, punt q, int i, int j);					//calcola la preferenza di un elemento nella composizione
double	CalcolaIntersectProb(punt p, punt q, punt ris, int i, int j);	//calcola la probabilità di un elemento nell'intersezione
double	CalcolaIntersectPref(punt p, punt q, punt ris, int i, int j);	//calcola la preferenza di un elemento nell'intersezione
void	ComponiDistanze (punt p, punt q, punt ris);						//operazione di composizione di due vettori di vincoli
bool	Intersect(punt** mat, int i, int j, punt q);								//operazione di intersezione dei vincoli della matrice
punt	Normalizza (int dim, punt p);									//normalizza le probabilita' del vettore intersezione

} // Unnamed namespace


bool floyd_warshall_orig(graph_t& graph)
{
    return Floyd(graph.adjacency_matrix(), graph.num_vertices());
}


namespace {

using namespace std;


double CalcolaCompoProb(punt p, punt q,  int i, int j)
{
	//Calcola il valore della probabilità di un elemento nella composizione


	double nuovo_contributo;

	if (p[0].prob == 1 && q[0].prob == 1)	//ci sono le probabilità in entrambi i vincoli
		nuovo_contributo = p[i].prob * q[j].prob;
	else
		if (p[0].prob == -1 && q[0].prob == -1)		//non c'è la probabilità in nessuno dei due vincoli
			nuovo_contributo = (1.0/p[0].dist) * (1.0/q[0].dist);
		else

			//c'è la probabilità in un solo vincolo
			if (p[0].prob == 1)		//cè solo la probabilità nel primo vincolo
				nuovo_contributo = p[i].prob / q[0].dist;

			else	//c'è la probabilità solo nel secondo
				nuovo_contributo = q[j].prob / p[0].dist;


	return nuovo_contributo;
}



double CalcolaCompoPref(punt p, punt q, int i, int j)
{
	//Calcola il valore della preferenza di un elemento nella composizione
	double nuovo_contributo;

	if (p[0].pref == 1 && q[0].pref == 1)	//ci sono le preferenze in entrambi i vincoli
		nuovo_contributo = min(p[i].pref, q[j].pref);
	else
		if (p[0].pref == -1 && q[0].pref == -1)		//non c'è la preferenza in nessuno dei due vincoli
			nuovo_contributo = p[i].pref;
		else

			//c'è la preferenza in un solo vincolo
			if (p[0].pref == 1)		//cè solo la preferenza nel primo vincolo
				nuovo_contributo = p[i].pref;

			else	//c'è la probabilità solo nel secondo
				nuovo_contributo = q[j].pref;


	return nuovo_contributo;
}
 


void ComponiDistanze(punt p, punt q, punt ris)
{
	// operazione di composizione di due vettori di vincoli


	int i;		//indice del primo vettore
	int j;		//indice del secondo vettore
	int dim1;	//dimensione del primo vettore
	int dim2;	//dimensione del secondo vettore
	int pos;	//indice della nuova posizione
	terne nuovo[1];	//elemento ausiliare per costruire il risultato
	int inizio;		//primo valore delle distanze composte

	if ((p[0].dist == 0) || (q[0].dist == 0))   //uno dei due vettori e' vuoto: risultato vuoto {elemento che azzera la composizione}
	{
		ris[0].dist= 0;
		ris[0].prob = -1;
		ris[0].pref = -1;
	}
	else
	{			//entrambi i vettori pieni

		dim1 = p[0].dist;
		dim2 = q[0].dist;

		ris[0].prob = 1;	//se entrambi i vincoli ci sono la composizione dà sempre probabilità

		if (p[0].pref == -1 && q[0].pref == -1)			//manca la preferenza in entrambi i vincoli
			ris[0].pref = -1;							//anche il risultato non ha preferenze
		else											//se almeno uno dei due vincoli ha le preferenze
			ris[0].pref = 1;							//il risultato ha la preferenza

		
		inizio = p[1].dist + q[1].dist;		//calcolo primo valore delle distanze composte

		//inizializzazione del vettore risultato

			// DA QUI IN AVANTI VIENE FATTA L'IPOTESI CHE I VETTORI SIANO CONTINUI E ORDINATI 
			// SE CAMBIA L'IPOTESI SI DEVE COSTRUIRE IL RISULTATO IN MANIERA DIFFERENTE:
			// SOMMARE OGNI ELEMENTO DEL PRIMO VETTORE CON TUTTI GLI ELEMENTI DEL SECONDO VETTORE,  
			// PER OGNI SOMMA SI DEVE FARE LA COMPOSIZIONE OTTENENDO NUOVO 
			// E POI CERCARE NUOVO[0].DIST IN TUTTO IL VETTORE RIS
			// SE VIENE TROVATO SI AGGIORNANO PREFERENZE E PROBABILITA'
			// SE NON VIENE TROVATO SI INCREMENTA PIENO DI 1 E A RIS[PIENO].DIST SI ASSEGNA IL VALORE DI NUOVO[0].DIST 
			// E A RIS[PIENO].PROB SI ASSEGNA IL VALORE DI NUOVO[0].PROB E A RIS[PIENO].PREF SI ASSEGNA IL VALORE DI NUOVO[0].PREF
			// NEL CASO DI NUOVA IPOTESI GUARDARE LA SOLUZIONE USATA NEL PRIMO ARTICOLO E ADATTARLA ALLE PREFERENZE!!!!

		if (ris[0].pref == -1)		//il risultato non ha preferenze
		{ 
			//inserisco nel vettore risultato tutte le distanze della composizione
			//inizializzando la probabilità a zero e la preferenza a null;

			for (i=1; i<= ris[0].dist ; i++)
			{
				ris[i].dist = inizio +i -1;
				ris[i].prob = 0;
			}
		}
		else		//il risultato ha preferenze
		{
			///inserisco nel vettore risultato tutte le distanze della composizione
			//inizializzando la probabilità a zero e la preferenza a 0 (elemento neutro di somma e max);
			for (i=1; i<= ris[0].dist ; i++)
			{
				ris[i].dist = inizio + i -1;
				ris[i].prob = 0;
				ris[i].pref = 0;
			}
		}
		for (i=1; i<=dim1; i++)
			for (j=1; j<= dim2; j++)
			{
				nuovo[0].dist = p[i].dist + q[j].dist;
				nuovo[0].prob = CalcolaCompoProb(p,q, i, j);  
				nuovo[0].pref = CalcolaCompoPref(p,q,i,j);

			  
			//							ATTENZIONE!!!!
			// LE OPERAZIONI SEGUENTI SI BASANO SULL'IPOTESI CHE I VETTORI SIANO CONTINUI E ORDINATI! 
			// SE CAMBIA QUESTA IPOTESI SI DEVE MODIFICARE IL PROCEDIMENTO:
			// INVECE DI CONTROLLARE CHE IL VALORE SUL QUALE STIAMO AGENDO SIA QUELLO CHE CI ASPETTIAMO
			// DOBBIAMO CERCARE NUOVO[0].DIST IN TUTTO IL VETTORE RIS
			// SE VIENE TROVATO SI SOMMA LA PROBABILITA' PRECEDENTE CON IL NUOVO CONTRIBUTO 
			// E SI FA IL MASSIMO DELLA PRECEDENTE PREFERENZA CON IL NUOVO CONTRIBUTO,
			// SE NON VIENE TROVATO SI AGGIUNGE UN NUOVO VALORE A RIS (CHIAMIAMOLO PIENO) E
			// A RIS[PIENO].DIST SI ASSEGNA IL VALORE DI NUOVO[0].DIST, 
			// A RIS[PIENO].PROB SI ASSEGNA IL VALORE DI NUOVO[0].PROB E 
			// A RIS[PIENO].PREF SI ASSEGNA IL VALORE DI NUOVO[0].PREF
		

				pos = i+j-1;
				if 	(ris[pos].dist!= nuovo[0].dist)   //la distanza da trattare è diversa da quella che ci aspettiamo
				{
					cout << "c'è qualcosa che non va! Distanza nella COMPOSIZIONE non corrispondente";
					//system ("pause");
				}

				else
				{
					ris[pos].prob = ris[pos].prob + nuovo[0].prob;		//somma del precedente valore con il nuovo contributo
					ris[pos].pref = max(ris[pos].pref, nuovo[0].pref);	//massimo tra il valore precedente e il nuovo contributo
				}
				
			}
	}
}



bool Floyd(punt** mat, int r_max/*, int c_max*/)
{
    bool consistente = true;

	//Algoritmo di Floyd Warshall

	int k;
	int i;
	int j;
	int dim;
	punt compo;
	punt p;
	punt q;
	

	compo = new terne[1];
	for (k=0; k<r_max; k++)
	{
		for (i=0; i<r_max; i++)
		{
			for(j=0; j< r_max; j++)
			{
				if ((i !=j ) && (i!=k) && (j!=k))
				{
					//cout << "\n\n composizione nodi " << i << "-"<< k << "  " << k <<"-"<< j <<"\n";
  					p= mat[i][k];
					q= mat[k][j];
					delete []compo;			//cancello il vecchio vettore composizione																	
					if ((p[0].dist != 0) && (q[0].dist != 0))	//se entrambe i vettori sono pieni il vettore compo si ottiene dalla composizione
					{
						dim =  (p[p[0].dist].dist + q[q[0].dist].dist) - (p[1].dist + q[1].dist) +1;	// valuta la dimensione del vettore composizione come
																										// somma dei massimi - somma dei minimi +1
						compo = new terne[dim+1];	//creo il nuovo vettore compo
						if (compo != NULL)
						{
							compo[0].dist = dim;
							ComponiDistanze (p,q,compo); //composizione di Cik e Ckj;
						}
						else
						{
							cout << "\nGenerazione Vettore non riuscita 6";
							//system ("pause");
						}
					}
					else			// uno dei due vettori e' infinito {elemento che annulla la composizione} il risulato della composizione e' infinito
					{
						compo= new terne[1];
						if (compo != NULL)
						{
							compo[0].dist=0;
							compo[0].prob = -1;
							compo[0].pref = -1;
						}
						else
						{
							cout << "\nGenerazione Vettore non riuscita 7";
							//system ("pause");
						}
					}
					consistente = Intersect (mat, i,j, compo);  //intersezione di Cij con la composizione di Cik Ckj;       ****
					if (mat[i][j] == NULL)  //grafo inconsistente; 
					{
						cout <<"\n 111 IL GRAFO E' INCONSISTENTE\n";
						//system ("pause");
						consistente= false;
					}
				}
			}
		}
	}

    return consistente;
}


double CalcolaIntersectProb(punt p, punt q, punt ris, int i, int j)
{
	//Calcola il valore della probabilità di un elemento nell'intersezione

	double nuovo_contributo;

	if (p[0].prob == 1 && q[0].prob == 1)	//ci sono le probabilità in entrambi i vincoli
		nuovo_contributo = p[i].prob * q[j].prob;
	else
		if (p[0].prob == -1 && q[0].prob == -1)		//non c'è la probabilità in nessuno dei due vincoli
			nuovo_contributo = (1.0/ris[0].dist);
		else

			//c'è la probabilità in un solo vincolo
			if (p[0].prob == 1)		//cè solo la probabilità nel primo vincolo
				nuovo_contributo = p[i].prob / ris[0].dist;

			else	//c'è la probabilità solo nel secondo
				nuovo_contributo = q[j].prob / ris[0].dist;

	return nuovo_contributo;
}



double CalcolaIntersectPref(punt p, punt q, punt ris, int i, int j)
{
	//Calcola il valore della preferenza di un elemento nell'intersezione

	double nuovo_contributo;

	if (p[0].pref == 1 && q[0].pref == 1)	//ci sono le preferenze in entrambi i vincoli
		nuovo_contributo = min(p[i].pref, q[j].pref);
	else
		if (p[0].pref == -1 && q[0].pref == -1)		//non c'è la preferenza in nessuno dei due vincoli
			nuovo_contributo = p[i].pref;
		else

			//c'è la preferenza in un solo vincolo
			if (p[0].pref == 1)		//cè solo la preferenza nel primo vincolo
				nuovo_contributo = p[i].pref;

			else	//c'è la probabilità solo nel secondo
				nuovo_contributo = q[j].pref;

	return nuovo_contributo;
}
 


bool Intersect(punt** mat, int i, int j, punt q)
{
    bool consistente = true;

	//							ATTENZIONE!!!!
	// QUALSIASI EVENTUALE MODIFICA DEVE ESSERE RIPORTATA, MAGARI AGGIUSTANDOLA,
	// ANCHE NELLA PROCEDURA jUSTINTERSECT TENENDO CONTO CHE IN JUST INTERSECT SI 
	// LAVORA SOLO CON DUE VINCOLI FUORI MATRICE MENTRE NELLA INTERSECT SI FA 
	// L'INTERSEZIONE ALL'INTERNO DELLA MATRICE


	int h;		//indice del primo vettore
	int k;		//indice del secondo vettore
	int dim1;	//dimensione del primo vettore
	int dim2;	//dimensione del secondovettore
	int dim;	//dimensione del vettore risultato
	int inizio;	//primo valore nell'intersezione
	int fine;	//ultimo valore nell'intersezione
	int cont;	//indice per scorrere il nuovo vettore intersezione
	//int min;	//dimensione del vettore risultato (pari alla dimensione minima dei vettori)
	//int rimuovi; // variabile per eliminare gli elementi che non servono


	punt ris;	//vettore risultato
	punt p;		//puntatore ausiliario

	p= mat[i][j];
	dim1 = p[0].dist;
	dim2 = q[0].dist;
	if ((dim1 == 0) && (dim2 ==0))		//vettori entrambi vuoti(infinito): intersezione vuota (infinito)
	{
		//cout<<"\n entrambi vuoti";
		if (mat[i][j][0].dist != 0)		//se il vettore vecchio non e' infinito 
		{								//cancella il vettore vecchio e ne crea uno nuovo infinito; 
										//altrimenti lascia il vecchio vettore infinito
		
			delete []mat[i][j];			//cancella il vettore vecchio
			mat[i][j] = new terne[1];	//crea il nuovo vettore infinito
			if (mat[i][j] != NULL)
			{
				mat[i][j][0].dist = 0;
				mat[i][j][0].prob = -1;
				mat[i][j][0].pref = -1;
			}
			else
			{
				cout << "\nGenerazione Vettore non riuscita 8";
				//system ("pause");
			}
		}
		ris = mat[i][j];
	}
	else 
		if (dim1 == 0)	//primo vettore vuoto ma secondo pieno: intersezione uguale al secondo vettore
		{
			//cout << "\nMat["<<i<<"]["<<j <<"] vuoto e compo pieno";
			delete []mat[i][j];				//cancello vecchio vettore
			mat[i][j] = new terne[dim2];	//creo il nuovo vettore
			p= mat[i][j];
			if (mat[i][j]!= NULL)
			{
				for (k=0; k<=dim2; k++)		//copio il secondo vettore nel primo
				{
					mat[i][j][k].dist = q[k].dist;
					mat[i][j][k].prob = q[k].prob;
					mat[i][j][k].pref = q[k].pref;
				}
				ris=mat[i][j];
			}
			else
			{
				cout << "\nGenerazione Vettore non riuscita 9";
				//system ("pause");
			}
		}
		else
			if (dim2 == 0)	//secondo vettore vuoto ma primo pieno: intersezione uguale al primo vettore - il risultato e' il vecchio vettore
			{
				//cout << "\nMat["<<i<<"]["<<j<<"] pieno e compo vuoto";
				ris = mat[i][j];
			}
			else   // entrambe i vettori pieni: intersezione data dagli elementi comuni 
			{
				
				 p= mat[i][j];
				 
				 //quanto segue si basa sulla supposizione che i vettori siano ordinati e contigui
				 //se si modifica questa ipotesi occorre modificare la costruzione del risultato:
				 //per ogni elemento del primo vettore si deve fare una ricerca sul secondo: se viene trovato si inserisce
				 //calcolando le probabilita' e le preferenze se non viene trovato si passa oltre;
				 
				 if ((p[dim1].dist < q[1].dist) || (q[dim2].dist < p[1].dist))	//i due vettori non si sovrapongono: intersezione vuota 
				 {																//il grafo dei vincoli e' inconsistente
					 ris = NULL;  
					 cout << "\n 222 Il Grafo e' inconsistente\n";
					 consistente= false;
					 //system("pause");
				 }
				 else // i due vettori si sovrappongono: intersezione elementi comuni
				 {
					inizio = max(p[1].dist, q[1].dist);			//si calcola il primo valore dell'intersezione
					fine = min (p[dim1].dist, q[dim2].dist);	//si calcola l'ultimo valore dell'intersezione
					dim = fine - inizio +1;		//si calcola la dimensione del vettore risultato con i valori comuni

					ris = new terne[dim+1];		//creo il vettore separato intersezione
					if (ris != NULL)
					{
						ris[0].dist = dim;
						ris[0].prob = 1;		//se c'è l'intersezione ci sono le probabilita' per ogni caso
						if (p[0].pref == -1 && q[0].pref == -1) //non ci sono preferenze in nessuno dei due vincoli
							ris[0].pref = -1;   //il risultato non ha preferenze
						else
							ris[0].pref = 1;	//altrimenti il risultato ha preferenze

						k=1;
						h=1;
						if (p[1].dist < q[1].dist)   //il primo vettore inizia prima del secondo
					 		while (p[k].dist < q[1].dist)	//finche' i due valori sui due vettori non sono uguali si scorre sul primo vettore
								 k++;
						else 
							while (q[h].dist < p[1].dist)	//finche' i due valori sui due vettori non sono uguali si scorre sul secondo vettore
								 h++;

						//si calcola il valore più piccolo in comune e il valore più grande in comune

						

						
						for (cont=0; cont<dim; cont++)
						{
							if (p[k].dist != q[h].dist || p[k].dist != inizio +cont)  //se almeno uno dei due valori nei vettori è diverso dal valore atteso
								cout << "Qualcosa non funziona!";
							else
							{
								
								ris[cont+1].dist = inizio+cont;
							//ris[pieno].prob = p[k].prob * q[h].prob;
								ris[cont+1].prob = CalcolaIntersectProb(p,q,ris,k,h);
								ris[cont+1].pref = CalcolaIntersectPref (p,q,ris,k,h);
								k++;
								h++;
							}
						}
					}
					else 
					{
						cout << "\nGenerazione Vettore non riuscita 10";
						//system ("pause");
					}


				 }
												 
			}
			if ((ris != mat[i][j]) && (ris != NULL))	//ho generato un vettore intersezione e questo non e' nullo: cancello il vecchio vettore,
												//normalizzo l'intersezione, creo il nuovo vettore e  copio l'intersezione normalizzata
			{
				//delete []mat[i][j];			//cancello il vecchio vettore
				if (ris[0].dist != 0)
					ris = Normalizza(ris[0].dist, ris);	//normalizzo le probabilta' del vettore itersezione
				dim1 = ris[0].dist;
				mat[i][j]= new terne[dim1];	//creo il nuovo vettore
				p=mat[i][j];
				if (mat[i][j] != NULL)
				{
					for (k=0; k<= dim1; k++)	//copio il vettore intersezione nel nuovo vettore
					{
						p[k].dist = ris[k].dist;
						p[k].prob = ris[k].prob;
						p[k].pref = ris[k].pref;
					}
					//cout <<"\nCancello ris";
					//delete []ris;
				}
				else 
				{
					cout <<"\n generazione errata";
					//system ("pause");
				}

			}

    return consistente;
}


punt  Normalizza (int dim, punt p)
{
	//normalizza le probabilità del vetore intersezione in modo che la loro somma dia 1

	double acc; //accumulatore delle probabilita'
	int i;

	acc=0;
	for (i=1; i<=dim; i++)
		acc = acc + p[i].prob;
	for (i=1; i<=dim; i++)
		p[i].prob = p[i].prob / acc;

	return p;
}

} // Unnamed namespace
