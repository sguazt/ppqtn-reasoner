/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cerrno>
#include <cstdlib>
#include <ppqtn/error.hpp>
#include <ppqtn/log.hpp>
#include <stdexcept>
#include <string>
#include <system_error>


void fatal_error(const std::string& msg)
{
    log_error(msg);

    std::abort();
}

void fatal_syserror(const std::string& msg)
{
    log_syserror(msg);

    std::terminate();
}

void throw_syserror(const std::string& msg)
{
    auto err_code = errno; // Save a copy of errno in case the call to log_error() changes it

    log_syserror(msg);

    throw std::system_error(err_code, std::generic_category());
}

