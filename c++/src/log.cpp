/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cerrno>
#include <cstring>
#include <iostream>
#include <ppqtn/log.hpp>
#include <sstream>
#include <string>

namespace {

const std::string ppqtn_log_tag = "ppqtn";

}

#ifdef PPQTN_DEBUG
void log_debug(const std::string& msg)
{
    std::clog << "[" << ppqtn_log_tag << "] (D) " << msg << '\n';
}
#else // PPQTN_DEBUG
# if __cplusplus >= 201703L
void log_debug([[maybe_unused]] const std::string& msg)
{
    // empty
}
# else
void log_debug(const std::string& msg)
{
    (void) msg; // suppress "unused variable" warning
}
# endif // __cplusplus
#endif // NDEBUG

void log_error(const std::string& msg)
{
    std::clog << "[" << ppqtn_log_tag << "] (E) " << msg << '\n';
}

void log_info(const std::string& msg)
{
    std::clog << "[" << ppqtn_log_tag << "] (I) " << msg << '\n';
}

std::ostream& log_stream()
{
    return std::clog;
}

void log_syserror(const std::string& msg)
{
    std::ostringstream oss;
    oss << msg << ": " << std::strerror(errno) << " (code: " << errno << ")";
    log_error(oss.str());
}

void log_warn(const std::string& msg)
{
    std::clog << "[" << ppqtn_log_tag << "] (W) " << msg << '\n';
}
