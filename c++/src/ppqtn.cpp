/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstddef>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <ppqtn/benchmark.hpp>
#include <ppqtn/error.hpp>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_algo.hpp>
#include <ppqtn/graph_algo_orig.hpp> //XXX
#include <ppqtn/graph_generator.hpp>
#include <ppqtn/graph_generator_orig.hpp> //XXX
#include <ppqtn/graph_io.hpp>
#include <ppqtn/graph_ui.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/util.hpp>
#include <ppqtn/version.hpp>
#include <random>
#include <sstream>
#include <utility>


namespace {

// File formats:
const std::string dot_file_format = "dot";
// Benchmark styles:
const std::string single_bench_style = "single"; // 'Single graph' benchmark style
const std::string multi_bench_style = "multi"; // 'Multiple graphs' benchmark style

// Default options:
const std::size_t default_bench_min_nreps = 3;
const std::size_t default_bench_max_nreps = std::numeric_limits<std::size_t>::max();
const std::string default_bench_style = multi_bench_style;
const std::string default_infmt = dot_file_format;
const bool default_outbidi = false;
const std::string default_outfmt = dot_file_format;
const bool default_rand = false;
const std::size_t default_rand_linear_brlen = 2;
const bool default_rand_linear_limit_conslen = false;
const std::size_t default_rand_linear_max_conslen = 10;
const std::size_t default_rand_linear_max_dist = 100;
const std::size_t default_rand_linear_min_conslen = 1;
const std::size_t default_rand_linear_min_dist = 1;
const std::size_t default_rand_linear_nverts = 10;
const unsigned long default_rand_seed = 5489U;
const bool default_ui = false;
const bool default_verbose = false;

// Prototypes:
void usage(const char* progname);
void print_exception(const std::exception& e);
void print_stacktrace(const std::exception& e, int level = 0);

//bool check_graph(const graph_t& g);

} // Unnamed namespace


int main(int argc, char* argv[])
{
    bool ok = true;
    std::string err_msg;

    // CLI options
    std::size_t opt_bench_max_nreps = default_bench_max_nreps;
    std::size_t opt_bench_min_nreps = default_bench_min_nreps;
    std::string opt_bench_style = default_bench_style;
    bool opt_help = false;
    std::string opt_infmt = default_infmt;
    std::string opt_infile;
    bool opt_outbidi = default_outbidi;
    std::string opt_outfile;
    std::string opt_outfmt = default_outfmt;
    bool opt_rand = default_rand;
    std::size_t opt_rand_linear_brlen = default_rand_linear_brlen;
    bool opt_rand_linear_limit_conslen = default_rand_linear_limit_conslen;
    std::size_t opt_rand_linear_max_conslen = default_rand_linear_max_conslen;
    std::size_t opt_rand_linear_max_dist = default_rand_linear_max_dist;
    std::size_t opt_rand_linear_min_conslen = default_rand_linear_min_conslen;
    std::size_t opt_rand_linear_min_dist = default_rand_linear_min_dist;
    std::size_t opt_rand_linear_nverts = default_rand_linear_nverts;
    unsigned long opt_rand_seed = default_rand_seed;
    bool opt_ui = default_ui;
    bool opt_verbose = default_verbose;

    // Splash screen
    std::cout << "P+PQTN reasoner v." << PPQTN_VERSION_STR << std::endl;

    // Parse CLI options

    for (int arg = 1; arg < argc; ++arg)
    {
        if (!std::strcmp(argv[arg], "--bench-max-nreps"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_bench_max_nreps;
            }
            else
            {
                err_msg = "Malformed '--bench-max-nreps' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--bench-min-nreps"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_bench_min_nreps;
            }
            else
            {
                err_msg = "Malformed '--bench-min-nreps' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--bench-style"))
        {
            ++arg;
            if (arg < argc)
            {
                opt_bench_style = argv[arg];
            }
            else
            {
                err_msg = "Malformed '--bench-style' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--help")
            || !std::strcmp(argv[arg], "-h"))
        {
            opt_help = true;
        }
        else if (!std::strcmp(argv[arg], "--infile")
                 || !std::strcmp(argv[arg], "-i"))
        {
            ++arg;
            if (arg < argc)
            {
                opt_infile = argv[arg];
            }
            else
            {
                err_msg = "Malformed '--infile' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--infmt"))
        {
            ++arg;
            if (arg < argc)
            {
                opt_infmt = argv[arg];
            }
            else
            {
                err_msg = "Malformed '--infmt' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--outbidi"))
        {
            opt_outbidi = true;
        }
        else if (!std::strcmp(argv[arg], "--outfile")
                 || !std::strcmp(argv[arg], "-o"))
        {
            ++arg;
            if (arg < argc)
            {
                opt_outfile = argv[arg];
            }
            else
            {
                err_msg = "Malformed '--outfile' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--outfmt"))
        {
            ++arg;
            if (arg < argc)
            {
                opt_outfmt = argv[arg];
            }
            else
            {
                err_msg = "Malformed '--outfmt' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--rand")
                 || !std::strcmp(argv[arg], "-r"))
        {
            opt_rand = true;
        }
        else if (!std::strcmp(argv[arg], "--rand-linear-brlen"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_linear_brlen;
            }
            else
            {
                err_msg = "Malformed '--rand-linear-brlen' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--rand-linear-limit-conslen"))
        {
            opt_rand_linear_limit_conslen = true;
        }
        else if (!std::strcmp(argv[arg], "--rand-linear-max-conslen"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_linear_max_conslen;
            }
            else
            {
                err_msg = "Malformed '--rand-linear-max-conslen' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--rand-linear-max-dist"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_linear_max_dist;
            }
            else
            {
                err_msg = "Malformed '--rand-linear-max-dist' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--rand-linear-min-conslen"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_linear_min_conslen;
            }
            else
            {
                err_msg = "Malformed '--rand-linear-min-conslen' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--rand-linear-min-dist"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_linear_min_dist;
            }
            else
            {
                err_msg = "Malformed '--rand-linear-min-dist' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--rand-linear-nverts"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_linear_nverts;
            }
            else
            {
                err_msg = "Malformed '--rand-linear-nverts' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--rand-seed"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_seed;
            }
            else
            {
                err_msg = "Malformed '--rand-seed' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--ui"))
        {
            opt_ui = true;
        }
        else if (!std::strcmp(argv[arg], "--verbose"))
        {
            opt_verbose = true;
        }
        else
        {
            err_msg = "Unknown options '" + std::string(argv[arg]) + "'";
            ok = false;
        }
    }

    // Checks CLI options
    if (!ok)
    {
        log_error(err_msg);
        usage(argv[0]);
        return 1;
    }

    if (opt_verbose)
    {
        log_stream() << "-- Options:\n"
                     << "- opt_bench_max_nreps: " << opt_bench_max_nreps << '\n'
                     << "- opt_bench_min_nreps: " << opt_bench_min_nreps << '\n'
                     << "- opt_bench_style: " << opt_bench_style << '\n'
                     << "- opt_help: " << std::boolalpha << opt_help << std::noboolalpha << '\n'
                     << "- opt_infmt: " << opt_infmt << '\n'
                     << "- opt_infile: " << opt_infile << '\n'
                     << "- opt_outbidi: " << std::boolalpha << opt_outbidi << std::noboolalpha << '\n'
                     << "- opt_outfile: " << opt_outfile << '\n'
                     << "- opt_outfmt: " << opt_outfmt << '\n'
                     << "- opt_rand: " << std::boolalpha << opt_rand << std::noboolalpha << '\n'
                     << "- opt_rand_linear_brlen: " << opt_rand_linear_brlen << '\n'
                     << "- opt_rand_linear_limit_conslen: " << std::boolalpha << opt_rand_linear_limit_conslen << std::noboolalpha << '\n'
                     << "- opt_rand_linear_max_conslen: " << opt_rand_linear_max_conslen << '\n'
                     << "- opt_rand_linear_max_dist: " << opt_rand_linear_max_dist << '\n'
                     << "- opt_rand_linear_min_conslen: " << opt_rand_linear_min_conslen << '\n'
                     << "- opt_rand_linear_min_dist: " << opt_rand_linear_min_dist << '\n'
                     << "- opt_rand_linear_nverts: " << opt_rand_linear_nverts << '\n'
                     << "- opt_rand_seed: " << opt_rand_seed << '\n'
                     << "- opt_ui: " << std::boolalpha << opt_ui << std::noboolalpha << '\n'
                     << "- opt_verbose: " << std::boolalpha << opt_verbose << std::noboolalpha << '\n'
                     << "--------------------------------------------------------------------------------\n"
                     << std::flush;
    }

    if (opt_help || (!opt_rand && !opt_ui && opt_infile.empty()))
    {
        usage(argv[0]);
        return 0;
    }

    try
    {
        if (opt_ui)
        {
            // Interactive input

            ui_params_t default_settings;
            default_settings.graph_input_file = opt_infile;
            default_settings.graph_input_file_format = opt_infmt;
            default_settings.graph_output_both_dir = opt_outbidi;
            default_settings.graph_output_file = opt_outfile;
            default_settings.graph_output_file_format = opt_outfmt;
            default_settings.rand_bridge_length = opt_rand_linear_brlen;
            default_settings.rand_constraint_length_bounds = std::make_pair(opt_rand_linear_min_conslen, opt_rand_linear_max_conslen);
            default_settings.rand_distance_bounds = std::make_pair(opt_rand_linear_min_dist, opt_rand_linear_max_dist);
            default_settings.rand_limit_constraint_length = opt_rand_linear_limit_conslen;
            default_settings.rand_num_vertices = opt_rand_linear_nverts;
            default_settings.rand_seed = opt_rand_seed;

            show_console_ui(default_settings);
        }
        else if (true)
        {
            std::unique_ptr<graph_generator_t> p_graph_gen;

            if (opt_bench_style == multi_bench_style)
            {
                //FIXME: currently, multi-graph benchmark style supports random graphs only

                p_graph_gen = std::unique_ptr<random_linear_graph_generator_t<std::default_random_engine>>(
                                    new random_linear_graph_generator_t<std::default_random_engine>(std::make_shared<std::default_random_engine>(opt_rand_seed),
                                                                                                    opt_rand_linear_nverts,
                                                                                                    opt_rand_linear_brlen,
                                                                                                    opt_rand_linear_min_conslen,
                                                                                                    opt_rand_linear_max_conslen,
                                                                                                    opt_rand_linear_min_dist,
                                                                                                    opt_rand_linear_max_dist,
                                                                                                    opt_rand_linear_limit_conslen));
            }
            else if (opt_bench_style == single_bench_style)
            {
                std::unique_ptr<graph_t> p_graph;
                graph_properties_t graph_props;

                if (opt_rand)
                {
                    // Generates a random graph

                    std::default_random_engine rng(opt_rand_seed);

                    //p_graph = random_linear_graph(5489U, 4);
                    //random_graph_orig(5489U, 4, graph);
                    //p_graph = generate_random_linear_graph(rng, 8, 4, 20, 1, 100);
                    p_graph = generate_random_linear_graph(rng,
                                                           opt_rand_linear_nverts,
                                                           opt_rand_linear_brlen,
                                                           opt_rand_linear_min_conslen,
                                                           opt_rand_linear_max_conslen,
                                                           opt_rand_linear_min_dist,
                                                           opt_rand_linear_max_dist,
                                                           opt_rand_linear_limit_conslen);
                }
                else if (!opt_infile.empty())
                {
                    // Read graph from file

                    std::string txt_graph = read_file(opt_infile);

                    if (opt_infmt == dot_file_format)
                    {
                        p_graph = read_dot_graph(txt_graph, graph_props);
                    }
                }
                else
                {
                    fatal_error("Unexpected input option");
                }

                if (opt_verbose)
                {
                    log_stream() << "Input graph: " << *p_graph << std::endl;
                }

                if (!check_graph(*p_graph))
                {
                    // Input graph is not consistent
                    log_error("Input graph is not consistent");
                    write_dot_graph(*p_graph, log_stream(), false, graph_props);
                    //write_dot_graph(*p_graph, log_stream(), true, graph_props);
                    return 0;
                }

                p_graph_gen = std::unique_ptr<identity_graph_generator_t>(new identity_graph_generator_t(*p_graph));
            }
            else
            {
                fatal_error("Unsupported benchmark style");
            }

            benchmark_t bench;
            bench.min_num_repetitions(opt_bench_min_nreps);
            bench.max_num_repetitions(opt_bench_max_nreps);
            bench.verbose(opt_verbose);

            auto res = bench.run(*p_graph_gen);

            if (opt_verbose)
            {
                auto const& cpu_time_stats = res.cpu_time_stats();
                auto const& real_time_stats = res.real_time_stats();

                log_stream() << "CPU time: " << cpu_time_stats.mean() << " (s.d. " << cpu_time_stats.standard_deviation() << ")" << '\n'
                             << "Real time: " << real_time_stats.mean() << " (s.d. " << real_time_stats.standard_deviation() << ")" << '\n'
                             << std::flush;

            }

            if (opt_outfile.empty())
            {
                log_stream() << "Output graph: " << res.out_graph() << std::endl;
            }
            else
            {
                std::ofstream ofs(opt_outfile.c_str());

                if (opt_outfmt == dot_file_format)
                {
                    write_dot_graph(res.out_graph(), ofs, opt_outbidi);
                }
                else
                {
                    ofs << res.out_graph() << '\n';
                }
            }
        }
        else
        {
            std::unique_ptr<graph_t> p_graph;
            graph_properties_t graph_props;

            if (opt_rand)
            {
                // Generates a random graph

                std::default_random_engine rng(opt_rand_seed);

                //p_graph = random_linear_graph(5489U, 4);
                //random_graph_orig(5489U, 4, graph);
                //p_graph = generate_random_linear_graph(rng, 8, 4, 20, 1, 100);
                p_graph = generate_random_linear_graph(rng,
                                                       opt_rand_linear_nverts,
                                                       opt_rand_linear_brlen,
                                                       opt_rand_linear_min_conslen,
                                                       opt_rand_linear_max_conslen,
                                                       opt_rand_linear_min_dist,
                                                       opt_rand_linear_max_dist,
                                                       opt_rand_linear_limit_conslen);
            }
            else if (!opt_infile.empty())
            {
                // Read graph from file

                std::string txt_graph = read_file(opt_infile);

                if (opt_infmt == dot_file_format)
                {
                    p_graph = read_dot_graph(txt_graph, graph_props);
                }
            }
            else
            {
                fatal_error("Unexpected input option");
            }

            if (opt_verbose)
            {
                log_stream() << "Input graph: " << *p_graph << std::endl;
            }

            if (check_graph(*p_graph))
            {
                fixed_graph_benchmark_t bench;
                bench.min_num_repetitions(opt_bench_min_nreps);
                bench.max_num_repetitions(opt_bench_max_nreps);
                bench.verbose(opt_verbose);
                auto res = bench.run(*p_graph);

                if (opt_verbose)
                {
                    auto const& cpu_time_stats = res.cpu_time_stats();
                    auto const& real_time_stats = res.real_time_stats();

                    log_stream() << "CPU time: " << cpu_time_stats.mean() << " (s.d. " << cpu_time_stats.standard_deviation() << ")" << '\n'
                                 << "Real time: " << real_time_stats.mean() << " (s.d. " << real_time_stats.standard_deviation() << ")" << '\n'
                                 << std::flush;

                }

                if (opt_outfile.empty())
                {
                    log_stream() << "Output graph: " << res.out_graph() << std::endl;
                }
                else
                {
                    std::ofstream ofs(opt_outfile.c_str());

                    if (opt_outfmt == dot_file_format)
                    {
                        write_dot_graph(res.out_graph(), ofs, opt_outbidi, graph_props);
                    }
                    else
                    {
                        ofs << res.out_graph() << '\n';
                    }
                }

//[XXX]: call original code
#if 0
                auto graph2 = *p_graph;
                std::clog << "Graph2: " << graph2 << '\n';
                std::clog << "Checking graph2...\n";
                check = check_graph(graph2);
                std::clog << "Graph2 checked: " << check << '\n';
                std::clog << "Calling Original Floyd-Warshall\n";
                auto ret_orig = floyd_warshall_orig(graph2);
                std::clog << "Original Floyd-Warshall returned => " << ret_orig << '\n';
                std::clog << "New Graph2: " << graph2 << '\n';
#endif
//[/XXX]: call original code
            }
            else
            {
                // Input graph is not consistent
                log_error("Input graph is not consistent");
                write_dot_graph(*p_graph, log_stream(), false, graph_props);
                //write_dot_graph(*p_graph, log_stream(), true, graph_props);
                return 0;
            }
        }
    }
    catch (const std::exception& e)
    {
        print_exception(e);
        std::terminate();
    }
    catch (...)
    {
        fatal_error("Unexpected error");
    }
}


namespace {

void usage(const char* progname)
{
    std::cout << "Usage: " << progname << " [options]\n"
              << "Options:\n"
              << "--bench-max-nreps <value>\n"
              << "  Executes the benchmark at most <value> times (even if the target relative precision has not been reached).\n"
              << "  [default: " << default_bench_max_nreps << "]\n"
              << "--bench-min-nreps <value>\n"
              << "  Executes the benchmark at least <value> times.\n"
              << "  [default: " << default_bench_min_nreps << "]\n"
              << "--bench-style <value>\n"
              << "  Executes the benchmark according to the given <style>.\n"
              << "  [default: " << default_bench_style << "]\n"
              << "-i <filename>\n"
              << "--infile <filename>\n"
              << "  Read the input graph from the file named <filename>\n"
              << "--infmt <format>\n"
                 "  Specify the format of the input graph file\n"
                 "  [default: " << default_infmt << "]\n"
              << "--outbidi\n"
              << "  Enables the inclusion in the output graph of edges in both direction (i.e.,\n"
              << "  for every pair <u,v> of vertices, the output graph will contain both (u,v) and (v,u) edges).\n"
              << "  [default: " << (default_outbidi ? "enabled" : "disabled") << "]\n"
              << "-o <filename>\n"
              << "--outfile <filename>\n"
              << "  Write the output graph to the file named <filename>\n"
              << "--outfmt <format>\n"
              << "  Specify the format of the output graph file\n"
              << "  [default: " << default_outfmt << "]\n"
              << "--rand\n"
              << "  Enables the random generation of the input graph.\n"
              << "  [default: " << (default_rand ? "enabled" : "disabled") << "]\n"
              << "--rand-linear-brlen <value>\n"
              << "  In a randomly generated linear graph, sets the number of edges in each bridge to <value>.\n"
              << "  [default: " << default_rand_linear_brlen << "]\n"
              << "--rand-linear-limit-conslen\n"
              << "  In a randomly generated linear graph, limit the length of each constraint obtained\n"
              << "  by composition by assuring that its length is between the range specified with the\n"
              << "  '--rand-linear-min-conslen' and '--rand-linear-max-conslen' value..\n"
              << "  The effective length of a constraint is randomly generated.\n"
              << "  [default: " << (default_rand_linear_limit_conslen ? "enabled" : "disabled") << "]\n"
              << "--rand-linear-max_conslen <value>\n"
              << "  In a randomly generated linear graph, sets the maximum number of triples in a constraint to <value>.\n"
              << "  [default: " << default_rand_linear_max_conslen << "]\n"
              << "--rand-linear-max_dist <value>\n"
              << "  In a randomly generated linear graph, sets the maximum base distance for constraints to <value>.\n"
              << "  In a constraint, the base distance d is used to derive consecutive distance values for the\n"
              << "  constraint's triples (i.e., d, d+1, d+nt, where nt is the number of triples in the constraint).\n"
              << "  [default: " << default_rand_linear_max_dist << "]\n"
              << "--rand-linear-min_conslen <value>\n"
              << "  In a randomly generated linear graph, sets the minimum number of triples in a constraint to <value>.\n"
              << "  [default: " << default_rand_linear_min_conslen << "]\n"
              << "--rand-linear-min_dist <value>\n"
              << "  In a randomly generated linear graph, sets the minimum base distance for constraints to <value>.\n"
              << "  In a constraint, the base distance d is used to derive consecutive distance values for the\n"
              << "  constraint's triples (i.e., d, d+1, d+nt, where nt is the number of triples in the constraint).\n"
              << "  [default: " << default_rand_linear_min_dist << "]\n"
              << "--rand-linear-nverts <value>\n"
              << "  In a randomly generated linear graph, sets the number of vertices to <value>.\n"
              << "  [default: " << default_rand_linear_nverts << "]\n"
              << "--rand-seed <value>\n"
              << "  Seed for the random number generator.\n"
              << "  [default: " << default_rand_seed << "]\n"
              << "--ui\n"
              << "  Enables interactive user interface.\n"
              << "  [default: " << (default_ui ? "enabled" : "disabled") << "]\n"
              << "--verbose\n"
              << "  Enables message verbosity.\n"
              << "  [default: " << (default_verbose ? "enabled" : "disabled") << "]\n"
              << '\n';
}

void print_exception(const std::exception& e)
{
    print_stacktrace(e);
}

void print_stacktrace(const std::exception& e, int level)
{
    std::ostringstream oss;
    oss << std::string(level, ' ') << "Exception: " << e.what();
    log_error(oss.str());

    try
    {
        std::rethrow_if_nested(e);
    }
    catch(const std::exception& e)
    {
        print_stacktrace(e, level+1);
    }
    catch(...)
    {
        std::ostringstream oss;
        oss << std::string(level, ' ') << "Unknow error.";
        log_error(oss.str());
    }
}

/*
bool check_graph(const graph_t& graph)
{
    bool ret = true;

    const std::string tag = "check_graph";

    auto mat = graph.adjacency_matrix();
    auto n = graph.num_vertices();
    for (std::size_t r = 0; r < n; ++r)
    {
        for (std::size_t c = 0; c < n; ++c)
        {
            punt p = mat[r][c];

            if (p == nullptr)
            {
                std::ostringstream oss;
                oss << "[" << tag << "] row: " << r << ", col: " << c << " => triples cannot be null\n";
                log_warn(oss.str());
                continue;
            }

            auto nt = p[0].dist;
            auto have_prob = !p[0].is_prob_undef();
            auto have_pref = !p[0].is_pref_undef();

            for (int k = 1; k <= nt; ++k)
            {
                if (r != c && p[k].dist == 0)
                {
                    std::ostringstream oss;
                    oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", component: " << k << " => distance cannot be zero\n";
                    log_warn(oss.str());
                    continue;
                }
                else if ((p[k].is_prob_undef() && have_prob)
                         || (!p[k].is_prob_undef() && !have_prob))
                {
                    std::ostringstream oss;
                    oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", component: " << k << " => inconsistent probability\n";
                    log_warn(oss.str());
                    continue;
                }
                else if ((p[k].is_pref_undef() && have_pref)
                         || (!p[k].is_pref_undef() && !have_pref))
                {
                    std::ostringstream oss;
                    oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", component: " << k << " => inconsistent preference\n";
                    log_warn(oss.str());
                    continue;
                }
            }
        }
    }

    return ret;
}
*/

} // Unnamed namespace
