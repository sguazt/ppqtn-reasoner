/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef PPQTN_USE_PEGTL

//FIXME
#error Do not use this code. There are still open issues!

#include <cassert>
#include <iostream>
#include <map>
#include <memory>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_io_dot.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/util.hpp>
#include <set>
#ifdef PPQTN_DEBUG
# include <sstream>
#endif // PPQTN_DEBUG
#include <stack>
#include <stdexcept>
#include <string>
#include <tao/pegtl.hpp>
#ifdef PPQTN_DEBUG
# include <tao/pegtl/analyze.hpp>
# include <tao/pegtl/contrib/tracer.hpp>
#endif // PPQTN_DEBUG
#include <vector>


namespace {

#ifdef PPQTN_DEBUG

template <typename KT, typename VT>
inline
std::ostream& operator<<(std::ostream& os, const std::map<KT,VT>& set);

template <typename T>
inline
std::ostream& operator<<(std::ostream& os, const std::set<T>& set);

template <typename T>
inline
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec);

template <typename KT, typename VT>
std::ostream& operator<<(std::ostream& os, const std::map<KT,VT>& set)
{
    os << "{";
    bool first = true;
    for (auto const& el : set)
    {
        if (!first)
        {
            os << ", ";
        }
        else
        {
            first = false;
        }
        os << "<" << el.first << ", " << el.second << ">";
    }
    os << "}";
    return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& set)
{
    os << "{";
    bool first = true;
    for (auto const& el : set)
    {
        if (!first)
        {
            os << ", ";
        }
        else
        {
            first = false;
        }
        os << el;
    }
    os << "}";
    return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    os << "[";
    bool first = true;
    for (auto const& el : vec)
    {
        if (!first)
        {
            os << ", ";
        }
        else
        {
            first = false;
        }
        os << el;
    }
    os << "]";
    return os;
}

#endif // PPQTN_DEBUG

namespace dot_parser
{

/*
 * Grammar for GraphViz DOT (https://www.graphviz.org/doc/info/lang.html)
 *
 * The following is an abstract grammar defining the DOT language.
 * Terminals are enclosed in angle brackets < and > (e.g., <graph>) and nonterminals are not (e.g., graph).
 * Literal characters are given in single quotes.
 * Parentheses ( and ) indicate grouping when needed.
 * Square brackets [ and ] enclose optional items.
 * Vertical bars | separate alternatives.
 *
 * graph         :   [ <strict> ] (<graph> | <digraph>) [ ID ] '{' stmt_list '}'
 * stmt_list     :   [ stmt [ ';' ] stmt_list ]
 * stmt          :   node_stmt
 *               |   edge_stmt
 *               |   attr_stmt
 *               |   ID '=' ID
 *               |   subgraph
 * attr_stmt     :   (<graph> | <node> | <edge>) attr_list
 * attr_list     :   '[' [ a_list ] ']' [ attr_list ]
 * a_list        :   ID '=' ID [ (';' | ',') ] [ a_list ]
 * edge_stmt     :   (node_id | subgraph) edgeRHS [ attr_list ]
 * edgeRHS       :   edgeop (node_id | subgraph) [ edgeRHS ]
 * node_stmt     :   node_id [ attr_list ]
 * node_id       :   ID [ port ]
 * port          :   ':' ID [ ':' compass_pt ]
 *               |   ':' compass_pt
 * subgraph      :   [ <subgraph> [ ID ] ] '{' stmt_list '}'
 * compass_pt    :   (<n> | <ne> | <e> | <se> | <s> | <sw> | <w> | <nw> | <c> | <_>)
 *
 *
 * The keywords <node>, <edge>, <graph>, <digraph>, <subgraph>, and <strict> are
 * case-independent.
 * Note also that the allowed compass point values (see compass_pt above) are not
 * keywords, so these strings can be used elsewhere as ordinary identifiers and,
 * conversely, the parser will actually accept any identifier.
 *
 * An ID is one of the following:
 * - any string of alphabetic ([a-zA-Z\200-\377]) characters, underscores ('_')
 *   or digits ([0-9]), not beginning with a digit;
 * - a numeral [-]?(.[0-9]+ | [0-9]+(.[0-9]*)? );
 * - any double-quoted string ("...") possibly containing escaped quotes (\")1;
 * - an HTML string (<...>).
 *
 * An ID is just a string; the lack of quote characters in the first two forms is
 * just for simplicity.
 * There is no semantic difference between abc_2 and "abc_2", or between 2.34 and
 * "2.34".
 * Obviously, to use a keyword as an ID, it must be quoted. Note that, in HTML
 * strings, angle brackets must occur in matched pairs, and newlines and other
 * formatting whitespace characters are allowed.
 * In addition, the content must be legal XML, so that the special XML escape
 * sequences for \", &, <, and > may be necessary in order to embed these
 * characters in attribute values or raw text.
 * As an ID, an HTML string can be any legal XML string.
 * However, if used as a label attribute, it is interpreted specially and must
 * follow the syntax for HTML-like labels.
 * Both quoted strings and HTML strings are scanned as a unit, so any embedded
 * comments will be treated as part of the strings.
 *
 * An edgeop is -> in directed graphs and -- in undirected graphs.
 *
 * The language supports C++-style comments: \/\* *\/ and //. In addition, a line
 * beginning with a '#' character is considered a line output from a C
 * preprocessor (e.g., # 34 to indicate line 34 ) and discarded.
 *
 * Semicolons and commas aid readability but are not required.
 * Also, any amount of whitespace may be inserted between terminals.
 *
 * As another aid for readability, dot allows double-quoted strings to span
 * multiple physical lines using the standard C convention of a backslash
 * immediately preceding a newline character2.
 * In addition, double-quoted strings can be concatenated using a '+' operator.
 * As HTML strings can contain newline characters, which are used solely for
 * formatting, the language does not allow escaped newlines or concatenation
 * operators to be used within them.
 *
 * The following grammar has been adapted from the context-free grammar
 * 'grammar.y' shipped with the GraphViz's cgraph source code.
 */

namespace pegtl = tao::pegtl;


////////////////////////////////////////////////////////////////////////////////
// GRAMMAR

// Forward declarations

struct atom;
struct attr_assign;
struct attr_defs;
struct attr_item;
struct attr_list;
struct attr_macro;
struct attr_stmt;
struct attr_type;
struct body;
struct body_end;
struct body_start;
struct comment;
struct compound;
struct graph;
struct graph_attr_defs;
struct graph_name;
struct graph_type;
struct header;
struct macro_name;
struct node;
struct node_list;
struct node_name;
struct node_ports;
struct qatom;
struct rcompound;
struct simple;
struct skip;
struct stmt;
struct stmt_list;
struct subgraph;
struct subgraph_header;
struct tok_at;
struct tok_atom;
struct tok_colon;
struct tok_comma;
struct tok_digraph;
struct tok_edge;
struct tok_edge_op;
struct tok_eq;
struct tok_graph;
struct tok_hstring;
struct tok_lbrace;
struct tok_lbrack;
struct tok_node;
struct tok_plus;
struct tok_qatom;
struct tok_qstring;
struct tok_rbrace;
struct tok_rbrack;
struct tok_scomment;
struct tok_semicolon;
struct tok_strict;
struct tok_subgraph;

// Definitions

/// Define tokens to be ignored: spaces and comments
struct skip: pegtl::sor< pegtl::space, comment > {};

template <typename R, typename P = skip>
struct padl: pegtl::seq< pegtl::star< P >, R > {};

template <typename R, typename P = skip>
struct padr: pegtl::seq< R, pegtl::star< P > > {};

//template <typename R, typename P = skip>
//struct pad: pegtl::seq< pegtl::star< P >, R, pegtl::star< P > > {};
template <typename R, typename P = skip>
using pad = padr<R,P>;


// grammar <- graph?
////struct grammar: pegtl::seq< pegtl::opt< graph >, pegtl::eof > {};
//struct grammar: pegtl::must< graph, pegtl::eof > {};
using grammar = pegtl::must< pegtl::star< skip >, graph, pegtl::eof >;

// graph <- header body
struct graph: pegtl::seq< header, body > {};

// header <- "strict"? graph_type graph_name?
struct header: pegtl::seq< pegtl::opt< tok_strict >, graph_type, pegtl::opt< graph_name > > {};

// graph_type <- "graph"
//             / "digraph"
struct graph_type: pegtl::sor< tok_graph, tok_digraph > {};

// body <- '{' stmt_list? '}'
//struct body: pegtl::seq< tok_lbrace, pegtl::opt<stmt_list>, tok_rbrace > {};
struct body: pegtl::seq< body_start, pegtl::opt<stmt_list>, body_end > {};
struct body_start: pegtl::sor< tok_lbrace > {};
struct body_end: pegtl::sor< tok_rbrace > {};

// graph_name <- atom
struct graph_name: pegtl::sor< atom > {};

// stmt_list <- ( stmt ';'? )+
struct stmt_list: pegtl::plus< pegtl::seq< stmt, pegtl::opt< tok_semicolon > > > {};

// stmt <- attr_stmt
//       / compound
struct stmt: pegtl::sor< attr_stmt, compound > {};

//// compound <- simple rcompound? attr_list?
//struct compound: pegtl::seq< simple, pegtl::opt< rcompound >, pegtl::opt< attr_list > > {};
// compound <- simple rcompound* attr_list?
struct compound: pegtl::seq< simple, pegtl::star< rcompound >, pegtl::opt< attr_list > > {};

// simple <- node_list
//         / subgraph
struct simple: pegtl::sor< node_list, subgraph > {};

//// rcompound <- edge_op simple rcompound?
//struct rcompound: pegtl::plus< pegtl::seq< tok_edge_op, simple > > {};
// rcompound <- edge_op simple
struct rcompound: pegtl::seq< tok_edge_op, simple > {};

// node_list <- node ( ',' node )*
struct node_list: pegtl::list< node, tok_comma > {};

// node <- atom
//       / atom ':' atom
//       / atom ':' atom ':' atom
//The alternative Alt#2 is equivalent to Alt#1 but it is more easier to parse and to attach actions
//Alt#1:
//struct node: pegtl::sor< atom, pegtl::seq< atom, tok_colon, atom, pegtl::opt< pegtl::seq< tok_colon, atom > > > > {};
//Alt#2:
struct node: pegtl::seq< node_name, pegtl::opt< node_ports > > {};
struct node_name: pegtl::sor< atom > {};
struct node_ports: pegtl::seq< tok_colon, atom, pegtl::opt< pegtl::seq< tok_colon, atom > > > {};

// attr_stmt  <- attr_type macro_name? attr_list
//             / graph_attr_defs
struct attr_stmt: pegtl::sor< pegtl::seq< attr_type, pegtl::opt< macro_name >, pegtl::opt< attr_list > >, graph_attr_defs > {};

// attr_type <- "graph"
//            / "node"
//            / "edge"
struct attr_type: pegtl::sor< tok_graph, tok_node, tok_edge > {};

// macro_name <- atom '=' 
struct macro_name: pegtl::seq< atom, tok_eq > {};

// attr_list  <- ( '[' attr_defs? ']' )+
struct attr_list: pegtl::plus< pegtl::seq< tok_lbrack, pegtl::opt< attr_defs >, tok_rbrack > > {};

// attr_defs <- attr_item attr_sep?
// attr_sep  <- [;,]
struct attr_defs: pegtl::seq< attr_item, pegtl::opt< pegtl::sor< tok_semicolon, tok_comma > > > {};

// attr_item <- attr_assign
//            / attr_macro
struct attr_item: pegtl::sor< attr_assign, attr_macro > {};

// attr_assign <- atom '=' atom
//struct attr_assign: pegtl::seq< atom, tok_eq, atom > {};
struct attr_name;
struct attr_value;
struct attr_assign: pegtl::seq< attr_name, tok_eq, attr_value > {};
struct attr_name: pegtl::sor< atom > {};
struct attr_value: pegtl::sor< atom > {};

// attr_macro <- '@' atom
struct attr_macro: pegtl::seq< tok_at, atom > {};

// graph_attr_defs <- attr_assign
struct graph_attr_defs: pegtl::sor< attr_assign > {};

// subgraph <- subgraph_header? body
struct subgraph: pegtl::seq< pegtl::opt< subgraph_header >, body > {};

// subgraph_header <- "subgraph" atom
//                  / "subgraph"
struct subgraph_header: pegtl::seq< tok_subgraph, pegtl::opt< atom > > {};

// atom <- tok_atom
//        / qatom
struct atom: pegtl::sor< tok_atom, qatom > {};

// qatom <- tok_qatom
//        / tok_qatom '+' qatom
struct qatom: pegtl::sor< tok_qatom, pegtl::seq< tok_qatom, tok_plus, qatom > > {};

// comment <- tok_scomment
//          / tok_mcomment
//TODO
struct comment: pegtl::disable< pegtl::sor< tok_scomment > > {};

struct tok_at: pad< pegtl::one< '@' > > {};

// tok_atom        <- < [A-Za-z_\x80-\xFF] ( [0-9A-Za-z_\x80-\xFF] )* >
//TODO: add special chars
struct tok_atom: pad< pegtl::seq< pegtl::ranges< 'a', 'z', 'A', 'Z', '_' >, pegtl::star< pegtl::ranges< 'a', 'z', 'A', 'Z', '0', '9', '_' > > > > {};

struct tok_colon: pad< pegtl::one< ':' > > {};

struct tok_comma: pad< pegtl::one< ',' > > {};

struct tok_digraph: pad< TAO_PEGTL_ISTRING("digraph") > {};

struct tok_edge: pad< TAO_PEGTL_ISTRING("edge") > {};

// tok_edge_op <- "->"
//              / "--"
struct tok_edge_op: pegtl::sor< pad< TAO_PEGTL_STRING("->") >, pad< TAO_PEGTL_STRING("--") > > {};

struct tok_eq: pad< pegtl::one< '=' > > {};

struct tok_graph: pad< TAO_PEGTL_ISTRING("graph") > {};

struct tok_lbrace: pad< pegtl::one< '{' > > {};

struct tok_lbrack: pad< pegtl::one< '[' > > {};

// mcomment        <- "/*" ( !"*/" . )* "*/"
struct tok_mcomment: pegtl::seq< TAO_PEGTL_STRING("/*"), pegtl::until< TAO_PEGTL_STRING("*/") > > {};

struct tok_node: pad< TAO_PEGTL_ISTRING("node") > {};

struct tok_plus: pad< pegtl::one< '+' > > {};

// tok_qatom       <- < '"' (!'"'.)* '"' >
//                  / < '<' (!'>'.)* '>' >
struct tok_qatom: pegtl::sor< tok_qstring, tok_hstring > {};

//Alt#1
//struct tok_qatom: pad< pegtl::seq< pegtl::one< '"' >, pegtl::until< pegtl::one< '"' >, pegtl::not_one< '"' > > > {};
//Alt#2
//   struct quoted_value : pegtl::if_must< pegtl::one< '"' >, string_without< '"' >, pegtl::one< '"' > > {};
//Alt#3
//  struct quoted_string_cont : until< DQUOTE, print > {};
//  struct quoted_string : if_must< DQUOTE, quoted_string_cont > {};
//Alt#4
//  template< char Q >
//  struct short_string : pegtl::if_must< pegtl::one< Q >, pegtl::until< pegtl::one< Q >, character > > {};
//  struct long_string : pegtl::raw_string< '[', '=', ']' > {};
//  struct literal_string : pegtl::sor< short_string< '"' >, short_string< '\'' >, long_string > {};
//Alt#5
//  struct plain : tao::pegtl::utf8::range< 0x20, 0x10FFFF > {};
//  struct escaped : tao::pegtl::one< '\'', '"', '?', '\\', 'a', 'b', 'f', 'n', 'r', 't', 'v' > {};
//  struct character : tao::pegtl::if_must_else< tao::pegtl::one< '\\' >, escaped, plain > {};
//  struct text : tao::pegtl::must< tao::pegtl::star< character >, tao::pegtl::eof > {};
//struct tok_qstring: pad< pegtl::seq< pegtl::one< '"' >, pegtl::until< pegtl::one< '"' >, pegtl::not_one< '"' > > > {};
struct tok_qstring: pad< pegtl::seq< pegtl::one< '"' >, pegtl::until< pegtl::one< '"' >, pegtl::print > > > {};

struct tok_hstring: pad< pegtl::seq< pegtl::one< '<' >, pegtl::until< pegtl::one< '>' >, pegtl::print > > > {};

struct tok_rbrace: pad< pegtl::one< '}' > > {};

struct tok_rbrack: pad< pegtl::one< ']' > > {};

// scomment        <- ( '#' / "//" ) ( !eol . )* ( eol / !. )
struct tok_scomment: pegtl::if_must< pegtl::sor< pegtl::one< '#' >, TAO_PEGTL_STRING("//") >, pegtl::until< pegtl::eolf > > {};

struct tok_semicolon: pad< pegtl::one< ';' > > {};

struct tok_strict: pad< TAO_PEGTL_ISTRING("strict") > {};

struct tok_subgraph: pad< TAO_PEGTL_ISTRING("subgraph") > {};


////////////////////////////////////////////////////////////////////////////////
// STATE

/// The parser state
struct state_t
{
    /// Hold information of a cluster of nodes
    struct cluster_context_t
    {
        std::vector<std::set<std::string>> edge_nodes_chain; //< Chain of nodes in the current edge statement (e.g., "A,B -> C -> D,C" represents a chain of 3 elements, namely "A,B", "C", and "D,C")
        std::set<std::string> nodes; //< The cluster's nodes set
        std::map<std::string, std::string> default_edge_attrs; //< Default edge attributes
    }; // cluster_context_t

    //enum attr_type_t
    //{
    //    chain_attr, //< For attributes defined inline in a edge chain; e.g., "A -> B -> ... -> N [name1 = value1, name2 = value2, ...]"
    //    edge_attr, //< For default edge attributes; e.g., "edge [name1 = value1, name2 = value2, ...]"
    //    graph_attr, //< For default graph attributes; e.g., "edge [name1 = value1, name2 = value2, ...]"
    //    node_attr //< For default node attributes; e.g., "node [name1 = value1, name2 = value2, ...]"
    //}; // attr_type_t

    state_t()
    : strict(false),
      directed(false),
      in_graph_header(false),
      in_edge_attr_stmt(false)
    {
    }

    bool strict; //< Tells whether this graph is strict (i.e., does not allow multi-edges)
    bool directed; //< Tells whether this graph is directed or not
    bool in_graph_header; //< Tells whether the parser is matching the header of the (root) graph
    bool in_edge_attr_stmt; //< Tells whether the parser is matching the default attribute list for edges (e.g., "edge [name1 = value1, name2=value2, ...]")
    std::string graph_name; //< The graph name
    std::set<std::string> nodes; //< The graph's node set
    //std::map<std::pair<std::string,std::string>, std::string> edges; //< edge map: each element <e,lbl> maps an edge e=<first node, second node> (i.e., <e.first,e.second>) to the associated edge label lbl
    std::map<std::pair<std::string,std::string>, std::map<std::string, std::string>> edges; //< edge map: each element <e,attrs> maps an edge e=<first node, second node> (i.e., <e.first,e.second>) to the associated attributes stored in the attrs container, which is a key-value map, mapping an attribute name to its value
    std::stack<std::shared_ptr<cluster_context_t>> cluster_stack; //< Collection of contexts associated with current "active" clusters (e.g., when parsing "E" in "A -> {B,C -> {D,E} -> F} -> G" there are 3 cluster context, namely the "root" cluster, the "B,C" cluster and "D,E" cluster)
    std::map<std::string, std::string> cur_attrs; //< Hold the attributes in the currently parsed attribute list "[ name1 = value1; name2 = value2; ...]"
    std::string cur_attr_name; //< The name of the currently parsed attribute
}; // state_t


////////////////////////////////////////////////////////////////////////////////
// ACTIONS

// Default action: do nothing
template <typename Rule>
struct action_t: pegtl::nothing< Rule > {};

// Specialized action for 'attr_list' rule
template <>
struct action_t< attr_list >
{
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "attr_list -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG
    } 
}; // attr_list

// Specialized action for 'attr_name' rule
template <>
struct action_t< attr_name >
{
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "attr_name -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        st.cur_attr_name = trim_copy(in.string());
    } 
}; // attr_name

// Specialized action for 'attr_stmt' rule
template <>
struct action_t< attr_stmt >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "attr_stmt -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        assert( st.cluster_stack.size() > 0 );

        if (st.in_edge_attr_stmt && !st.cur_attrs.empty())
        {
            st.cluster_stack.top()->default_edge_attrs = st.cur_attrs;
        }
        st.cur_attrs.clear();
        st.in_edge_attr_stmt = false;
    } 
}; // attr_stmt

// Specialized action for 'attr_value' rule
template <>
struct action_t< attr_value >
{
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "attr_value -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        if (!st.cur_attr_name.empty())
        {
            st.cur_attrs[st.cur_attr_name] = trim_copy(in.string());
        }
    } 
}; // attr_value

// Specialized action for 'body_end' rule
template <>
struct action_t< body_end >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "body_end -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        assert( st.cluster_stack.size() > 0 );

        if (st.cluster_stack.size() > 1)
        {
            // Remove the more recent cluster context from the stack
            auto p_cluster_ctx = st.cluster_stack.top();
            st.cluster_stack.pop();

            // Copy the nodes set of the recent cluster context into the cluster context on top of the stack
            if (!p_cluster_ctx->nodes.empty())
            {
                st.cluster_stack.top()->nodes.insert(p_cluster_ctx->nodes.begin(), p_cluster_ctx->nodes.end());

                //st.cluster_stack.top()->edge_nodes_chain.push_back(p_cluster_ctx->nodes);
                st.cluster_stack.top()->edge_nodes_chain.back().insert(p_cluster_ctx->nodes.begin(), p_cluster_ctx->nodes.end());
            }
        }
    } 
}; // body_end

// Specialized action for 'body_start' rule
template <>
struct action_t< body_start >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template< typename Input >
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "body_start -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        // Push a new cluster context onto the stack
        auto p_cluster_ctx = std::make_shared<state_t::cluster_context_t>();
        //p_cluster_ctx->edge_nodes_chain.push_back(std::set<std::string>());
        p_cluster_ctx->edge_nodes_chain.resize(1);
        st.cluster_stack.push(p_cluster_ctx);
        st.in_graph_header = false;
    } 
}; // body_start

// Specialized action for 'compound' rule
template <>
struct action_t< compound >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "compound -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        assert( st.cluster_stack.size() > 0 );
        assert( st.cluster_stack.top()->edge_nodes_chain.size() > 0 );

        auto p_cluster_ctx = st.cluster_stack.top();
        // Link nodes in current chain
        auto chain_len = p_cluster_ctx->edge_nodes_chain.size();
        for (std::size_t i = 1; i < chain_len; ++i)
        {
            // Link (i-1)-th chain element to i-th chain element
            for (auto from_node : p_cluster_ctx->edge_nodes_chain[i-1])
            {
                for (auto to_node : p_cluster_ctx->edge_nodes_chain[i])
                {
                    auto edge = std::make_pair(from_node, to_node);
                    // Insert default attributes
                    st.edges[edge] = p_cluster_ctx->default_edge_attrs;
                    // Insert/override attributes according to those defined locally in this chain (if any)
                    for (auto const& attr : st.cur_attrs)
                    {
                        st.edges[edge][attr.first] = attr.second;
                    }
                }
            }
        }
        // Clear this chain
        p_cluster_ctx->edge_nodes_chain.clear();
        st.cur_attrs.clear();
        //p_cluster_ctx->edge_nodes_chain.push_back(std::set<std::string>());
        p_cluster_ctx->edge_nodes_chain.resize(p_cluster_ctx->edge_nodes_chain.size()+1);
    } 
}; // compound

// Specialized action for 'graph_name' rule
template <>
struct action_t< graph_name >
{
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "graph_name -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        if (st.in_graph_header)
        {
            st.graph_name = trim_copy(in.string());
        }
    } 
}; // action_t< graph_name >

// Specialized action for 'graph_type' rule
template <>
struct action_t< graph_type >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "graph_type -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        st.in_graph_header = true;
    } 
}; // action_t< graph_type >

// Specialized action for 'node_name' rule
template <>
struct action_t< node_name >
{
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "node_name -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        assert( st.cluster_stack.size() > 0 );
        assert( st.cluster_stack.top()->edge_nodes_chain.size() > 0 );

        auto const node_name = trim_copy(in.string());
        // Add node to the graph's nodes set 
        st.nodes.insert(node_name);
        // Add node to current cluster's nodes set
        auto p_cluster_ctx = st.cluster_stack.top();
        p_cluster_ctx->edge_nodes_chain.back().insert(node_name);
        p_cluster_ctx->nodes.insert(node_name);
    } 
}; // node_name

// Specialized action for 'tok_digraph' rule
template <>
struct action_t< tok_digraph >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "tok_digraph -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        st.directed = true;
    } 
}; // tok_digraph

// Specialized action for 'tok_edge' rule
template <>
struct action_t< tok_edge>
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "tok_edge -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        st.in_edge_attr_stmt = true;
    } 
}; // tok_edge

// Specialized action for 'tok_edge_op' rule
template <>
struct action_t< tok_edge_op >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "tok_edge_op -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        assert( st.cluster_stack.size() > 0 );

        auto p_cluster_ctx = st.cluster_stack.top();
        //p_cluster_ctx->edge_nodes_chain.push_back(std::set<std::string>());
        p_cluster_ctx->edge_nodes_chain.resize(p_cluster_ctx->edge_nodes_chain.size()+1);
    } 
}; // tok_edge_op

// Specialized action for 'tok_lbrack' rule
template <>
struct action_t< tok_lbrack >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "tok_lbrack -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        st.cur_attrs.clear();
    } 
}; // tok_lbrack

// Specialized action for 'tok_strict' rule
template <>
struct action_t< tok_strict >
{
    //TODO: we could use 'apply0' instead of 'apply'
    template <typename Input>
    static void apply(const Input& in, state_t& st)
    {
#if PPQTN_DEBUG
{
        std::ostringstream oss;
        oss << "tok_strict -> '" << in.string() << "'";
        log_debug(oss.str());
}
#endif // PPQTN_DEBUG

        st.strict = true;
    } 
}; // tok_strict


//// CONTROL
//
//template <typename Rule>
//struct control_t: pegtl::normal< Rule > { };
//
//template <>
//struct control_t< body >: pegtl::normal< body >
//{
//    template< typename Input >
//    static void start( Input& /*unused*/, state_t& st)
//    {
//        std::clog << "[body} Start\n";
//    }
//
//    template< typename Input >
//    static void success( Input& /*unused*/, state_t& st)
//    {
//        std::clog << "[body} Success\n";
//    }
//
//    template< typename Input >
//    static void failure( Input& /*unused*/, state_t& st)
//    {
//        std::clog << "[body} Failiure\n";
//    }
//}; // control_t< body >

} // Namespace dot_parser

} // Unnamed namespace


void read_dot(const std::string& dot_graph, graph_t& graph)
{
    namespace pegtl = tao::pegtl;

    pegtl::string_input<> in( dot_graph, "" );
    dot_parser::state_t state;
#ifdef PPQTN_DEBUG
    // Check the grammar for possible issues
    if (pegtl::analyze< dot_parser::grammar >() != 0)
    {
        throw std::logic_error("DOT grammar is not well-formed");
    }

    //auto val = pegtl::parse< dot_parser::grammar, dot_parser::action_t, pegtl::trace< dot_parser::control_t >::control >( in, state );
    auto val = pegtl::parse< dot_parser::grammar, dot_parser::action_t, pegtl::tracer >( in, state );
    //auto val = pegtl::parse< dot_parser::grammar, dot_parser::action_t >( in, state );
#else
    auto val = pegtl::parse< dot_parser::grammar, dot_parser::action_t >( in, state );
#endif // PPQTN_DEBUG

    //TODO: come evitare di includere in node_nome gli spazi?

    std::cout << "Nodes: [";
    for (auto node : state.nodes)
    {
        std::cout << node << ", ";
    }
    std::cout << "]\n";

    std::cout << "Edges: [";
    for (auto edge : state.edges)
    {
        std::cout << "<" << edge.first.first << ", " << edge.first.second << "> (" << edge.second << ")" << ", ";
    }
    std::cout << "]\n";

    //TODO: translate parser state into a graph...

    std::cout << "VAL: " << val << '\n';
}


void write_dot(const graph_t& g, std::ostream& os, bool both_dir)
{
    //TODO
    os  << "digraph " << g.name() << " {\n";
    os  << "}\n";
}

#endif // PPQTN_USE_PEGTL
