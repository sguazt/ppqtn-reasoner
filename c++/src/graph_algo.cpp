/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <cmath>
#include <map>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_algo.hpp>
#include <ppqtn/graph_io.hpp>
#include <ppqtn/error.hpp>
#include <ppqtn/log.hpp>
#include <sstream>
#include <stdexcept>
#include <unordered_set>

#ifdef PPQTN_DEBUG
# include <sstream>
#endif // PPQTN_DEBUG


namespace {

/// Applies the composition operator to the given constraints.
punt compose_impl(punt c1, punt c2);

template <typename T>
bool float_eq(T x, T y, T eps = 1e-5);

/// Applies the intersection operator to the given constraints.
punt intersect_impl(punt c1, punt c2);

} // Unnamed namespace


bool check_graph(const graph_t& graph)
{
    bool ok = true;

    const std::string tag = "check_graph";

    auto n = graph.num_vertices();
    for (std::size_t r = 0; r < n && ok; ++r)
    {
        for (std::size_t c = 0; c < n && ok; ++c)
        {
            punt p = graph(r, c);

            if (p == nullptr)
            {
                std::ostringstream oss;
                oss << "[" << tag << "] row: " << r << ", col: " << c << " => triples cannot be null\n";
                log_warn(oss.str());
                ok = false;
                continue;
            }

            auto nt = p[0].dist;
            auto have_prob = !p[0].is_prob_undef();
            auto have_pref = !p[0].is_pref_undef();

            std::unordered_set<std::size_t> specular_triples_pos; // Hold the positions of specular triples that have a corresponding triple in p

            for (int k = 1; k <= nt; ++k)
            {
//[XXX] a zero distance is a valid value even in the case r != c
//                if (r != c && p[k].dist == 0)
//                {
//                    std::ostringstream oss;
//                    oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", component: " << k << " => distance cannot be zero\n";
//                    log_warn(oss.str());
//                      ok = false;
//                    continue;
//                }
//                else if ((p[k].is_prob_undef() && have_prob)
//[/XXX] a zero distance is a valid value even in the case r != c
                if ((p[k].is_prob_undef() && have_prob)
                         || (!p[k].is_prob_undef() && !have_prob))
                {
                    std::ostringstream oss;
                    oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", component: " << k << " => inconsistent probability\n";
                    log_warn(oss.str());
                    ok = false;
                    continue;
                }
                else if ((p[k].is_pref_undef() && have_pref)
                         || (!p[k].is_pref_undef() && !have_pref))
                {
                    std::ostringstream oss;
                    oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", component: " << k << " => inconsistent preference\n";
                    log_warn(oss.str());
                    ok = false;
                    continue;
                }
                else if (r > c)
                {
                    // Check specular edges (r,c) vs. (c,r): opposite distances, same probabilities and preferences.
 
                    punt q = graph(c, r);

                    // NOTE: if we are here q cannot be null otherwise the check on edge (c,r) would have failed in
                    //       the previous iterations

                    // NOTE: the triples may have been rearranged in a sorted order by some algorithms
                    //       (e.g., by Floyd-Warshall).
                    //       So, simply checking that `p[k].dist == -q[k].dist` to check for distance consistency does
                    //       not work anymore.
                    //       Instead, we need to scan the triples and look for the opposite distance.
                    //       Specifically, given a position `k` for triples in `p` we look for a position `qk` in the
                    //       specular triples `q` (with `1 <= qk <= nt`) such that `qk` has not been already used by
                    //       a previous value of `k` (this way we check for inconsistencies due to duplicate
                    //       distances) and that `p[k].dist == -q[qk].dist`.
                    //       To keep track of positions already used by previous values of `k` we use the set
                    //       `specular_triples_pos`.

                    std::size_t qk = 0;
                    for (qk = 1; qk <= nt; ++qk)
                    {
                        if (specular_triples_pos.count(qk) > 0)
                        {
                            // Position qk already analyzed for another triple, try another value in the next iteration
                            continue;
                        }

                        if (p[k].dist == -q[qk].dist)
                        {
                            specular_triples_pos.insert(qk);
                            break;
                        }
                    }

                    if (qk > nt)
                    {
                        std::ostringstream oss;
                        oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", specular triples: " << q << ", component: " << k << " => inconsistent distances\n";
                        log_warn(oss.str());
                        ok = false;
                        continue;
                    }
                    else if ((p[k].is_prob_undef() && !q[qk].is_prob_undef())
                             || (!p[k].is_prob_undef() && q[qk].is_prob_undef())
                             || (!p[k].is_prob_undef() && !q[qk].is_prob_undef() && !float_eq(p[k].prob, q[qk].prob)))
                    {
                        std::ostringstream oss;
                        oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", specular triples: " << q << ", component: " << k << " => inconsistent probabilities\n";
                        log_warn(oss.str());
                        ok = false;
                        continue;
                    }
                    else if ((p[k].is_pref_undef() && !q[qk].is_pref_undef())
                             || (!p[k].is_pref_undef() && q[qk].is_pref_undef())
                             || (!p[k].is_pref_undef() && !q[qk].is_pref_undef() && !float_eq(p[k].pref, q[qk].pref)))
                    {
                        std::ostringstream oss;
                        oss << "[" << tag << "] row: " << r << ", col: " << c << ", triples: " << p << ", specular triples: " << q << ", component: " << k << " => inconsistent probabilities\n";
                        log_warn(oss.str());
                        ok = false;
                        continue;
                    }
                }
            }
        }
    }

    return ok;
}

punt compose(punt c1, punt c2)
{
    // Checks for empty constraints
    if (c1 == nullptr || c2 == nullptr)
    {
        PPQTN_LOG_DEBUG_AT("Composition: at least of the input constraints is empty");

        return nullptr;
    }

    // Since compose_impl() expects that its arguments are sorted according to
    // their distance, sorts the input arguments.

    auto const nt1 = c1[0].dist;
    auto const nt2 = c2[0].dist;

    punt c1_aux = nullptr;
    if (nt1 > 0)
    {
        c1_aux = new terne[nt1+1];
        std::copy(c1, c1+nt1+1, c1_aux);
        std::sort(c1_aux+1,
                  c1_aux+nt1+1, 
                  [](const terne& a, const terne& b) { return a.dist < b.dist; });
    }
    else
    {
        c1_aux = c1;
    }

    punt c2_aux = nullptr;
    if (nt2 > 0)
    {
        c2_aux = new terne[nt2+1];
        std::copy(c2, c2+nt2+1, c2_aux);
        std::sort(c2_aux+1,
                  c2_aux+nt2+1, 
                  [](const terne& a, const terne& b) { return a.dist < b.dist; });
    }
    else
    {
        c2_aux = c2;
    }

    // Performs the composition

    auto res = compose_impl(c1_aux, c2_aux);

    delete[] c1_aux;
    delete[] c2_aux;

    return res;
}

bool floyd_warshall(graph_t& graph)
{
    const bool ignore_errors = false; //FIXME: maybe this could become a parameter to this function

    bool consistent = true;
    auto const n = graph.num_vertices();

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "min network - input graph: " << graph;
    PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

    // Preprocesses the input graph to make sure that all triples are sorted
    for (std::size_t r = 0; r < n; ++r)
    {
        for (std::size_t c = 0; c < n; ++c)
        {
            auto const nt = graph(r, c)[0].dist;
            if (graph(r, c) != nullptr && nt > 0)
            {
                std::sort(graph(r, c)+1,
                          graph(r, c)+nt+1,
                          [](const terne& a, const terne& b) { return a.dist < b.dist; });
            }
        }
    }

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "min network - sorted graph: " << graph;
    PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

    // Does Floyd-Warshall
    for (std::size_t k = 0; k < n && (consistent || ignore_errors); ++k)
    {
        for (std::size_t i = 0; i < n && (consistent || ignore_errors); ++i)
        {
            if (i == k)
            {
                continue;
            }

            for (std::size_t j = 0; j < n && (consistent || ignore_errors); ++j)
            {
                if (i == j || j == k)
                {
                    continue;
                }

#ifdef PPQTN_DEBUG
{
                std::ostringstream oss;
                oss << "k: " << k << ", i: " << i << ", j: " << j;
                PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

                // Performs composition
                punt comp = compose_impl(graph(i,k), graph(k,j));
                if (comp != nullptr)
                {
                    // Performs intersection
                    punt inters = intersect_impl(graph(i,j), comp);

                    // Checks for consistency
                    if (inters == nullptr)
                    {
                        // Empty intersection => constraints do not overlap => graph is inconsistent
#ifdef PPQTN_DEBUG
{
                        std::ostringstream oss;
                        oss << "Floyd-Warshall: found inconsistency at iteration " << k << " and edge (" << i << ", " << j << ")";
                        PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

                        consistent = false;
                    }

                    // Cleans-up memory
                    delete[] comp;
                    delete[] graph(i, j);

                    // Updates the graph
                    graph(i, j) = inters;
                }
                else
                {
                    // An empty composition may only arise as a consequence of a previous empty intersection.
                    // Therefore, we should have already found out that the graph is not consistent.
                    // If not, it is an error!

                    if (consistent)
                    {
                        throw std::runtime_error("Floyd-Warshall: unexpected empty constraint from composition");
                    }
                }
            }
        }
    }

    return consistent;
}

/* TODO: applies STP (i.e., do not consider probabilities and preferences) --> EXPERIMENTAL
bool floyd_warshall_stp(graph_t& graph)
{
    const bool ignore_errors = false; //FIXME: maybe this could become a parameter to this function

    bool consistent = true;
    auto const n = graph.num_vertices();

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "min network - input graph: " << graph;
    PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

    // Preprocesses the input graph to make sure that all triples are sorted
    for (std::size_t r = 0; r < n; ++r)
    {
        for (std::size_t c = 0; c < n; ++c)
        {
            auto const nt = graph(r, c)[0].dist;
            if (graph(r, c) != nullptr && nt > 0)
            {
                std::sort(graph(r, c)+1,
                          graph(r, c)+nt+1,
                          [](const terne& a, const terne& b) { return a.dist < b.dist; });
            }
        }
    }

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "min network - sorted graph: " << graph;
    PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

    // Does Floyd-Warshall
    for (std::size_t k = 0; k < n && (consistent || ignore_errors); ++k)
    {
        for (std::size_t i = 0; i < n && (consistent || ignore_errors); ++i)
        {
            if (i == k)
            {
                continue;
            }

            for (std::size_t j = 0; j < n && (consistent || ignore_errors); ++j)
            {
                if (i == j || j == k)
                {
                    continue;
                }

#ifdef PPQTN_DEBUG
{
                std::ostringstream oss;
                oss << "k: " << k << ", i: " << i << ", j: " << j;
                PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

                auto c_ij = graph(i, j)
                auto const c_ik = graph(i, k)
                auto const c_kj = graph(k, j)

                if (c_ik[0].dist == 0 || c_kj[0].dist == 0)
                {
                    // Either C_{ik} or C_{kj} is infinite => C_{ij} is the infinite constraint

                    c_ij[0].dist = 0;
                    c_ij[0].prob = constants::undef_prob;
                    c_ij[0].pref = constants::undef_pref;
                }
                else
                {
                    // At least one among C1 and C2 is not infinite

                    auto const n_ik = c_ik[0].dist;
                    auto const n_kj = c_kj[0].dist;

                    c_ij[0].dist = ...
                }
                else
                {
                    // An empty composition may only arise as a consequence of a previous empty intersection.
                    // Therefore, we should have already found out that the graph is not consistent.
                    // If not, it is an error!

                    if (consistent)
                    {
                        throw std::runtime_error("Floyd-Warshall: unexpected empty constraint from composition");
                    }
                }
            }
        }
    }

    return consistent;
}
*/

punt intersect(punt c1, punt c2)
{
    // Checks for empty constraints
    if (c1 == nullptr || c2 == nullptr)
    {
        PPQTN_LOG_DEBUG_AT("Intersection: at least of the input constraints is empty");

        return nullptr;
    }

    // Since intersect_impl() expects that its arguments are sorted according to
    // their distance, sorts the input arguments.

    auto const nt1 = c1[0].dist;
    auto const nt2 = c2[0].dist;

    punt c1_aux = nullptr;
    if (nt1 > 0)
    {
        c1_aux = new terne[nt1+1];
        std::copy(c1, c1+nt1+1, c1_aux);
        std::sort(c1_aux+1,
                  c1_aux+nt1+1, 
                  [](const terne& a, const terne& b) { return a.dist < b.dist; });
    }
    else
    {
        c1_aux = c1;
    }

    punt c2_aux = nullptr;
    if (nt2 > 0)
    {
        c2_aux = new terne[nt2+1];
        std::copy(c2, c2+nt2+1, c2_aux);
        std::sort(c2_aux+1,
                  c2_aux+nt2+1, 
                  [](const terne& a, const terne& b) { return a.dist < b.dist; });
    }
    else
    {
        c2_aux = c2;
    }

    // Performs the intersection

    auto res = intersect_impl(c1_aux, c2_aux);

    delete[] c1_aux;
    delete[] c2_aux;

    return res;
}


namespace {

punt compose_impl(punt c1, punt c2)
{
    // If at least one of the input constraint is empty, return empty
    if (c1 == nullptr || c2 == nullptr)
    {
        PPQTN_LOG_DEBUG_AT("Composition: at least of the input constraints is empty");

        return nullptr;
    }

    punt res = nullptr; // The composition's result

    if (c1[0].dist == 0 || c2[0].dist == 0)
    {
        // Either C1 or C2 is infinite => composition is the infinite constraint

        res = new terne[1];
        if (res == nullptr)
        {
            throw_syserror("Unable to allocate memory for composition's result");
        }

        res[0].dist = 0;
        res[0].prob = constants::undef_prob;
        res[0].pref = constants::undef_pref;
    }
    else
    {
        // At least one among C1 and C2 is not infinite

        auto const n1 = c1[0].dist;
        auto const n2 = c2[0].dist;
        auto const c1_has_prob = !c1[0].is_prob_undef();
        auto const c2_has_prob = !c2[0].is_prob_undef();
        auto const c1_has_pref = !c1[0].is_pref_undef();
        auto const c2_has_pref = !c2[0].is_pref_undef();

        // The purpose of the following associative array `triples_map` is to
        // incrementally compute the probability and preference values for
        // every distance resulting from the composition operator.
        // In this way, every time a new triple `t` with distance `d` is
        // obtained from the composition of two triples, the probability and
        // preference contributions from `t` will be used to update the
        // probabilities and preference values stored in `triples_map[d]`.
        std::map<int,terne> triples_map;

        for (int i1 = 1; i1 <= n1; ++i1)
        {
            for (int i2 = 1; i2 <= n2; ++i2)
            {
                // Computes distance
                auto const d = c1[i1].dist + c2[i2].dist;
                if (triples_map.count(d) == 0)
                {
                    terne t;
                    t.dist = d;
                    t.prob = 0; // Note, composition always yield a triple with a defined probability
                    t.pref = constants::undef_pref;
                    triples_map[d] = t;
                }

                // Computes probability
                auto prob = constants::undef_prob;
                if (c1_has_prob)
                {
                    if (c2_has_prob)
                    {
                        // Both constraints have probabilities
                        prob = c1[i1].prob * c2[i2].prob;
                    }
                    else
                    {
                        // Only C1 has probabilities
                        prob = c1[i1].prob/n2;
                    }
                }
                else if (c2_has_prob)
                {
                    // Only C2 has probabilities
                    prob = c2[i2].prob/n1;
                }
                else
                {
                    // No constraint has probabilities
                    prob = 1.0/(n1 * n2);
                }
                triples_map[d].prob += prob;

                // Computes preferences
                auto pref = constants::undef_pref;
                if (c1_has_pref)
                {
                    if (c2_has_pref)
                    {
                        // Both constraints have preferences
                        pref = std::min(c1[i1].pref, c2[i2].pref);
                    }
                    else
                    {
                        // Only C1 has preferences
                        pref = c1[i1].pref;
                    }
                }
                else if (c2_has_pref)
                {
                    // Only C2 has probabilities
                    pref = c2[i2].pref;
                }
                else
                {
                    // No constraint has probabilities
                    pref = constants::undef_pref;
                }
                triples_map[d].pref = std::max(triples_map[d].pref, pref);
            }
        }

        // Build the result of the composition
        auto const n = triples_map.size();
        res = new terne[n+1];
        if (res == nullptr)
        {
            throw_syserror("Unable to allocate memory for composition's result");
        }
        if (n > 0)
        {
            res[0].dist = n;
            res[0].prob = constants::def_prob; // When both C1 and C2 are not infinite, triples have always probabilities
            res[0].pref = (c1[0].is_pref_undef() && c2[0].is_pref_undef()) ? constants::undef_pref : constants::def_pref;
            std::size_t k = 1;
            for (auto const& p : triples_map)
            {
                assert( k <= n );

                res[k] = p.second;
                ++k;
            }
        }
    }

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "COMPOSE(" << c1 << ", " << c2 << ") => result: " << res;
    PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

    return res;
}

template <typename T>
bool float_eq(T x, T y, T eps)
{
    if (x == y)
    {
        return true;
    }

    if (!std::isfinite(x) || !std::isfinite(y))
    {
        return x == y;
    }

    // inspired by GNU gsl_fcmp

    const T max = (std::fabs(x) > std::fabs(y)) ? x : y;

    int exponent;

    std::frexp(max, &exponent);

    const T delta = std::ldexp(eps, exponent);

    const T difference = x - y;

    if (difference > delta || difference < -delta)
    {
        return false;
    }

    return true;
}

punt intersect_impl(punt c1, punt c2)
{
    // ASSUMPTIONS
    // - Triples in a constraints are sorted by their distance

    // Checks for empty constraints
    if (c1 == nullptr || c2 == nullptr)
    {
        PPQTN_LOG_WARN_AT("Intersection: at least of the input constraints is empty");

        return nullptr;
    }

    punt res = nullptr; // The intersection's result

    if (c1[0].dist == 0)
    {
        if (c2[0].dist == 0)
        {
            // Both constraints are infinite => intersection is the infinite constraint

            res = new terne[1];
            if (res == nullptr) 
            {
                throw_syserror("Unable to allocate memory for intersection's result");
            }
            res[0].dist = 0;
            res[0].prob = constants::undef_prob;
            res[0].pref = constants::undef_pref;
        }
        else
        {
            // Only C1 is infinite => intersection is C2

            auto const n = c2[0].dist;

            res = new terne[n+1];
            if (res == nullptr) 
            {
                throw_syserror("Unable to allocate memory for intersection's result");
            }

            // Copies descriptor and triples
            std::copy_n(c2, n+1, res);
        }
    }
    else if (c2[0].dist == 0)
    {
        // Only C2 is infinite => intersection is C1

        auto const n = c1[0].dist;

        res = new terne[n+1];
        if (res == nullptr) 
        {
            throw_syserror("Unable to allocate memory for intersection's result");
        }

        // Copies descriptor and triples
        std::copy_n(c1, n+1, res);
    }
    else
    {
        // Both C1 and C2 are non-infinite => intersection is given by components with the same distance

        auto const n1 = c1[0].dist;
        auto const n2 = c2[0].dist;

        std::vector<terne> triples;
        double prob_sum = 0; // Normalization constant for probabilities

        if (c1[n1].dist >= c2[1].dist
            && c2[n2].dist >= c1[1].dist)
        {
            // C1 and C2 overlap

            int k1 = 1;
            int k2 = 1;

            // Computes denormalized probabilities
            while (k1 <= n1 && k2 <= n2)
            {
                while (k2 <= n2 && c1[k1].dist >= c2[k2].dist)
                {
                    if (c1[k1].dist == c2[k2].dist)
                    {
                        terne t;
                        t.dist = c1[k1].dist;

                        // Computes probability
                        t.prob = constants::undef_prob;
                        if (c1[k1].is_prob_undef())
                        {
                            if (c2[k2].is_prob_undef())
                            {
                                // NOTE: the real value will be computed only at the end
                                //       of this loop, once the number of common
                                //       components is known
                                t.prob = 1;
                            }
                            else
                            {
                                t.prob = c2[k2].prob;
                            }
                        }
                        else if (c2[k2].is_prob_undef())
                        {
                            t.prob = c1[k1].prob;
                        }
                        else
                        {
                            t.prob = c1[k1].prob * c2[k2].prob;
                        }
                        prob_sum += t.prob;

                        // Computes preferences
                        t.pref = constants::undef_pref;
                        if (c1[k1].is_pref_undef())
                        {
                            if (c2[k2].is_pref_undef())
                            {
                                t.pref = constants::undef_pref;
                            }
                            else
                            {
                                t.pref = c2[k2].pref;
                            }
                        }
                        else if (c2[k2].is_pref_undef())
                        {
                            t.pref = c1[k1].pref;
                        }
                        else
                        {
                            t.pref = std::min(c1[k1].pref, c2[k2].pref);
                        }

                        triples.push_back(t);
                    }
                    ++k2;
                }
                ++k1;
            }
        }

        // Build intersection's result and normalizes probabilities
        auto const n = triples.size();
        if (n > 0)
        {
            // The two constraints overlap

            res = new terne[n+1];
            if (res == nullptr) 
            {
                throw_syserror("Unable to allocate memory for intersection's result");
            }
            res[0].dist = n;
            res[0].prob = constants::def_prob; // when both C1 and C2 are not infinite, triples have always probabilities
            res[0].pref = (c1[0].is_pref_undef() && c2[0].is_pref_undef())
                          ? constants::undef_pref
                          : constants::def_pref;
            for (std::size_t k = 1; k <= n; ++k)
            {

                res[k].dist = triples[k-1].dist;
                res[k].prob = triples[k-1].prob/prob_sum;
                res[k].pref = triples[k-1].pref;
            }
        }
    }

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "INTERSECT(" << c1 << ", " << c2 << ") => result: " << res;
    PPQTN_LOG_DEBUG_AT(oss.str());
}
#endif // PPQTN_DEBUG

    return res;
}

} // Unnamed namespace
