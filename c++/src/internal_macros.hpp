/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/**
 * \file internal_macros.hpp
 *
 * \brief Macros used internally for development.
 *
 * \author Marco Guazzone (marco.guazzone@gmail.com)
 *
 * <hr/>
 *
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


// Retrieved from Google's benchmark project (https://github.com/google/benchmark)
#if defined(__CYGWIN__)
# define PPQTN_OS_CYGWIN 1
#elif defined(_WIN32)
# define PPQTN_OS_WINDOWS 1
# if defined(__MINGW32__)
#  define PPQTN_OS_MINGW 1
# endif // __MINGW32__
#elif defined(__APPLE__)
# define PPQTN_OS_APPLE 1
# include "TargetConditionals.h"
# if defined(TARGET_OS_MAC)
#  define PPQTN_OS_MACOSX 1
#  if defined(TARGET_OS_IPHONE)
#   define PPQTN_OS_IOS 1
#  endif // TARGET_OS_IPHONE
# endif // TARGET_OS_MAC
#elif defined(__FreeBSD__)
# define PPQTN_OS_FREEBSD 1
#elif defined(__NetBSD__)
# define PPQTN_OS_NETBSD 1
#elif defined(__OpenBSD__)
# define PPQTN_OS_OPENBSD 1
#elif defined(__linux__)
# define PPQTN_OS_LINUX 1
#elif defined(__native_client__)
# define PPQTN_OS_NACL 1
#elif defined(__EMSCRIPTEN__)
# define PPQTN_OS_EMSCRIPTEN 1
#elif defined(__rtems__)
# define PPQTN_OS_RTEMS 1
#elif defined(__Fuchsia__)
# define PPQTN_OS_FUCHSIA 1
#elif defined (__SVR4) && defined (__sun)
# define PPQTN_OS_SOLARIS 1
#elif defined(__QNX__)
# define PPQTN_OS_QNX 1
#endif // PPQTN_OS_...

#define PPQTN_TOSTRING2_(x) #x
#define PPQTN_TOSTRING_(x) PPQTN_TOSTRING2_(x)
#define PPQTN_MAKE_PRAGME_MESSAGE_(x) __FILE__ "[" PPQTN_TOSTRING_(__LINE__) "]: " #x

// Usage: PPQTN_TODO(Remember to do this)
#if defined(__GNUC__) || defined(__clang__)
# define PPQTN_DO_PRAGMA_(x) _Pragma (#x)
# define PPQTN_TODO(x) PPQTN_DO_PRAGMA_(message ("TODO - " #x))
//# define PPQTN_FIXME(x) PPQTN_DO_PRAGMA_(message (PPQTN_MAKE_PRAGME_MESSAGE_("FIXME - " #x))) //FIXME: this add some extra quotes; e.g., _FIXME(Remember to fix this) becomes: file.cpp[#]: "FIXME - " "Remember to fix this"
# define PPQTN_FIXME(x) PPQTN_DO_PRAGMA_(message ("FIXME - " #x))
# define PPQTN_WARN(x) PPQTN_DO_PRAGMA_(message ("WARN - " #x))
#elif defined(_MSC_VER)
//# define PPQTN_DO_PRAGMA_(x) __pragma(message(__FILE__ "[" PPQTN_TOSTRING_(__LINE__) "]: " #x))
# define PPQTN_DO_PRAGMA_(x) __pragma(message(#x))
# define PPQTN_TODO(x) PPQTN_DO_PRAGMA_(message ("TODO - " #x))
# define PPQTN_FIXME(x) PPQTN_DO_PRAGMA_(message ("FIXME - " #x))
# define PPQTN_WARN(x) PPQTN_DO_PRAGMA_(message ("WARN - " #x))
#else
# define PPQTN_TODO(x) /* empty */
# define PPQTN_FIXME(x) /* empty */
# define PPQTN_WARN(x) /* empty */
#endif
