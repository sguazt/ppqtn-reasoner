/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstddef>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <ppqtn/benchmark.hpp>
#include <ppqtn/error.hpp>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_algo.hpp>
#include <ppqtn/graph_algo_orig.hpp> //XXX
#include <ppqtn/graph_generator.hpp>
#include <ppqtn/graph_generator_orig.hpp> //XXX
#include <ppqtn/graph_io.hpp>
#include <ppqtn/graph_ui.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/util.hpp>
#include <ppqtn/version.hpp>
#include <random>
#include <sstream>
#include <utility>


namespace {

// Default options:
const std::size_t default_bench_min_nreps = 3;
const std::size_t default_bench_max_nreps = std::numeric_limits<std::size_t>::max();
const bool default_outbidi = false;
const bool default_rand = false;
const std::size_t default_rand_linear_brlen = 2;
const bool default_rand_linear_limit_conslen = false;
const std::size_t default_rand_linear_max_conslen = 10;
const std::size_t default_rand_linear_max_dist = 100;
const std::size_t default_rand_linear_min_conslen = 1;
const std::size_t default_rand_linear_min_dist = 1;
const std::size_t default_rand_linear_nverts = 10;
const unsigned long default_rand_seed = 5489U;
const bool default_verbose = false;

// Prototypes:
void usage(const char* progname);
void print_exception(const std::exception& e);
void print_stacktrace(const std::exception& e, int level = 0);

//bool check_graph(const graph_t& g);

} // Unnamed namespace


int main(int argc, char* argv[])
{
    bool ok = true;
    std::string err_msg;

    // CLI options
    std::size_t opt_bench_max_nreps = default_bench_max_nreps;
    std::size_t opt_bench_min_nreps = default_bench_min_nreps;
    bool opt_help = false;
    bool opt_outbidi = default_outbidi;
    std::size_t opt_rand_linear_brlen = default_rand_linear_brlen;
    bool opt_rand_linear_limit_conslen = default_rand_linear_limit_conslen;
    std::size_t opt_rand_linear_max_conslen = default_rand_linear_max_conslen;
    std::size_t opt_rand_linear_max_dist = default_rand_linear_max_dist;
    std::size_t opt_rand_linear_min_conslen = default_rand_linear_min_conslen;
    std::size_t opt_rand_linear_min_dist = default_rand_linear_min_dist;
    std::size_t opt_rand_linear_nverts = default_rand_linear_nverts;
    unsigned long opt_rand_seed = default_rand_seed;
    bool opt_verbose = default_verbose;

    // Fixed settings as used in the paper (not changeable via CLI options)
    opt_outbidi = false;
    opt_rand_linear_brlen = 10;
    opt_rand_linear_limit_conslen = true;
    opt_rand_linear_max_conslen = 10;
    opt_rand_linear_max_dist = 100;
    opt_rand_linear_min_conslen = 1;
    opt_rand_linear_min_dist = 1;

    // Splash screen
    std::cout << "P+PQTN benchmark v." << PPQTN_VERSION_STR << std::endl;
 
    // Parse CLI options

    for (int arg = 1; arg < argc; ++arg)
    {
        if (!std::strcmp(argv[arg], "--bench-max-nreps"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_bench_max_nreps;
            }
            else
            {
                err_msg = "Malformed '--bench-max-nreps' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--bench-min-nreps"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_bench_min_nreps;
            }
            else
            {
                err_msg = "Malformed '--bench-min-nreps' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--help")
            || !std::strcmp(argv[arg], "-h"))
        {
            opt_help = true;
        }
        else if (!std::strcmp(argv[arg], "--nverts"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_linear_nverts;
            }
            else
            {
                err_msg = "Malformed '--nverts' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--rand-seed"))
        {
            ++arg;
            if (arg < argc)
            {
                std::istringstream iss(argv[arg]);
                iss >> opt_rand_seed;
            }
            else
            {
                err_msg = "Malformed '--rand-seed' argument";
                ok = false;
            }
        }
        else if (!std::strcmp(argv[arg], "--verbose"))
        {
            opt_verbose = true;
        }
        else
        {
            err_msg = "Unknown options '" + std::string(argv[arg]) + "'";
            ok = false;
        }
    }

    // Checks CLI options
    if (!ok)
    {
        log_error(err_msg);
        usage(argv[0]);
        return 1;
    }

    if (opt_verbose)
    {
        log_stream() << "-- Options:\n"
                     << "- opt_bench_max_nreps: " << opt_bench_max_nreps << '\n'
                     << "- opt_bench_min_nreps: " << opt_bench_min_nreps << '\n'
                     << "- opt_help: " << std::boolalpha << opt_help << std::noboolalpha << '\n'
                     << "- opt_nverts: " << opt_rand_linear_nverts << '\n'
                     << "- opt_rand_seed: " << opt_rand_seed << '\n'
                     << "- opt_verbose: " << std::boolalpha << opt_verbose << std::noboolalpha << '\n'
                     << "--------------------------------------------------------------------------------\n"
                     << std::flush;
    }

    if (opt_help)
    {
        usage(argv[0]);
        return 0;
    }

    try
    {
        std::unique_ptr<graph_generator_t> p_graph_gen;

        p_graph_gen = std::unique_ptr<random_linear_graph_generator_t<std::default_random_engine>>(
                            new random_linear_graph_generator_t<std::default_random_engine>(std::make_shared<std::default_random_engine>(opt_rand_seed),
                                                                                            opt_rand_linear_nverts,
                                                                                            opt_rand_linear_brlen,
                                                                                            opt_rand_linear_min_conslen,
                                                                                            opt_rand_linear_max_conslen,
                                                                                            opt_rand_linear_min_dist,
                                                                                            opt_rand_linear_max_dist,
                                                                                            opt_rand_linear_limit_conslen));

        benchmark_t bench;
        bench.min_num_repetitions(opt_bench_min_nreps);
        bench.max_num_repetitions(opt_bench_max_nreps);
        bench.verbose(opt_verbose);

        auto res = bench.run(*p_graph_gen);

        if (opt_verbose)
        {
            auto const& cpu_time_stats = res.cpu_time_stats();
            auto const& real_time_stats = res.real_time_stats();

            log_stream() << "CPU time: " << cpu_time_stats.mean() << " (s.d. " << cpu_time_stats.standard_deviation() << ")" << '\n'
                         << "Real time: " << real_time_stats.mean() << " (s.d. " << real_time_stats.standard_deviation() << ")" << '\n'
                         << std::flush;

        }
    }
    catch (const std::exception& e)
    {
        print_exception(e);
        std::terminate();
    }
    catch (...)
    {
        fatal_error("Unexpected error");
    }
}


namespace {

void usage(const char* progname)
{
    std::cout << "Usage: " << progname << " [options]\n"
              << "Options:\n"
              << "--bench-max-nreps <value>\n"
              << "  Executes the benchmark at most <value> times (even if the target relative precision has not been reached).\n"
              << "  [default: " << default_bench_max_nreps << "]\n"
              << "--bench-min-nreps <value>\n"
              << "  Executes the benchmark at least <value> times.\n"
              << "  [default: " << default_bench_min_nreps << "]\n"
              << "--nverts <value>\n"
              << "  In a randomly generated graph, sets the number of vertices to <value>.\n"
              << "  [default: " << default_rand_linear_nverts << "]\n"
              << "--rand-seed <value>\n"
              << "  Seed for the random number generator.\n"
              << "  [default: " << default_rand_seed << "]\n"
              << "--verbose\n"
              << "  Enables message verbosity.\n"
              << "  [default: " << (default_verbose ? "enabled" : "disabled") << "]\n"
              << '\n';
}

void print_exception(const std::exception& e)
{
    print_stacktrace(e);
}

void print_stacktrace(const std::exception& e, int level)
{
    std::ostringstream oss;
    oss << std::string(level, ' ') << "Exception: " << e.what();
    log_error(oss.str());

    try
    {
        std::rethrow_if_nested(e);
    }
    catch(const std::exception& e)
    {
        print_stacktrace(e, level+1);
    }
    catch(...)
    {
        std::ostringstream oss;
        oss << std::string(level, ' ') << "Unknow error.";
        log_error(oss.str());
    }
}

} // Unnamed namespace
