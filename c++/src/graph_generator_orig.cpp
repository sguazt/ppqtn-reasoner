/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Antonella Andolina, Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdlib>
# include <iostream>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_generator_orig.hpp>


namespace {

void    GeneraMatrice(unsigned int seed, int d, int dimV, punt** mat);  //generazione casuale della matrice
void	GeneraDistanze(punt p, int dim);								//generatore casuale di vincoli consistenti
void	GeneraProb (punt p, int dim);									//generatore casuale di probabilita'
void	CaricaDistanze (punt p, punt q, int dim, int esiste_prob, int esiste_pref);		//generatore manuale di vincoli consistenti
double	CalcolaCompoProb(punt p, punt q,  int i, int j);				//calcola la probabilità di un elemento nella composizione
double	CalcolaCompoPref(punt p, punt q, int i, int j);					//calcola la preferenza di un elemento nella composizione
void	ComponiDistanze (punt p, punt q, punt ris);						//operazione di composizione di due vettori di vincoli

} // Unnamed namespace


void random_graph_orig(unsigned long seed, std::size_t nv, graph_t& g)
{
    const int d = 3;		// costante dimensione della matrice. 
                            // ATTENZIONE!!!!
                            // Inserire il giusto numero di nodi prima dell'esecuzione!!!!



    const int dimV=20;		// costante dimensione massima delle terne nei vincoli.
                            // ATTENZIONE!!!
                            // Prima dell'esecuzione assicurarsi che il valore non sia
                            // inferiore all'effettivo numero massimo di terne nei vincoli!
                            // Se maggiore non crea problemi.

    graph_t graph(nv);
    GeneraMatrice(static_cast<unsigned int>(seed), d, dimV, graph.adjacency_matrix());
    g = graph;
}


namespace {

using namespace std;

void GeneraMatrice(unsigned int seed, int d, int dimV, punt** mat)
{
	//////////////////////////////////////////////////
	//												//
	//		generazione casuale della matrice		//
	//												//
	//					ATTENZIONE!!!!				//
	//		NON AGGIORNATA CON LE PREFERENZE!!!		//
	//												//
	/////////////////////////////////////////////////
	int i;
	int j;
	int k;
	int dim;
	int newdim;
	punt p;
	punt q;
	punt aux;
	punt compo;
	double acc;
	//int elimina;
	int aggiungi;
	int lato;
	int base;
	int pos;
	


			//inizializzazione della matrice mettendo distanza 0 con probabilita' 1 sulla diagonale
			//e infinito nelle altre celle;

	for (i=0; i < d; i++)    
	{
		for (j=0;  j < d; j++)
		{
			if (i == j )  
			{ //cella sulla diagonale quindi  distanza 0 con probabilita' 1
				mat[i][j] = new terne[2];  //2 elementi: 1 per il numero di vincoli, l'altro per il vincolo vero e proprio
				if (mat[i][j] != NULL)
				{
					p = mat[i][j];
					p[0].dist = 1;
					p[1].dist = 0;
					p[1].prob = 1;
				}
				else 
				{
					cout << "\n Generazione Vettore non riuscita 1";
					//system ("pause");
				}
			}

			else
			{   //altre celle: inizializzate a infinito (numero di vincoli 0!)

				mat[i][j] = new terne[1];
				if (mat[i][j] != NULL)
					mat[i][j][0].dist = 0;
				else 
				{
					cout << "\n Generazione Vettore non riuscita 2";
					//system ("pause");
				}
			}
		}
	}	//fine inizializzazione matrice;

	//StampaMatrice(d,d);  //da cancellare

			    // caricamento vincoli  nodi consecutivi (i e i+1)
				// ottenuta mediante generazione casuale della dimensione (compresa fra 0 e 10),
				// generazione casuale dei valori CONSECUTIVI delle distanze (passo compreso fra 0 e 100 e vettore centrato sul passo)
				// e generazione casuale/normalizzazione delle probabilita' (reali compresi tra 0 e 1)
		
	//[marco]
	//srand(time(NULL));	//inizializzatore del seme del generatore semicasuale;
	srand(seed);	//inizializzatore del seme del generatore semicasuale;
	//[/marco]
	for (i=0; i<d-1; i++)
	{
		dim = (rand()% dimV)+1;	//generatore casuale di un numero compreso tra 1 e la dimensione massima del vettore dei vincoli
		if (mat[i][i+1] != NULL)
			delete []mat[i][i+1];
		mat[i][i+1] = new terne[dim+1];	//genera in modo casuale il nuovo vettore delle distanze [i][i+1]
		if (mat[i][i+1] != NULL)
		{
			p = mat[i][i+1];
			GeneraDistanze (p, dim); 
			GeneraProb (p, dim);			//fine generazione casuale del vettore [i][i+1]
			delete []mat[i+1][i];			//generazione del vettore speculare [i+1][i] con distanze invertite
			mat[i+1][i] = new terne[dim+1];
			if (mat[i+1][i] != NULL)
			{
				q = mat[i+1][i];
				q[0].dist=dim;
				for (k =1; k <= dim; k++)
				{
					q[dim-k+1].dist = p[k].dist * (-1);
					q[dim-k+1].prob = p[k].prob;
				}
			}
			else
			{
				cout <<"\nGenerazione non riuscita !!";
				//system("pause");
			}
		}
		else 
		{
			cout << "\n Generazione Vettore non riuscita3";
			//system ("pause");
		}
	}	//fine caricamento vincoli nodi consecutivi

	//StampaMatrice(d,d);  //da cancellare

			//caricamento vincoli nodi con distanza 1 (i e i+2)
			//ottenuta mediante composizione dei nodi (i, i+1) e (i+1 e i+2)
	
	for (i=0; i < d-2 ; i++)
	{
		p = mat[i][i+1];
		q = mat[i+1][i+2];
		if ((p[0].dist != 0) && (q[0].dist != 0))	//se entrambe i vettori sono pieni il vettore mat[i][i+2] si ottiene dalla composizione
		{											//altrimenti e' infinito e il vettore mat[i][i+2] non viene modificato
									
			dim =  (p[p[0].dist].dist + q[q[0].dist].dist) - (p[1].dist + q[1].dist) +1;	// valuta la dimensione del vettore composizione come
																							// somma dei massimi - somma dei minimi +1
		
			delete []mat[i][i+2];		//cancella vecchio vettore
			mat[i][i+2] = new terne[dim +1];  //crea il nuovo vettore composizione
			if (mat[i][i+2] != NULL)
			{
				compo = mat[i][i+2];
				compo[0].dist = dim;
				ComponiDistanze (p,q, compo);
			
						//modifica composizione con eliminazione di alcuni elementi determinati con generazione casuale

				aggiungi = rand()% 4;	//generatore casuale del numero di elementi da aggiungere (numero compreso tra 0 e 3)
				lato = rand ()%2;		//generatore casuale del lato da cui eliminare gli elementi: 
										//0 si aggiunge in testa e si cancella in coda, 1 si cancella in testa e si aggiunge in coda
		
				newdim = dim + aggiungi;
				compo = new terne[newdim+1];
				if (compo != NULL)
				{
					acc = 0;
					base = mat[i][i+2][1].dist;
					compo[0].dist = newdim;
					if ((lato == 0) && (base-aggiungi > 0))
					{
						//si aggiunge in testa e si cancella in coda
						base = base - aggiungi-1;
						for (k=1; k<=aggiungi; k++)
						{
							compo[k].dist = base+k;
							compo[k].prob = rand() / (double)RAND_MAX;
						}
						pos = newdim - aggiungi;
						for (k= 1; k <= pos; k++)
						{
							compo[aggiungi+k].dist = mat[i][i+2][k].dist;
							compo[aggiungi+k].prob = mat[i][i+2][k].prob;
						}
					}
					else
					{
						for (k= 1 ; k<= dim ; k++)
						{
							compo[k].dist = mat[i][i+2][k].dist;
							compo[k].prob = mat[i][i+2][k].prob;
						}
						pos = dim;
						base = compo[pos].dist;
						for (k= 1; k<= aggiungi; k++)
						{
							compo[pos+k].dist = base + k; 
							compo[pos+k].prob = rand() / (double)RAND_MAX;
						}
					}
					if (mat[i][i+2] != NULL)
						delete []mat[i][i+2];
					mat[i][i+2] = new terne[newdim+1]; 
					aux=mat[i][i+2];
					if (mat[i][i+2] != NULL)
					{
						aux[0].dist = compo[0].dist;
						acc=0;
						for (k=1; k<=newdim; k++)
						{
							aux[k].dist = compo[k].dist;
							aux[k].prob = compo[k].prob;
							acc = acc + aux[k].prob;		//somma le nuove probabilita'
						}
						delete []compo;
						for (k=1; k<=newdim; k++)	//normalizza le probabilita'
							aux[k].prob = aux[k].prob/acc;

						delete []mat[i+2][i];			//generazione del vettore speculare [i+1][i] con distanze invertite
						mat[i+2][i] = new terne[newdim+1];
						if (mat[i+2][i] != NULL)
						{
							q = mat[i+2][i];
							q[0].dist= newdim;
							for (k =1; k <= newdim; k++)
							{
								q[newdim-k+1].dist = aux[k].dist * (-1);
								q[newdim-k+1].prob = aux[k].prob;
							}
						}
						else
						{
							cout <<"\nGenerazione non riuscita !!";
							//system("pause");
						}


					}
					else
					{
						cout << "\n Generazione Vettore non riuscita15";
						//system ("pause");
					}
				}
				else
				{
					cout << "\n Generazione Vettore non riuscita4";
					//system ("pause");
				}
			}	//fine modifica vettore composizione
			else
			{
				cout << "\n Generazione Vettore non riuscita5";
				//system ("pause");
			}
		} // fine vettori pieni 
			
	}  // fine caricamento vincoli nodi con distanza 1 

	//StampaMatrice(d,d);  //da cancellare
}


void GeneraDistanze (punt p, int dim)
{
	// generatore casuale di vincoli consistenti 
	// NON AGGIORNATA CON LE PREFERENZE!

	int k;
	int passo;
	int base;
	int meta; 

	p[0].dist = dim;		//assegnamento al primo elemento del vettore del numero di terne del vincolo
	passo = rand()% 101;	//generatore casuale di un numero compreso tra 0 e 100 (per determinare i valori consecutivi del vettore)
	meta = (int) dim/2;		//meta' della dimensione. I valori del vettore si centrano sul valore del passo 
							//e andranno da passo + meta' in poi (MA NON E' NECESSARIO! POTREBBERO ANDARE DA PASSO IN AVANTI
	base = passo - meta;	//valore iniziale del primo elemento del vettore (che verra' incrementato di uno per i successivi)
	for (k=0; k<dim; k++)
		p[k+1].dist=base+k;
			
}


void GeneraProb (punt p, int dim)
{
	// generatore casuale di probabilità
	// NON AGGIORNATO ALLA POSSIBILITA' DI NON AVERE PROBABILITA'NEL VINCOLO!!

	int k;
	double acc;

	acc = 0;
	for (k=1; k <= dim; k++)  //genera le probabilita' in modo casuale
	{
		p[k].prob = rand() / (double)RAND_MAX;  //generatore casuale di un numero reale tra 0 e 1 compreso
		acc = acc + p[k].prob;
	}

	for (k=1; k <= dim; k++)  //normalizza le probabilita' in modo che la somma sia 1
	{
		p[k].prob = p[k].prob/acc;
	}
}


double CalcolaCompoProb(punt p, punt q,  int i, int j)
{
	//Calcola il valore della probabilità di un elemento nella composizione


	double nuovo_contributo;

	if (p[0].prob == 1 && q[0].prob == 1)	//ci sono le probabilità in entrambi i vincoli
		nuovo_contributo = p[i].prob * q[j].prob;
	else
		if (p[0].prob == -1 && q[0].prob == -1)		//non c'è la probabilità in nessuno dei due vincoli
			nuovo_contributo = (1.0/p[0].dist) * (1.0/q[0].dist);
		else

			//c'è la probabilità in un solo vincolo
			if (p[0].prob == 1)		//cè solo la probabilità nel primo vincolo
				nuovo_contributo = p[i].prob / q[0].dist;

			else	//c'è la probabilità solo nel secondo
				nuovo_contributo = q[j].prob / p[0].dist;


	return nuovo_contributo;
}



double CalcolaCompoPref(punt p, punt q, int i, int j)
{
	//Calcola il valore della preferenza di un elemento nella composizione
	double nuovo_contributo;

	if (p[0].pref == 1 && q[0].pref == 1)	//ci sono le preferenze in entrambi i vincoli
		nuovo_contributo = min(p[i].pref, q[j].pref);
	else
		if (p[0].pref == -1 && q[0].pref == -1)		//non c'è la preferenza in nessuno dei due vincoli
			nuovo_contributo = p[i].pref;
		else

			//c'è la preferenza in un solo vincolo
			if (p[0].pref == 1)		//cè solo la preferenza nel primo vincolo
				nuovo_contributo = p[i].pref;

			else	//c'è la probabilità solo nel secondo
				nuovo_contributo = q[j].pref;


	return nuovo_contributo;
}
 


void ComponiDistanze(punt p, punt q, punt ris)
{
	// operazione di composizione di due vettori di vincoli


	int i;		//indice del primo vettore
	int j;		//indice del secondo vettore
	int dim1;	//dimensione del primo vettore
	int dim2;	//dimensione del secondo vettore
	int pos;	//indice della nuova posizione
	terne nuovo[1];	//elemento ausiliare per costruire il risultato
	int inizio;		//primo valore delle distanze composte

	if ((p[0].dist == 0) || (q[0].dist == 0))   //uno dei due vettori e' vuoto: risultato vuoto {elemento che azzera la composizione}
	{
		ris[0].dist= 0;
		ris[0].prob = -1;
		ris[0].pref = -1;
	}
	else
	{			//entrambi i vettori pieni

		dim1 = p[0].dist;
		dim2 = q[0].dist;

		ris[0].prob = 1;	//se entrambi i vincoli ci sono la composizione dà sempre probabilità

		if (p[0].pref == -1 && q[0].pref == -1)			//manca la preferenza in entrambi i vincoli
			ris[0].pref = -1;							//anche il risultato non ha preferenze
		else											//se almeno uno dei due vincoli ha le preferenze
			ris[0].pref = 1;							//il risultato ha la preferenza

		
		inizio = p[1].dist + q[1].dist;		//calcolo primo valore delle distanze composte

		//inizializzazione del vettore risultato

			// DA QUI IN AVANTI VIENE FATTA L'IPOTESI CHE I VETTORI SIANO CONTINUI E ORDINATI 
			// SE CAMBIA L'IPOTESI SI DEVE COSTRUIRE IL RISULTATO IN MANIERA DIFFERENTE:
			// SOMMARE OGNI ELEMENTO DEL PRIMO VETTORE CON TUTTI GLI ELEMENTI DEL SECONDO VETTORE,  
			// PER OGNI SOMMA SI DEVE FARE LA COMPOSIZIONE OTTENENDO NUOVO 
			// E POI CERCARE NUOVO[0].DIST IN TUTTO IL VETTORE RIS
			// SE VIENE TROVATO SI AGGIORNANO PREFERENZE E PROBABILITA'
			// SE NON VIENE TROVATO SI INCREMENTA PIENO DI 1 E A RIS[PIENO].DIST SI ASSEGNA IL VALORE DI NUOVO[0].DIST 
			// E A RIS[PIENO].PROB SI ASSEGNA IL VALORE DI NUOVO[0].PROB E A RIS[PIENO].PREF SI ASSEGNA IL VALORE DI NUOVO[0].PREF
			// NEL CASO DI NUOVA IPOTESI GUARDARE LA SOLUZIONE USATA NEL PRIMO ARTICOLO E ADATTARLA ALLE PREFERENZE!!!!

		if (ris[0].pref == -1)		//il risultato non ha preferenze
		{ 
			//inserisco nel vettore risultato tutte le distanze della composizione
			//inizializzando la probabilità a zero e la preferenza a null;

			for (i=1; i<= ris[0].dist ; i++)
			{
				ris[i].dist = inizio +i -1;
				ris[i].prob = 0;
			}
		}
		else		//il risultato ha preferenze
		{
			///inserisco nel vettore risultato tutte le distanze della composizione
			//inizializzando la probabilità a zero e la preferenza a 0 (elemento neutro di somma e max);
			for (i=1; i<= ris[0].dist ; i++)
			{
				ris[i].dist = inizio + i -1;
				ris[i].prob = 0;
				ris[i].pref = 0;
			}
		}
		for (i=1; i<=dim1; i++)
			for (j=1; j<= dim2; j++)
			{
				nuovo[0].dist = p[i].dist + q[j].dist;
				nuovo[0].prob = CalcolaCompoProb(p,q, i, j);  
				nuovo[0].pref = CalcolaCompoPref(p,q,i,j);

			  
			//							ATTENZIONE!!!!
			// LE OPERAZIONI SEGUENTI SI BASANO SULL'IPOTESI CHE I VETTORI SIANO CONTINUI E ORDINATI! 
			// SE CAMBIA QUESTA IPOTESI SI DEVE MODIFICARE IL PROCEDIMENTO:
			// INVECE DI CONTROLLARE CHE IL VALORE SUL QUALE STIAMO AGENDO SIA QUELLO CHE CI ASPETTIAMO
			// DOBBIAMO CERCARE NUOVO[0].DIST IN TUTTO IL VETTORE RIS
			// SE VIENE TROVATO SI SOMMA LA PROBABILITA' PRECEDENTE CON IL NUOVO CONTRIBUTO 
			// E SI FA IL MASSIMO DELLA PRECEDENTE PREFERENZA CON IL NUOVO CONTRIBUTO,
			// SE NON VIENE TROVATO SI AGGIUNGE UN NUOVO VALORE A RIS (CHIAMIAMOLO PIENO) E
			// A RIS[PIENO].DIST SI ASSEGNA IL VALORE DI NUOVO[0].DIST, 
			// A RIS[PIENO].PROB SI ASSEGNA IL VALORE DI NUOVO[0].PROB E 
			// A RIS[PIENO].PREF SI ASSEGNA IL VALORE DI NUOVO[0].PREF
		

				pos = i+j-1;
				if 	(ris[pos].dist!= nuovo[0].dist)   //la distanza da trattare è diversa da quella che ci aspettiamo
				{
					cout << "c'è qualcosa che non va! Distanza nella COMPOSIZIONE non corrispondente";
					//system ("pause");
				}

				else
				{
					ris[pos].prob = ris[pos].prob + nuovo[0].prob;		//somma del precedente valore con il nuovo contributo
					ris[pos].pref = max(ris[pos].pref, nuovo[0].pref);	//massimo tra il valore precedente e il nuovo contributo
				}
				
			}
	}
}

} // Unnamed namespace
