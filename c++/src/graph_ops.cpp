/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_ops.hpp>

#ifdef PPQTN_DEBUG
# include <sstream>
# include <ppqtn/log.hpp>
#endif // PPQTN_DEBUG


namespace {

/// Given a constraint t1<...>t2, performs query "(t1-t2) = d", where d is the distance offset.
std::tuple<double,double> query_eq_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset = 0);
/// Given a constraint t1<...>t2, performs query "(t1-t2) >= d", where d is the distance offset.
std::tuple<double,double> query_ge_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset = 0);
/// Given a constraint t1<...>t2, performs query "(t1-t2) > d", where d is the distance offset.
std::tuple<double,double> query_gt_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset = 0);
/// Given a constraint t1<...>t2, performs query "(t1-t2) <= d", where d is the distance offset.
std::tuple<double,double> query_le_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset = 0);
/// Given a constraint t1<...>t2, performs query "(t1-t2) < d", where d is the distance offset.
std::tuple<double,double> query_lt_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset = 0);
/// Given a constraint t1<...>t2, performs query "(t1-t2) != d", where d is the distance offset.
std::tuple<double,double> query_ne_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset = 0);

} // Unnamed namespace


//----------------------------
// POINT-POINT QUERIES
//----------------------------


std::tuple<double,double> query_eq(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex)
{
    return query_eq_impl(graph, left_vertex, right_vertex);
}

std::tuple<double,double> query_ge(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex)
{
    return query_ge_impl(graph, left_vertex, right_vertex);
}

std::tuple<double,double> query_gt(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex)
{
    return query_gt_impl(graph, left_vertex, right_vertex);
}

std::tuple<double,double> query_le(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex)
{
    return query_le_impl(graph, left_vertex, right_vertex);
}

std::tuple<double,double> query_lt(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex)
{
    return query_lt_impl(graph, left_vertex, right_vertex);
}

std::tuple<double,double> query_ne(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex)
{
    return query_ne_impl(graph, left_vertex, right_vertex);
}


//----------------------------
// INTERVAL-INTERVAL QUERIES
//----------------------------


std::tuple<double,double> query_after(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY AFTER(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        return query_lt_impl(graph, event2_right_vertex, event1_left_vertex, -1); // end(e2)+1 < start(e1)
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_before(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY BEFORE(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        return query_lt_impl(graph, event1_right_vertex, event2_left_vertex, -1); // end(e1)+1 < start(e2)
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_contains(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY CONTAINS(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_lt_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) < start(e2)
        auto res2 = query_gt_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) > end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2),
                               std::min(std::get<1>(res1), std::get<1>(res2)));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_disjoint(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY DISJOINT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res_after = query_after(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // after(e1, e2)
        auto res_before = query_before(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // before(e1, e2)
        auto res_meets = query_meets(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // meets(e1, e2)
        auto res_met_by = query_met_by(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // met-by(e1, e2)

        return std::make_tuple(std::get<0>(res_after) + std::get<0>(res_before) + std::get<0>(res_meets) + std::get<0>(res_met_by),
                               std::max({std::get<1>(res_after),
                                         std::get<1>(res_before),
                                         std::get<1>(res_meets),
                                         std::get<1>(res_met_by)}));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_during(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY DURING(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_gt_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) > start(e2)
        auto res2 = query_lt_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) < end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2),
                               std::min(std::get<1>(res1), std::get<1>(res2)));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_ended_by(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY ENDED-BY(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_lt_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) < start(e2)
        auto res2 = query_eq_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) = end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2),
                               std::min(std::get<1>(res1), std::get<1>(res2)));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_ends(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY ENDS(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_gt_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) > start(e2)
        auto res2 = query_eq_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) = end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2),
                               std::min(std::get<1>(res1), std::get<1>(res2)));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_equal(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY EQUAL(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_eq_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) = start(e2)
        auto res2 = query_eq_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) = end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2),
                               std::min(std::get<1>(res1), std::get<1>(res2)));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_intersect(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res_contains = query_contains(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // contains(e1, e2)
        auto res_disjoint = query_disjoint(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // disjoint(e1, e2)
        auto res_during = query_during(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // during(e1, e2)
        auto res_ended_by = query_ended_by(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // ended-by(e1, e2)
        auto res_ends = query_ends(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // ends(e1, e2)
        auto res_equal = query_equal(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // equal(e1, e2)
        auto res_overlapped_by = query_overlapped_by(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // overlapped-by(e1, e2)
        auto res_overlaps = query_overlaps(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // overlaps(e1, e2)
        auto res_started_by = query_started_by(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // started-by(e1, e2)
        auto res_starts = query_starts(graph, event1_left_vertex, event1_right_vertex, event2_left_vertex, event2_right_vertex); // starts(e1, e2)

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - CONTAINS - prob: " << std::get<0>(res_contains) << ", pref: " << std::get<1>(res_contains);
    log_debug(oss.str()); 
    oss.str("");
    oss.clear();
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - DURING - prob: " << std::get<0>(res_during) << ", pref: " << std::get<1>(res_during);
    log_debug(oss.str()); 
    oss.str("");
    oss.clear();
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - ENDED-BY - prob: " << std::get<0>(res_ended_by) << ", pref: " << std::get<1>(res_ended_by);
    log_debug(oss.str()); 
    oss.str("");
    oss.clear();
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - ENDS - prob: " << std::get<0>(res_ends) << ", pref: " << std::get<1>(res_ends);
    log_debug(oss.str()); 
    oss.str("");
    oss.clear();
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - EQUAL - prob: " << std::get<0>(res_equal) << ", pref: " << std::get<1>(res_equal);
    log_debug(oss.str()); 
    oss.str("");
    oss.clear();
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - OVERLAPPED-BY - prob: " << std::get<0>(res_overlapped_by) << ", pref: " << std::get<1>(res_overlapped_by);
    log_debug(oss.str()); 
    oss.str("");
    oss.clear();
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - OVERLAPS - prob: " << std::get<0>(res_overlaps) << ", pref: " << std::get<1>(res_overlaps);
    log_debug(oss.str()); 
    oss.str("");
    oss.clear();
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - STARTED-BY - prob: " << std::get<0>(res_started_by) << ", pref: " << std::get<1>(res_started_by);
    log_debug(oss.str()); 
    oss.str("");
    oss.clear();
    oss << "QUERY INTERSECT(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") - STARTS - prob: " << std::get<0>(res_starts) << ", pref: " << std::get<1>(res_starts);
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

        return std::make_tuple(1 - std::get<0>(res_disjoint),
                               std::max({std::get<1>(res_contains),
                                         std::get<1>(res_during),
                                         std::get<1>(res_ended_by),
                                         std::get<1>(res_ends),
                                         std::get<1>(res_equal),
                                         std::get<1>(res_overlapped_by),
                                         std::get<1>(res_overlaps),
                                         std::get<1>(res_started_by),
                                         std::get<1>(res_starts)}));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_meets(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY MEETS(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        return query_eq_impl(graph, event1_right_vertex, event2_left_vertex, -1); // end(e1)+1 = start(e2)
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_met_by(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY MET-BY(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        return query_eq_impl(graph, event2_right_vertex, event1_left_vertex, -1); // end(e2)+1 = start(e1)
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_overlapped_by(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY OVERLAPPED-BY(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_gt_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) > start(e2)
        auto res2 = query_le_impl(graph, event1_left_vertex, event2_right_vertex); // start(e1) <= end(e2)
        auto res3 = query_gt_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) > end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2) * std::get<0>(res3),
                               std::min({std::get<1>(res1),
                                         std::get<1>(res2),
                                         std::get<1>(res3)}));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_overlaps(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY OVERLAPS(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_lt_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) < start(e2)
        auto res2 = query_ge_impl(graph, event1_right_vertex, event2_left_vertex); // end(e1) >= start(e2)
        auto res3 = query_lt_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) < end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2) * std::get<0>(res3),
                               std::min({std::get<1>(res1),
                                         std::get<1>(res2),
                                         std::get<1>(res3)}));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_started_by(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY STARTED-BY(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_eq_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) = start(e2)
        auto res2 = query_gt_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) > end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2),
                               std::min(std::get<1>(res1), std::get<1>(res2)));
    }

    return std::make_tuple(0.0, 0.0);
}

std::tuple<double,double> query_starts(const graph_t& graph, std::size_t event1_left_vertex, std::size_t event1_right_vertex, std::size_t event2_left_vertex, std::size_t event2_right_vertex)
{
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY STARTS(" << event1_left_vertex << ", " << event1_right_vertex << ", " << event2_left_vertex << ", " << event2_right_vertex << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    if (graph.has_edge(event1_left_vertex, event1_right_vertex)
        && graph.has_edge(event2_left_vertex, event2_right_vertex))
    {
        auto res1 = query_eq_impl(graph, event1_left_vertex, event2_left_vertex); // start(e1) = start(e2)
        auto res2 = query_lt_impl(graph, event1_right_vertex, event2_right_vertex); // end(e1) < end(e2)

        return std::make_tuple(std::get<0>(res1) * std::get<0>(res2),
                               std::min(std::get<1>(res1), std::get<1>(res2)));
    }

    return std::make_tuple(0.0, 0.0);
}


namespace {

std::tuple<double,double> query_eq_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset)
{
    double prob = 0;
    double pref = 0;

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY EQ(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG
    if (graph.has_edge(left_vertex, right_vertex))
    {
        auto const triples = graph(left_vertex, right_vertex);
        auto const nt = triples[0].dist;
        auto const neg_dist_offset= -dist_offset;
        for (int k = 1; k <= nt; ++k)
        {
            if (triples[k].dist == neg_dist_offset)
            {
                if (triples[k].is_prob_undef())
                {
                    prob = 1.0/nt;
                }
                else
                {
                    prob = triples[k].prob;
                }
                pref = triples[k].pref;
                break;
            }
        }
    }
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY EQ(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") EXIT";
    oss << " --> prob: " << prob << ", pref: " << pref;
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    return std::make_tuple(prob, pref);
}

std::tuple<double,double> query_ge_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset)
{
    //NOTE: given the temporal constraint:
    //        t_1 <(d_1,p_1,P_1),...,(d_k,p_k,P_k)> t_2,
    //      (t_1-t_2) >= d iif there exists a d_i <= -d, where d is the distance offset

    //NOTE: initially, we decided that if in the computation of Pref(t1 >= t2) = MAX(Pref(t1 > t2),Pref(t1 = t2)),
    //      one of two operands of MAX() is undefined and the other is not, we take as result the defined preference.
    //      However, this rule is not consistent if we compute Pref(t1 >= t2) directly (i.e., whithout using
    //      Pref(t1 > t2) and Pref(t1 = t2)) as follows:
    //        Pref(t2 >= t1) = MAX_{d in D}(sigma(d)), if D={d | d in {d1,...,dk} and d > 0} != empty and sigma(d) != #,
    //                         #,                      if D={d | d in {d1,...,dk} and d > 0} != empty and sigma(d) = #,
    //                         0,                      otherwise.
    //      For example, given the following constraint t1 <(0,0.5,#), (1,0.4,#), (2,0.1,#)> t2, then the computation
    //      of Pref(t1 >= t2) with the indirect and direct methods leads to different results:
    //      - indirect method: Pref(t1 >= t2) = MAX(Pref(t1 > t2), Pref(t1 = t2)) = MAX(0,#) = 0
    //      - direct method: Pref(t1 >= t2) = #
    //      In the following, we use the direct method.

    double prob = 0;
    double pref = 0;

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY GE(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG
    if (graph.has_edge(left_vertex, right_vertex))
    {
        auto const triples = graph(left_vertex, right_vertex);
        auto const nt = triples[0].dist;
        auto const neg_dist_offset = -dist_offset;
        for (int k = 1; k <= nt; ++k)
        {
            if (triples[k].dist <= neg_dist_offset)
            {
                if (triples[k].is_prob_undef())
                {
                    prob += 1.0/nt;
                }
                else
                {
                    prob += triples[k].prob;
                }
                if (triples[k].is_pref_undef())
                {
                    pref = triples[k].pref;
                }
                else
                {
                    pref = std::max(pref, triples[k].pref);
                }
            }
        }
    }
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY GE(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") EXIT";
    oss << " --> prob: " << prob << ", pref: " << pref;
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    return std::make_tuple(prob, pref);
}

std::tuple<double,double> query_gt_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset)
{
    //NOTE: given the temporal constraint:
    //        t_1 <(d_1,p_1,P_1),...,(d_k,p_k,P_k)> t_2,
    //      (t_1-t_2) > d iif there exists a d_i < -d, where d is the distance offset

    double prob = 0;
    double pref = 0;

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY GT(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG
    if (graph.has_edge(left_vertex, right_vertex))
    {
        auto const triples = graph(left_vertex, right_vertex);
        auto const nt = triples[0].dist;
        auto const neg_dist_offset = -dist_offset;
        for (int k = 1; k <= nt; ++k)
        {
            if (triples[k].dist < neg_dist_offset)
            {
                if (triples[k].is_prob_undef())
                {
                    prob += 1.0/nt;
                }
                else
                {
                    prob += triples[k].prob;
                }
                if (triples[k].is_pref_undef())
                {
                    pref = triples[k].pref;
                }
                else
                {
                    pref = std::max(pref, triples[k].pref);
                }
            }
        }
    }
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY GT(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") EXIT";
    oss << " --> prob: " << prob << ", pref: " << pref;
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    return std::make_tuple(prob, pref);
}

std::tuple<double,double> query_le_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset)
{
    //NOTE: given the temporal constraint:
    //        t_1 <(d_1,p_1,P_1),...,(d_k,p_k,P_k)> t_2,
    //      (t_1-t_2) <= d iif there exists a d_i >= -d, where d is the distance offset

    double prob = 0;
    double pref = 0;

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY LE(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG
    if (graph.has_edge(left_vertex, right_vertex))
    {
        auto const triples = graph(left_vertex, right_vertex);
        auto const nt = triples[0].dist;
        auto const neg_dist_offset = -dist_offset;
        for (int k = 1; k <= nt; ++k)
        {
            if (triples[k].dist >= neg_dist_offset)
            {
                if (triples[k].is_prob_undef())
                {
                    prob += 1.0/nt;
                }
                else
                {
                    prob += triples[k].prob;
                }
                if (triples[k].is_pref_undef())
                {
                    pref = triples[k].pref;
                }
                else
                {
                    pref = std::max(pref, triples[k].pref);
                }
            }
        }
    }
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY LE(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") EXIT";
    oss << " --> prob: " << prob << ", pref: " << pref;
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    return std::make_tuple(prob, pref);
}

std::tuple<double,double> query_lt_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset)
{
    //NOTE: given the temporal constraint:
    //        t_1 <(d_1,p_1,P_1),...,(d_k,p_k,P_k)> t_2,
    //      (t_1-t_2) < d iif there exists a d_i > -d, where d is the distance offset

    double prob = 0;
    double pref = 0;

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY LT(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG
    if (graph.has_edge(left_vertex, right_vertex))
    {
        auto const triples = graph(left_vertex, right_vertex);
        auto const nt = triples[0].dist;
        auto const neg_dist_offset = -dist_offset;
        for (int k = 1; k <= nt; ++k)
        {
#ifdef PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "QUERY LT(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") - k: " << k << ", triple: (" << triples[k].dist << ", " << triples[k].prob << ", " << triples[k].pref << ") -> Included? " << std::boolalpha << (triples[k].dist > dist_offset) << std::noboolalpha;
            log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG
            if (triples[k].dist > neg_dist_offset)
            {
                if (triples[k].is_prob_undef())
                {
                    prob += 1.0/nt;
                }
                else
                {
                    prob += triples[k].prob;
                }
                if (triples[k].is_pref_undef())
                {
                    pref = triples[k].pref;
                }
                else
                {
                    pref = std::max(pref, triples[k].pref);
                }
            }
        }
    }
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY LT(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") EXIT";
    oss << " --> prob: " << prob << ", pref: " << pref;
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    return std::make_tuple(prob, pref);
}

std::tuple<double,double> query_ne_impl(const graph_t& graph, std::size_t left_vertex, std::size_t right_vertex, int dist_offset)
{
    double prob = 0;
    double pref = 0;

#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY NE(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") ENTER";
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG
    if (graph.has_edge(left_vertex, right_vertex))
    {
        auto const triples = graph(left_vertex, right_vertex);
        auto const nt = triples[0].dist;
        auto const neg_dist_offset = -dist_offset;
        for (int k = 1; k <= nt; ++k)
        {
            if (triples[k].dist != neg_dist_offset)
            {
                if (triples[k].is_prob_undef())
                {
                    prob += 1.0/nt;
                }
                else
                {
                    prob += triples[k].prob;
                }
                if (triples[k].is_pref_undef())
                {
                    pref = triples[k].pref;
                }
                else
                {
                    pref = std::max(pref, triples[k].pref);
                }
            }
        }
    }
#ifdef PPQTN_DEBUG
{
    std::ostringstream oss;
    oss << "QUERY NE(" << left_vertex << ", " << right_vertex << ", " << dist_offset << ") EXIT";
    oss << " --> prob: " << prob << ", pref: " << pref;
    log_debug(oss.str()); 
}
#endif // PPQTN_DEBUG

    return std::make_tuple(prob, pref);
}

} // Unnamed namespace
