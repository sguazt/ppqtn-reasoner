/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <array>
#include <cctype>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_algo.hpp>
#include <ppqtn/graph_generator.hpp>
#include <ppqtn/graph_io.hpp>
#include <ppqtn/graph_ops.hpp>
#include <ppqtn/graph_ui_console.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/util.hpp>
#include <random>
#include <sstream>
#include <string>
#include <tuple>


namespace {

void show_change_graph_console_ui(const ui_params_t& default_settings, graph_t& graph, graph_t& min_network_graph, graph_properties_t& graph_props);
std::unique_ptr<graph_t> show_input_graph_console_ui(const ui_params_t& default_settings, graph_properties_t& graph_props);
void show_query_graph_console_ui(const ui_params_t& default_settings, const graph_t& graph, const graph_t& min_network_graph, graph_properties_t& graph_props);
std::unique_ptr<graph_t> show_random_graph_console_ui(const ui_params_t& default_settings);
std::unique_ptr<graph_t> show_read_graph_console_ui(const ui_params_t& default_settings, graph_properties_t& graph_props);
void show_write_graph_console_ui(const ui_params_t& default_settings, const graph_t& graph, graph_properties_t& graph_props);

} // Unnamed namespace


void show_console_ui(const ui_params_t& default_settings)
{
    enum menu_option_t
    {
        quit_option,
        input_graph_option,
        random_graph_option,
        read_graph_option,
        compute_min_network_option,
        query_option,
        change_graph_option,
        use_in_graph_as_graph_option,
        use_min_network_as_graph_option,
        print_in_graph_option,
        print_graph_option,
        print_min_network_option,
        save_in_graph_option,
        save_graph_option,
        save_min_network_option
    };

    const menu_option_t min_opt = quit_option;
    const menu_option_t max_opt = save_min_network_option;

    std::unique_ptr<graph_t> p_input_graph;
    std::unique_ptr<graph_t> p_work_graph;
    std::unique_ptr<graph_t> p_min_network_graph;
    graph_properties_t graph_props;
    bool min_network_consistent = false;

    int opt = quit_option;
    do
    {
        do
        {
            std::cout   << '\n'
                        << "--------------------------------------------------------------------------------\n"
                        << "Main Menu\n"
                        << "--------------------------------------------------------------------------------\n"
                        << "[" << input_graph_option << "] Enter a graph manually.\n"
                        << "[" << random_graph_option << "] Generate a random graph.\n"
                        << "[" << read_graph_option << "] Read graph from a file.\n"
                        << "[" << compute_min_network_option << "] Compute minimal network.\n"
                        << "[" << query_option << "] Query the working graph.\n"
                        << "[" << change_graph_option << "] Change the working graph.\n"
                        << "[" << use_in_graph_as_graph_option << "] Use input graph as working graph.\n"
                        << "[" << use_min_network_as_graph_option << "] Use minimal network as working graph.\n"
                        << "[" << print_in_graph_option << "] Show input graph.\n"
                        << "[" << print_graph_option << "] Show working graph.\n"
                        << "[" << print_min_network_option << "] Show minimal network.\n"
                        << "[" << save_in_graph_option << "] Write input graph to a file.\n"
                        << "[" << save_graph_option << "] Write working graph to a file.\n"
                        << "[" << save_min_network_option << "] Write minimal network to a file.\n"
                        << "[" << quit_option << "] Quit.\n"
                        << '\n'
                        << "Enter your choice [" << min_opt << "-" << max_opt << "]: ";
            std::cin >> opt;
        }
        while (!std::cin.good() || opt < min_opt || opt > max_opt);

        switch (opt)
        {
            case input_graph_option:
                p_input_graph = show_input_graph_console_ui(default_settings, graph_props);
                if (!p_input_graph)
                {
                    throw std::runtime_error("Null graph pointer");
                }
                if (p_work_graph == nullptr)
                {
                    p_work_graph = std::unique_ptr<graph_t>(new graph_t(*p_input_graph));
                }
                else
                {
                    *p_work_graph = *p_input_graph;
                }
                break;
            case random_graph_option:
                p_input_graph = show_random_graph_console_ui(default_settings);
                if (!p_input_graph)
                {
                    throw std::runtime_error("Null graph pointer");
                }
                if (p_work_graph == nullptr)
                {
                    p_work_graph = std::unique_ptr<graph_t>(new graph_t(*p_input_graph));
                }
                else
                {
                    *p_work_graph = *p_input_graph;
                }
                break;
            case read_graph_option:
                p_input_graph = show_read_graph_console_ui(default_settings, graph_props);
                if (!p_input_graph)
                {
                    throw std::runtime_error("Null graph pointer");
                }
                if (p_work_graph == nullptr)
                {
                    p_work_graph = std::unique_ptr<graph_t>(new graph_t(*p_input_graph));
                }
                else
                {
                    *p_work_graph = *p_input_graph;
                }
                break;
            case compute_min_network_option:
                if (p_min_network_graph == nullptr)
                {
                    p_min_network_graph = std::unique_ptr<graph_t>(new graph_t(*p_work_graph));
                }
                else
                {
                    *p_min_network_graph = *p_work_graph;
                }
                min_network_consistent = floyd_warshall(*p_min_network_graph);
                if (min_network_consistent)
                {
                    std::cout << "Constraints are consistent\n";
                }
                else
                {
                    log_warn("Constraint are not consistent");
                }
                break;
            case query_option:
                if (p_work_graph != nullptr && p_min_network_graph != nullptr)
                {
                    show_query_graph_console_ui(default_settings, *p_work_graph, *p_min_network_graph, graph_props);
                }
                else
                {
                    log_warn("Working graph and/or minimal network not yet available");
                }
                break;
            case change_graph_option:
                if (p_work_graph != nullptr && p_min_network_graph != nullptr)
                {
                    show_change_graph_console_ui(default_settings, *p_work_graph, *p_min_network_graph, graph_props);
                }
                else
                {
                    log_warn("Working graph and/or minimal network not yet available");
                }
                break;
            case use_min_network_as_graph_option:
                if (p_min_network_graph != nullptr)
                {
                    *p_work_graph = *p_min_network_graph;
                    std::cout << "Working graph successfully changed\n";
                }
                else
                {
                    log_warn("Minimal network not yet available");
                }
                break;
            case use_in_graph_as_graph_option:
                if (p_input_graph != nullptr)
                {
                    *p_work_graph = *p_input_graph;
                    std::cout << "Working graph successfully changed\n";
                }
                else
                {
                    log_warn("Input graph not yet available");
                }
                break;
            case print_in_graph_option:
                if (p_input_graph != nullptr)
                {
                    write_graph(*p_input_graph, std::cout, false, graph_props);
                    std::cout << '\n';
                }
                else
                {
                    log_warn("Input graph not yet available");
                }
                break;
            case print_graph_option:
                if (p_work_graph != nullptr)
                {
                    write_graph(*p_work_graph, std::cout, false, graph_props);
                    std::cout << '\n';
                }
                else
                {
                    log_warn("Graph not yet available");
                }
                break;
            case print_min_network_option:
                if (p_min_network_graph != nullptr)
                {
                    write_graph(*p_min_network_graph, std::cout, false, graph_props);
                    std::cout << '\n';
                }
                else
                {
                    log_warn("Minimal network not yet available");
                }
                break;
            case save_in_graph_option:
                if (p_input_graph != nullptr)
                {
                    show_write_graph_console_ui(default_settings, *p_input_graph, graph_props);
                }
                else
                {
                    log_warn("Input graph not yet available");
                }
                break;
            case save_graph_option:
                if (p_work_graph != nullptr)
                {
                    show_write_graph_console_ui(default_settings, *p_work_graph, graph_props);
                }
                else
                {
                    log_warn("Graph not yet available");
                }
                break;
            case save_min_network_option:
                if (p_min_network_graph != nullptr)
                {
                    show_write_graph_console_ui(default_settings, *p_min_network_graph, graph_props);
                }
                else
                {
                    log_warn("Minimal network not yet available");
                }
                break;
            case quit_option:
                break;
        }
    }
    while (opt != quit_option);

    std::cout << "Bye!\n";
}


namespace {

std::unique_ptr<graph_t> show_input_graph_console_ui(const ui_params_t& default_settings, graph_properties_t& graph_props)
{
    enum menu_option_t
    {
        quit_option/*,
        print_graph_option*/
    };

    const menu_option_t min_opt = quit_option;
    const menu_option_t max_opt = quit_option;

    std::unique_ptr<graph_t> p_graph;

    int opt = quit_option;
    do
    {
        do
        {
            std::cout   << '\n'
                        << "--------------------------------------------------------------------------------\n"
                        << "Manual Graph Insertion Menu\n"
                        << "--------------------------------------------------------------------------------\n"
                        << "** TO BE IMPLEMENTED **\n"
                        << "[" << quit_option << "] Back to main menu.\n"
                        << '\n'
                        << "Enter your choice [" << min_opt << "-" << max_opt << "]: ";
            std::cin >> opt;
        }
        while (!std::cin.good() || opt < min_opt || opt > max_opt);

        switch (opt)
        {
            case quit_option:
                break;
        }
    }
    while (opt != quit_option);

    return p_graph;
}

std::unique_ptr<graph_t> show_read_graph_console_ui(const ui_params_t& default_settings, graph_properties_t& graph_props)
{
    enum menu_option_t
    {
        quit_option,
        path_option,
//[FIXME]: currently we only support the DOT format so this question is useless 
//        format_option,
//[/FIXME]: currently we only support the DOT format so this question is useless 
        read_option
//[FIXME]: probably useless as it is already present in the main menu
//        print_graph_option
//[/FIXME]: probably useless as it is already present in the main menu
    };

    const menu_option_t min_opt = quit_option;
    const menu_option_t max_opt = read_option;

    std::unique_ptr<graph_t> p_graph;

    std::string opt_infile = default_settings.graph_input_file;
    std::string opt_infmt = default_settings.graph_input_file_format;

    int opt = quit_option;
    do
    {
        do
        {
            std::cout   << '\n'
                        << "--------------------------------------------------------------------------------\n"
                        << "Graph Reading Menu\n"
                        << "--------------------------------------------------------------------------------\n"
                        << "[" << path_option << "] Enter the path to the input file.\n"
//[FIXME]: currently we only support the DOT format so this question is useless 
//                        << "[" << format_option << "] Enter the format of the input file.\n"
//[/FIXME]: currently we only support the DOT format so this question is useless 
                        << "[" << read_option << "] Read.\n"
//[FIXME]: probably useless as it is already present in the main menu
//                        << "[" << print_graph_option << "] Show read graph.\n"
//[/FIXME]: probably useless as it is already present in the main menu
                        << "[" << quit_option << "] Back to main menu.\n"
                        << '\n'
                        << "Enter your choice [" << min_opt << "-" << max_opt << "]: ";
            std::cin >> opt;
        }
        while (!std::cin.good() || opt < min_opt || opt > max_opt);

        switch (opt)
        {
            case path_option:
                {
                    std::string val;
                    do
                    {
                        std::cout << "Enter the path [" << opt_infile << "]: ";
                        std::cin >> val;
                    }
                    while (!std::cin.good() || val.empty());
                    opt_infile = val;
                }
                break;
//[FIXME]: currently we only support the DOT format so this question is useless 
//            case format_option:
//                {
//                    std::string val;
//                    do
//                    {
//                        std::cout << "Enter the file format [" << opt_infmt << "]: ";
//                        std::cin >> val;
//                    }
//                    while (!std::cin.good() || val.empty());
//                    opt_infmt = val;
//                }
//                break;
//[/FIXME]: currently we only support the DOT format so this question is useless 
            case read_option:
                {
                    auto txt_graph = read_file(opt_infile);
                    p_graph = read_dot_graph(txt_graph, graph_props);
                    log_info("Graph successfully read");
                }
                break;
//[FIXME]: probably useless as it is already present in the main menu
//            case print_graph_option:
//                if (p_graph != nullptr)
//                {
//                    write_graph(*p_graph, std::cout, false, graph_props);
//                    std::cout << '\n';
//                }
//                else
//                {
//                    log_warn("Graph not yet available");
//                }
//                break;
//[/FIXME]: probably useless as it is already present in the main menu
            case quit_option:
                break;
        }
    }
    while (opt != quit_option);

    return p_graph;
}

void show_write_graph_console_ui(const ui_params_t& default_settings, const graph_t& graph, graph_properties_t& graph_props)
{
    enum menu_option_t
    {
        quit_option,
        path_option,
//[FIXME]: currently we only support the DOT format so this question is useless 
//        format_option,
//[/FIXME]: currently we only support the DOT format so this question is useless 
        both_dir_option,
        write_option
    };

    const menu_option_t min_opt = quit_option;
    const menu_option_t max_opt = write_option;

    std::string opt_outfile = default_settings.graph_output_file;
    std::string opt_outfmt = default_settings.graph_output_file_format;
    bool opt_bothdir = default_settings.graph_output_both_dir;

    int opt = quit_option;
    do
    {
        do
        {
            std::cout   << '\n'
                        << "--------------------------------------------------------------------------------\n"
                        << "Graph Writing Menu\n"
                        << "--------------------------------------------------------------------------------\n"
                        << "[" << path_option << "] Enter the path to the output file.\n"
//[FIXME]: currently we only support the DOT format so this question is useless 
//                        << "[" << format_option << "] Enter the format of the output file.\n"
//[/FIXME]: currently we only support the DOT format so this question is useless 
                        << "[" << both_dir_option << "] Include edges in both direction.\n"
                        << "[" << write_option << "] Write.\n"
                        << "[" << quit_option << "] Back to main menu.\n"
                        << '\n'
                        << "Enter your choice [" << min_opt << "-" << max_opt << "]: ";
            std::cin >> opt;
        }
        while (!std::cin.good() || opt < min_opt || opt > max_opt);

        switch (opt)
        {
            case path_option:
                {
                    std::string val;
                    do
                    {
                        std::cout << "Enter the path [" << opt_outfile << "]: ";
                        std::cin >> val;
                    }
                    while (!std::cin.good() || val.empty());
                    opt_outfile = val;
                }
                break;
//[FIXME]: currently we only support the DOT format so this question is useless 
//            case format_option:
//                {
//                    std::string val;
//                    do
//                    {
//                        std::cout << "Enter the file format [" << opt_outfmt << "]: ";
//                        std::cin >> val;
//                    }
//                    while (!std::cin.good() || val.empty());
//                    opt_outfmt = val;
//                }
//                break;
//[/FIXME]: currently we only support the DOT format so this question is useless 
            case both_dir_option:
                {
                    char val;
                    do
                    {
                        std::cout << "Write both (u,v) and (v,u) edges [" << (opt_bothdir ? "Y" : "N") << "] (Y/N)? ";
                        std::cin >> val;
                    }
                    while (!std::cin.good());
                    opt_bothdir = (std::tolower(val) == 'y' ? true : false);
                }
                break;
            case write_option:
                {
                    std::ofstream ofs(opt_outfile);
                    write_dot_graph(graph, ofs, opt_bothdir, graph_props);
                    ofs.close();
                    log_info("File '" + opt_outfile + "' successfully written");
                }
                break;
            case quit_option:
                break;
        }
    }
    while (opt != quit_option);
}

std::unique_ptr<graph_t> show_random_graph_console_ui(const ui_params_t& default_settings)
{
    enum menu_option_t
    {
        quit_option,
        seed_option,
        num_vertices_option,
        bridge_length_option,
        constraint_length_bounds_option,
        distance_bounds_option,
        limit_constraint_length_option,
        print_settings_option,
        generate_option,
        print_graph_option
    };

    const menu_option_t min_opt = quit_option;
    const menu_option_t max_opt = print_graph_option;

    std::unique_ptr<graph_t> p_graph;

    unsigned long opt_seed = default_settings.rand_seed;
    std::size_t opt_nverts = default_settings.rand_num_vertices;
    std::size_t opt_brlen = default_settings.rand_bridge_length;
    std::size_t opt_min_dist = default_settings.rand_distance_bounds.first;
    std::size_t opt_max_dist = default_settings.rand_distance_bounds.second;
    std::size_t opt_min_conslen = default_settings.rand_constraint_length_bounds.first;
    std::size_t opt_max_conslen = default_settings.rand_constraint_length_bounds.second;
    std::size_t opt_limit_conslen = default_settings.rand_limit_constraint_length;

    int opt = quit_option;
    do
    {
        do
        {
            std::cout   << '\n'
                        << "--------------------------------------------------------------------------------\n"
                        << "Random Graph Generation Menu\n"
                        << "--------------------------------------------------------------------------------\n"
                        << "[" << seed_option << "] Enter the seed for the random number engine.\n"
                        << "[" << num_vertices_option << "] Enter the number of vertices.\n"
                        << "[" << bridge_length_option << "] Enter the bridge length (as numner of edges).\n"
                        << "[" << constraint_length_bounds_option << "] Enter the min/max number of triples in each constraint.\n"
                        << "[" << distance_bounds_option << "] Enter the min/max base distance for constraints.\n"
                        << "[" << limit_constraint_length_option << "] Limit the number of triples in a constraint obtained by composition.\n"
                        << "[" << print_settings_option << "] Show current settings.\n"
                        << "[" << generate_option << "] Generate.\n"
                        << "[" << print_graph_option << "] Show generated graph.\n"
                        << "[" << quit_option << "] Back to main menu.\n"
                        << '\n'
                        << "Enter your choice [" << min_opt << "-" << max_opt << "]: ";
            std::cin >> opt;
        }
        while (!std::cin.good() || opt < min_opt || opt > max_opt);

        switch (opt)
        {
            case seed_option:
                {
                    unsigned long val;
                    do
                    {
                        std::cout << "Enter the seed [" << opt_seed << "]: ";
                        std::cin >> val;
                    }
                    while (!std::cin.good());
                    opt_seed = val;
                }
                break;
            case num_vertices_option:
                {
                    std::size_t val;
                    do
                    {
                        std::cout << "Enter the number of vertices [" << opt_nverts << "]: ";
                        std::cin >> val;
                    }
                    while (!std::cin.good());
                    opt_nverts = val;
                }
                break;
            case bridge_length_option:
                {
                    std::size_t val;
                    do
                    {
                        std::cout << "Enter the bridge length [" << opt_brlen << "]: ";
                        std::cin >> val;
                    }
                    while (!std::cin.good());
                    opt_brlen = val;
                }
                break;
            case constraint_length_bounds_option:
                {
                    std::size_t min_val;
                    std::size_t max_val;
                    do
                    {
                        do
                        {
                            std::cout << "Enter the min length [" << opt_min_conslen << "]: ";
                            std::cin >> min_val;
                        }
                        while (!std::cin.good());
                        do
                        {
                            std::cout << "Enter the max length [" << opt_max_conslen << "]: ";
                            std::cin >> max_val;
                        }
                        while (!std::cin.good());
                        if (min_val > max_val)
                        {
                            log_warn("Invalid range");
                        }
                    }
                    while (min_val > max_val);
                    opt_min_conslen = min_val;
                    opt_max_conslen = max_val;
                }
                break;
            case distance_bounds_option:
                {
                    std::size_t min_val;
                    std::size_t max_val;
                    do
                    {
                        do
                        {
                            std::cout << "Enter the min distance [" << opt_min_dist << "]: ";
                            std::cin >> min_val;
                        }
                        while (!std::cin.good());
                        do
                        {
                            std::cout << "Enter the max distance [" << opt_max_dist << "]: ";
                            std::cin >> max_val;
                        }
                        while (!std::cin.good());
                        if (min_val > max_val)
                        {
                            log_warn("Invalid range");
                        }
                    }
                    while (min_val > max_val);
                    opt_min_dist = min_val;
                    opt_max_dist = max_val;
                }
                break;
            case limit_constraint_length_option:
                {
                    char val;
                    do
                    {
                        std::cout << "Limit the constraint length [" << (opt_limit_conslen ? "Y" : "N") << "] (Y/N)? ";
                        std::cin >> val;
                    }
                    while (!std::cin.good());
                    opt_limit_conslen = (std::tolower(val) == 'y' ? true : false);
                }
                break;
            case print_settings_option:
                std::cout   << "Current settings:\n"
                            << "* Seed: " << opt_seed << '\n'
                            << "* Number of vertices: " << opt_nverts << '\n'
                            << "* Bridge length (as number of edges): " << opt_brlen << '\n'
                            << "* Min/max number of triples in each constraint: [" << opt_min_conslen << ", " << opt_max_conslen << "]" << '\n'
                            << "* Min/max base distance for constraints: [" << opt_min_dist << ", " << opt_max_dist << "]" << '\n'
                            << "* Limit the number of triples in a constraint obtained by composition? " << (opt_limit_conslen ? "Yes" : "No") << '\n'
                            << '\n';
                break;
            case generate_option:
                {
                    std::default_random_engine rng(opt_seed);

                    p_graph = generate_random_linear_graph(rng,
                                                           opt_nverts,
                                                           opt_brlen,
                                                           opt_min_conslen,
                                                           opt_max_conslen,
                                                           opt_min_dist,
                                                           opt_max_dist,
                                                           opt_limit_conslen);
                }
                break;
            case print_graph_option:
                if (p_graph != nullptr)
                {
                    std::cout << *p_graph  << '\n';
                    //write_graph(*p_input_graph, std::cout, false, graph_props);
                    //std::cout << '\n';
                }
                else
                {
                    log_warn("Graph not yet available");
                }
                break;
            case quit_option:
                break;
        }
    }
    while (opt != quit_option);

    return p_graph;
}

void show_query_graph_console_ui(const ui_params_t& default_settings, const graph_t& graph, const graph_t& min_network_graph, graph_properties_t& graph_props)
{
    enum menu_option_t
    {
        quit_option,
        extraction_query_option,
        filter_query_option,
        boolean_query_option,
        point_point_qualitative_query_option,
        point_interval_qualitative_query_option,
        interval_interval_qualitative_query_option,
        interval_interval_structural_query_option,
        print_graph_option
    };

    const menu_option_t min_opt = quit_option;
    const menu_option_t max_opt = print_graph_option;

    std::map<std::string,std::size_t> name_to_vertex_map;

    auto const nv = graph.num_vertices();
    for (std::size_t v = 0; v < nv; ++v)
    {
        name_to_vertex_map[graph_props.node_names[v]] = v;
    }

    // Point-Point Qualitative query operators
    const std::string ppq_op_eq = "=";
    const std::string ppq_op_ge = ">";
    const std::string ppq_op_gt = ">=";
    const std::string ppq_op_le = "<=";
    const std::string ppq_op_lt = "<";
    const std::string ppq_op_ne = "!=";
    // Interval-Interval Qualitative query operators
    const std::string iiq_op_after = "after";
    const std::string iiq_op_before = "before";
    const std::string iiq_op_contains = "contains";
    const std::string iiq_op_disjoint = "disjoint";
    const std::string iiq_op_during = "during";
    const std::string iiq_op_ended_by = "ended-by";
    const std::string iiq_op_ends = "ends";
    const std::string iiq_op_equal = "equal";
    const std::string iiq_op_intersect = "intersect";
    const std::string iiq_op_meets = "meets";
    const std::string iiq_op_met_by = "met-by";
    const std::string iiq_op_overlapped_by = "overlapped-by";
    const std::string iiq_op_overlaps = "overlaps";
    const std::string iiq_op_started_by = "started-by";
    const std::string iiq_op_starts = "starts";
    // Interval-Interval Structural query operators
    const std::string iis_op_composition = "composition";
    const std::string iis_op_intersection = "intersection";

    int opt = quit_option;
    do
    {
        do
        {
            std::cout   << '\n'
                        << "--------------------------------------------------------------------------------\n"
                        << "Graph Query Menu\n"
                        << "--------------------------------------------------------------------------------\n"
                        << "[" << extraction_query_option << "] Extraction query.\n"
                        << "[" << filter_query_option << "] Filter query option.\n"
                        << "[" << boolean_query_option << "] Boolean query.\n"
                        << "[" << point_point_qualitative_query_option << "] Point-Point Qualitative query.\n"
                        << "[" << point_interval_qualitative_query_option << "] Point-Interval Qualitative query.\n"
                        << "[" << interval_interval_qualitative_query_option << "] Interval-Interval Qualitative query.\n"
                        << "[" << interval_interval_structural_query_option << "] Interval-Interval Structural query.\n"
                        << "[" << print_graph_option << "] Show graph.\n"
                        << "[" << quit_option << "] Back to main menu.\n"
                        << '\n'
                        << "Enter your choice [" << min_opt << "-" << max_opt << "]: ";
            std::cin >> opt;
        }
        while (!std::cin.good() || opt < min_opt || opt > max_opt);

        switch (opt)
        {
            case extraction_query_option:
                {
                    std::string line;
                    std::istringstream iss;

                    std::string left_time_point;
                    std::string right_time_point;

                    //std::cin.clear();
                    do
                    {
                        std::cout << "Enter the left time point (either as number or symbolic name): ";
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    // Extract a single word from the line
                    //std::string left_time_point_str(line.cbegin(),
                    //                                std::find_if(line.cbegin(), line.cend(), std::isspace()));
                    iss.clear();
                    iss.str(line);
                    iss >> left_time_point;

                    line.clear();
                    do
                    {
                        std::cout << "Enter the right time point (either as number or symbolic name): ";
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    // Extract a single word from the line
                    //std::string right_time_point_str(line.cbegin(),
                    //                                 std::find_if(line.cbegin(), line.cend(), std::isspace()));
                    iss.clear();
                    iss.str(line);
                    iss >> right_time_point;

                    if (left_time_point.empty() || right_time_point.empty())
                    {
                        log_warn("A time point cannot be empty");
                    }
                    else
                    {
                        bool have_alpha_left_vertex = !std::all_of(left_time_point.cbegin(), left_time_point.cend(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_right_vertex = !std::all_of(right_time_point.cbegin(), right_time_point.cend(), [](char c){ return std::isdigit(c); });

                        bool ok = true;

                        // Parse the vertex in the left-hand side

                        std::size_t left_vertex;
                        if (have_alpha_left_vertex)
                        {
                            if (name_to_vertex_map.count(left_time_point) > 0)
                            {
                                left_vertex = name_to_vertex_map.at(left_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << left_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> left_vertex;
                        }

                        // Parse the vertex in the right-hand side

                        std::size_t right_vertex;
                        if (have_alpha_right_vertex)
                        {
                            if (name_to_vertex_map.count(right_time_point) > 0)
                            {
                                right_vertex = name_to_vertex_map.at(right_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << right_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> right_vertex;
                        }

                        if (ok)
                        {
                            if (graph.has_edge(left_vertex, right_vertex))
                            {
                                std::cout << left_time_point << " ? " << right_time_point << ": " << graph(left_vertex, right_vertex) << '\n';
                            }
                            else
                            {
                                std::cout << left_time_point << " ? " << right_time_point << ": not found\n";
                            }
                        }
                    }
                }
                break;
            case filter_query_option:
                log_warn("TO BE IMPLEMENTED!");
                break;
            case boolean_query_option:
                log_warn("TO BE IMPLEMENTED!");
                break;
            case point_point_qualitative_query_option:
                {
                    std::string line;
                    std::istringstream iss;

                    std::string left_time_point;
                    std::string right_time_point;
                    std::string op;

                    // Read the first (left) time point

                    //std::cin.clear();
                    do
                    {
                        std::cout << "Enter the left time point (either as number or symbolic name): ";
                        //FIXME: without std::cin.ignore(), this do-while is performed twice. Why?
                        std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    // Extract a single word from the line
                    //std::string left_time_point_str(line.cbegin(),
                    //                                std::find_if(line.cbegin(), line.cend(), std::isspace()));
                    iss.clear();
                    iss.str(line);
                    iss >> left_time_point;

                    // Read the second (right) time point

                    line.clear();
                    std::cin.clear();
                    do
                    {
                        std::cout << "Enter the right time point (either as number or symbolic name): ";
                        //FIXME: with std::cin.ignore() (as done above), the getline() below skips the first character inserted. Why?
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    // Extract a single word from the line
                    iss.clear();
                    iss.str(line);
                    iss >> right_time_point;

                    // Read the operator

                    const std::array<std::string, 6> valid_ops = {ppq_op_eq, ppq_op_ge, ppq_op_gt, ppq_op_lt, ppq_op_le, ppq_op_ne};
                    const std::string valid_ops_str = join(valid_ops.begin(), valid_ops.end(), ", ");
                    while (true)
                    {
                        line.clear();
                        do
                        {
                            std::cout << "Enter the operator (one of: " << valid_ops_str << "): ";
                            //std::cin.ignore();
                            std::getline(std::cin, line);
                        }
                        while (!std::cin.good() || line.empty());

                        iss.clear();
                        iss.str(line);
                        iss >> op;
                        trim(op);

                        if (std::any_of(valid_ops.begin(), valid_ops.end(), [&](const std::string& s){ return op == s; }))
                        {
                            break;
                        }
                    }

                    if (left_time_point.empty() || right_time_point.empty())
                    {
                        log_warn("A time point cannot be empty");
                    }
                    else if (op.empty())
                    {
                        log_warn("The operator cannot be empty");
                    }
                    else
                    {
                        bool have_alpha_left_vertex = !std::all_of(left_time_point.cbegin(), left_time_point.cend(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_right_vertex = !std::all_of(right_time_point.cbegin(), right_time_point.cend(), [](char c){ return std::isdigit(c); });

                        bool ok = true;

                        // Parse the vertex in the left-hand side

                        std::size_t left_vertex;
                        if (have_alpha_left_vertex)
                        {
                            if (name_to_vertex_map.count(left_time_point) > 0)
                            {
                                left_vertex = name_to_vertex_map.at(left_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << left_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> left_vertex;
                        }

                        // Parse the vertex in the right-hand side

                        std::size_t right_vertex;
                        if (have_alpha_right_vertex)
                        {
                            if (name_to_vertex_map.count(right_time_point) > 0)
                            {
                                right_vertex = name_to_vertex_map.at(right_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << right_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> right_vertex;
                        }

                        if (ok)
                        {
                            std::tuple<double,double> query_res;
                            if (op == ppq_op_eq)
                            {
                                query_res = query_eq(graph, left_vertex, right_vertex);
                            }
                            else if (op == ppq_op_ge)
                            {
                                query_res = query_ge(graph, left_vertex, right_vertex);
                            }
                            else if (op == ppq_op_gt)
                            {
                                query_res = query_gt(graph, left_vertex, right_vertex);
                            }
                            else if (op == ppq_op_lt)
                            {
                                query_res = query_lt(graph, left_vertex, right_vertex);
                            }
                            else if (op == ppq_op_le)
                            {
                                query_res = query_le(graph, left_vertex, right_vertex);
                            }
                            else if (op == ppq_op_ne)
                            {
                                query_res = query_ne(graph, left_vertex, right_vertex);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Unrecognized operator '" << op << "'";
                                log_warn(oss.str());
                            }

                            std::ostringstream prob_str;
                            std::cout << "Query: " << left_time_point << " " << op << " " << right_time_point << " ? <";
                            std::cout << "prob: ";
                            if (std::get<0>(query_res) == constants::undef_prob)
                            {
                                std::cout << constants::undef_prob_symbol;
                            }
                            else
                            {
                                std::cout << std::get<0>(query_res);
                            }
                            std::cout << ", pref: ";
                            if (std::get<1>(query_res) == constants::undef_pref)
                            {
                                std::cout << constants::undef_pref_symbol;
                            }
                            else
                            {
                                std::cout << std::get<1>(query_res);
                            }
                            std::cout << ">\n";
                        }
                    }
                }
                break;
            case point_interval_qualitative_query_option:
                log_warn("TO BE IMPLEMENTED!");
                break;
            case interval_interval_qualitative_query_option:
                {
                    std::string line;
                    std::istringstream iss;

                    std::string evt1_left_time_point;
                    std::string evt1_right_time_point;
                    std::string evt2_left_time_point;
                    std::string evt2_right_time_point;
                    std::string op;

                    // Read the first (left) time point of the first event

                    //std::cin.clear();
                    do
                    {
                        std::cout << "Enter the left time point of the first durative event (either as number or symbolic name): ";
                        //FIXME: without std::cin.ignore(), this do-while is performed twice. Why?
                        std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    // Extract a single word from the line
                    //std::string left_time_point_str(line.cbegin(),
                    //                                std::find_if(line.cbegin(), line.cend(), std::isspace()));
                    iss.clear();
                    iss.str(line);
                    iss >> evt1_left_time_point;

                    // Read the second (right) time point of the first event

                    line.clear();
                    do
                    {
                        std::cout << "Enter the right time point of the first durative event (either as number or symbolic name): ";
                        //FIXME: with std::cin.ignore() (as done above), the getline() below skips the first character inserted. Why?
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    iss.clear();
                    iss.str(line);
                    iss >> evt1_right_time_point;

                    // Read the first (left) time point of the second event

                    //std::cin.clear();
                    do
                    {
                        std::cout << "Enter the left time point of the second durative event (either as number or symbolic name): ";
                        //FIXME: with std::cin.ignore() (as done above), the getline() below skips the first character inserted. Why?
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    iss.clear();
                    iss.str(line);
                    iss >> evt2_left_time_point;

                    // Read the second (right) time point of the second event

                    line.clear();
                    do
                    {
                        std::cout << "Enter the right time point of the second durative event (either as number or symbolic name): ";
                        //FIXME: with std::cin.ignore() (as done above), the getline() below skips the first character inserted. Why?
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    iss.clear();
                    iss.str(line);
                    iss >> evt2_right_time_point;

                    // Read the operator

                    const std::array<std::string, 15> valid_ops = {iiq_op_after,
                                                                   iiq_op_before,
                                                                   iiq_op_contains,
                                                                   iiq_op_disjoint,
                                                                   iiq_op_during,
                                                                   iiq_op_ended_by,
                                                                   iiq_op_ends,
                                                                   iiq_op_equal,
                                                                   iiq_op_intersect,
                                                                   iiq_op_meets,
                                                                   iiq_op_met_by,
                                                                   iiq_op_overlapped_by,
                                                                   iiq_op_overlaps,
                                                                   iiq_op_started_by,
                                                                   iiq_op_starts};
                    const std::string valid_ops_str = join(valid_ops.begin(), valid_ops.end(), ", ");
                    while (true)
                    {
                        line.clear();
                        do
                        {
                            std::cout << "Enter the operator (one of: " << valid_ops_str << "): ";
                            //std::cin.ignore();
                            std::getline(std::cin, line);
                        }
                        while (!std::cin.good() || line.empty());

                        iss.clear();
                        iss.str(line);
                        iss >> op;
                        trim(op);
                        to_lower(op);

                        if (std::any_of(valid_ops.begin(), valid_ops.end(), [&](const std::string& s){ return op == s; }))
                        {
                            break;
                        }
                    }

                    if (evt1_left_time_point.empty()
                        || evt1_right_time_point.empty()
                        || evt2_left_time_point.empty()
                        || evt2_right_time_point.empty())
                    {
                        log_warn("A time point cannot be empty");
                    }
                    else if (op.empty())
                    {
                        log_warn("The operator cannot be empty");
                    }
                    else
                    {
                        bool have_alpha_evt1_left_vertex = !std::all_of(evt1_left_time_point.cbegin(), evt1_left_time_point.cend(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_evt1_right_vertex = !std::all_of(evt1_right_time_point.cbegin(), evt1_right_time_point.cend(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_evt2_left_vertex = !std::all_of(evt2_left_time_point.cbegin(), evt2_left_time_point.cend(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_evt2_right_vertex = !std::all_of(evt2_right_time_point.cbegin(), evt2_right_time_point.cend(), [](char c){ return std::isdigit(c); });

                        bool ok = true;

                        // Parse the first vertex in the left-hand side

                        std::size_t evt1_left_vertex;
                        if (have_alpha_evt1_left_vertex)
                        {
                            if (name_to_vertex_map.count(evt1_left_time_point) > 0)
                            {
                                evt1_left_vertex = name_to_vertex_map.at(evt1_left_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << evt1_left_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> evt1_left_vertex;
                        }

                        // Parse the second vertex in the left-hand side

                        std::size_t evt1_right_vertex;
                        if (have_alpha_evt1_right_vertex)
                        {
                            if (name_to_vertex_map.count(evt1_right_time_point) > 0)
                            {
                                evt1_right_vertex = name_to_vertex_map.at(evt1_right_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << evt1_right_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> evt1_right_vertex;
                        }

                        // Parse the first vertex in the right-hand side

                        std::size_t evt2_left_vertex;
                        if (have_alpha_evt2_left_vertex)
                        {
                            if (name_to_vertex_map.count(evt2_left_time_point) > 0)
                            {
                                evt2_left_vertex = name_to_vertex_map.at(evt2_left_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << evt2_left_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> evt2_left_vertex;
                        }

                        // Parse the second vertex in the right-hand side

                        std::size_t evt2_right_vertex;
                        if (have_alpha_evt2_right_vertex)
                        {
                            if (name_to_vertex_map.count(evt2_right_time_point) > 0)
                            {
                                evt2_right_vertex = name_to_vertex_map.at(evt2_right_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << evt2_right_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> evt2_right_vertex;
                        }

                        if (ok)
                        {
                            std::tuple<double,double> query_res;
                            if (op == iiq_op_after)
                            {
                                query_res = query_after(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_before)
                            {
                                query_res = query_before(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_contains)
                            {
                                query_res = query_contains(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_disjoint)
                            {
                                query_res = query_disjoint(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_during)
                            {
                                query_res = query_during(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_ended_by)
                            {
                                query_res = query_ended_by(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_ends)
                            {
                                query_res = query_ends(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_equal)
                            {
                                query_res = query_equal(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_intersect)
                            {
                                query_res = query_intersect(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_meets)
                            {
                                query_res = query_meets(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_met_by)
                            {
                                query_res = query_met_by(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_overlapped_by)
                            {
                                query_res = query_overlapped_by(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_overlaps)
                            {
                                query_res = query_overlaps(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_started_by)
                            {
                                query_res = query_started_by(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else if (op == iiq_op_starts)
                            {
                                query_res = query_starts(graph, evt1_left_vertex, evt1_right_vertex, evt2_left_vertex, evt2_right_vertex);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Unrecognized operator '" << op << "'";
                                log_warn(oss.str());
                            }

                            std::ostringstream prob_str;
                            std::cout << "Query: [" << evt1_left_time_point << ", " << evt1_right_time_point << "] " << op << " [" << evt2_left_time_point << ", " << evt2_right_time_point << "] ? <";
                            std::cout << "prob: ";
                            if (std::get<0>(query_res) == constants::undef_prob)
                            {
                                std::cout << constants::undef_prob_symbol;
                            }
                            else
                            {
                                std::cout << std::get<0>(query_res);
                            }
                            std::cout << ", pref: ";
                            if (std::get<1>(query_res) == constants::undef_pref)
                            {
                                std::cout << constants::undef_pref_symbol;
                            }
                            else
                            {
                                std::cout << std::get<1>(query_res);
                            }
                            std::cout << ">\n";
                        }
                    }
                }
                break;
            case interval_interval_structural_query_option:
                {
                    std::string line;
                    std::istringstream iss;

                    std::string evt1_left_time_point;
                    std::string evt1_right_time_point;
                    std::string evt2_left_time_point;
                    std::string evt2_right_time_point;
                    std::string op;

                    // Read the first (left) time point of the first event

                    //std::cin.clear();
                    do
                    {
                        std::cout << "Enter the left time point of the first durative event (either as number or symbolic name): ";
                        //FIXME: without std::cin.ignore(), this do-while is performed twice. Why?
                        std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    // Extract a single word from the line
                    //std::string left_time_point_str(line.cbegin(),
                    //                                std::find_if(line.cbegin(), line.cend(), std::isspace()));
                    iss.clear();
                    iss.str(line);
                    iss >> evt1_left_time_point;

                    // Read the second (right) time point of the first event

                    line.clear();
                    do
                    {
                        std::cout << "Enter the right time point of the first durative event (either as number or symbolic name): ";
                        //FIXME: with std::cin.ignore() (as done above), the getline() below skips the first character inserted. Why?
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    iss.clear();
                    iss.str(line);
                    iss >> evt1_right_time_point;

                    // Read the first (left) time point of the second event

                    //std::cin.clear();
                    do
                    {
                        std::cout << "Enter the left time point of the second durative event (either as number or symbolic name): ";
                        //FIXME: with std::cin.ignore() (as done above), the getline() below skips the first character inserted. Why?
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    iss.clear();
                    iss.str(line);
                    iss >> evt2_left_time_point;

                    // Read the second (right) time point of the second event

                    line.clear();
                    do
                    {
                        std::cout << "Enter the right time point of the second durative event (either as number or symbolic name): ";
                        //FIXME: with std::cin.ignore() (as done above), the getline() below skips the first character inserted. Why?
                        //std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    iss.clear();
                    iss.str(line);
                    iss >> evt2_right_time_point;

                    // Read the operator

                    const std::array<std::string, 2> valid_ops = {iis_op_intersection,
                                                                  iis_op_composition};
                    const std::string valid_ops_str = join(valid_ops.begin(), valid_ops.end(), ", ");
                    while (true)
                    {
                        line.clear();
                        do
                        {
                            std::cout << "Enter the operator (one of: " << valid_ops_str << "): ";
                            //std::cin.ignore();
                            std::getline(std::cin, line);
                        }
                        while (!std::cin.good() || line.empty());

                        iss.clear();
                        iss.str(line);
                        iss >> op;
                        trim(op);
                        to_lower(op);

                        if (std::any_of(valid_ops.begin(), valid_ops.end(), [&](const std::string& s){ return op == s; }))
                        {
                            break;
                        }
                    }

                    if (evt1_left_time_point.empty()
                        || evt1_right_time_point.empty()
                        || evt2_left_time_point.empty()
                        || evt2_right_time_point.empty())
                    {
                        log_warn("A time point cannot be empty");
                    }
                    else if (op.empty())
                    {
                        log_warn("The operator cannot be empty");
                    }
                    else
                    {
                        bool have_alpha_evt1_left_vertex = !std::all_of(evt1_left_time_point.cbegin(), evt1_left_time_point.cend(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_evt1_right_vertex = !std::all_of(evt1_right_time_point.cbegin(), evt1_right_time_point.cend(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_evt2_left_vertex = !std::all_of(evt2_left_time_point.cbegin(), evt2_left_time_point.cend(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_evt2_right_vertex = !std::all_of(evt2_right_time_point.cbegin(), evt2_right_time_point.cend(), [](char c){ return std::isdigit(c); });

                        bool ok = true;

                        // Parse the first vertex in the left-hand side

                        std::size_t evt1_left_vertex;
                        if (have_alpha_evt1_left_vertex)
                        {
                            if (name_to_vertex_map.count(evt1_left_time_point) > 0)
                            {
                                evt1_left_vertex = name_to_vertex_map.at(evt1_left_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << evt1_left_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> evt1_left_vertex;
                        }

                        // Parse the second vertex in the left-hand side

                        std::size_t evt1_right_vertex;
                        if (have_alpha_evt1_right_vertex)
                        {
                            if (name_to_vertex_map.count(evt1_right_time_point) > 0)
                            {
                                evt1_right_vertex = name_to_vertex_map.at(evt1_right_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << evt1_right_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> evt1_right_vertex;
                        }

                        // Parse the first vertex in the right-hand side

                        std::size_t evt2_left_vertex;
                        if (have_alpha_evt2_left_vertex)
                        {
                            if (name_to_vertex_map.count(evt2_left_time_point) > 0)
                            {
                                evt2_left_vertex = name_to_vertex_map.at(evt2_left_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << evt2_left_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> evt2_left_vertex;
                        }

                        // Parse the second vertex in the right-hand side

                        std::size_t evt2_right_vertex;
                        if (have_alpha_evt2_right_vertex)
                        {
                            if (name_to_vertex_map.count(evt2_right_time_point) > 0)
                            {
                                evt2_right_vertex = name_to_vertex_map.at(evt2_right_time_point);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << evt2_right_time_point << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> evt2_right_vertex;
                        }

                        if (ok)
                        {
                            punt query_res = nullptr;
                            if (op == iis_op_composition)
                            {
                                query_res = compose(graph(evt1_left_vertex, evt1_right_vertex), graph(evt2_left_vertex, evt2_right_vertex));
                            }
                            else if (op == iis_op_intersection)
                            {
                                query_res = intersect(graph(evt1_left_vertex, evt1_right_vertex), graph(evt2_left_vertex, evt2_right_vertex));
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Unrecognized operator '" << op << "'";
                                log_warn(oss.str());
                            }

                            if (query_res == nullptr)
                            {
                                throw std::runtime_error("Null pointer in interval-interval structural query");
                            }

                            std::ostringstream prob_str;
                            std::cout << "Query: [" << evt1_left_time_point << ", " << evt1_right_time_point << "] " << op << " [" << evt2_left_time_point << ", " << evt2_right_time_point << "] ? " << query_res << '\n';

                            delete[] query_res;
                        }
                    }
                }
                break;
            case print_graph_option:
                write_graph(graph, std::cout, false, graph_props);
                std::cout << '\n';
                break;
            case quit_option:
                break;
        }
    }
    while (opt != quit_option);
}

void show_change_graph_console_ui(const ui_params_t& default_settings, graph_t& graph, graph_t& min_network, graph_properties_t& graph_props)
{
    enum menu_option_t
    {
        quit_option,
        add_constraint_option,
        del_constraint_option,
        compute_min_network_option,
        //reset_last_changes_option,
        reset_all_changes_option,
        print_graph_option,
        print_min_network_option
    };

    struct constraint_t
    {
        std::size_t left_vertex;
        std::size_t right_vertex;
        punt triples;
    };

    const menu_option_t min_opt = quit_option;
    const menu_option_t max_opt = print_min_network_option;

    graph_t input_graph(graph); // A copy of the original graph useful in case the user ask to reset all changes
    graph_t input_min_network(min_network); // A copy of the original minimal network useful in case the user ask to reset all changes
    //graph_t backup_graph(work_graph);
    //graph_t backup_min_network(work_min_network);
    //std::vector<constraint_t> added_constraints;
    //std::vector<constraint_t> removed_constraints;
    bool graph_changed = false;

    int opt = quit_option;
    do
    {
        do
        {
            std::cout   << '\n'
                        << "--------------------------------------------------------------------------------\n"
                        << "Graph Change Menu\n"
                        << "--------------------------------------------------------------------------------\n"
                        << "[" << add_constraint_option << "] Add constraint.\n"
                        << "[" << del_constraint_option << "] Remove constraint.\n"
                        << "[" << compute_min_network_option << "] Propagate changes.\n"
                        //<< "[" << reset_last_changes_option << "] Reset last changes.\n"
                        << "[" << reset_all_changes_option << "] Reset all changes.\n"
                        << "[" << print_graph_option << "] Show current graph.\n"
                        << "[" << print_min_network_option << "] Show current minimal network.\n"
                        << "[" << quit_option << "] Back to main menu.\n"
                        << '\n'
                        << "Enter your choice [" << min_opt << "-" << max_opt << "]: ";
            std::cin >> opt;
        }
        while (!std::cin.good() || opt < min_opt || opt > max_opt);

        switch (opt)
        {
            case add_constraint_option:
                {
                    std::string line;
                    do
                    {
                        std::cout << "Enter the constraint.\n"
                                  << " Syntax:\n"
                                  << "   X <(dist,prob,pref),(dist,prob,pref),...> Y\n"
                                  << " where:\n"
                                  << " - 'X' and 'Y' are non-negative integer numbers (or its symbolic counterpart)\n"
                                  << "    denoting graph vertices,\n"
                                  << " - 'dist' is a non-negative real number denoting a temporal distance,\n"
                                 <<  " - 'prob' is either a non-negative real number denoting a probability, or the symbol '" << constants::undef_prob_symbol << "' denoting an undefined probability,\n"
                                 <<  " - 'pref' is either a non-negative real number denoting a preference, or the symbol '" << constants::undef_pref_symbol << "' denoting an undefined preference,\n"
                                  << ": ";
                        std::cin.ignore();
                        std::getline(std::cin, line);
                    }
                    while (!std::cin.good() || line.empty());

                    auto parsed_constraint = parse_constraint(line);

                    if (parsed_constraint.left_time_point.empty() || parsed_constraint.right_time_point.empty())
                    {
                        log_warn("A time point cannot be empty");
                    }
                    else if (parsed_constraint.triples.size() == 0)
                    {
                        log_warn("Triples cannot be empty");
                    }
                    else
                    {
                        // If the user has inserted symbolic vertex names, build a <name> => <vertex> map

                        std::map<std::string,std::size_t> name_to_vertex_map;

                        bool have_alpha_left_vertex = !std::all_of(parsed_constraint.left_time_point.begin(), parsed_constraint.left_time_point.end(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_right_vertex = !std::all_of(parsed_constraint.right_time_point.begin(), parsed_constraint.right_time_point.end(), [](char c){ return std::isdigit(c); });

                        if (have_alpha_left_vertex || have_alpha_right_vertex)
                        {
                            auto const nv = graph.num_vertices();
                            for (std::size_t v = 0; v < nv; ++v)
                            {
                                name_to_vertex_map[graph_props.node_names[v]] = v;
                            }
                        }

                        // Parse the entered triple

                        bool ok = true;
                        std::istringstream iss;

                        // Parse the vertex in the left-hand side of the constraint

                        std::size_t left_vertex;
                        iss.clear();
                        iss.str(parsed_constraint.left_time_point);
                        if (have_alpha_left_vertex)
                        {
                            std::string label;
                            iss >> label;
                            if (name_to_vertex_map.count(label) > 0)
                            {
                                left_vertex = name_to_vertex_map.at(label);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << label << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> left_vertex;
                        }

                        // Parse the vertex in the right-hand side of the constraint

                        std::size_t right_vertex;
                        iss.clear();
                        iss.str(parsed_constraint.right_time_point);
                        if (have_alpha_right_vertex)
                        {
                            std::string label;
                            iss >> label;
                            if (name_to_vertex_map.count(label) > 0)
                            {
                                right_vertex = name_to_vertex_map.at(label);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << label << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> right_vertex;
                        }

                        // Add or intersect a constraint
                        // Specifically,
                        // - if the graph already contains an edge between the same vertices, perform an intersection,
                        // - otherwise, simply add the new edge

                        if (ok)
                        {
                            terne descriptor;
                            descriptor.dist = parsed_constraint.triples.size();
                            bool have_prob = true;
                            bool have_pref = true;
                            for (auto const& triple : parsed_constraint.triples)
                            {
                                if (triple.is_prob_undef())
                                {
                                    have_prob = false;
                                }
                                if (triple.is_pref_undef())
                                {
                                    have_pref = false;
                                }
                            }
                            descriptor.prob = have_prob ? constants::def_prob : constants::undef_prob;
                            descriptor.pref = have_pref ? constants::def_pref : constants::undef_pref;
                            parsed_constraint.triples.insert(parsed_constraint.triples.begin(), descriptor);

                            punt triples = nullptr;
                            if (graph.has_edge(left_vertex, right_vertex))
                            {
                                // There is already an edge => Intersect

                                triples = intersect(graph(left_vertex, right_vertex), parsed_constraint.triples.data());

                            }
                            else
                            {
                                // There is not an edge between the same vertex => Add new edge

                                punt triples = new terne[parsed_constraint.triples.size()];
                                if (triples == nullptr)
                                {
                                    fatal_error("Unable to allocate memory for triples");
                                }

                                std::copy(parsed_constraint.triples.begin(), parsed_constraint.triples.end(), triples);
                            }

                            if (triples != nullptr)
                            {
                                if (graph.has_edge(left_vertex, right_vertex))
                                {
                                    graph.remove_edge(left_vertex, right_vertex);
                                }
                                graph.add_edge(left_vertex, right_vertex, triples+1, triples+triples[0].dist+1);
                                delete[] triples;
                                graph_changed = true;

                                log_info("Constraint successfully added");
                            }
                            else
                            {
                                log_warn("Empty intersection: constraint not added");
                            }
                        }
                    }
                }
                break;
            case del_constraint_option:
                {
                    std::string left_time_point;
                    do
                    {
                        std::cout << "Enter the first time point: ";
                        std::cin.ignore();
                        std::getline(std::cin, left_time_point);
                    }
                    while (!std::cin.good() || left_time_point.empty());

                    std::string right_time_point;
                    do
                    {
                        std::cout << "Enter the second time point: ";
                        std::cin.ignore();
                        std::getline(std::cin, right_time_point);
                    }
                    while (!std::cin.good() || right_time_point.empty());

                    if (left_time_point.empty() || right_time_point.empty())
                    {
                        log_warn("A time point cannot be empty");
                    }
                    else
                    {
                        // If the user has inserted symbolic vertex names, build a <name> => <vertex> map

                        std::map<std::string,std::size_t> name_to_vertex_map;

                        bool have_alpha_left_vertex = !std::all_of(left_time_point.begin(), left_time_point.end(), [](char c){ return std::isdigit(c); });
                        bool have_alpha_right_vertex = !std::all_of(right_time_point.begin(), right_time_point.end(), [](char c){ return std::isdigit(c); });

                        if (have_alpha_left_vertex || have_alpha_right_vertex)
                        {
                            auto const nv = graph.num_vertices();
                            for (std::size_t v = 0; v < nv; ++v)
                            {
                                name_to_vertex_map[graph_props.node_names[v]] = v;
                            }
                        }

                        // Parse the entered triple

                        bool ok = true;
                        std::istringstream iss;

                        // Parse the vertex in the left-hand side of the constraint

                        std::size_t left_vertex;
                        iss.clear();
                        iss.str(left_time_point);
                        if (have_alpha_left_vertex)
                        {
                            std::string label;
                            iss >> label;
                            if (name_to_vertex_map.count(label) > 0)
                            {
                                left_vertex = name_to_vertex_map.at(label);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << label << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> left_vertex;
                        }

                        // Parse the vertex in the right-hand side of the constraint

                        std::size_t right_vertex;
                        iss.clear();
                        iss.str(right_time_point);
                        if (have_alpha_right_vertex)
                        {
                            std::string label;
                            iss >> label;
                            if (name_to_vertex_map.count(label) > 0)
                            {
                                right_vertex = name_to_vertex_map.at(label);
                            }
                            else
                            {
                                std::ostringstream oss;
                                oss << "Vertex '" << label << "' not found";
                                log_warn(oss.str());
                                ok = false;
                            }
                        }
                        else
                        {
                            iss >> right_vertex;
                        }

                        // Remove a constraint

                        if (ok)
                        {
                            if (graph.has_edge(left_vertex, right_vertex))
                            {
                                graph.remove_edge(left_vertex, right_vertex);
                                graph_changed = true;
                                if (!graph.has_edge(left_vertex, right_vertex))
                                {
                                    log_info("Constraint successfully removed");
                                }
                                else
                                {
                                    log_warn("Unable to remove the constraint");
                                }
                            }
                            else
                            {
                                log_warn("Constraint not found");
                            }
                        }
                    }
                }
                break;
            case compute_min_network_option:
                {
                    if (graph_changed)
                    {
                        min_network = graph;
                        bool min_network_consistent = floyd_warshall(min_network);
                        if (min_network_consistent)
                        {
                            log_info("Constraints are consistent");
                        }
                        else
                        {
                            log_warn("Constraint are not consistent");
                        }
                        graph_changed = false;
                    }
                    else
                    {
                        log_info("Nothing to update");
                    }
                }
                break;
//            case reset_last_changes_option:
//                graph = backup_graph;
//                min_network = backup_min_network;
//                graph_changed = false;
//                log_info("Last changes have been successfully reset");
//                break;
            case reset_all_changes_option:
//                graph = backup_graph = graph;
//                min_network = backup_min_network = min_network;
                graph = input_graph;
                min_network = input_min_network;
                graph_changed = false;
                log_info("All changes have been successfully reset");
                break;
            case print_graph_option:
                write_graph(graph, std::cout, false, graph_props);
                std::cout << '\n';
                break;
            case print_min_network_option:
                write_graph(min_network, std::cout, false, graph_props);
                std::cout << '\n';
                break;
            case quit_option:
                break;
        }
    }
    while (opt != quit_option);
}

} // Unnamed namespace
