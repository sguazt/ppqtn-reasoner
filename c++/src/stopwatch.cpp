/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <chrono>
#include <ctime>
#include "internal_macros.hpp"
#include <ppqtn/error.hpp>
#include <ppqtn/stopwatch.hpp>
#include <stdexcept>
#include <type_traits>

#ifdef PPQTN_OS_WINDOWS
# include <shlwapi.h>
# undef StrCat  // Don't let StrCat in string_util.h be renamed to lstrcatA
# include <versionhelpers.h>
# include <windows.h>
#else
# include <fcntl.h>
# include <sys/resource.h>
# include <sys/time.h>
# include <sys/types.h>  // this header must be included before 'sys/sysctl.h' to avoid compilation error on FreeBSD
# include <unistd.h>
# if defined PPQTN_OS_FREEBSD || defined PPQTN_OS_MACOSX
#  include <sys/sysctl.h>
# endif
# if defined(PPQTN_OS_MACOSX)
#  include <mach/mach_init.h>
#  include <mach/mach_port.h>
#  include <mach/thread_act.h>
# endif
#endif


namespace
{

#if defined(PPQTN_OS_WINDOWS)
double make_time(FILETIME const& kernel_time, FILETIME const& user_time);
#else
double make_time(struct rusage const& ru);
#endif

#if defined(PPQTN_OS_MACOSX)
double make_time(thread_basic_info_data_t const& info);
#endif

#if defined(CLOCK_PROCESS_CPUTIME_ID) || defined(CLOCK_THREAD_CPUTIME_ID)
double make_time(struct timespec const& ts);
#endif

double get_process_cpu_time();

double get_thread_cpu_time();

template <typename ResolutionT = std::chrono::seconds>
double get_real_time();

} // Unnamed namespace


stopwatch_t::stopwatch_t(bool use_process_cpu_time)
: use_proc_cpu_time_(use_process_cpu_time),
  started_(false),
  cpu_time_start_(0),
  cpu_time_stop_(0)
{
}

void stopwatch_t::start()
{
    cpu_time_start_ = get_cpu_time();
    real_time_start_ = clock_type::now();
    started_ = true;
}

void stopwatch_t::stop()
{
    if (started_)
    {
        started_ = false;
        cpu_time_stop_ = get_cpu_time();
        real_time_stop_ = clock_type::now();
    }
}

void stopwatch_t::reset()
{
    if (started_)
    {
        cpu_time_start_ = cpu_time_stop_ = 0;
        real_time_start_ = real_time_stop_ = clock_type::time_point{};
        started_ = false;
    }
}

bool stopwatch_t::started() const
{
    return started_;
}

double stopwatch_t::elapsed_cpu_time() const
{
    auto elapsed = started_
                   ? (get_cpu_time() - cpu_time_start_)
                   : (cpu_time_stop_ - cpu_time_start_);

    // NOTE: floating point errors can result in the subtraction producing a negative time
    return std::max(elapsed, 0.0);
}

double stopwatch_t::elapsed_real_time() const
{
    std::chrono::duration<double> elapsed = started_
                                            ? (clock_type::now() - real_time_start_)
                                            : (real_time_stop_ - real_time_start_);

    // NOTE: floating point errors can result in the subtraction producing a negative time
    return std::max(elapsed.count(), 0.0);
}

double stopwatch_t::get_cpu_time() const
{
    return use_proc_cpu_time_ ? get_process_cpu_time() : get_thread_cpu_time();
}


namespace {

#if defined(PPQTN_OS_WINDOWS)

double make_time(FILETIME const& kernel_time, FILETIME const& user_time)
{
    ULARGE_INTEGER kernel;
    ULARGE_INTEGER user;
    kernel.HighPart = kernel_time.dwHighDateTime;
    kernel.LowPart = kernel_time.dwLowDateTime;
    user.HighPart = user_time.dwHighDateTime;
    user.LowPart = user_time.dwLowDateTime;
    return (static_cast<double>(kernel.QuadPart) + static_cast<double>(user.QuadPart)) * 1e-7;
}

#else

double make_time(struct rusage const& ru)
{
    return (static_cast<double>(ru.ru_utime.tv_sec) +
            static_cast<double>(ru.ru_utime.tv_usec) * 1e-6 +
            static_cast<double>(ru.ru_stime.tv_sec) +
            static_cast<double>(ru.ru_stime.tv_usec) * 1e-6);
}

#endif

#if defined(PPQTN_OS_MACOSX)

// See: https://stackoverflow.com/questions/17372110/clock-thread-cputime-id-on-macosx

double make_time(thread_basic_info_data_t const& info)
{
    return (static_cast<double>(info.user_time.seconds) +
            static_cast<double>(info.user_time.microseconds) * 1e-6 +
            static_cast<double>(info.system_time.seconds) +
            static_cast<double>(info.system_time.microseconds) * 1e-6);
}

#endif

#if defined(CLOCK_PROCESS_CPUTIME_ID) || defined(CLOCK_THREAD_CPUTIME_ID)

double make_time(struct timespec const& ts)
{
    return ts.tv_sec + (static_cast<double>(ts.tv_nsec) * 1e-9);
}

#endif

double get_process_cpu_time()
{
#if defined(PPQTN_OS_WINDOWS)

    HANDLE proc = GetCurrentProcess();
    FILETIME creation_time;
    FILETIME exit_time;
    FILETIME kernel_time;
    FILETIME user_time;
    if (GetProcessTimes(proc, &creation_time, &exit_time, &kernel_time, &user_time))
    {
        return make_time(kernel_time, user_time);
    }
    throw std::runtime_error("GetProccessTimes() failed");

#elif defined(CLOCK_PROCESS_CPUTIME_ID) && !defined(PPQTN_OS_MACOSX)

    // NOTE: clock_gettime is not available in MacOS 10.11.
    struct timespec spec;
    if (clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &spec) != 0)
    {
        throw_syserror("clock_gettime(CLOCK_PROCESS_CPUTIME_ID, ...) failed");
    }

    return make_time(spec);

#else

    struct rusage ru;
    if (getrusage(RUSAGE_SELF, &ru) != 0)
    {
        throw_syserror("getrusage(RUSAGE_SELF, ...) failed");
    }

    return make_time(ru);

#endif // PPTQN_OS_...
}

double get_thread_cpu_time()
{
#if defined(PPQTN_OS_WINDOWS)

    HANDLE this_thread = GetCurrentThread();
    FILETIME creation_time;
    FILETIME exit_time;
    FILETIME kernel_time;
    FILETIME user_time;
    if (GetThreadTimes(this_thread, &creation_time, &exit_time, &kernel_time, &user_time) == 0)
    {
        throw std::runtime_error("GetThreadTimes(...) failed");
    }

    return make_time(kernel_time, user_time);

#elif defined(PPQTN_OS_MACOSX)

    // NOTE: clock_gettime is not available in MacOS 10.11.

    mach_msg_type_number_t count = THREAD_BASIC_INFO_COUNT;
    thread_basic_info_data_t info;
    mach_port_t thread = pthread_mach_thread_np(pthread_self());
    if (thread_info(thread, THREAD_BASIC_INFO, (thread_info_t)&info, &count) != KERN_SUCCESS)
    {
        throw std::runtime_error("thread_info() failed");
    }

    return make_time(info);

#elif defined(PPQTN_OS_SOLARIS)

    struct rusage ru;
    if (getrusage(RUSAGE_LWP, &ru) != 0)
    {
        throw std::runtime_error("getrusage(RUSAGE_LWP, ...) failed");
    }

    return make_time(ru);

#elif defined(CLOCK_THREAD_CPUTIME_ID)

    struct timespec ts;
    if (clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts) != 0)
    {
        throw std::runtime_error("clock_gettime(CLOCK_THREAD_CPUTIME_ID, ...) failed");
    }

    return make_time(ts);

#else

# error Per-thread timing is not available on your system.

#endif // PPTQN_OS_...
}

template <typename ResolutionT>
double get_real_time()
{
    using clock_type = typename std::conditional<std::chrono::high_resolution_clock::is_steady,
                                                 std::chrono::high_resolution_clock,
                                                 std::chrono::steady_clock>::type;

    auto const now = clock_type::now();
    return std::chrono::duration_cast<ResolutionT>(now.time_since_epoch()).count();
}

} // Unnamed namespace
