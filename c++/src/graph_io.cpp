/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ppqtn/error.hpp>
#include <ppqtn/graph_io.hpp>
#include <ppqtn/graph_io_dot.hpp>


std::unique_ptr<graph_t> read_graph(const std::string& txt_graph, graph_file_format_t graph_format)
{
    switch (graph_format)
    {
        case dot_graph_file_format:
            return read_dot_graph(txt_graph);
    }

    fatal_error("Unsupported graph file format");

    return nullptr;
}

std::unique_ptr<graph_t> read_graph(const std::string& txt_graph, graph_properties_t& graph_props, graph_file_format_t graph_format)
{
    switch (graph_format)
    {
        case dot_graph_file_format:
            return read_dot_graph(txt_graph, graph_props);
    }

    fatal_error("Unsupported graph file format");

    return nullptr;
}

void write_graph(const graph_t& g, std::ostream& os, bool both_dir, graph_file_format_t graph_format)
{
    switch (graph_format)
    {
        case dot_graph_file_format:
            write_dot_graph(g, os, both_dir);
            break;
        default:
            fatal_error("Unsupported graph file format");
    }
}

void write_graph(const graph_t& g, std::ostream& os, bool both_dir, const graph_properties_t& graph_props, graph_file_format_t graph_format)
{
    switch (graph_format)
    {
        case dot_graph_file_format:
            write_dot_graph(g, os, both_dir, graph_props);
            break;
        default:
            fatal_error("Unsupported graph file format");
    }
}


/*
void write_gml(const graph_t& g, std::ostream& os)
{
	os << "graph []" << '\n';
}

void write_dot(const graph_t& g, std::ostream& os)
{
    os  << "digraph " << g.name() << " {\n";
    os  << "}\n";
}
*/
