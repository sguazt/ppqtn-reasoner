#include <iostream>
#include <time.h>

using namespace std;



				//////////////////////////////////
				// dichiarazione delle costanti	//
				/////////////////////////////////


const int d = 3;		// costante dimensione della matrice. 
						// ATTENZIONE!!!!
						// Inserire il giusto numero di nodi prima dell'esecuzione!!!!



const int dimV=20;		// costante dimensione massima delle terne nei vincoli.
						// ATTENZIONE!!!
						// Prima dell'esecuzione assicurarsi che il valore non sia
						// inferiore all'effettivo numero massimo di terne nei vincoli!
						// Se maggiore non crea problemi.

                 

				///////////////////////////////
				//	dichiarazione dei tipi	//
				/////////////////////////////

struct terne     //struttura delle terne distanza/probabilit�/preferenze.
{
	int dist;
	double prob;	// se il vincolo non ha delle probabilit� il valore � NULL 
	double pref;	// se il vincolo non ha delle preferenze il valore � NULL
};


typedef terne *punt; //tipo puntatore alle terne




				///////////////////////////////////////
				//	dichiarazione delle variabili	//
				/////////////////////////////////////


punt  mat[d][d];	//matrice di puntatori ai vincoli
					//ogni cella mat[i][j] della matrice rappresenta un puntatore
					//ad un array dinamico che ha tanti elementi quanti
					//sono le terne nel vincolo tra il nodo i e il nodo j
					//pi� uno (l'elemento 0) che fa da descrittore del vincolo:
					//mat[i][j][0].dist  specifica il numero di terne nel vincolo
					//mat[i][j][0].prob  specifica se nel vincolo ci sono le probabilit� (valore 1) oppure no (valore -1)
					//mat[i][j][0].pref  specifica se nel vincolo ci sono le preferenze (valore 1) oppure no (valore -1)


bool consistente;	//indica se il grafo dei vincoli � consistente
int time_start;		//tempo in cui viene attivato il programma
int time_mat;		//tempo in cui viene concluso il caricamento della matrice
int time_stop;		//tempo in cui viene concluso il programma
double time_diff;	//tempo di esecuzione del programma per il caricamento della matrice
double time_elab;	//tempo di esecuzione dell'elaborazione, escluso il tempo di caricamento matrice





							///////////////////////////////////////
							//	dichiarazione delle procedure	//
							/////////////////////////////////////




void	GeneraMatrice();												//generazione casuale della matrice
void	CaricaMatrice ( int r_max, int c_max);							//generazione manuale della matrice
void	Floyd(int r_max, int c_max);									//algoritmo di Foyd Warshall
void	GeneraDistanze(punt p, int dim);								//generatore casuale di vincoli consistenti
void	CaricaDistanze (punt p, punt q, int dim, int esiste_prob, int esiste_pref);		//generatore manuale di vincoli consistenti
bool	ControllaDistanze(punt p, int dim);								//controla che il vincolo inserito sia corretto
void	GeneraProb (punt p, int dim);									//generatore casuale di probabilita'
double	CalcolaCompoProb(punt p, punt q,  int i, int j);				//calcola la probabilit� di un elemento nella composizione
double	CalcolaCompoPref(punt p, punt q, int i, int j);					//calcola la preferenza di un elemento nella composizione
double	CalcolaIntersectProb(punt p, punt q, punt ris, int i, int j);	//calcola la probabilit� di un elemento nell'intersezione
double	CalcolaIntersectPref(punt p, punt q, punt ris, int i, int j);	//calcola la preferenza di un elemento nell'intersezione
void	ComponiDistanze (punt p, punt q, punt ris);						//operazione di composizione di due vettori di vincoli
void	StampaMatrice(int r_max, int c_max);							//stampa gli elementi della matrice
void	StampaDistanze (punt p, int dim);								//stampa i vettori dei vincoli per ogni coppia di nodi
void	Intersect(int i, int j, punt q);								//operazione di intersezione dei vincoli della matrice
punt	Normalizza (int dim, punt p);									//normalizza le probabilita' del vettore intersezione
void	Statistiche();													//calcola info statistiche sulla lunghezza dei nodi risultato
void	justIntersect();												//effetua solo l'intersezione tra due vincoli non inseriti in matrice
void	justCompo();													//effettua solo composizione tra due vincoli non inseriti in matrice
void	CaricaVincoli (punt p, int dim, int esiste_prob, int esiste_pref);		//carica vincoli singoli fuori matrice










int main()
{
	int risposta;	//valore per la scelta dell'attivit�
	
	consistente = true;
	time_start = time(NULL);
	cout << "\n							MENU           \n";
	cout << "\n			1 - GENERAZIONE CASUALE DEI VINCOLI";
	cout << "\n			2 - CARICAMENTO MANUALE VINCOLI";
	cout << "\n			3 - SINGOLA INTERSEZIONE DI VINCOLI";
	cout << "\n			4 - SINGOLA COMPOSIZIONE DI VINCOLI";
	cout << "\n			0 - ESCI DAL PROGRAMMA";
	cout << "\n\n    SCEGLI: ";
	cin >> risposta;

	if (risposta == 0)
		system ("pause");
	else
	{
		if (risposta == 4)
			justCompo();
		else
		{
			if (risposta == 3) 
					justIntersect();
			else 
				{
					if (risposta == 2)
						CaricaMatrice(d,d);		//caricamento manuale della matrice
					else
						GeneraMatrice();			//caricamento casuale della matrice
					cout << "\n  VUOI LA STAMPA DEL GRAFO GENERATO?  (1 = Si / 0 = No):  ";
					cin >> risposta;
					if (risposta == 1)
						StampaMatrice(d,d);
					time_mat = time(NULL);	//calcola quando si e' conclusa la generazione 
					Floyd(d,d);
					cout << "\n\n\n  VUOI LA STAMPA DELLA MINIMAL NET?  (1 = Si / 0 = No):  ";
					cin >> risposta;
					if (risposta == 1)
						StampaMatrice(d,d);
					time_stop = time(NULL);		//calcola quando si e' conclusa l'elaborazione 
					cout << "\n\n\n	VUOI LA STAMPA DELLE STATISTICHE?  (1 = Si / 0 = No):  ";
					cin >> risposta;
					if (risposta == 1)
						Statistiche();
					//if (consistente == true)
						//StampaMatrice(d,d);
					cout << "\n\n\n  VUOI LA STAMPA DEI TEMPI?  (1 = Si / 0 = No):  ";
					cin >> risposta;
					if (risposta == 1)
						{
							time_diff = (time_stop - time_start);		//tempo totale di esecuzione (compresa la generazione della matrice)
							time_elab = (time_stop - time_mat);		//tempo di elaborazione di Floyd Warshall 
							cout << "\n\n\n			TEMPI ";
							cout << "\n\n Tempo di fine elaborazione:   " <<time_stop;
							cout << "\n Tempo di inizio elaborazione: "<< time_mat;
							cout << "\n Tempo di elaborazione: " << time_elab;
							cout << "\n tempo globale (compresa generazione matrice): " << time_diff<<"\n\n\n";
						}
				}
		}
	}
	system ("pause");
	return 0;
}


void GeneraMatrice()
{
	//////////////////////////////////////////////////
	//												//
	//		generazione casuale della matrice		//
	//												//
	//					ATTENZIONE!!!!				//
	//		NON AGGIORNATA CON LE PREFERENZE!!!		//
	//												//
	/////////////////////////////////////////////////
	int i;
	int j;
	int k;
	int dim;
	int newdim;
	punt p;
	punt q;
	punt aux;
	punt compo;
	double acc;
	//int elimina;
	int aggiungi;
	int lato;
	int base;
	int pos;
	


			//inizializzazione della matrice mettendo distanza 0 con probabilita' 1 sulla diagonale
			//e infinito nelle altre celle;

	for (i=0; i < d; i++)    
	{
		for (j=0;  j < d; j++)
		{
			if (i == j )  
			{ //cella sulla diagonale quindi  distanza 0 con probabilita' 1
				mat[i][j] = new terne[2];  //2 elementi: 1 per il numero di vincoli, l'altro per il vincolo vero e proprio
				if (mat[i][j] != NULL)
				{
					p = mat[i][j];
					p[0].dist = 1;
					p[1].dist = 0;
					p[1].prob = 1;
				}
				else 
				{
					cout << "\n Generazione Vettore non riuscita 1";
					system ("pause");
				}
			}

			else
			{   //altre celle: inizializzate a infinito (numero di vincoli 0!)

				mat[i][j] = new terne[1];
				if (mat[i][j] != NULL)
					mat[i][j][0].dist = 0;
				else 
				{
					cout << "\n Generazione Vettore non riuscita 2";
					system ("pause");
				}
			}
		}
	}	//fine inizializzazione matrice;

	//StampaMatrice(d,d);  //da cancellare

			    // caricamento vincoli  nodi consecutivi (i e i+1)
				// ottenuta mediante generazione casuale della dimensione (compresa fra 0 e 10),
				// generazione casuale dei valori CONSECUTIVI delle distanze (passo compreso fra 0 e 100 e vettore centrato sul passo)
				// e generazione casuale/normalizzazione delle probabilita' (reali compresi tra 0 e 1)
		
	srand(time(NULL));	//inizializzatore del seme del generatore semicasuale;
	for (i=0; i<d-1; i++)
	{
		dim = (rand()% dimV)+1;	//generatore casuale di un numero compreso tra 1 e la dimensione massima del vettore dei vincoli
		if (mat[i][i+1] != NULL)
			delete []mat[i][i+1];
		mat[i][i+1] = new terne[dim+1];	//genera in modo casuale il nuovo vettore delle distanze [i][i+1]
		if (mat[i][i+1] != NULL)
		{
			p = mat[i][i+1];
			GeneraDistanze (p, dim); 
			GeneraProb (p, dim);			//fine generazione casuale del vettore [i][i+1]
			delete []mat[i+1][i];			//generazione del vettore speculare [i+1][i] con distanze invertite
			mat[i+1][i] = new terne[dim+1];
			if (mat[i+1][i] != NULL)
			{
				q = mat[i+1][i];
				q[0].dist=dim;
				for (k =1; k <= dim; k++)
				{
					q[dim-k+1].dist = p[k].dist * (-1);
					q[dim-k+1].prob = p[k].prob;
				}
			}
			else
			{
				cout <<"\nGenerazione non riuscita !!";
				system("pause");
			}
		}
		else 
		{
			cout << "\n Generazione Vettore non riuscita3";
			system ("pause");
		}
	}	//fine caricamento vincoli nodi consecutivi

	//StampaMatrice(d,d);  //da cancellare

			//caricamento vincoli nodi con distanza 1 (i e i+2)
			//ottenuta mediante composizione dei nodi (i, i+1) e (i+1 e i+2)
	
	for (i=0; i < d-2 ; i++)
	{
		p = mat[i][i+1];
		q = mat[i+1][i+2];
		if ((p[0].dist != 0) && (q[0].dist != 0))	//se entrambe i vettori sono pieni il vettore mat[i][i+2] si ottiene dalla composizione
		{											//altrimenti e' infinito e il vettore mat[i][i+2] non viene modificato
									
			dim =  (p[p[0].dist].dist + q[q[0].dist].dist) - (p[1].dist + q[1].dist) +1;	// valuta la dimensione del vettore composizione come
																							// somma dei massimi - somma dei minimi +1
		
			delete []mat[i][i+2];		//cancella vecchio vettore
			mat[i][i+2] = new terne[dim +1];  //crea il nuovo vettore composizione
			if (mat[i][i+2] != NULL)
			{
				compo = mat[i][i+2];
				compo[0].dist = dim;
				ComponiDistanze (p,q, compo);
			
						//modifica composizione con eliminazione di alcuni elementi determinati con generazione casuale

				aggiungi = rand()% 4;	//generatore casuale del numero di elementi da aggiungere (numero compreso tra 0 e 3)
				lato = rand ()%2;		//generatore casuale del lato da cui eliminare gli elementi: 
										//0 si aggiunge in testa e si cancella in coda, 1 si cancella in testa e si aggiunge in coda
		
				newdim = dim + aggiungi;
				compo = new terne[newdim+1];
				if (compo != NULL)
				{
					acc = 0;
					base = mat[i][i+2][1].dist;
					compo[0].dist = newdim;
					if ((lato == 0) && (base-aggiungi > 0))
					{
						//si aggiunge in testa e si cancella in coda
						base = base - aggiungi-1;
						for (k=1; k<=aggiungi; k++)
						{
							compo[k].dist = base+k;
							compo[k].prob = rand() / (double)RAND_MAX;
						}
						pos = newdim - aggiungi;
						for (k= 1; k <= pos; k++)
						{
							compo[aggiungi+k].dist = mat[i][i+2][k].dist;
							compo[aggiungi+k].prob = mat[i][i+2][k].prob;
						}
					}
					else
					{
						for (k= 1 ; k<= dim ; k++)
						{
							compo[k].dist = mat[i][i+2][k].dist;
							compo[k].prob = mat[i][i+2][k].prob;
						}
						pos = dim;
						base = compo[pos].dist;
						for (k= 1; k<= aggiungi; k++)
						{
							compo[pos+k].dist = base + k; 
							compo[pos+k].prob = rand() / (double)RAND_MAX;
						}
					}
					if (mat[i][i+2] != NULL)
						delete []mat[i][i+2];
					mat[i][i+2] = new terne[newdim+1]; 
					aux=mat[i][i+2];
					if (mat[i][i+2] != NULL)
					{
						aux[0].dist = compo[0].dist;
						acc=0;
						for (k=1; k<=newdim; k++)
						{
							aux[k].dist = compo[k].dist;
							aux[k].prob = compo[k].prob;
							acc = acc + aux[k].prob;		//somma le nuove probabilita'
						}
						delete []compo;
						for (k=1; k<=newdim; k++)	//normalizza le probabilita'
							aux[k].prob = aux[k].prob/acc;

						delete []mat[i+2][i];			//generazione del vettore speculare [i+1][i] con distanze invertite
						mat[i+2][i] = new terne[newdim+1];
						if (mat[i+2][i] != NULL)
						{
							q = mat[i+2][i];
							q[0].dist= newdim;
							for (k =1; k <= newdim; k++)
							{
								q[newdim-k+1].dist = aux[k].dist * (-1);
								q[newdim-k+1].prob = aux[k].prob;
							}
						}
						else
						{
							cout <<"\nGenerazione non riuscita !!";
							system("pause");
						}


					}
					else
					{
						cout << "\n Generazione Vettore non riuscita15";
						system ("pause");
					}
				}
				else
				{
					cout << "\n Generazione Vettore non riuscita4";
					system ("pause");
				}
			}	//fine modifica vettore composizione
			else
			{
				cout << "\n Generazione Vettore non riuscita5";
				system ("pause");
			}
		} // fine vettori pieni 
			
	}  // fine caricamento vincoli nodi con distanza 1 

	//StampaMatrice(d,d);  //da cancellare
}


void GeneraDistanze (punt p, int dim)
{
	// generatore casuale di vincoli consistenti 
	// NON AGGIORNATA CON LE PREFERENZE!

	int k;
	int passo;
	int base;
	int meta; 

	p[0].dist = dim;		//assegnamento al primo elemento del vettore del numero di terne del vincolo
	passo = rand()% 101;	//generatore casuale di un numero compreso tra 0 e 100 (per determinare i valori consecutivi del vettore)
	meta = (int) dim/2;		//meta' della dimensione. I valori del vettore si centrano sul valore del passo 
							//e andranno da passo + meta' in poi (MA NON E' NECESSARIO! POTREBBERO ANDARE DA PASSO IN AVANTI
	base = passo - meta;	//valore iniziale del primo elemento del vettore (che verra' incrementato di uno per i successivi)
	for (k=0; k<dim; k++)
		p[k+1].dist=base+k;
			
}


void GeneraProb (punt p, int dim)
{
	// generatore casuale di probabilit�
	// NON AGGIORNATO ALLA POSSIBILITA' DI NON AVERE PROBABILITA'NEL VINCOLO!!

	int k;
	double acc;

	acc = 0;
	for (k=1; k <= dim; k++)  //genera le probabilita' in modo casuale
	{
		p[k].prob = rand() / (double)RAND_MAX;  //generatore casuale di un numero reale tra 0 e 1 compreso
		acc = acc + p[k].prob;
	}

	for (k=1; k <= dim; k++)  //normalizza le probabilita' in modo che la somma sia 1
	{
		p[k].prob = p[k].prob/acc;
	}
}


double CalcolaCompoProb(punt p, punt q,  int i, int j)
{
	//Calcola il valore della probabilit� di un elemento nella composizione


	double nuovo_contributo;

	if (p[0].prob == 1 && q[0].prob == 1)	//ci sono le probabilit� in entrambi i vincoli
		nuovo_contributo = p[i].prob * q[j].prob;
	else
		if (p[0].prob == -1 && q[0].prob == -1)		//non c'� la probabilit� in nessuno dei due vincoli
			nuovo_contributo = (1.0/p[0].dist) * (1.0/q[0].dist);
		else

			//c'� la probabilit� in un solo vincolo
			if (p[0].prob == 1)		//c� solo la probabilit� nel primo vincolo
				nuovo_contributo = p[i].prob / q[0].dist;

			else	//c'� la probabilit� solo nel secondo
				nuovo_contributo = q[j].prob / p[0].dist;


	return nuovo_contributo;
}



double CalcolaCompoPref(punt p, punt q, int i, int j)
{
	//Calcola il valore della preferenza di un elemento nella composizione
	double nuovo_contributo;

	if (p[0].pref == 1 && q[0].pref == 1)	//ci sono le preferenze in entrambi i vincoli
		nuovo_contributo = min(p[i].pref, q[j].pref);
	else
		if (p[0].pref == -1 && q[0].pref == -1)		//non c'� la preferenza in nessuno dei due vincoli
			nuovo_contributo = p[i].pref;
		else

			//c'� la preferenza in un solo vincolo
			if (p[0].pref == 1)		//c� solo la preferenza nel primo vincolo
				nuovo_contributo = p[i].pref;

			else	//c'� la probabilit� solo nel secondo
				nuovo_contributo = q[j].pref;


	return nuovo_contributo;
}
 


void ComponiDistanze(punt p, punt q, punt ris)
{
	// operazione di composizione di due vettori di vincoli


	int i;		//indice del primo vettore
	int j;		//indice del secondo vettore
	int dim1;	//dimensione del primo vettore
	int dim2;	//dimensione del secondo vettore
	int pos;	//indice della nuova posizione
	terne nuovo[1];	//elemento ausiliare per costruire il risultato
	int inizio;		//primo valore delle distanze composte

	if ((p[0].dist == 0) || (q[0].dist == 0))   //uno dei due vettori e' vuoto: risultato vuoto {elemento che azzera la composizione}
	{
		ris[0].dist= 0;
		ris[0].prob = -1;
		ris[0].pref = -1;
	}
	else
	{			//entrambi i vettori pieni

		dim1 = p[0].dist;
		dim2 = q[0].dist;

		ris[0].prob = 1;	//se entrambi i vincoli ci sono la composizione d� sempre probabilit�

		if (p[0].pref == -1 && q[0].pref == -1)			//manca la preferenza in entrambi i vincoli
			ris[0].pref = -1;							//anche il risultato non ha preferenze
		else											//se almeno uno dei due vincoli ha le preferenze
			ris[0].pref = 1;							//il risultato ha la preferenza

		
		inizio = p[1].dist + q[1].dist;		//calcolo primo valore delle distanze composte

		//inizializzazione del vettore risultato

			// DA QUI IN AVANTI VIENE FATTA L'IPOTESI CHE I VETTORI SIANO CONTINUI E ORDINATI 
			// SE CAMBIA L'IPOTESI SI DEVE COSTRUIRE IL RISULTATO IN MANIERA DIFFERENTE:
			// SOMMARE OGNI ELEMENTO DEL PRIMO VETTORE CON TUTTI GLI ELEMENTI DEL SECONDO VETTORE,  
			// PER OGNI SOMMA SI DEVE FARE LA COMPOSIZIONE OTTENENDO NUOVO 
			// E POI CERCARE NUOVO[0].DIST IN TUTTO IL VETTORE RIS
			// SE VIENE TROVATO SI AGGIORNANO PREFERENZE E PROBABILITA'
			// SE NON VIENE TROVATO SI INCREMENTA PIENO DI 1 E A RIS[PIENO].DIST SI ASSEGNA IL VALORE DI NUOVO[0].DIST 
			// E A RIS[PIENO].PROB SI ASSEGNA IL VALORE DI NUOVO[0].PROB E A RIS[PIENO].PREF SI ASSEGNA IL VALORE DI NUOVO[0].PREF
			// NEL CASO DI NUOVA IPOTESI GUARDARE LA SOLUZIONE USATA NEL PRIMO ARTICOLO E ADATTARLA ALLE PREFERENZE!!!!

		if (ris[0].pref == -1)		//il risultato non ha preferenze
		{ 
			//inserisco nel vettore risultato tutte le distanze della composizione
			//inizializzando la probabilit� a zero e la preferenza a null;

			for (i=1; i<= ris[0].dist ; i++)
			{
				ris[i].dist = inizio +i -1;
				ris[i].prob = 0;
			}
		}
		else		//il risultato ha preferenze
		{
			///inserisco nel vettore risultato tutte le distanze della composizione
			//inizializzando la probabilit� a zero e la preferenza a 0 (elemento neutro di somma e max);
			for (i=1; i<= ris[0].dist ; i++)
			{
				ris[i].dist = inizio + i -1;
				ris[i].prob = 0;
				ris[i].pref = 0;
			}
		}
		for (i=1; i<=dim1; i++)
			for (j=1; j<= dim2; j++)
			{
				nuovo[0].dist = p[i].dist + q[j].dist;
				nuovo[0].prob = CalcolaCompoProb(p,q, i, j);  
				nuovo[0].pref = CalcolaCompoPref(p,q,i,j);

			  
			//							ATTENZIONE!!!!
			// LE OPERAZIONI SEGUENTI SI BASANO SULL'IPOTESI CHE I VETTORI SIANO CONTINUI E ORDINATI! 
			// SE CAMBIA QUESTA IPOTESI SI DEVE MODIFICARE IL PROCEDIMENTO:
			// INVECE DI CONTROLLARE CHE IL VALORE SUL QUALE STIAMO AGENDO SIA QUELLO CHE CI ASPETTIAMO
			// DOBBIAMO CERCARE NUOVO[0].DIST IN TUTTO IL VETTORE RIS
			// SE VIENE TROVATO SI SOMMA LA PROBABILITA' PRECEDENTE CON IL NUOVO CONTRIBUTO 
			// E SI FA IL MASSIMO DELLA PRECEDENTE PREFERENZA CON IL NUOVO CONTRIBUTO,
			// SE NON VIENE TROVATO SI AGGIUNGE UN NUOVO VALORE A RIS (CHIAMIAMOLO PIENO) E
			// A RIS[PIENO].DIST SI ASSEGNA IL VALORE DI NUOVO[0].DIST, 
			// A RIS[PIENO].PROB SI ASSEGNA IL VALORE DI NUOVO[0].PROB E 
			// A RIS[PIENO].PREF SI ASSEGNA IL VALORE DI NUOVO[0].PREF
		

				pos = i+j-1;
				if 	(ris[pos].dist!= nuovo[0].dist)   //la distanza da trattare � diversa da quella che ci aspettiamo
				{
					cout << "c'� qualcosa che non va! Distanza nella COMPOSIZIONE non corrispondente";
					system ("pause");
				}

				else
				{
					ris[pos].prob = ris[pos].prob + nuovo[0].prob;		//somma del precedente valore con il nuovo contributo
					ris[pos].pref = max(ris[pos].pref, nuovo[0].pref);	//massimo tra il valore precedente e il nuovo contributo
				}
				
			}
	}
}



void Floyd(int r_max, int c_max)
{
	//Algoritmo di Floyd Warshall

	int k;
	int i;
	int j;
	int dim;
	punt compo;
	punt p;
	punt q;
	

	compo = new terne[1];
	for (k=0; k<r_max; k++)
	{
		for (i=0; i<r_max; i++)
		{
			for(j=0; j< r_max; j++)
			{
				if ((i !=j ) && (i!=k) && (j!=k))
				{
					//cout << "\n\n composizione nodi " << i << "-"<< k << "  " << k <<"-"<< j <<"\n";
  					p= mat[i][k];
					q= mat[k][j];
					delete []compo;			//cancello il vecchio vettore composizione																	
					if ((p[0].dist != 0) && (q[0].dist != 0))	//se entrambe i vettori sono pieni il vettore compo si ottiene dalla composizione
					{
						dim =  (p[p[0].dist].dist + q[q[0].dist].dist) - (p[1].dist + q[1].dist) +1;	// valuta la dimensione del vettore composizione come
																										// somma dei massimi - somma dei minimi +1
						compo = new terne[dim+1];	//creo il nuovo vettore compo
						if (compo != NULL)
						{
							compo[0].dist = dim;
							ComponiDistanze (p,q,compo); //composizione di Cik e Ckj;
						}
						else
						{
							cout << "\nGenerazione Vettore non riuscita 6";
							system ("pause");
						}
					}
					else			// uno dei due vettori e' infinito {elemento che annulla la composizione} il risulato della composizione e' infinito
					{
						compo= new terne[1];
						if (compo != NULL)
						{
							compo[0].dist=0;
							compo[0].prob = -1;
							compo[0].pref = -1;
						}
						else
						{
							cout << "\nGenerazione Vettore non riuscita 7";
							system ("pause");
						}
					}
					Intersect (i,j, compo);  //intersezione di Cij con la composizione di Cik Ckj;       ****
					if (mat[i][j] == NULL)  //grafo inconsistente; 
					{
						cout <<"\n 111 IL GRAFO E' INCONSISTENTE\n";
						system ("pause");
						consistente= false;
					}
				}
			}
		}
	}
}


double CalcolaIntersectProb(punt p, punt q, punt ris, int i, int j)
{
	//Calcola il valore della probabilit� di un elemento nell'intersezione

	double nuovo_contributo;

	if (p[0].prob == 1 && q[0].prob == 1)	//ci sono le probabilit� in entrambi i vincoli
		nuovo_contributo = p[i].prob * q[j].prob;
	else
		if (p[0].prob == -1 && q[0].prob == -1)		//non c'� la probabilit� in nessuno dei due vincoli
			nuovo_contributo = (1.0/ris[0].dist);
		else

			//c'� la probabilit� in un solo vincolo
			if (p[0].prob == 1)		//c� solo la probabilit� nel primo vincolo
				nuovo_contributo = p[i].prob / ris[0].dist;

			else	//c'� la probabilit� solo nel secondo
				nuovo_contributo = q[j].prob / ris[0].dist;

	return nuovo_contributo;
}



double CalcolaIntersectPref(punt p, punt q, punt ris, int i, int j)
{
	//Calcola il valore della preferenza di un elemento nell'intersezione

	double nuovo_contributo;

	if (p[0].pref == 1 && q[0].pref == 1)	//ci sono le preferenze in entrambi i vincoli
		nuovo_contributo = min(p[i].pref, q[j].pref);
	else
		if (p[0].pref == -1 && q[0].pref == -1)		//non c'� la preferenza in nessuno dei due vincoli
			nuovo_contributo = p[i].pref;
		else

			//c'� la preferenza in un solo vincolo
			if (p[0].pref == 1)		//c� solo la preferenza nel primo vincolo
				nuovo_contributo = p[i].pref;

			else	//c'� la probabilit� solo nel secondo
				nuovo_contributo = q[j].pref;

	return nuovo_contributo;
}
 


void Intersect(int i, int j, punt q)
{
	//							ATTENZIONE!!!!
	// QUALSIASI EVENTUALE MODIFICA DEVE ESSERE RIPORTATA, MAGARI AGGIUSTANDOLA,
	// ANCHE NELLA PROCEDURA jUSTINTERSECT TENENDO CONTO CHE IN JUST INTERSECT SI 
	// LAVORA SOLO CON DUE VINCOLI FUORI MATRICE MENTRE NELLA INTERSECT SI FA 
	// L'INTERSEZIONE ALL'INTERNO DELLA MATRICE


	int h;		//indice del primo vettore
	int k;		//indice del secondo vettore
	int dim1;	//dimensione del primo vettore
	int dim2;	//dimensione del secondovettore
	int dim;	//dimensione del vettore risultato
	int inizio;	//primo valore nell'intersezione
	int fine;	//ultimo valore nell'intersezione
	int cont;	//indice per scorrere il nuovo vettore intersezione
	//int min;	//dimensione del vettore risultato (pari alla dimensione minima dei vettori)
	//int rimuovi; // variabile per eliminare gli elementi che non servono


	punt ris;	//vettore risultato
	punt p;		//puntatore ausiliario

	p= mat[i][j];
	dim1 = p[0].dist;
	dim2 = q[0].dist;
	if ((dim1 == 0) && (dim2 ==0))		//vettori entrambi vuoti(infinito): intersezione vuota (infinito)
	{
		//cout<<"\n entrambi vuoti";
		if (mat[i][j][0].dist != 0)		//se il vettore vecchio non e' infinito 
		{								//cancella il vettore vecchio e ne crea uno nuovo infinito; 
										//altrimenti lascia il vecchio vettore infinito
		
			delete []mat[i][j];			//cancella il vettore vecchio
			mat[i][j] = new terne[1];	//crea il nuovo vettore infinito
			if (mat[i][j] != NULL)
			{
				mat[i][j][0].dist = 0;
				mat[i][j][0].prob = -1;
				mat[i][j][0].pref = -1;
			}
			else
			{
				cout << "\nGenerazione Vettore non riuscita 8";
				system ("pause");
			}
		}
		ris = mat[i][j];
	}
	else 
		if (dim1 == 0)	//primo vettore vuoto ma secondo pieno: intersezione uguale al secondo vettore
		{
			//cout << "\nMat["<<i<<"]["<<j <<"] vuoto e compo pieno";
			delete []mat[i][j];				//cancello vecchio vettore
			mat[i][j] = new terne[dim2];	//creo il nuovo vettore
			p= mat[i][j];
			if (mat[i][j]!= NULL)
			{
				for (k=0; k<=dim2; k++)		//copio il secondo vettore nel primo
				{
					mat[i][j][k].dist = q[k].dist;
					mat[i][j][k].prob = q[k].prob;
					mat[i][j][k].pref = q[k].pref;
				}
				ris=mat[i][j];
			}
			else
			{
				cout << "\nGenerazione Vettore non riuscita 9";
				system ("pause");
			}
		}
		else
			if (dim2 == 0)	//secondo vettore vuoto ma primo pieno: intersezione uguale al primo vettore - il risultato e' il vecchio vettore
			{
				//cout << "\nMat["<<i<<"]["<<j<<"] pieno e compo vuoto";
				ris = mat[i][j];
			}
			else   // entrambe i vettori pieni: intersezione data dagli elementi comuni 
			{
				
				 p= mat[i][j];
				 
				 //quanto segue si basa sulla supposizione che i vettori siano ordinati e contigui
				 //se si modifica questa ipotesi occorre modificare la costruzione del risultato:
				 //per ogni elemento del primo vettore si deve fare una ricerca sul secondo: se viene trovato si inserisce
				 //calcolando le probabilita' e le preferenze se non viene trovato si passa oltre;
				 
				 if ((p[dim1].dist < q[1].dist) || (q[dim2].dist < p[1].dist))	//i due vettori non si sovrapongono: intersezione vuota 
				 {																//il grafo dei vincoli e' inconsistente
					 ris = NULL;  
					 cout << "\n 222 Il Grafo e' inconsistente\n";
					 consistente= false;
					 system("pause");
				 }
				 else // i due vettori si sovrappongono: intersezione elementi comuni
				 {
					inizio = max(p[1].dist, q[1].dist);			//si calcola il primo valore dell'intersezione
					fine = min (p[dim1].dist, q[dim2].dist);	//si calcola l'ultimo valore dell'intersezione
					dim = fine - inizio +1;		//si calcola la dimensione del vettore risultato con i valori comuni

					ris = new terne[dim+1];		//creo il vettore separato intersezione
					if (ris != NULL)
					{
						ris[0].dist = dim;
						ris[0].prob = 1;		//se c'� l'intersezione ci sono le probabilita' per ogni caso
						if (p[0].pref == -1 && q[0].pref == -1) //non ci sono preferenze in nessuno dei due vincoli
							ris[0].pref = -1;   //il risultato non ha preferenze
						else
							ris[0].pref = 1;	//altrimenti il risultato ha preferenze

						k=1;
						h=1;
						if (p[1].dist < q[1].dist)   //il primo vettore inizia prima del secondo
					 		while (p[k].dist < q[1].dist)	//finche' i due valori sui due vettori non sono uguali si scorre sul primo vettore
								 k++;
						else 
							while (q[h].dist < p[1].dist)	//finche' i due valori sui due vettori non sono uguali si scorre sul secondo vettore
								 h++;

						//si calcola il valore pi� piccolo in comune e il valore pi� grande in comune

						

						
						for (cont=0; cont<dim; cont++)
						{
							if (p[k].dist != q[h].dist || p[k].dist != inizio +cont)  //se almeno uno dei due valori nei vettori � diverso dal valore atteso
								cout << "Qualcosa non funziona!";
							else
							{
								
								ris[cont+1].dist = inizio+cont;
							//ris[pieno].prob = p[k].prob * q[h].prob;
								ris[cont+1].prob = CalcolaIntersectProb(p,q,ris,k,h);
								ris[cont+1].pref = CalcolaIntersectPref (p,q,ris,k,h);
								k++;
								h++;
							}
						}
					}
					else 
					{
						cout << "\nGenerazione Vettore non riuscita 10";
						system ("pause");
					}


				 }
												 
			}
			if ((ris != mat[i][j]) && (ris != NULL))	//ho generato un vettore intersezione e questo non e' nullo: cancello il vecchio vettore,
												//normalizzo l'intersezione, creo il nuovo vettore e  copio l'intersezione normalizzata
			{
				//delete []mat[i][j];			//cancello il vecchio vettore
				if (ris[0].dist != 0)
					ris = Normalizza(ris[0].dist, ris);	//normalizzo le probabilta' del vettore itersezione
				dim1 = ris[0].dist;
				mat[i][j]= new terne[dim1];	//creo il nuovo vettore
				p=mat[i][j];
				if (mat[i][j] != NULL)
				{
					for (k=0; k<= dim1; k++)	//copio il vettore intersezione nel nuovo vettore
					{
						p[k].dist = ris[k].dist;
						p[k].prob = ris[k].prob;
						p[k].pref = ris[k].pref;
					}
					//cout <<"\nCancello ris";
					//delete []ris;
				}
				else 
				{
					cout <<"\n generazione errata";
					system ("pause");
				}

			}
}


punt  Normalizza (int dim, punt p)
{
	//normalizza le probabilit� del vetore intersezione in modo che la loro somma dia 1

	double acc; //accumulatore delle probabilita'
	int i;

	acc=0;
	for (i=1; i<=dim; i++)
		acc = acc + p[i].prob;
	for (i=1; i<=dim; i++)
		p[i].prob = p[i].prob / acc;

	return p;
}


void Statistiche()
{
	int i;
	int j;
	int cont;
	int acc;
	double media;
	int min;
	int max;
	int dim;
 
	cont=0;
	acc= 0;
	min = 32767;
	max = 0;
	//cout << " Numero vincoli tra i nodi:" ;
	for (i=0; i<d; i++)
	{
		for (j=0;  j< d; j++)
		{
			cont++;
			dim = mat[i][j][0].dist;
			//cout << "\n" << i << "-"<< j << ":{"<< mat[i][j][0].dist <<  "}";
			acc = acc + dim;
			if (dim < min) 
				min = dim;
			if (dim > max)
				max = dim;
		}
	}
	media = acc/cont;
	cout << "\n\n\n			STATISTICHE";
	cout << "\n\nPer "<< d <<" nodi e  " << dimV <<"      vincoli:";
	cout << "\nil numero di vincoli totale e' " << acc;
	cout << "\nla media dei vincoli e' " << media;
	cout << "\nil numero minimo di vincoli e' "<< min;
	cout << "\nil numero massimo di vincoli e' "<< max;

}


void StampaMatrice(int r_max, int c_max)
{
	int c;
	int r;
	punt p;
	int dim;

	for (r=0; r<r_max; r++)
	{
		for (c=0;  c< c_max; c++)
		{
			p = mat[r][c];
			dim = p[0].dist;
			cout << " Distanze nodi " << r << "-"<< c << ":{";
			StampaDistanze (p, dim);
			cout << "}";
		}
	}
}


void StampaDistanze (punt p, int dim)
{
	int i;

	for (i=1; i<= dim; i++)
	{
		cout << "[" << p[i].dist <<", " << p[i].prob <<", " << p[i].pref;
		cout << "] ";
	}
	cout << "\n";
}


void CaricaMatrice ( int r_max, int c_max)   
{
	//caricamento manuale della matrice dei vincoli

	int c;
	int r;
	punt vet1;
	punt vet2;
	punt vet;
	int dim;
	int risposta;
	int risp_prob;	//indica se ci sono probabilit� nel vincolo
	int risp_pref;	//indica se ci sono preferenze nel vincolo
	bool corretto;	//indica se il vincolo inserito � o meno corretto

	for (r=0; r<r_max; r++)
	{
		for (c=0;  c < c_max; c++)
		{
			if (c == r )  
			{ //cella sulla diagonale
				vet = new terne[2];

				//inizializzazione del descrittore del vincolo che si trova in vet[0]: 
				//1 solo vincolo con distanza 0, probabilit� 1 e preferenza 1
				//quindi:
				//vet[0].dist (che indica il numero di terne del vincolo) = 1
				//vet[0].prob (che indica se il vincolo ha probabilit�) = 1 cio� c'� la probabilit�
				//vet[0].pref (che indica se il vincolo ha preferenze) = 1 cio� c'� la preferenza

				vet[0].dist = 1;
				vet[0].prob = 1;
				vet[0].pref = 1;

				//inserimento valori della terna
				vet[1].dist = 0;
				vet[1].prob = 1;
				vet[1].pref = 1;
			}

			else
			{   //altre celle: inizializzate a infinito (numero terne 0!)

				vet = new terne[1];
				vet[0].dist = 0;
			}
			mat[r][c]= vet;
		}
	}
			    // caricamento vincoli solo per i nodi collegati

	cout << "Ci sono vincoli? [1=Si/ 0=No]: ";
	cin >> risposta;
	while (risposta ==1)
	{
		cout << "\nDistanza tra il nodo: "; 
		cin >> r;
		cout << "e il nodo: ";
		cin >> c;
		cout << "\n\nInserisci il numero di terne: ";
		cin >> dim;
		
		// generazione del vettore vincolo nodi rc e 
		// del vettore vincolo speculare nodi cr
		//entrambi avranno dimensione pari al numero di terne date dall'utente 

		vet1 = new terne[dim+1];
		vet2 = new terne[dim+1];
		vet1[0].dist = dim;
		vet2[0].dist = dim;
		
		//controllo esistenza probabilit� e inserimento di
		//valore 1 se ce ne sono  
		//valore -1 se non ce ne sono
		//sia per il vincolo sui nodi rc che per il vincolo speculare cr

		cout <<"\n\nCi sono probabilita'? (1 = Si / 0 = No)  ";
		cin >> risp_prob;
		if (risp_prob == 1)
		{
			vet1[0].prob = 1;
			vet2[0].prob = 1;
		}
		else 
		{
			vet1[0].prob = -1;
			vet2[0].prob = -1;
		}

		//controllo esistenza preferenze e inserimento di
		//valore 1 se ce ne sono  
		//valore -1 se non ce ne sono
		//sia per il vincolo sui nodi rc che per il vincolo speculare cr

		cout <<"\n\nCi sono preferenze'? (1 = Si / 0 = No)  ";
		cin >> risp_pref;
		if (risp_pref == 1)
		{
			vet1[0].pref = 1;
			vet2[0].pref = 1;
		}
		else
		{
			vet1[0].pref = -1;
			vet2[0].pref = -1;
		}

		//  
		corretto = false;
		while (!corretto)
		{
			cout << "\n\nVincolo tra " << r << " e " << c;
			CaricaDistanze(vet1, vet2, dim, risp_prob, risp_pref);
			corretto = ControllaDistanze(vet1, dim);
		}
		delete []mat[r][c];
		delete []mat[c][r];
		mat[r][c] = vet1;
		mat[c][r] = vet2;
		cout << "Ci sono altri vincoli? [1=Si/ 0=No]: ";
		cin >> risposta;
	}
}



void CaricaDistanze (punt p, punt q, int dim, int esiste_prob, int esiste_pref)
{
	//carica contemporaneamente il vincolo tra i nodi i-j  e il vincolo tra i nodi j-i  
	//in modo che le distanze siano opposte (se ij 0,5   allora  ji -0,5)
	//mentre le probabilit� e le preferenze, se esistono, sono uguali


	//								ATTENZIONE !!!!
	// TUTTE LE EVENTUALI MODIFICHE A QUESTA PROCEDURA DEVONO ESSERE RIPORTATE ANCHE 
	// NELLA PROCEDURA CARICA VINCOLI CHE CARICA UN SINGOLO VINCOLO TRA 2 NODI IJ 
	// AL DI FUORI DELLA MATRICE (PERTANTO IN QUEL CASO NON OCCORRE COSTRUIRE ANCHE 
	// IL VINCOLO TRA I NODI JI


	int i;      // indice di caricamento vettore dinamico
	//int risp_prob;	//indica se ci sono probabilit� nel vincolo
	//int risp_pref;	//indica se ci sono preferenze nel vincolo
	int conferma;	//indica se la singola terna viene confermata (valore 1) oppure no (valore 0)
	

	
	for (i=1; i<=dim; i++)    //caricamento del vettore delle terne distanza/probabilita'/preferenza
	{
		conferma = 0;
		while (conferma == 0)
		{
			cout << "\n\nscrivi la " << i << "^ distanza: ";
			cin >> p[i].dist;						//i_esima distanza tra i nodi ij
			q[dim-i+1].dist = p[i].dist * (-1);		//i_esima distanza tra i nodi Ji

			if (esiste_prob == 1) //ci sono le probabilit�: richiesta probabilit� e caricamento nel vettore
			{
				cout << "Scrivi la " << i << "^ probabilita': ";
				cin >> p[i].prob;
				while (p[i].prob > 1)
				{
					cout<< "\nProbabilita' impossibile! \nDevi scrivere un numero <=1. \nScrivi la probabilita'";
					cin >> p[i].prob;			//i_esima Probabilit� del vincolo tra i nodi ij
				};
				q[dim-i+1].prob = p[i].prob;	//i_esima probabilit� del vincolo tra i nodi ji
			};
		
			if (esiste_pref == 1) //ci sono le preferenze: richiesta preferenza e caricamento nel vettore
			{
				cout << "Scrivi la " << i << "^ preferenza': ";
				cin >> p[i].pref;
				while (p[i].pref > 1)
				{
					cout<< "\nPreferenza impossibile! \nDevi scrivere un numero <=1. \nScrivi la preferenza";
					cin >> p[i].pref;			//i_esima Preferenza del vincolo tra i nodi ij
				};
				q[dim-i+1].pref = p[i].pref;	//i_esima Preferenza del vincolo tra i nodi ji
			}
			cout << "CONFERMI LA " << i << "^ TERNA? (1 = Si / 0 = No)  ";
			cin >> conferma;
		}

	}
	cout << "\n\n";

}


bool ControllaDistanze (punt p, int dim)
{
	int risposta;	//indica se il vincolo inserito � corretto oppure no
		
	cout << "Il vincolo inserito e': \n\n";
	StampaDistanze(p,dim);
	cout << "\n\nCorretto? (1 = Si / 0 = No):  ";
	cin >> risposta;
	if (risposta == 1)
		return true;
	else
		return false;
}


void CaricaVincoli (punt p, int dim, int esiste_prob, int esiste_pref)    
{
	//carica vincoli singoli eventualmente anche fuori matrice



	//						ATTENZIONE !!!!
	// TUTTE LE MODIFICHE A QUESTA PROCEDURA DEVONO ESSERE RIPORTATE ANCHE 
	// NELLA PROCEDURA CARICA DISTANZE CHE CARICA CONTEMPORANEAMENTE  
	// IL VINCOLO TRA 2 NODI IJ E IL VINCOLO TRA JI.
	// SI TENGA CONTO CHE QUESTA PROCEDURA UTILIZZA UN SOLO VETTORE VINCOLO
	// MENTRE LA PROCEDURA CARICA DISTANZA UTILIZZA 2 VETTORI:
	// QUELLO DEL VINCOLO TRA IJ E QUELLO DEL VINCOLO TRA JI 
	// COSTRUITI IN MODO DA AVERE DISTANZE RECIPROCHE (IJ 0.5  JI -0.5)
	// E PROBABILITA' E PREFERENZE UGUALI.



	int i;      // indice di caricamento vettore dinamico
	

	
	for (i=1; i<=dim; i++)    //caricamento del vettore delle terne distanza/probabilita'/preferenza
	{
		cout << "\n\nscrivi la " << i << "^ distanza: ";
		cin >> p[i].dist;

		if (esiste_prob == 1) //ci sono le probabilit�: richiesta probabilit� e caricamento nel vettore
		{
			cout << "Scrivi la " << i << "^ probabilita': ";
			cin >> p[i].prob;
			while (p[i].prob > 1)
			{
				cout<< "\nProbabilita' impossibile! \nDevi scrivere un numero <=1. \nScrivi la probabilita'";
				cin >> p[i].prob;
			};
		};
		
		if (esiste_pref == 1) //ci sono le preferenze: richiesta preferenza e caricamento nel vettore
		{
			cout << "Scrivi la " << i << "^ preferenza': ";
			cin >> p[i].pref;
			while (p[i].pref > 1)
			{
				cout<< "\nPreferenza impossibile! \nDevi scrivere un numero <=1. \nScrivi la preferenza";
				cin >> p[i].pref;
			};
		}
	}
	cout << "\n\n";

}


void justIntersect()   // intersezione tra due soli vincoli fuori matrice
{
	//							ATTENZIONE!!!!
	// QUALSIASI EVENTUALE MODIFICA DEVE ESSERE RIPORTATA, MAGARI AGGIUSTANDOLA
	// ANCHE NELLA PROCEDURA INTERSECT TENENDO CONTO CHE IN JUST INTERSECT SI 
	// LAVORA SOLO CON DUE VINCOLI FUORI MATRICE MENTRE NELLA INTERSECT SI FA 
	// L'INTERSEZIONE ALL'INTERNO DELLA MATRICE


	int pieno;	//indice del vettore gia' riempito
	int h;		//indice del primo vettore
	int k;		//indice del secondo vettore
	int i;		//indice scorimento vettore risultato
	int dim1;	//dimensione del primo vettore
	int dim2;	//dimensione del secondovettore
	int dim;	//dimensione del vettore risultato
	int inizio;	//primo valore nell'intersezione
	int fine;	//ultimo valore nell'intersezione
	int minimo;	//dimensione del vettore risultato (pari alla dimensione minima dei vettori)
	//int rimuovi; // variabile per eliminare gli elementi che non servono

	punt vincolo1;		//primo vincolo
	punt vincolo2;		//secondo vincolo
	punt ris;			//vincolo risultato
	punt p;				//puntatore ausiliario
	punt q;				//puntatore ausiliario
	int risp_prob;		//indica se nel vincolo ci sono le probabilit�
	int risp_pref;		//indica se nel vincolo ci sono le preferenze
	bool corretto;		//indica se il vincolo caricato � corretto


	cout << "\n Carica primo vincolo\n";
	cout << "\n\nInserisci il numero di terne: ";
	cin >> dim1;
	vincolo1 = new terne[dim1+1];
	vincolo1[0].dist = dim1;
	
	// richiesta se ci sono probabilit� nel primo vincolo e nel vettore vincolo 
	// scrive 1 se ci sono -1 se non ci sono
	cout << "\n\nCi sono probabilita'? (1 = Si / 0 = No):  ";
	cin >> risp_prob;
	if (risp_prob == 1)
		vincolo1[0].prob = 1;
	else
		vincolo1[0].prob = -1;

	// richiesta se ci sono preferenze nel primo vincolo e nel vettore vincolo 
	// scrive 1 se ci sono -1 se non ci sono
	cout << "\n\nCi sono preferenze? (1 = Si / 0 = No):  ";
	cin >> risp_pref;
	if (risp_pref == 1)
		vincolo1[0].pref = 1;
	else
		vincolo1[0].pref = -1;

	corretto=false;
	while(!corretto)
	{
		CaricaVincoli(vincolo1,dim1, risp_prob, risp_pref);
		corretto = ControllaDistanze(vincolo1,dim1);
	}

	cout << "\n\n Carica secondo vincolo\n";
	cout << "\n\nInserisci il numero di terne: ";
	cin >> dim2;
	vincolo2 = new terne[dim2 +1];
	vincolo2[0].dist = dim2;

	// richiesta se ci sono probabilit� nel secondo vincolo e nel vettore vincolo 
	// scrive 1 se ci sono -1 se non ci sono
	cout << "\n\nCi sono probabilita'? (1 = Si / 0 = No):  ";
	cin >> risp_prob;
	if (risp_prob == 1)
		vincolo2[0].prob = 1;
	else
		vincolo2[0].prob = -1;

	// richiesta se ci sono preferenze nel secondo vincolo e nel vettore vincolo 
	// scrive 1 se ci sono -1 se non ci sono
	cout << "\n\nCi sono preferenze? (1 = Si / 0 = No):  ";
	cin >> risp_pref;
	if (risp_pref == 1)
		vincolo2[0].pref = 1;
	else
		vincolo2[0].pref = -1;

	corretto=false;
	while(!corretto)
	{
		CaricaVincoli(vincolo2,dim2, risp_prob, risp_pref);
		corretto = ControllaDistanze(vincolo2,dim2);
	}

	//							ATTENZIONE!!!
	// nel seguito si parla di vettori vuoti o infiniti per indicare che
	// ci sono TUTTE le possibili distanze, quindi un vettore vuoto (infinito)
	// intersecato con qualunque altra cosa fornisce come risultato l'altra cosa!!

	if ((dim1 == 0) && (dim2 ==0))		//vettori entrambi vuoti(infinito): intersezione vuota (infinito)
	{
		//cout<<"\n entrambi vuoti";
		ris = vincolo1;					//risultato uno dei due vettori vuoti (cio� infinito, cio� vettore con tutte le possibili distanze)
	}
	else 
		if (dim1 == 0)	//primo vettore vuoto ma secondo pieno: intersezione uguale al secondo vettore
		{
			//cout << "primo vincolo vuoto e secondo pieno";
			ris = vincolo2;
		}
		else
			if (dim2 == 0)	//secondo vettore vuoto ma primo pieno: intersezione uguale al primo vettore - il risultato e' il vecchio vettore
			{
				//cout << "primo vettore pieno e secondo vuoto";
				ris = vincolo1;
			}
			else   // entrambe i vettori pieni: intersezione data dagli elementi comuni con prodotto delle probabilita'
			{
				//cout << "entrambi vincoli pieni"; 
				if (dim1<dim2)
					 minimo = dim1;
				 else
					 minimo = dim2;
				 pieno = 0;
				 p= vincolo1;
				 q= vincolo2;
				 
				 //quanto segue si basa sulla supposizione che i vettori siano ordinati e contigui
				 //se si modifica questa ipotesi occorre modificare la costruzione del risultato:
				 //per ogni elemento del primo vettore si deve fare una ricerca sul secondo: se viene trovato si inserisce
				 //calcolando le probabilita' e le preferenze se non viene trovato si passa oltre;
				 
				 if ((p[dim1].dist < q[1].dist) || (q[dim2].dist < p[1].dist))	//i due vettori non si sovrappongono: intersezione vuota 
				 {																//il grafo dei vincoli e' inconsistente
					 ris = NULL;  
					 cout << "\n 222 Il Grafo e' inconsistente\n";
					 consistente= false;
					 system("pause");
				 }
				 else // i due vettori si sovrappongono: intersezione elementi comuni
				 {
					inizio = max(p[1].dist, q[1].dist);			//si calcola il primo valore dell'intersezione
					fine = min (p[dim1].dist, q[dim2].dist);	//si calcola l'ultimo valore dell'intersezione
					dim = fine - inizio +1;		//si calcola la dimensione del vettore risultato con i valori comuni

					ris = new terne[dim+1];		//creo il vettore separato intersezione
					if (ris != NULL)
					{
						ris[0].dist = dim;
						ris[0].prob = 1;		//se c'� l'intersezione ci sono le probabilita' per ogni caso
						if (p[0].pref == -1 && q[0].pref == -1) //non ci sono preferenze in nessuno dei due vincoli
							ris[0].pref = -1;   //il risultato non ha preferenze
						else
							ris[0].pref = 1;	//se almeno uno dei due ha preferenze il risultato ha preferenze

						k=1;
						h=1;
						if (p[1].dist < q[1].dist)   //il primo vettore inizia prima del secondo
					 		while (p[k].dist < q[1].dist)	//finche' i due valori sui due vettori non sono uguali si scorre sul primo vettore
								 k++;
						else 
							while (q[h].dist < p[1].dist)	//finche' i due valori sui due vettori non sono uguali si scorre sul secondo vettore
								 h++;

						//si calcola il valore pi� piccolo in comune e il valore pi� grande in comune

						

						
						for (i=0; i<dim; i++)
						{
							if (p[k].dist != q[h].dist || p[k].dist != inizio +i)  //se almeno uno dei due valori nei vettori � diverso dal valore atteso
								cout << "Qualcosa non funziona!";
							else
							{
								
								ris[i+1].dist = inizio+i;
							//ris[pieno].prob = p[k].prob * q[h].prob;
								ris[i+1].prob = CalcolaIntersectProb(p,q,ris,k,h);
								ris[i+1].pref = CalcolaIntersectPref (p,q,ris,k,h);
								k++;
								h++;
							}
						}
					}
					else 
					{
						cout << "\nGenerazione Vettore non riuscita 10";
						system ("pause");
					}


				 }							 
			}
			if  (ris != NULL)	//ho generato un vettore intersezione e questo non e' nullo: normalizzo l'intersezione
			{
				if (ris[0].dist != 0)
					ris = Normalizza(ris[0].dist, ris);	//normalizzo le probabilta' del vettrore itersezione
				dim1 = ris[0].dist;
				cout << "\n  nuovo vincolo = {"	;	//stampo il vincolo intersezione
				StampaDistanze (ris, dim1);
				cout << " }\n";
				delete []ris;
			}
			else 
			{
				cout <<"\n generazione errata";
				system ("pause");
			}

}


void justCompo()	//combinazione tra due soli vincoli fuori matrice
{
	//int h;		//indice del primo vettore
	//int k;		//indice del secondo vettore
	int dim1;	//dimensione del primo vettore
	int dim2;	//dimensione del secondovettore
	int dim;	//dimensione del vettore risultato
	punt v1;		//primo vincolo
	punt v2;		//secondo vincolo
	punt compo;			//vincolo risultato
	//punt p;				//puntatore ausiliario
	//punt q;				//puntatore ausiliario
	int risp_prob;		//indica se nel vincolo ci sono le probabilit�
	int risp_pref;		//indica se nel vincolo ci sono le preferenze
	bool corretto;		//indica se il vincolo caricato � corretto


	cout << "\n Carica primo vincolo\n";
	cout << "\n\nInserisci il numero di terne: ";
	cin >> dim1;
	v1 = new terne[dim1+1];
	v1[0].dist = dim1;
	
	// richiesta se ci sono probabilit� nel primo vincolo e nel vettore vincolo 
	// scrive 1 se ci sono -1 se non ci sono
	cout << "\n\nCi sono probabilita'? (1 = Si / 0 = No):  ";
	cin >> risp_prob;
	if (risp_prob == 1)
		v1[0].prob = 1;
	else
		v1[0].prob = -1;

	// richiesta se ci sono preferenze nel primo vincolo e nel vettore vincolo 
	// scrive 1 se ci sono -1 se non ci sono
	cout << "\n\nCi sono preferenze? (1 = Si / 0 = No):  ";
	cin >> risp_pref;
	if (risp_pref == 1)
		v1[0].pref = 1;
	else
		v1[0].pref = -1;

	corretto=false;
	while(!corretto)
	{
		CaricaVincoli(v1,dim1, risp_prob, risp_pref);
		corretto = ControllaDistanze(v1,dim1);
	}

	cout << "\n\n Carica secondo vincolo\n";
	cout << "\n\nInserisci il numero di terne: ";
	cin >> dim2;
	v2 = new terne[dim2 +1];
	v2[0].dist = dim2;

	// richiesta se ci sono probabilit� nel secondo vincolo e nel vettore vincolo 
	// scrive 1 se ci sono -1 se non ci sono
	cout << "\n\nCi sono probabilita'? (1 = Si / 0 = No):  ";
	cin >> risp_prob;
	if (risp_prob == 1)
		v2[0].prob = 1;
	else
		v2[0].prob = -1;

	// richiesta se ci sono preferenze nel secondo vincolo e nel vettore vincolo 
	// scrive 1 se ci sono -1 se non ci sono
	cout << "\n\nCi sono preferenze? (1 = Si / 0 = No):  ";
	cin >> risp_pref;
	if (risp_pref == 1)
		v2[0].pref = 1;
	else
		v2[0].pref = -1;

	corretto=false;
	while(!corretto)
	{
		CaricaVincoli(v2,dim2, risp_prob, risp_pref);
		corretto = ControllaDistanze(v2,dim2);
	}

	if ((v1[0].dist != 0) && (v2[0].dist != 0))	//se entrambe i vettori sono pieni il vettore compo si ottiene dalla composizione
	{
		dim =  (v1[v1[0].dist].dist + v2[v2[0].dist].dist) - (v1[1].dist + v2[1].dist) +1;	// valuta la dimensione del vettore composizione come
																										// somma dei massimi - somma dei minimi +1
		compo = new terne[dim+1];	//creo il nuovo vettore compo
		if (compo != NULL)
		{
			compo[0].dist = dim;
			ComponiDistanze (v1,v2,compo); //composizione dei due vincoli;
			StampaDistanze (compo, compo[0].dist);
			delete []compo;
		}
		else
		{
			cout << "\nGenerazione Vettore non riuscita 6";
			system ("pause");
		}
	}
	else			// uno dei due vettori e' infinito {elemento che annulla la composizione} il risulato della composizione e' infinito
	{
		compo= new terne[1];
		if (compo != NULL)
		{
			compo[0].dist=0;
			compo[0].prob = -1;
			compo[0].pref = -1;
		}
		else
		{
			cout << "\nGenerazione Vettore non riuscita 7";
			system ("pause");
		}
	}
}