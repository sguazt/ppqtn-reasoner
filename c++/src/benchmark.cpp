/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <algorithm>
#include <boost/math/distributions/normal.hpp>
#include <boost/math/distributions/students_t.hpp>
#include <chrono>
#include <cmath>
#include <cstddef>
#include <ctime>
#include "internal_macros.hpp"
#include <iostream>
#include <iomanip>
#include <limits>
#include <ppqtn/benchmark.hpp>
#include <ppqtn/graph_algo.hpp>
#include <ppqtn/graph_io.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/stopwatch.hpp>
#include <random>
//#include <thread>
#include <stdexcept>
#include <tuple>

#ifdef PPQTN_DEBUG
# include <iomanip>
#endif // PPQTN_DEBUG


//FIXME: Try to remove Boost dependency.
//       This requires to implement the quantile functions for both Normal and Student's t distribution.


namespace { namespace internal {

/// Represents statistics about a graph.
struct graph_stats_t
{
    statistics_t constraint_length_stats;
    statistics_t constraint_distance_stats;
};

/// Compute the square of the \a x.
template <typename T>
T sqr(T x);

/// Returns a textual representation of the given statistic.
std::string to_string(const statistics_t& stats, bool exclude_name = false);

/// Returns a textual representation of the given statistic, including its confidence interval at the given confidence level.
std::string to_string(const statistics_t& stats, double ci_level, bool exclude_name = false);

/// Compute statistics for the given graph
graph_stats_t compute_graph_stats(const graph_t& graph);

}} // Namespace <unnamed>::internal


//------------------------------------------------------------------------------
// BENCHMARK_T
//------------------------------------------------------------------------------


void benchmark_t::max_num_repetitions(std::size_t value)
{
    max_nreps_ = value;
}

std::size_t benchmark_t::max_num_repetitions() const
{
    return max_nreps_;
}

void benchmark_t::min_num_repetitions(std::size_t value)
{
    min_nreps_ = value;
}

std::size_t benchmark_t::min_num_repetitions() const
{
    return min_nreps_;
}

void benchmark_t::ci_level(double value)
{
    ci_level_ = value;
}

double benchmark_t::ci_level() const
{
    return ci_level_;
}

benchmark_result_t benchmark_t::run(graph_generator_t& graph_gen)
{
	benchmark_result_t res;

    res.cpu_time_stats_.name("CPU time");
    res.real_time_stats_.name("Real time");
    res.graph_constraint_length_stats_.name("Output graph average constraint length");
    res.graph_constraint_distance_stats_.name("Output graph average constraint distance");

    ci_relative_precision_num_runs_estimator_t num_runs_estimator;
    //FIXME provides getters/setters for ci_relative_precision_num_runs_estimator_t and use those here
    num_runs_estimator.ci_level_ = ci_level_;
    num_runs_estimator.n_max_ = max_nreps_;
    num_runs_estimator.n_min_ = min_nreps_;
    num_runs_estimator.target_rel_prec_ = ci_target_rel_prec_;

    // Prints a starting message

    auto start_clock = std::chrono::system_clock::now();
    auto start_time = std::chrono::system_clock::to_time_t(start_clock);

    log_stream() << "****************************************************************\n"
                 << "**** [" << std::put_time(std::localtime(&start_time), "%c %Z") << "]\n"
                 << "****************************************************************" << std::endl;

    // Runs the benchamrk

    auto nreps = min_nreps_;
    for (std::size_t r = 0; r < nreps; ++r)
	{
		auto p_graph = graph_gen();

        // Print the input graph (optional)

        if (verbose_)
        {
            log_stream() << "**** INPUT GRAPH: " << *p_graph << std::endl;
        }

        if (!check_graph(*p_graph))
        {
            // Input graph is not consistent
            //log_error("Input graph is not consistent");
            throw std::runtime_error("Input graph is not consistent");
        }

        stopwatch_t timer;

        timer.start();
    	floyd_warshall(*p_graph);
        timer.stop();

        res.cpu_time_stats_.collect(timer.elapsed_cpu_time());
        res.real_time_stats_.collect(timer.elapsed_real_time());
        auto graph_stats = internal::compute_graph_stats(*p_graph);
        //res.graph_constraint_length_stats_ = graph_stats.constraint_length_stats;
        res.graph_constraint_length_stats_.collect(graph_stats.constraint_length_stats.mean());
        //res.graph_constraint_distance_stats_ = graph_stats.constraint_distance_stats;
        res.graph_constraint_distance_stats_.collect(graph_stats.constraint_distance_stats.mean());

        if (!check_graph(*p_graph))
        {
            log_warn("Graph resulting from Floyd-Warshall is not consistent");
        }

        if (r == 0)
        {
            // NOTE: we generate a different graph in each repetition.
            //       Storing all generated graph would be memory consuming.
            //       Therefore, we save just the output graph resulting from
            //       the first repetition.
            res.out_graph_ = *p_graph;

        }

        // Print the output graph (optional)

if (false) //XXX: to be restored in future versions
{
        if (verbose_)
        {
            log_stream() << "**** OUTPUT GRAPH: " << *p_graph << std::endl;
        }
}

        // Prints incremental statistics (optional)

        if (verbose_)
        {
            std::ostringstream oss;
            log_stream() << "-- RUN #" << (r+1) << " REPORT:" << '\n'
                         << "--- Run statistics:\n"
                         << "* CPU time: " << timer.elapsed_cpu_time() << '\n'
                         << "* Real time: " << timer.elapsed_real_time() << '\n'
                         //<< "* Graph constraint length: " << graph_stats.constraint_length_stats.mean() << '\n'
                         << "* Output graph constraint length: " << internal::to_string(graph_stats.constraint_length_stats, true) << '\n'
                         //<< "* Graph constraint distance: " << graph_stats.constraint_distance_stats.mean() << '\n'
                         << "* Output graph constraint distance: " << internal::to_string(graph_stats.constraint_distance_stats, true) << '\n'
                         << "--- Incremental benchmark statistics:\n"
                         << "* " << internal::to_string(res.cpu_time_stats_, ci_level_) << '\n'
                         << "* " << internal::to_string(res.real_time_stats_, ci_level_) << '\n'
                         << "* " << internal::to_string(res.graph_constraint_length_stats_, ci_level_) << '\n'
                         << "* " << internal::to_string(res.graph_constraint_distance_stats_, ci_level_) << '\n'
                         << "--------------------------------------------------------------------------------\n"
                         << std::flush;
        }

        nreps = num_runs_estimator.estimate(res.cpu_time_stats());
	}

    // Prints final statitics (optional)

    if (verbose_)
    {
        std::ostringstream oss;
        log_stream() << "-- FINAL REPORT" << ":" << '\n'
                     << "--- Statistics:\n"
                     << "* " << internal::to_string(res.cpu_time_stats_, ci_level_) << '\n'
                     << "* " << internal::to_string(res.real_time_stats_, ci_level_) << '\n'
                     << "* " << internal::to_string(res.graph_constraint_length_stats_, ci_level_) << '\n'
                     << "* " << internal::to_string(res.graph_constraint_distance_stats_, ci_level_) << '\n'
                     << "--------------------------------------------------------------------------------\n"
                     << std::flush;
    }

    // Prints a final message

    auto stop_clock = std::chrono::system_clock::now();
    auto stop_time = std::chrono::system_clock::to_time_t(stop_clock);
    std::chrono::duration<double> elapsed_seconds = stop_clock-start_clock;

    log_stream() << "**** ELAPSED TIME: " << elapsed_seconds.count() << "s\n"
                 << "**** [" << std::put_time(std::localtime(&stop_time), "%c %Z") << "]\n"
                 << "****************************************************************"
                 << std::endl;

	return res;
}

void benchmark_t::verbose(bool value)
{
    verbose_ = value;
}

bool benchmark_t::verbose() const
{
    return verbose_;
}


//------------------------------------------------------------------------------
// FIXED_GRAPH_BENCHMARK_T
//------------------------------------------------------------------------------


void fixed_graph_benchmark_t::max_num_repetitions(std::size_t value)
{
    max_nreps_ = value;
}

std::size_t fixed_graph_benchmark_t::max_num_repetitions() const
{
    return max_nreps_;
}

void fixed_graph_benchmark_t::min_num_repetitions(std::size_t value)
{
    min_nreps_ = value;
}

std::size_t fixed_graph_benchmark_t::min_num_repetitions() const
{
    return min_nreps_;
}

void fixed_graph_benchmark_t::ci_level(double value)
{
    ci_level_ = value;
}

double fixed_graph_benchmark_t::ci_level() const
{
    return ci_level_;
}

benchmark_result_t fixed_graph_benchmark_t::run(const graph_t& graph)
{
	benchmark_result_t res;

    res.cpu_time_stats_.name("CPU time");
    res.real_time_stats_.name("Real time");
    res.graph_constraint_length_stats_.name("Output graph average constraint length");
    res.graph_constraint_distance_stats_.name("Output graph average constraint distance");

    ci_relative_precision_num_runs_estimator_t num_runs_estimator;
    //FIXME provides getters/setters for ci_relative_precision_num_runs_estimator_t and use those here
    num_runs_estimator.ci_level_ = ci_level_;
    num_runs_estimator.n_max_ = max_nreps_;
    num_runs_estimator.n_min_ = min_nreps_;
    num_runs_estimator.target_rel_prec_ = ci_target_rel_prec_;

    // Prints a starting message

    auto start_clock = std::chrono::system_clock::now();
    auto start_time = std::chrono::system_clock::to_time_t(start_clock);

    log_stream() << "****************************************************************\n"
                 << "**** [" << std::put_time(std::localtime(&start_time), "%c %Z") << "]\n"
                 << "****************************************************************\n"
                 << "**** INPUT GRAPH: " << graph << std::endl;

    // Runs the benchamrk

    auto nreps = min_nreps_;
    for (std::size_t r = 0; r < nreps; ++r)
	{
		auto graph_clone = graph;

        stopwatch_t timer;

        timer.start();
    	floyd_warshall(graph_clone);
        timer.stop();

        res.cpu_time_stats_.collect(timer.elapsed_cpu_time());
        res.real_time_stats_.collect(timer.elapsed_real_time());
        auto graph_stats = internal::compute_graph_stats(graph_clone);
        //res.graph_constraint_length_stats_ = graph_stats.constraint_length_stats;
        res.graph_constraint_length_stats_.collect(graph_stats.constraint_length_stats.mean());
        //res.graph_constraint_distance_stats_ = graph_stats.constraint_distance_stats;
        res.graph_constraint_distance_stats_.collect(graph_stats.constraint_distance_stats.mean());

        if (!check_graph(graph_clone))
        {
            log_warn("Graph resulting from Floyd-Warshall is not consistent");
        }

        if (r == 0)
        {
            // NOTE: since every repetition is the same, the resulting graph is the same too.
            //       Therefore, save the graph just the first time.
            res.out_graph_ = graph_clone;
        }

        // Prints incremental statistics (optional)

        if (verbose_)
        {
            std::ostringstream oss;
            log_stream() << "-- RUN #" << (r+1) << " REPORT:" << '\n'
                         << "--- Run statistics:\n"
                         << "* CPU time: " << timer.elapsed_cpu_time() << '\n'
                         << "* Real time: " << timer.elapsed_real_time() << '\n'
                         //<< "* Graph constraint length: " << graph_stats.constraint_length_stats.mean() << '\n'
                         << "* Output graph constraint length: " << internal::to_string(graph_stats.constraint_length_stats, true) << '\n'
                         //<< "* Graph constraint distance: " << graph_stats.constraint_distance_stats.mean() << '\n'
                         << "* Output graph constraint distance: " << internal::to_string(graph_stats.constraint_distance_stats, true) << '\n'
                         << "--- Incremental benchmark statistics:\n"
                         << "* " << internal::to_string(res.cpu_time_stats_, ci_level_) << '\n'
                         << "* " << internal::to_string(res.real_time_stats_, ci_level_) << '\n'
                         << "* " << internal::to_string(res.graph_constraint_length_stats_, ci_level_) << '\n'
                         << "* " << internal::to_string(res.graph_constraint_distance_stats_, ci_level_) << '\n'
                         << "--------------------------------------------------------------------------------\n"
                         << std::flush;
        }

        nreps = num_runs_estimator.estimate(res.cpu_time_stats());
	}

    // Prints final statitics (optional)

    if (verbose_)
    {
        std::ostringstream oss;
        log_stream() << "-- FINAL REPORT" << ":" << '\n'
                     << "--- Statistics:\n"
                     << "* " << internal::to_string(res.cpu_time_stats_, ci_level_) << '\n'
                     << "* " << internal::to_string(res.real_time_stats_, ci_level_) << '\n'
                     << "* " << internal::to_string(res.graph_constraint_length_stats_, ci_level_) << '\n'
                     << "* " << internal::to_string(res.graph_constraint_distance_stats_, ci_level_) << '\n'
                     << "--------------------------------------------------------------------------------\n"
                     << std::flush;
    }

    // Prints a final message

    auto stop_clock = std::chrono::system_clock::now();
    auto stop_time = std::chrono::system_clock::to_time_t(stop_clock);
    std::chrono::duration<double> elapsed_seconds = stop_clock-start_clock;

    log_stream() << "**** ELAPSED TIME: " << elapsed_seconds.count() << "s\n"
                 << "**** [" << std::put_time(std::localtime(&stop_time), "%c %Z") << "]\n"
                 << "****************************************************************"
                 << std::endl;

	return res;
}

void fixed_graph_benchmark_t::verbose(bool value)
{
    verbose_ = value;
}

bool fixed_graph_benchmark_t::verbose() const
{
    return verbose_;
}


//------------------------------------------------------------------------------
// BENCHMARK_RESULT_T
//------------------------------------------------------------------------------

const statistics_t& benchmark_result_t::cpu_time_stats() const
{
    return cpu_time_stats_;
}

const statistics_t& benchmark_result_t::real_time_stats() const
{
    return real_time_stats_;
}

const graph_t& benchmark_result_t::out_graph() const
{
    return out_graph_;
}


//------------------------------------------------------------------------------
// STATISTICS_T
//------------------------------------------------------------------------------

void statistics_t::clear()
{
    count_ = 0;
    m1_ = m2_
        = min_
        = max_
        = 0;
}

void statistics_t::collect(double value)
{
    ++count_;
    max_ = std::max(max_, value);
    min_ = std::min(min_, value);

    // This algorithm comes from:
    //   D. Knuth, "The Art of Computer Programming, Volume II"
    // and is used to avoid round-off errors
    double delta = value - m1_;
    m1_ += delta/count_; // Uses the new value of count_!
    m2_ += delta*(value-m1_); // Uses the new value of m1!
}

std::tuple<double,double,double,double> statistics_t::ci_mean(double level) const
{
    if (count_ == 0)
    {
        auto const nan = std::numeric_limits<double>::quiet_NaN();
        return std::make_tuple(nan, nan, nan, nan);
    }
    else if (count_ == 1)
    {
        auto const m = mean();
        return std::make_tuple(m, m, 0, 1);
    }

    auto const m = mean();
    auto const sd = standard_deviation();
    auto const prob = (1-level)/2;

    boost::math::students_t_distribution<double> tdist(count_-1);
    auto t = boost::math::quantile(boost::math::complement(tdist, prob));
    auto hw = t*sd/std::sqrt(count_); // Half-width of the confidence interval
    auto rp = (m != 0) ? hw/std::fabs(m) : std::numeric_limits<double>::infinity(); // Relative precision

    return std::make_tuple(m-hw, m+hw, hw, rp);
}

std::size_t statistics_t::count() const
{
    return count_;
}

double statistics_t::min() const
{
    return min_;
}

double statistics_t::max() const
{
    return max_;
}

double statistics_t::mean() const
{
    return m1_;
}

void statistics_t::name(const std::string& s)
{
    name_ = s;
}

const std::string& statistics_t::name() const
{
    return name_;
}

double statistics_t::standard_deviation() const
{
    return std::sqrt(variance());
}

double statistics_t::sum() const
{
    return m1_*count_;
}

double statistics_t::variance() const
{
    if (count_ == 0)
    {
        return std::numeric_limits<double>::quiet_NaN();
    }

    if (count_ == 1)
    {
        return 0;
    }

    // See: D. Knuth, "The Art of Computer Programming, Volume II"
    return m2_/(count_-1);
}


//------------------------------------------------------------------------------
// CI_RELATIVE_PRECISION_NUM_RUNS_ESTIMATOR_T
//------------------------------------------------------------------------------


std::size_t ci_relative_precision_num_runs_estimator_t::estimate(const statistics_t& stats)
{
    auto n = stats.count();

    if (done_)
    {
        return n_target_;
    }
    else if (std::isinf(target_rel_prec_))
    {
        n_target_ = n;
        detected_ = true;
        done_ = true;
    }
    else if (n < n_min_)
    {
        detected_ = false;
        n_target_ = n_min_;
    }
    else if (n >= n_max_)
    {
        aborted_ = true;
        n_target_ = n_max_;
    }
    else
    {
        // Use the procedure described in [1], chapter 11.
        // However, instead of use an absolute precision (as done in [1]) we use
        // a relative precision with respect to the mean point estimator
        //
        // REFERENCES
        // 1. J. Banks, J.S. Carson II, B.L. Nelson and D.M. Nicol.
        //    "Discrete-event System Simulation," 4th Ed., Prentice-Hall, 2005
        // .
        //

        auto const mean = stats.mean();
        auto const sd = stats.standard_deviation();

        if (sd < 0 || std::isinf(sd))
        {
            log_warn("Standard deviation is negative or infinite");
            detected_ = false;
        }
        else
        {
            auto const prob = (1+ci_level_)*0.5;
            auto n = stats.count();

            // Compute an initial estimate of sample size
            if (initial_check_)
            {
                initial_check_ = false;

                boost::math::normal_distribution<double> norm;
                auto const z = boost::math::quantile(norm, prob);
                n =  static_cast<std::size_t>(internal::sqr(z*sd/(target_rel_prec_*mean)));

                if (n < n_min_)
                {
                    n = n_min_;
                }
            }

            double n_want = 0;

            // Compute the real estimate of sample size
            do
            {
                boost::math::students_t_distribution<double> student_t(n-1);
                auto const t = boost::math::quantile(student_t, prob);
                n_want = internal::sqr(t*sd/(target_rel_prec_*mean));

                if (n < n_want)
                {
                    ++n;
                }
            }
            while (n < n_want && n < n_max_);

            if (n <= n_max_)
            {
                if (n <= stats.count())
                {
                    done_ = true;
                }
                n_target_ = n;
                detected_ = true;
            }
            else
            {
                n_target_ = n_max_;
                detected_ = false;
                aborted_ = true;
            }
        }
    }

#ifdef PPQTN_DEBUG
    std::ostringstream oss;
    oss << "(" << stats.name() << ") Detecting Sample Size --> " << std::boolalpha << detected_ << " (n_target_: " << n_target_ << " - n_max_: " << n_max_ << " - aborted_: " << aborted_ << " - unstable: " << unstable_ << " - done: " << done_ << std::noboolalpha << ")";//XXX
    log_debug(oss.str());
#endif // PPQTN_DEBUG

    return n_target_;
}


//------------------------------------------------------------------------------
// INTERNAL STUFF
//------------------------------------------------------------------------------


namespace { namespace internal {

template <typename T>
T sqr(T x)
{
    return x*x;
}

std::string to_string(const statistics_t& stats, bool exclude_name)
{
    std::ostringstream oss;
    auto const fp_prec = oss.precision();
    if (!exclude_name)
    {
        oss << stats.name() << " - ";
    }
    oss << "mean: " << stats.mean()
        << ", s.d.: " << stats.standard_deviation()
        << ", min: " << stats.min()
        << ", max: " << stats.max()
        << ", count: " << stats.count();

    return oss.str();
}

std::string to_string(const statistics_t& stats, double ci_level, bool exclude_name)
{
    auto const ci = stats.ci_mean(ci_level);

    std::ostringstream oss;
    auto const fp_prec = oss.precision();
    if (!exclude_name)
    {
        oss << stats.name() << " - ";
    }
    oss << "mean: " << stats.mean()
        << ", s.d.: " << stats.standard_deviation()
        << ", ci@" << std::fixed << std::setprecision(3) << (100*ci_level) << std::defaultfloat << std::setprecision(fp_prec) << "%: {[" << std::get<0>(ci) << ", " << std::get<1>(ci) << "], half-width: " << std::get<2>(ci) << ", relative precision: " << std::get<3>(ci) << "}"
        << ", min: " << stats.min()
        << ", max: " << stats.max()
        << ", count: " << stats.count();

    return oss.str();
}

graph_stats_t compute_graph_stats(const graph_t& graph)
{
    statistics_t cons_len_stats;
    statistics_t cons_dist_stats;

    graph_stats_t graph_stats;
 
    graph_stats.constraint_length_stats.name("Constraint Length");
    graph_stats.constraint_distance_stats.name("Constraint Distance");

    auto const nv = graph.num_vertices();
    for (std::size_t r = 0; r < nv; ++r)
    {
        for (std::size_t c = 0; c < nv; ++c)
        {
            if (graph.has_edge(r, c))
            {
                //FIXME: should we take into account self-loops (i.e., graph(r,r))?
                //       Even if they have

                auto const nt = graph(r, c)[0].dist;

                graph_stats.constraint_length_stats.collect(nt);

                for (int k = 1; k <= nt; ++k)
                {
                    graph_stats.constraint_distance_stats.collect(std::abs(graph(r, c)[k].dist));
                }
            }
        }
    }

    return graph_stats;
}

}} // Namespace <unnamed>::internal
