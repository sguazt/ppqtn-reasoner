/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */
  
/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef PPQTN_USE_PEGLIB

#include <cassert>
#include <cstddef>
#include <iostream>
//#include <iterator>
#include <map>
#include <memory>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_io_dot.hpp>
#include <ppqtn/graph_io_util.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/util.hpp>
#include <set>
#include <stack>
#include <stdexcept>
#include <string>
#include <peglib.h>
//#include <regex>
#include <sstream>
#include <vector>

#ifdef PPQTN_DEBUG
# include <iostream>
# include <sstream>
#endif // PPQTN_DEBUG


namespace {

// -----------------------------------------------------------------------------
// ENUMS

enum class node_ranking_t
{
    lexicographic_rank,
    appearance_rank
}; // node_ranking_t


// -----------------------------------------------------------------------------
// AUXILIARY FUNCTIONS

void read_dot_graph_impl(const std::string& dot_graph, node_ranking_t node_rank, graph_t& graph, graph_properties_t* p_graph_props);
void write_dot_graph_impl(const graph_t& g, std::ostream& os, bool both_dir, const graph_properties_t* p_graph_props);

#ifdef PPQTN_DEBUG

template <typename KT, typename VT>
inline
std::ostream& operator<<(std::ostream& os, const std::map<KT,VT>& set);

template <typename T>
inline
std::ostream& operator<<(std::ostream& os, const std::set<T>& set);

template <typename T>
inline
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec);

#endif // PPQTN_DEBUG


namespace dot_parser {

// -----------------------------------------------------------------------------
// GRAMMAR

/*
 * Grammar for GraphViz DOT (https://www.graphviz.org/doc/info/lang.html)
 *
 * The following is an abstract grammar defining the DOT language.
 * Terminals are enclosed in angle brackets < and > (e.g., <graph>) and nonterminals are not (e.g., graph).
 * Literal characters are given in single quotes.
 * Parentheses ( and ) indicate grouping when needed.
 * Square brackets [ and ] enclose optional items.
 * Vertical bars | separate alternatives.
 *
 * graph         :   [ <strict> ] (<graph> | <digraph>) [ ID ] '{' stmt_list '}'
 * stmt_list     :   [ stmt [ ';' ] stmt_list ]
 * stmt          :   node_stmt
 *               |   edge_stmt
 *               |   attr_stmt
 *               |   ID '=' ID
 *               |   subgraph
 * attr_stmt     :   (<graph> | <node> | <edge>) attr_list
 * attr_list     :   '[' [ a_list ] ']' [ attr_list ]
 * a_list        :   ID '=' ID [ (';' | ',') ] [ a_list ]
 * edge_stmt     :   (node_id | subgraph) edgeRHS [ attr_list ]
 * edgeRHS       :   edgeop (node_id | subgraph) [ edgeRHS ]
 * node_stmt     :   node_id [ attr_list ]
 * node_id       :   ID [ port ]
 * port          :   ':' ID [ ':' compass_pt ]
 *               |   ':' compass_pt
 * subgraph      :   [ <subgraph> [ ID ] ] '{' stmt_list '}'
 * compass_pt    :   (<n> | <ne> | <e> | <se> | <s> | <sw> | <w> | <nw> | <c> | <_>)
 *
 *
 * The keywords <node>, <edge>, <graph>, <digraph>, <subgraph>, and <strict> are
 * case-independent.
 * Note also that the allowed compass point values (see compass_pt above) are not
 * keywords, so these strings can be used elsewhere as ordinary identifiers and,
 * conversely, the parser will actually accept any identifier.
 *
 * An ID is one of the following:
 * - any string of alphabetic ([a-zA-Z\200-\377]) characters, underscores ('_')
 *   or digits ([0-9]), not beginning with a digit;
 * - a numeral [-]?(.[0-9]+ | [0-9]+(.[0-9]*)? );
 * - any double-quoted string ("...") possibly containing escaped quotes (\")1;
 * - an HTML string (<...>).
 *
 * An ID is just a string; the lack of quote characters in the first two forms is
 * just for simplicity.
 * There is no semantic difference between abc_2 and "abc_2", or between 2.34 and
 * "2.34".
 * Obviously, to use a keyword as an ID, it must be quoted. Note that, in HTML
 * strings, angle brackets must occur in matched pairs, and newlines and other
 * formatting whitespace characters are allowed.
 * In addition, the content must be legal XML, so that the special XML escape
 * sequences for \", &, <, and > may be necessary in order to embed these
 * characters in attribute values or raw text.
 * As an ID, an HTML string can be any legal XML string.
 * However, if used as a label attribute, it is interpreted specially and must
 * follow the syntax for HTML-like labels.
 * Both quoted strings and HTML strings are scanned as a unit, so any embedded
 * comments will be treated as part of the strings.
 *
 * An edgeop is -> in directed graphs and -- in undirected graphs.
 *
 * The language supports C++-style comments: \/\* *\/ and //. In addition, a line
 * beginning with a '#' character is considered a line output from a C
 * preprocessor (e.g., # 34 to indicate line 34 ) and discarded.
 *
 * Semicolons and commas aid readability but are not required.
 * Also, any amount of whitespace may be inserted between terminals.
 *
 * As another aid for readability, dot allows double-quoted strings to span
 * multiple physical lines using the standard C convention of a backslash
 * immediately preceding a newline character2.
 * In addition, double-quoted strings can be concatenated using a '+' operator.
 * As HTML strings can contain newline characters, which are used solely for
 * formatting, the language does not allow escaped newlines or concatenation
 * operators to be used within them.
 *
 * The following grammar has been adapted from the context-free grammar
 * 'grammar.y' shipped with the GraphViz's cgraph source code.
 */

auto grammar = R"(
    graph                   <- _ header body _ tok_eof
    header                  <- tok_strict? graph_type graph_name?
    graph_type              <- tok_graph / tok_digraph
    graph_name              <- atom
    body                    <- tok_lbrace stmt_list? tok_rbrace
    stmt_list               <- ( stmt tok_semicolon? )+
    stmt                    <- attr_stmt / compound
    attr_stmt               <- attr_type macro_name? attr_list
    attr_type               <- tok_graph / tok_edge / tok_node
    macro_name              <- atom tok_eq
    attr_list               <- ( tok_lbrack attr_defs? tok_rbrack )+
    attr_defs               <- ( attr_item ( tok_semicolon / tok_comma )? )*
    attr_item               <- attr_assign / attr_macro
    #attr_assign             <- atom tok_eq atom
    attr_assign             <- atom tok_eq ( atom / tok_number )
    attr_macro              <- tok_at atom
    graph_attr_defs         <- attr_assign
    compound                <- simple rcompound attr_list?
    simple                  <- node_list / subgraph
    rcompound               <- ( tok_edge_op simple )*
    node_list               <- node ( tok_comma node )*
    node                    <- atom ( tok_colon atom ( tok_colon atom )? )?
    subgraph                <- subgraph_header? body
    subgraph_header         <- tok_subgraph atom?
    atom                    <- tok_atom / tok_qatom / tok_hatom
    comment                 <- tok_scomment / tok_mcomment
    tok_at                  <- < '@' > _
    tok_atom                <- < [A-Za-z_\x80-\xFF] [A-Za-z0-9_\x80-\xFF]* > _
    tok_colon               <- < ':' > _
    tok_comma               <- < ',' > _
    tok_eof                 <- < !. >
    tok_eol                 <- < '\r\n' / '\n' / '\r' >
    tok_eolf                <- tok_eol / tok_eof
    tok_eq                  <- < '=' > _
    tok_langle              <- < '<' > _
    tok_lbrace              <- < '{' > _
    tok_lbrack              <- < '[' > _
    tok_rangle              <- < '>' > _
    tok_rbrace              <- < '}' > _
    tok_rbrack              <- < ']' > _
    tok_digraph             <- < [Dd] [Ii] [Gg] [Rr] [Aa] [Pp] [Hh] > _
    tok_edge                <- < [Ee] [Dd] [Gg] [Ee] > _
    tok_edge_op             <- < '-' ('>' / '-') > _
    tok_graph		        <- < [Gg] [Rr] [Aa] [Pp] [Hh] > _
    tok_node                <- < [Nn] [Oo] [Dd] [Ee] > _
    tok_hatom               <- tok_langle < tok_hatom_content > tok_rangle _
    tok_hatom_content       <- ( tok_hatom_element / tok_hatom_text )*
    tok_hatom_element       <- tok_hatom_void_tag / $(tok_hatom_start_tag tok_hatom_content tok_hatom_end_tag)
    tok_hatom_void_tag      <- '<' tok_hatom_tag_name tok_hatom_void_attrs? '/>'
    tok_hatom_void_attrs    <- ( !'/>' . )+
    #[FIXME]: see https://github.com/yhirose/cpp-peglib/issues/70
    #tok_hatom_start_tag     <- '<' $tok_hatom_tag< tok_hatom_tag_name > tok_hatom_attrs? '>'
    #tok_hatom_end_tag       <- '</' $tok_hatom_tag '>'
    #tok_hatom_tag_name      <- < [A-Za-z0-9]+ > _
    tok_hatom_start_tag     <- '<' $tok_hatom_tag< tok_hatom_tag_name > _ tok_hatom_attrs? '>'
    tok_hatom_end_tag       <- '</' $tok_hatom_tag _ '>'
    tok_hatom_tag_name      <- < [A-Za-z0-9]+ >
    #[/FIXME]
    tok_hatom_attrs         <- ( !'>' . )+
    tok_hatom_text          <- ![<>] .
    tok_qatom               <- ["] < ( '\\"' / ( !["] . ) )* > ["] _
    tok_mcomment            <- < "/*" ( !"*/" . )* "*/" >
    tok_number              <- < [0-9]+ > _
    #[FIXME]: Why do not the rules below work?
    # This one should be *the* rule to use (don't work)
    #tok_number              <- ( [+-]? ( ( '.' [0-9]+ ) / ( [0-9]+ ( '.' [0-9]* )? ) ) ) _
    # Simpler alternative #1 (don't work)
    #tok_number              <- < [+-]? [0-9]+ ( '.' [0-9]+ )? > _
    # Simpler alternative #2 (don't work)
    #tok_number              <- tok_number_dec / tok_number_full _
    #tok_number_dec          <- < "." [0-9]+ > _
    #tok_number_full         <- < [+-]? [0-9]+ ( '.' [0-9]* )? > _
    #[/FIXME]
    tok_scomment            <- < ( '#' / "//" ) ( !tok_eolf . )* &tok_eolf >
    tok_semicolon           <- < ';' > _
    tok_strict              <- < [Ss] [Tt] [Rr] [Ii] [Cc] [Tt] > _
    tok_subgraph            <- < [Ss] [Uu] [Bb] [Gg] [Rr] [Aa] [Pp] [Hh] > _
    ~_                      <- ( [ \t\r\n] / comment )*
    #~__                     <- ![a-z0-9_] _
)"; // grammar


// -----------------------------------------------------------------------------
// STATE

/// The parser state
struct state_t
{
    /// Hold information of a cluster of nodes
    struct cluster_context_t
    {
        std::vector<std::set<std::string>> edge_nodes_chain; ///< Chain of nodes in the current edge statement (e.g., "A,B -> C -> D,C" represents a chain of 3 elements, namely "A,B", "C", and "D,C")
        std::set<std::string> nodes; ///< The cluster's nodes set
        std::map<std::string, std::string> default_edge_attrs; ///< Default edge attributes
    }; // cluster_context_t

    //enum attr_type_t
    //{
    //    chain_attr, ///< For attributes defined inline in a edge chain; e.g., "A -> B -> ... -> N [name1 = value1, name2 = value2, ...]"
    //    edge_attr, ///< For default edge attributes; e.g., "edge [name1 = value1, name2 = value2, ...]"
    //    graph_attr, ///< For default graph attributes; e.g., "edge [name1 = value1, name2 = value2, ...]"
    //    node_attr ///< For default node attributes; e.g., "node [name1 = value1, name2 = value2, ...]"
    //}; // attr_type_t

    state_t()
    : strict(false),
      directed(false),
      in_edge_attr_stmt(false)
    {
    }

    bool strict; ///< Tells whether this graph is strict (i.e., does not allow multi-edges)
    bool directed; ///< Tells whether this graph is directed or not
    bool in_edge_attr_stmt; ///< Tells whether the parser is matching the default attribute list for edges (e.g., "edge [name1 = value1, name2=value2, ...]")
    std::string graph_name; ///< The graph name
    std::set<std::string> nodes; ///< The graph's node set
    std::vector<std::string> nodes_rank; ///< Auxiliary data structure used for enumerating nodes according the order they appear in the input (otherwise, the `nodes`'s `std::set` data structure orders the nodes with lexicographic order)
    //std::map<std::pair<std::string,std::string>, std::string> edges; ///< edge map: each element <e,lbl> maps an edge e=<first node, second node> (i.e., <e.first,e.second>) to the associated edge label lbl
    std::map<std::pair<std::string,std::string>, std::map<std::string, std::string>> edges; ///< edge map: each element <e,attrs> maps an edge e=<first node, second node> (i.e., <e.first,e.second>) to the associated attributes stored in the attrs container, which is a key-value map, mapping an attribute name to its value
    std::stack<std::shared_ptr<cluster_context_t>> cluster_stack; ///< Collection of contexts associated with current "active" clusters (e.g., when parsing "E" in "A -> {B,C -> {D,E} -> F} -> G" there are 3 cluster context, namely the "root" cluster, the "B,C" cluster and "D,E" cluster)
    std::map<std::string, std::string> cur_attrs; ///< Hold the attributes in the currently parsed attribute list "[ name1 = value1; name2 = value2; ...]"
    //std::string cur_attr_name; ///< The name of the currently parsed attribute
}; // state_t


// -----------------------------------------------------------------------------
// ACTIONS

void set_semantic_actions(peg::parser& parser)
{
    parser["attr_assign"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "ATTR_ASSIGN => '" << sv.str() << '\'';
            oss << "\nATTR_ASSIGN => tokens (" << sv.tokens.size() << "):\n";
            for (std::size_t i = 0; i < sv.tokens.size(); ++i)
            {
                oss << "\ttoken #" << i << ": '" << sv.token(i) << "'\n";
            }
            oss << "\nATTR_ASSIGN => semantic values (" << sv.size() << "):\n";
            for (std::size_t i = 0; i < sv.size(); ++i)
            {
                if (i != 1)
                oss << "\tsv[" << i << "]: '" << sv[i].get<std::string>() << "'\n";
            }
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            // Memo: attr_assign <- atom eq atom
            st.cur_attrs[sv[0].get<std::string>()] = trim_copy(sv[2].get<std::string>());
        };

    parser["attr_stmt"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "ATTR_STMT => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            st.cur_attrs.clear();
            st.in_edge_attr_stmt = false;
        }; // Semantic action for "attr_stmt"

    parser["compound"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "COMPOUND => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            assert( st.cluster_stack.size() > 0 );
            assert( st.cluster_stack.top()->edge_nodes_chain.size() > 0 );

            auto p_cluster_ctx = st.cluster_stack.top();
            // Link nodes in current chain
            auto chain_len = p_cluster_ctx->edge_nodes_chain.size();
            for (std::size_t i = 1; i < chain_len; ++i)
            {
                // Link (i-1)-th chain element to i-th chain element
                for (auto from_node : p_cluster_ctx->edge_nodes_chain[i-1])
                {
                    for (auto to_node : p_cluster_ctx->edge_nodes_chain[i])
                    {
                        auto edge = std::make_pair(from_node, to_node);
                        // Insert default attributes
                        st.edges[edge] = p_cluster_ctx->default_edge_attrs;
                        // Insert/override attributes according to those defined locally in this chain (if any)
                        for (auto const& attr : st.cur_attrs)
                        {
                            st.edges[edge][attr.first] = attr.second;
                        }
                    }
                }
            }
            // Clear this chain
            p_cluster_ctx->edge_nodes_chain.clear();
            st.cur_attrs.clear();
            //p_cluster_ctx->edge_nodes_chain.push_back(std::set<std::string>());
            p_cluster_ctx->edge_nodes_chain.resize(p_cluster_ctx->edge_nodes_chain.size()+1);
        }; // Semantic action for "compound"

    parser["graph_name"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "GRAPH_NAME => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            st.graph_name = trim_copy(sv.str());
         }; // Semantic action for "graph_name"

    parser["node"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "NODE => '" << sv.str() << '\'' << " (token: '" << sv.token() << "')";
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            assert( st.cluster_stack.size() > 0 );
            assert( st.cluster_stack.top()->edge_nodes_chain.size() > 0 );

            auto const node_name = trim_copy(sv[0].get<std::string>());
            // Add node to the graph's nodes set 
            if (st.nodes.count(node_name) == 0)
            {
                st.nodes.insert(node_name);
                st.nodes_rank.push_back(node_name);
            }
            // Add node to current cluster's nodes set
            auto p_cluster_ctx = st.cluster_stack.top();
            p_cluster_ctx->edge_nodes_chain.back().insert(node_name);
            p_cluster_ctx->nodes.insert(node_name);
        }; // Semantic action for "node"

    parser["tok_atom"] = [](const peg::SemanticValues& sv) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_ATOM => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG
            return sv.token();
        }; // Semantic action for "tok_atom"

    parser["tok_digraph"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_DIGRAPH => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            st.directed = true;
        }; // Semantic action for "tok_digraph"

    parser["tok_edge"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_EDGE => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            st.in_edge_attr_stmt = true;
        }; // Semantic action for "tok_edge"

    parser["tok_edge_op"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_EDGE_OP => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            assert( st.cluster_stack.size() > 0 );

            auto p_cluster_ctx = st.cluster_stack.top();
            //p_cluster_ctx->edge_nodes_chain.push_back(std::set<std::string>());
            p_cluster_ctx->edge_nodes_chain.resize(p_cluster_ctx->edge_nodes_chain.size()+1);
        }; // Semantic action for "tok_edge_op"

    parser["tok_hatom"] = [](const peg::SemanticValues& sv) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_HATOM => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            return sv.token();
        }; // Semantic action for "tok_hatom"

    parser["tok_hatom_tag_name"] = [](const peg::SemanticValues& sv) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_HATOM_TAG_NAME => '" << sv.str() << '\'' << " - token: '" << sv.token() << "'";
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            return sv.token();
        }; // Semantic action for "tok_hatom_tag_name"

    parser["tok_lbrace"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_LBRACE => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            // Push a new cluster context onto the stack
            auto p_cluster_ctx = std::make_shared<state_t::cluster_context_t>();
            //p_cluster_ctx->edge_nodes_chain.push_back(std::set<std::string>());
            p_cluster_ctx->edge_nodes_chain.resize(1);
            st.cluster_stack.push(p_cluster_ctx);
        }; // Semantic action for "tok_lbrace"

    parser["tok_lbrack"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_LBRACK => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            st.cur_attrs.clear();
        }; // Semantic action for "tok_lbrack"

    parser["tok_number"] = [](const peg::SemanticValues& sv) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_NUMBER => '" << sv.str() << '\'' << ", token: '" << sv.token() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG
            return sv.token();
        }; // Semantic action for "tok_atom"

//    parser["tok_number_dec"] = [](const peg::SemanticValues& sv) {
//#if PPQTN_DEBUG
//{
//            std::ostringstream oss;
//            oss << "TOK_NUMBER_DEC => '" << sv.str() << '\'' << ", token: '" << sv.token() << '\'';
//            log_debug(oss.str());
//}
//#endif // PPQTN_DEBUG
//            return sv.token();
//        }; // Semantic action for "tok_atom"
//
//    parser["tok_number_full"] = [](const peg::SemanticValues& sv) {
//#if PPQTN_DEBUG
//{
//            std::ostringstream oss;
//            oss << "TOK_NUMBER_FULL => '" << sv.str() << '\'' << ", token: '" << sv.token() << '\'';
//            log_debug(oss.str());
//}
//#endif // PPQTN_DEBUG
//            return sv.token();
//        }; // Semantic action for "tok_atom"

    parser["tok_qatom"] = [](const peg::SemanticValues& sv) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_QATOM => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG
            return sv.token();
        }; // Semantic action for "tok_qatom"

    parser["tok_rbrace"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_RBRACE => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            assert( st.cluster_stack.size() > 0 );

            if (st.cluster_stack.size() > 1)
            {
                // Remove the more recent cluster context from the stack
                auto p_cluster_ctx = st.cluster_stack.top();
                st.cluster_stack.pop();

                // Copy the nodes set of the recent cluster context into the cluster context on top of the stack
                if (!p_cluster_ctx->nodes.empty())
                {
                    st.cluster_stack.top()->nodes.insert(p_cluster_ctx->nodes.begin(), p_cluster_ctx->nodes.end());

                    //st.cluster_stack.top()->edge_nodes_chain.push_back(p_cluster_ctx->nodes);
                    st.cluster_stack.top()->edge_nodes_chain.back().insert(p_cluster_ctx->nodes.begin(), p_cluster_ctx->nodes.end());
                }
            }
        }; // Semantic action for "tok_brace"

    parser["tok_strict"] = [](const peg::SemanticValues& sv, peg::any& dt) {
#if PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "TOK_STRICT => '" << sv.str() << '\'';
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            assert( !dt.is_undefined() );

            auto& st = *dt.get<state_t*>();

            st.strict = true;
        }; // Semantic action for "tok_strict"
}


// -----------------------------------------------------------------------------
// UTILITIES

struct tracer_t
{
    tracer_t(std::ostream& os = std::cerr)
    : os_(os),
      prev_pos_(0)
    {
        os_ << "pos:lev\trule/ope\ts\tsv\n";
        os_ << "-------\t--------\t-\t--\n";
    }

    void operator()(const char* name, const char* s, std::size_t n, const peg::SemanticValues& sv, const peg::Context& c, const peg::any& /*dt*/)
    {
        auto pos = static_cast<std::size_t>(s - c.s);
        auto backtrack = (pos < prev_pos_ ? "*" : "");
        std::string indent;
        auto level = c.nest_level;
        while (level--)
        {
            indent += "  ";
        }
        os_ << pos << ':' << c.nest_level << backtrack
            << '\t' << indent << name
            << '\t' << indent << "'" << std::string(s,n) << "'"
            << '\t' << indent << "'" << sv.str() << "'"
            << '\n';
        prev_pos_ = pos;
    }

private:
    std::ostream& os_;
    std::size_t prev_pos_;
}; // tracer_t

} // dot_parser

} // Unnamed namespace


std::unique_ptr<graph_t> read_dot_graph(const std::string& dot_graph)
{
    const node_ranking_t node_rank = node_ranking_t::appearance_rank; //FIXME: specify as parameter

    std::unique_ptr<graph_t> p_graph(new graph_t);

    read_dot_graph_impl(dot_graph, node_rank, *p_graph, nullptr);

    return p_graph;
}

std::unique_ptr<graph_t> read_dot_graph(const std::string& dot_graph, graph_properties_t& graph_props)
{
    const node_ranking_t node_rank = node_ranking_t::appearance_rank; //FIXME: specify as parameter

    std::unique_ptr<graph_t> p_graph(new graph_t);

    read_dot_graph_impl(dot_graph, node_rank, *p_graph, &graph_props);

    return p_graph;
}

void write_dot_graph(const graph_t& g, std::ostream& os, bool both_dir)
{
    write_dot_graph_impl(g, os, both_dir, nullptr);
}

void write_dot_graph(const graph_t& g, std::ostream& os, bool both_dir, const graph_properties_t& graph_props)
{
    write_dot_graph_impl(g, os, both_dir, &graph_props);
}


namespace {

void read_dot_graph_impl(const std::string& dot_graph, node_ranking_t node_rank, graph_t& graph, graph_properties_t* p_graph_props)
{
    dot_parser::state_t state;

    // Setup a PEG parser
    peg::parser parser;

    parser.log = [](std::size_t ln, std::size_t col, const std::string& msg) {
        std::ostringstream oss;
        oss << "Parse error at " << ln << ":" << col << ": " << msg;
        log_warn(oss.str());
    };

    auto ok = parser.load_grammar(dot_parser::grammar);
    assert( ok );

    parser.enable_packrat_parsing(); // Enable packrat parsing.

    dot_parser::set_semantic_actions(parser);

#ifdef PPQTN_DEBUG
    parser.enable_trace(dot_parser::tracer_t());
#endif // PPQTN_DEBUG

    peg::any dt = &state;
    ok = parser.parse(dot_graph.c_str(), dt);
    assert( ok );

#ifdef PPQTN_DEBUG
{
    std::cerr << "OK: " << ok << '\n';

    std::cerr << "State::Nodes: [";
    for (auto const& node : state.nodes)
    {
        std::cerr << node << ", ";
    }
    std::cerr << "]\n";

    std::cerr << "State::Nodes rank: [";
    std::size_t rank_pos = 0;
    for (auto const& node : state.nodes_rank)
    {
        std::cerr << node << " (" << rank_pos << "), ";
        ++rank_pos;
    }
    std::cerr << "]\n";

    std::cerr << "State::Edges: [";
    for (auto const& edge : state.edges)
    {
        std::cerr << "<" << edge.first.first << ", " << edge.first.second << "> (" << edge.second << ")" << ", ";
    }
    std::cerr << "]\n";
}
#endif // PPQTN_DEBUG

    // Translate parser state into a graph...

    graph.num_vertices(state.nodes.size());

    std::map<std::string,std::size_t> node_name_to_id_map;
    std::size_t vertex_count = 0;

    if (node_rank == node_ranking_t::appearance_rank)
    {
        for (auto const& node : state.nodes_rank)
        {
            node_name_to_id_map[node] = vertex_count++;
        }
    }
    else if (node_rank == node_ranking_t::lexicographic_rank)
    {
        for (auto const& node : state.nodes)
        {
            node_name_to_id_map[node] = vertex_count++;
        }
    }
    else
    {
        fatal_error("Unknown node rank");
    }

#ifdef PPQTN_DEBUG
{
    std::cerr << "Node => ID map: {";
    for (auto const& node_name_id : node_name_to_id_map)
    {
        std::cerr << "<" << node_name_id.first << " => " << node_name_id.second << ">, ";
    }
    std::cerr << "}\n";
}
#endif // PPQTN_DEBUG

    // Transform edge labels into temporal objects
    for (auto edge : state.edges)
    {
        std::vector<terne> triples;

        if (edge.second.count("label") > 0)
        {
            const std::string label  = edge.second.at("label");

//[XXX]: moved to parse_constraint_triples() in graph_io_util.cpp
#if 0
            // Expected format:
            //   (i,r|%,r|#), ..., (i,r|%,r|#),
            // where: 'i' is an integer number, 'r' is a real number, '%' denotes an undefined probability, and '#' denotes an undefined preference

            std::regex re(R"(\(\s*([+-]?\d+)\s*,\s*((?:[+-]?(?:\.\d+)|(?:\d+(?:\.\d*)?))|%)\s*,\s*((?:[+-]?(?:\.\d+)|(?:\d+(?:\.\d*)?))|#)\s*\)\s*,?)");

            auto triples_begin = std::sregex_iterator(label.begin(), label.end(), re);
            auto triples_end = std::sregex_iterator();

#ifdef PPQTN_DEBUG
{
            std::ostringstream oss;
            oss << "Found " << std::distance(triples_begin, triples_end) << " triples\n";
            log_debug(oss.str());
}
#endif // PPQTN_DEBUG

            //// Reserves a slot for triples descriptor
            //triples.resize(1);

            // Fills triples
            //bool have_prob = true;
            //bool have_pref = true;
            for (std::sregex_iterator it = triples_begin; it != triples_end; ++it)
            {
                std::smatch match = *it;

                // We expect 4 matches:
                // - the first one is the whole matched string,
                // - the second one is the first grouping (i.e., the time distance),
                // - the third one is the second grouping (i.e., the probability),
                // - the fourth one is the third grouping (i.e., the preference).
                if (match.size() == 4)
                {
                    terne t;

                    std::istringstream iss;

                    iss.str(match[1]);
                    iss >> t.dist;

                    if (match[2] == dot_parser::constants::undef_prob_symbol)
                    {
                        t.prob = constants::undef_prob;
                        //have_prob = false;
                    }
                    else
                    {
                        iss.clear();
                        iss.str(match[2]);
                        iss >> t.prob;
                        if (t.prob < 0)
                        {
                            t.prob = constants::undef_prob;
                            //have_prob = false;
                        }
                    }

                    if (match[3] == dot_parser::constants::undef_pref_symbol)
                    {
                        t.pref = constants::undef_pref;
                        //have_pref = false;
                    }
                    else
                    {
                        iss.clear();
                        iss.str(match[3]);
                        iss >> t.pref;
                        if (t.pref < 0)
                        {
                            t.pref = constants::undef_pref;
                            //have_pref = false;
                        }
                    }
#ifdef PPQTN_DEBUG
                    std::cerr << "Matched Triple -> distance: " << match[1] << ", prob: " << match[2] << ", pref: " << match[3] << '\n';
                    std::cerr << "Built Triple -> distance: " << t.dist << ", prob: " << t.prob << ", pref: " << t.pref << '\n';
#endif // PPQTN_DEBUG

                    triples.push_back(t);
                }
                else
                {
                    std::ostringstream oss;
                    oss << "Unexpected triple format in '" << match.str() << "': skip";
                    log_warn(oss.str());
                }
            }
#endif //[XXX]: moved to parse_constraint_triples() in graph_io_util.cpp

            triples = parse_constraint_triples(label);

            //// Fills triples' descriptor
            //triples[0].dist = triples.size() > 0 ? (triples.size()-1) : 0;
            //triples[0].prob = have_prob ? constants::def_prob : constants::undef_prob;
            //triples[0].pref = have_pref ? constants::def_pref : constants::undef_pref;
        }

        auto node1 = node_name_to_id_map.at(edge.first.first);
        auto node2 = node_name_to_id_map.at(edge.first.second);
        graph.add_edge(node1, node2, triples.begin(), triples.end());
    }

    // Set graph properties
    if (p_graph_props)
    {
        p_graph_props->graph_name = state.graph_name;

        p_graph_props->node_names.clear();
        p_graph_props->node_names.resize(state.nodes.size());
        //std::copy(state.nodes.begin(), state.nodes.end(), p_graph_props->node_names.begin());
        for (auto const& node_rank : node_name_to_id_map)
        {
            p_graph_props->node_names[node_rank.second] = node_rank.first;
        }
#ifdef PPQTN_DEBUG
{
        std::cerr << "Property::node_names: [";
        for (auto const& node_rank : node_name_to_id_map)
        {
            std::cerr << p_graph_props->node_names[node_rank.second] << " (idx: " << node_rank.second << "), ";
        }
        std::cerr << "]\n";
}
#endif // PPQTN_DEBUG
    }
}

void write_dot_graph_impl(const graph_t& g, std::ostream& os, bool both_dir, const graph_properties_t* p_props)
{
    const std::string indent = "  ";

    os  << "digraph " << (p_props ? p_props->graph_name : "") << " {\n";
    for (std::size_t n1 = 0; n1 < g.num_vertices(); ++n1)
    {
        for (std::size_t n2 = both_dir ? 0 : n1+1; n2 < g.num_vertices(); ++n2)
        {
            if (n1 != n2 && g.has_edge(n1, n2))
            {
                os << indent;
                if (p_props && p_props->node_names.size() > 0)
                {
                    os << p_props->node_names[n1];
                }
                else
                {
                    os << "n" << n1;
                }
                os << " -> ";
                if (p_props && p_props->node_names.size() > 0)
                {
                    os << p_props->node_names[n2];
                }
                else
                {
                    os << "n" << n2;
                }

                auto nt = g(n1, n2)[0].dist;
                os << " [label=\"";
                if (nt > 0)
                {
                    os << "<";
                }
                for (int i = 1; i <= nt; ++i)
                {
                    if (i > 1)
                    {
                        os << ", ";
                    }
                    os << '(';
                    os << g(n1, n2)[i].dist;
                    if (g(n1, n2)[i].is_prob_undef())
                    {
                        os << ", " << constants::undef_prob_symbol;
                    }
                    else
                    {
                        os << ", " << g(n1, n2)[i].prob;
                    }
                    if (g(n1, n2)[i].is_pref_undef())
                    {
                        os << ", " << constants::undef_pref_symbol;
                    }
                    else
                    {
                        os << ", " << g(n1, n2)[i].pref;
                    }
                    os << ')';
                }
                if (nt > 0)
                {
                    os << ">";
                }
                os << "\"]";
                os << '\n';
            }
        }

        if (g.vertex_degree(n1) == 0)
        {
            // n1 is an isolated node -> just print it

            if (p_props && p_props->node_names.size() > 0)
            {
                os << p_props->node_names[n1];
            }
            else
            {
                os << "n" << n1;
            }
            os << '\n';
        }
    }
    os  << "}\n";
    os << std::flush;
}

#ifdef PPQTN_DEBUG

template <typename KT, typename VT>
std::ostream& operator<<(std::ostream& os, const std::map<KT,VT>& set)
{
    os << "{";
    bool first = true;
    for (auto const& el : set)
    {
        if (!first)
        {
            os << ", ";
        }
        else
        {
            first = false;
        }
        os << "<" << el.first << ", " << el.second << ">";
    }
    os << "}";
    return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::set<T>& set)
{
    os << "{";
    bool first = true;
    for (auto const& el : set)
    {
        if (!first)
        {
            os << ", ";
        }
        else
        {
            first = false;
        }
        os << el;
    }
    os << "}";
    return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
    os << "[";
    bool first = true;
    for (auto const& el : vec)
    {
        if (!first)
        {
            os << ", ";
        }
        else
        {
            first = false;
        }
        os << el;
    }
    os << "]";
    return os;
}

#endif // PPQTN_DEBUG

} // Unnamed namespace

#endif // PPQTN_USE_PEGLIB
