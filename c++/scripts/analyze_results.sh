#!/bin/sh

# Path to the directory containing the scripts used to generated charts
scriptdir=$(dirname $0)
# Path to the output CSV file where to store simulation statitics
datfile=./exp_results.csv
# Output image format
outfmt=eps
# Fit yes or no?
fit=0
fitmetric=""

if [ "$#" -gt 0 ]; then
    datfile=$1
fi
if [ "$#" -gt 1 ]; then
    outfmt=$2
fi
if [ "$#" -gt 2 ]; then
    fit=$3
fi
if [ "$#" -gt 3 ]; then
    fitmetric=$4
fi

extra_args=""
if [ "$fit" -eq 1 ]; then
	extra_args+=" --fit"
fi
if [ "$fitmetric" != "" ]; then
	extra_args+=" --fitmetric $fitmetric"
fi

Rscript --vanilla "$scriptdir/analyze_results.R" --datfile "$datfile" --outfmt "$outfmt" --verbose $extra_args
