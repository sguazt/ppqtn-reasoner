/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstddef>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <ppqtn/error.hpp>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_algo.hpp>
#include <ppqtn/graph_io.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/util.hpp>
#include <stdexcept>


int main(/*int argc, char* argv[]*/)
{
    try
    {
        const std::string infile = "test/graph-terenziani_ieee_tkde_2019_ex1.gv";
        const std::size_t nk = 10;

        std::string txt_graph = read_file(infile);

        std::unique_ptr<graph_t> p_graph;
        graph_properties_t graph_props;

        p_graph = read_dot_graph(txt_graph, graph_props);

        if (p_graph == nullptr)
        {
            fatal_error("Bad input file");
        }

        std::cout << "Input graph: ";
        write_dot_graph(*p_graph, std::cout, false, graph_props);
        std::cout << '\n';

        graph_t min_network(*p_graph);

        size_t k = 1;
        do
        {
            std::cout << "ITERATION " << k << "\n";
            bool consistent = floyd_warshall(min_network);
            if (consistent)
            {
                write_dot_graph(min_network, std::cout, false, graph_props);
            }
            else
            {
                std::cout << "Constraints are not consistent!\n";
            }
            ++k;
        }
        while (k <= nk);
        
    }
    catch (const std::exception& e)
    {
        fatal_error(e.what());
    }
    catch (...)
    {
        fatal_error("Unexpected error");
    }
}
