/* vim: set tabstop=4 expandtab shiftwidth=4 softtabstop=4: */

/*
 * Copyright 2019 Marco Guazzone (marco.guazzone@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstddef>
#include <cstring>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <ppqtn/error.hpp>
#include <ppqtn/graph.hpp>
#include <ppqtn/graph_algo.hpp>
#include <ppqtn/graph_io.hpp>
#include <ppqtn/log.hpp>
#include <ppqtn/util.hpp>
#include <stdexcept>


int main(/*int argc, char* argv[]*/)
{
    try
    {
        const std::string infile = "test/graph-terenziani_ieee_tkde_2019_ex1.gv";

        std::string txt_graph = read_file(infile);

        std::unique_ptr<graph_t> p_graph;
        graph_properties_t graph_props;

        p_graph = read_dot_graph(txt_graph, graph_props);

        if (p_graph == nullptr)
        {
            fatal_error("Bad input file");
        }

        std::cout << "Input graph: ";
        write_dot_graph(*p_graph, std::cout, false, graph_props);
        std::cout << '\n';

        std::size_t X0 = 0;
        std::size_t CCA = 0;
        std::size_t DGAs = 0;

        for (std::size_t i = 0; i < graph_props.node_names.size(); ++i)
        {
            if (graph_props.node_names[i] == "X0")
            {
                X0 = i;
            }
            else if (graph_props.node_names[i] == "CCA")
            {
                CCA = i;
            }
            else if (graph_props.node_names[i] == "DGAs")
            {
                DGAs = i;
            }
        }

        auto comp_triples = compose((*p_graph)(X0, CCA), (*p_graph)(CCA, DGAs));
        std::cout << "COMPOSITION: (X0,CCA) @ (CCA,DGAs) ==> " << comp_triples << '\n';
        delete[] comp_triples;
    }
    catch (const std::exception& e)
    {
        fatal_error(e.what());
    }
    catch (...)
    {
        fatal_error("Unexpected error");
    }
}
