#!/usr/bin/env bash
  
release=1

if [ $# -ge 1 ]; then
    if [ "$1" == "debug" ]; then
        release=0
    fi
fi

cwd=$PWD
srcdir=$cwd
destname=ppqtn-bin_x64

if [ -e "$cwd/../LICENSE" ]; then
    srcdir=$cwd
elif [ -e "$cwd/../../LICENSE" ]; then
    srcdir=$cwd/..
else
    echo "[WARNING] Bad source directory '$srcdir'"
    exit 1
fi

version=$(cat $srcdir/VERSION)
workdir=$srcdir/dist
bundledir=$workdir/bundles
destdir=$bundledir/$destname
#dest_archive=./bundles/${destname}-v${version}.tar.gz
dest_archive=./bundles/${destname}-v${version}
if [ $release -eq 0 ]; then
    dest_archive+=-debug
fi
dest_archive+=.tar.gz
make_target=release
if [ $release -eq 0 ]; then
    make_target=debug
fi

cd $workdir

rm -rf $destdir
rm -f $dest_archive

cd $srcdir \
&& make clean $make_target \
&& cd $workdir \
&& ./cde_2011-08-15_64bit $srcdir/src/ppqtn --rand --rand-linear-nverts 1 --rand-linear-brlen 1 --bench-min-nreps 1 --bench-max-nreps 1 \
&& mkdir -p $destdir \
&& mv cde-package $destdir \
&& mv cde.options $destdir \
&& cp cde-ppqtn.sh $destdir/ppqtn.sh \
&& cp -r process-watcher $destdir/process-watcher \
&& cp $srcdir/ppqtn-wrapper.sh $destdir/ppqtn-wrapper.sh \
&& cp $srcdir/ppqtn-wrapper_debug.sh $destdir/ppqtn-wrapper_debug.sh \
&& tar -zcvf $dest_archive -C $bundledir $destname \
&& cd $cwd
