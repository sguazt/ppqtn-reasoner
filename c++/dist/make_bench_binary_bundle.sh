#!/usr/bin/env bash
  
cwd=$PWD
basedir=$(realpath $(dirname $0))
#srcdir=$basedir/..

release=1
workdir=$basedir/bundles

if [ $# -ge 1 ]; then
    if [ "$1" == "debug" ]; then
        release=0
    fi
fi
if [ $# -ge 2 ]; then
    workdir="$2"
fi

echo "Options:"
echo "* release: $release"
echo "* workdir: $workdir"


srcdir=
if [ -e "$basedir/../LICENSE" ]; then
    srcdir="$basedir"
elif [ -e "$basedir/../../LICENSE" ]; then
    srcdir="$basedir/.."
else
    echo "ERROR: Unable to find the source directory."
    exit 1
fi

version=$(cat "$srcdir/VERSION")
bundledir="$srcdir/dist/bundles"
destname=ppqtn_bench-bin_x64
destdir="$workdir/$destname"
bindir="$destdir/bin"
#dest_archive=./bundles/${destname}-v${version}.tar.gz
dest_archive="$bundledir/${destname}-v${version}"
if [ $release -eq 0 ]; then
    dest_archive+=-debug
fi
dest_archive+=.tar.gz
make_target=release
if [ $release -eq 0 ]; then
    make_target=debug
fi
wrapper_script=ppqtn_bench-wrapper.sh
if [ $release -eq 0 ]; then
	wrapper_script=ppqtn_bench-wrapper_debug.sh
fi

cd $workdir

rm -rf $destdir
rm -f $dest_archive

#cd $srcdir \
#&& make clean $make_target \
#&& cd $workdir \
#&& mkdir -p $destdir \
#&& mkdir -p $bindir \
#&& cd $destdir \
#&& cp $srcdir/src/ppqtn_bench $bindir \
#&& $basedir/cde_2011-08-15_64bit $bindir/ppqtn_bench --nverts 1 --bench-min-nreps 1 --bench-max-nreps 1 \
#&& cp $basedir/cde-ppqtn_bench.sh $destdir/ppqtn_bench.sh \
#&& cp $srcdir/$wrapper_script $destdir/$wrapper_script \
#&& tar -zcvf $dest_archive -C $workdir $destname \
#&& cd $cwd

cd $srcdir \
&& make clean $make_target \
&& cd $workdir \
&& mkdir -p $destdir/bin \
&& cd $destdir \
&& cp $srcdir/src/ppqtn_bench ./bin \
&& cp $basedir/cde_2011-08-15_64bit . \
&& ./cde_2011-08-15_64bit ./bin/ppqtn_bench --nverts 1 --bench-min-nreps 1 --bench-max-nreps 1 \
&& rm cde_2011-08-15_64bit \
&& cp $basedir/cde-ppqtn_bench.sh ./ppqtn_bench.sh \
&& cp $srcdir/$wrapper_script ./$wrapper_script \
&& tar -zcvf $dest_archive -C $workdir $destname \
&& cd $cwd
