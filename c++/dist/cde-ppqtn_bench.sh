#!/usr/bin/env bash

basepath=$(dirname $0)
binpath=$(dirname $(find $basepath/cde-package/cde-root -name ppqtn_bench))
binpath=${binpath#*$basepath/cde-package/cde-root}

$basepath/cde-package/cde-exec $binpath/ppqtn_bench $*

