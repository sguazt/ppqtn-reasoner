#!/usr/bin/env bash

basepath="/home/sguazt/Projects/research/TemporalReasoning/code/ppqtn-reasoner"
#fullpath="$PWD/cde-package/cde-root/$basepath"

./cde-package/cde-exec $basepath/c++/src/ppqtn $*

#TODO: use this because it does not make any assumption on the directory where this script is executed
#basepath=$(dirname $0)
#$basepath/cde-package/cde-exec $basepath/c++/src/ppqtn $*
